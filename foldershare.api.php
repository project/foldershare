<?php

/**
 * @file
 * Documents the principal hooks available in the module.
 *
 * This file provides sample definitions for hooks that are available
 * to respond to and override access control requests, file name extension
 * lists, menu commands, user interface components, and more. Additional
 * hooks provide notifications when operations are completed, such as those
 * to view, create, delete, move and copy.
 *
 * Access control hooks are provided by the Drupal core
 * EntityAccessControlHandler used as a base class for FolderShare
 * access control. Please see that class for further discussion of
 * these hooks.
 *
 * Entity create, delete, etc., hooks are provided by the Drupal core
 * EntityStorageBase and ContentEntityStorageBase classes used by
 * the core Entity and ContentEntity classes, respectively. And ContentEntity
 * is the base class for FolderShare entity mangement. Please see those
 * classes for further discussion of these hooks.
 *
 * View and form hooks are provided by the Drupal core EntityViewBuilder
 * and EntityForm base classes used by FolderShare. Please see those classes
 * for further discussion of these hooks.
 *
 * Search index and result hooks are provided by FolderShare's
 * FolderShareSearch class that defines a Drupal core Search plugin for
 * searching files and folders. Please see that class for further
 * discussion of these hooks.
 *
 * @addtogroup hooks
 * @{
 */

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\file\FileInterface;

use Drupal\foldershare\FolderShareInterface;

/*----------------------------------------------------------------------
 *
 * Notice of operation completion.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds after completion of operations that add files.
 *
 * This includes operations that upload files, create a new ZIP archive
 * file from existing files, or unarchive a ZIP file into multiple new files.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $parent
 *   The parent entity. When NULL, the parent is a root list.
 * @param \Drupal\foldershare\FolderShareInterface[] $fileEntities
 *   An array of added entities.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_add_files(
  FolderShareInterface $parent = NULL,
  array $fileEntities,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Change owner" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being updated.
 * @param int $fromUid
 *   The user ID of the entity before it was changed.
 * @param int $toUid
 *   The user ID of the entity after it was changed. This will equal
 *   $entity->getOwnerId().
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_change_owner(
  FolderShareInterface $entity,
  int $fromUid,
  int $toUid,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Copy" operation.
 *
 * Folder tree copies will call this hook after each individual file or
 * folder is copied.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The new copy.
 * @param \Drupal\foldershare\FolderShareInterface $origEntity
 *   The original entity that was copied.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_copy(
  FolderShareInterface $entity,
  FolderShareInterface $origEntity,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Delete" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param int $entityId
 *   The deleted entity's ID.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_delete(
  int $entityId,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Download" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The downloaded entity.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_download(
  FolderShareInterface $entity,
  int $requestingUid) {
}

/**
 * Responds after completion of an "Edit" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The edited entity.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_edit(
  FolderShareInterface $entity,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Move" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being moved.
 * @param \Drupal\foldershare\FolderShareInterface $oldParent
 *   The old parent.
 * @param \Drupal\foldershare\FolderShareInterface $newParent
 *   The new parent.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_move(
  FolderShareInterface $entity,
  FolderShareInterface $oldParent = NULL,
  FolderShareInterface $newParent = NULL,
  int $requestingUid) {
}

/**
 * Responds after completion of a "New folder" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * If the operation creates a new root folder, $entity->isRootItem() will
 * be TRUE. Otherwise the operation created a new subfolder.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The newly created entity.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_new_folder(
  FolderShareInterface $entity,
  int $requestingUid) {
}

/**
 * Responds after completion of a "New object" operation.
 *
 * If the operation creates a new root object, $entity->isRootItem() will
 * be TRUE. Otherwise the operation created a new object within a folder.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The newly created entity.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_new_object(
  FolderShareInterface $entity,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Rename" operation.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being updated.
 * @param string $oldName
 *   The previous name for the entity.
 * @param string $newName
 *   The new name for the entity. This will equal $entity->getName().
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_rename(
  FolderShareInterface $entity,
  string $oldName,
  string $newName,
  int $requestingUid) {
}

/**
 * Responds after completion of a "Share" operation.
 *
 * The access grants array arguments are both unordered associative arrays
 * with user IDs as keys and arrays as values. Array values contain strings
 * indicating 'view' or 'author' access.
 *
 * This hook is called in response to user interface operations, REST
 * operations, or direct calls to the FolderShare entity's API.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being updated.
 * @param array $oldGrants
 *   The previous access grants.
 * @param array $newGrants
 *   The new access grants. This will equal $entity->getAccessGrants().
 * @param int $requestingUid
 *   The user ID of the user requesting the operation. This may differ from
 *   the current user ID, such as when the operation takes place via a
 *   background task.
 */
function hook_foldershare_post_operation_share(
  FolderShareInterface $entity,
  array $oldGrants,
  array $newGrants,
  int $requestingUid) {
}

/**
 * Responds after completion of a "View" operation.
 *
 * This hook is called in response to user interface operations only.
 * Unlike "hook_foldershare_view()", this hook is only called when an
 * entity view is created to present the entity, rather than to create
 * search indexes or search results lists.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being viewed.
 * @param int $requestingUid
 *   The user ID of the user requesting the operation.
 */
function hook_foldershare_post_operation_view(
  FolderShareInterface $entity,
  int $requestingUid) {
}

/*----------------------------------------------------------------------
 *
 * Access control.
 *
 *----------------------------------------------------------------------*/
/**
 * Controls access to a FolderShare entity.
 *
 * FolderShare's access control mechanism has these steps:
 * - Check if the user is an admin
 * - Check user permissions.
 * - Check entity access grants.
 * - Invoke this hook.
 *
 * Steps are executed in order and the remaining steps are skipped if there
 * is a definitive answer for a step. For instance, if the user is an
 * administrator, they are granted access and the remaining steps are not
 * executed. If the user does not have permission or the entity does not
 * grant them access, then they are denied access and the remaining steps
 * are not executed. This means this hook is only executed if the user
 * is not an admin and does have permission and access granted. This hook,
 * then, can only make a final allow/deny choice.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity the user wishes to access. If the entity is NULL, the user
 *   is requesting access to the root folder list.
 * @param string $op
 *   The operation to be performed. Access operators are one of:
 *   - 'create'.
 *   - 'delete'.
 *   - 'share'.
 *   - 'update'.
 *   - 'view'.
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user's account.
 *
 * @return \Drupal\Core\Access\AccessResultInterface
 *   Returns the access result to allow or forbid access, or have a neutral
 *   opinion.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityAccessControlHandler
 * @see \Drupal\foldershare\Entity\FolderShareAccessControlHandler
 */
function hook_foldershare_access(
  FolderShareInterface $entity,
  string $op,
  AccountInterface $account) {

  // A hook could query additional database tables to determine if access
  // should be denied. For instance, special values could mark an entity
  // as immutable due to some external contract. For instance, a legal
  // or accounting requirement might require that certain entities be locked
  // to limit access or changes.
  return AccessResult::neutral();
}

/**
 * Controls access to create a FolderShare entity.
 *
 * @param \Drupal\Core\Session\AccountInterface $account
 *   The user's account.
 * @param array $context
 *   An associative array with additional context values, including at least:
 *   - 'langcode' - the current language code.
 *   - 'parentId' - the entity ID of the parent folder, or
 *      FolderShareInterface::USER_ROOT_LIST if the entity is a root item and
 *      therefore has no parent folder.
 * @param string $entityBundle
 *   The entity bundle name.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityAccessControlHandler
 * @see \Drupal\foldershare\Entity\FolderShareAccessControlHandler
 */
function hook_foldershare_create_access(
  AccountInterface $account,
  array $context,
  $entityBundle) {

  return AccessResult::neutral();
}

/*----------------------------------------------------------------------
 *
 * Entity create.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds when creating a new FolderShare entity.
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object that has been created, but not yet saved.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 * @see ::hook_foldershare_post_operation_new_folder()
 */
function hook_foldershare_create(EntityInterface $entity) {
}

/**
 * Responds after initializing the fields of a FolderShare entity.
 *
 * This hook runs after a new entity object has just been instantiated.
 * It can be used to set initial values (e.g. defaults).
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\FieldableEntityInterface $entity
 *   The entity object to be initialized.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\ContentEntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 * @see ::hook_foldershare_post_operation_new_folder()
 */
function hook_foldershare_field_values_init(FieldableEntityInterface $entity) {
}

/*----------------------------------------------------------------------
 *
 * Entity storage load.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds when loading a FolderShare entity.
 *
 * Loading an entity goes through these steps:
 * - Load entity from storage.
 * - Call Entity's postLoad().
 * - Invoke load hooks.
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param array $entities
 *   An associative array of entities with entity IDs as keys.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_load(array $entities) {

  // A hook could add virtual fields to the loaded entity, retreiving
  // their values from other database tables or computing them.
}

/*----------------------------------------------------------------------
 *
 * Entity storage save.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds before a FolderShare entity is validated and saved.
 *
 * Saving an entity goes through these steps:
 * - Call ContentEntityBase's preSave().
 * - Invoke each field's presave hooks.
 * - Invoke presave hooks.
 * - Save the entity to storage.
 * - Call ContentEntityBase's postSave().
 * - Invoke each field's update or insert hooks.
 * - Invoke update or insert hooks.
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object that has been created, but not yet saved.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_presave(EntityInterface $entity) {
}

/*----------------------------------------------------------------------
 *
 * Entity storage insert and update.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds after a new FolderShare entity has been stored.
 *
 * Saving a new entity goes through these steps:
 * - Call ContentEntityBase's preSave().
 * - Invoke each field's presave hooks.
 * - Invoke presave hooks.
 * - Save the entity to storage.
 * - Call ContentEntityBase's postSave().
 * - Invoke each field's insert hooks.
 * - Invoke insert hooks.
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object that has been created, but not yet saved.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_insert(EntityInterface $entity) {

  // A hook could save any of its own information about the entity into
  // its own database tables. It may not modify the $entity.
}

/**
 * Responds after storage has been updated for a changed FolderShare entity.
 *
 * Saving an updated entity goes through these steps:
 * - Call ContentEntityBase's preSave().
 * - Invoke each field's presave hooks.
 * - Invoke presave hooks.
 * - Save the entity to storage.
 * - Call ContentEntityBase's postSave().
 * - Invoke each field's update hooks.
 * - Invoke update hooks.
 *
 * FolderShare does not override ContentEntityBase's preSave() and postSave().
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object that is being updated.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_update(EntityInterface $entity) {

  // A hook could save any of its own information about the entity into
  // its own database tables. It may not modify the $entity.
}

/*----------------------------------------------------------------------
 *
 * Entity storage delete.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds before a FolderShare entity is deleted from storage.
 *
 * Deletion of an entity follows these steps:
 * - Call ContentEntityBase's preDelete().
 * - Invoke predelete hooks.
 * - Delete the entity from storage.
 * - Call ContentEntityBase's postDelete().
 * - Invoke delete hooks.
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object that is about to be deleted.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_predelete(EntityInterface $entity) {
}

/**
 * Responds after a FolderShare entity has been deleted from storage.
 *
 * Deletion of an entity follows these steps:
 * - Call ContentEntityBase's preDelete().
 * - Invoke predelete hooks.
 * - Delete the entity from storage.
 * - Call ContentEntityBase's postDelete().
 * - Invoke delete hooks.
 *
 * This is a low-level hook provided by Drupal core. See also higher-level
 * hooks provide by the FolderShare module.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object that has been deleted from storage.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityStorageBase
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_delete(EntityInterface $entity) {

  // A hook could delete any externally saved information associated
  // with this entity, such as records in its own database tables.
}

/*----------------------------------------------------------------------
 *
 * View.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds as a FolderShare entity is being assembled before rendering.
 *
 * Rendered view creation follows these steps:
 * - Load entity.
 * - Create renderable view of the entity.
 * - Invoke view hooks.
 * - Invoke view_alter hooks.
 *
 * @param array $build
 *   A renderable array representing the entity content. This may include
 *   render elements for pseudo-fields and embedded views, such as for a
 *   view listing the contents of a folder.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object being rendered.
 * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
 *   The entity view display holding the options configured for the
 *   entity components, such as their weights on a page and their field
 *   formatters.
 * @param string $viewMode
 *   The name of the view mode.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityViewBuilder
 * @see \Drupal\foldershare\Entity\FolderShare
 */
function hook_foldershare_view(
  array &$build,
  EntityInterface $entity,
  EntityViewDisplayInterface $display,
  $viewMode) {

  // A hook could add renderable information about the entity, such as
  // special marks for favorite entities, indicators for popular or
  // heavily used entities, or flags indicating problem content.
}

/**
 * Responds after a FolderShare entity has been assembled before rendering.
 *
 * Rendered view creation follows these steps:
 * - Load entity.
 * - Create renderable view of the entity.
 * - Invoke view hooks.
 * - Invoke view_alter hooks.
 *
 * @param array $build
 *   A renderable array representing the entity content. This may include
 *   render elements for pseudo-fields and embedded views, such as for a
 *   view listing the contents of a folder.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object being rendered.
 * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface $display
 *   The entity view display holding the options configured for the
 *   entity components, such as their weights on a page and their field
 *   formatters.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityViewBuilder
 * @see \Drupal\foldershare\Entity\Builder\FolderShareViewBuilder
 */
function hook_foldershare_view_alter(
  array &$build,
  EntityInterface $entity,
  EntityViewDisplayInterface $display) {

  // A hook may be used to do post-processing on a renderable version
  // of the entity. This could re-arrange content or suppress content.
}

/**
 * Responds before a cache has been checked for a renderable FolderShare entity.
 *
 * The hook may modify renderable elements relating to cacheing under the
 * '#cache' key.
 *
 * @param array $build
 *   A renderable array representing the entity content. This may include
 *   render elements for pseudo-fields and embedded views, such as for a
 *   view listing the contents of a folder.
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object being rendered.
 * @param string $viewMode
 *   The name of the view mode.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityViewBuilder
 * @see \Drupal\foldershare\Entity\Builder\FolderShareViewBuilder
 */
function hook_foldershare_build_defaults_alter(
  array &$build,
  EntityInterface $entity,
  $viewMode) {

  // A hook may add or remove cache control, but it should not modify
  // the build otherwise.
}

/**
 * Alters or adds links for a viewed FolderShare entity.
 *
 * The hook may modify or add links to include in the 'Links' page component
 * when viewing a FolderShare entity.
 *
 * @param array $links
 *   A renderable array representing the current links.
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity object being rendered.
 * @param array $context
 *   Information on the context in which the entity links are being displayed,
 *   including:
 *   - 'view_mode': the view mode in which the entity is being viewed.
 *   - 'langcode': the language in which the entity is being viewed.
 */
function foldershare_foldershare_links_alter(
  array &$links,
  FolderShareInterface $entity,
  array &$context) {

  $links['mymodule'] = [
    '#theme'         => 'links__foldershare__mymodule',
    '#links'         => [
      'my_feature'   => [
        'title'      => t('My feature'),
        'url'        => $entity->toUrl(),
        'language'   => $entity->language(),
        'attributes' => [
          'rel'      => 'tag',
          'title'    => t('Hover my feature'),
        ],
      ],
    ],
    '#attributes'    => [
      'class'        => [
        'links',
        'inline',
      ],
    ],
  ];
}

/*----------------------------------------------------------------------
 *
 * Edit form.
 *
 *----------------------------------------------------------------------*/
/**
 * Responds before a FolderShare edit form is created.
 *
 * Form creation follows these steps:
 * - Load entity.
 * - Invoke prepare_form hooks.
 * - Build form.
 *
 * @param \Drupal\Core\Entity\EntityInterface $entity
 *   The entity object being edited.
 * @param string $operation
 *   The current operation.
 * @param \Drupal\Core\Form\FormStateInterface $formState
 *   The current state of the form.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\Core\Entity\EntityForm
 * @see \Drupal\foldershare\Form\EditFolderShare
 */
function hook_foldershare_prepare_form(
  EntityInterface $entity,
  $operation,
  FormStateInterface $formState) {

  // A hook may modify the entity before it a form is created to edit
  // the entity. This could add or suppress portions of the $entity.
}

/*----------------------------------------------------------------------
 *
 * Search.
 *
 *----------------------------------------------------------------------*/
/**
 * Appends to search results for a FolderShare entity.
 *
 * FolderShare's search results are created by adding entity names,
 * field values, and file contents to a search index. This index is
 * then searched to produce a list of search results to present to
 * the user. This hook may be used to append to those search results.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being include in the search results.
 *
 * @return array
 *   Returns an associative array with additional named pieces of information
 *   to be included in the search results. This array, along with FolderShare's
 *   array of values, is passed to the search result theme to present.
 *
 * @see \Drupal\foldershare\Plugin\Search\FolderShareSearch
 */
function hook_foldershare_search_result(FolderShareInterface $entity) {

  // A hook could query additional database tables for attributes to
  // include in the search results, such as to mark an item from a
  // favorites list.
  return [];
}

/**
 * Appends to the search index for a FolderShare entity.
 *
 * FolderShare's search indexing accumulates data extracted from an entity,
 * including its name and the values of its text fields and file content.
 * This hook may be used to append additional values to the search index.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being include in the search index.
 *
 * @return string
 *   Returns a string containing additional information to include in the
 *   search index entry for this entity.
 *
 * @see \Drupal\foldershare\Plugin\Search\FolderShareSearch
 */
function hook_foldershare_update_index(FolderShareInterface $entity) {

  // A hook could query additional database tables to get further attributes
  // of an entity.
  return '';
}

/*----------------------------------------------------------------------
 *
 * Misc attributes.
 *
 *----------------------------------------------------------------------*/
/**
 * Alters a proposed MIME type for a FolderShare entity.
 *
 * The setMimeType() method on a FolderShare entity sets the MIME type.
 * This method is called whenever a file, image, media, object, or folder
 * is created, copied, or renamed. setMimeType() calls this hook, passing
 * it the entity being changed and the proposed MIME type. Hooks may
 * return an alternate MIME type. The first non-empty MIME type returned by
 * module hooks is used.
 *
 * Process locks are in place during this hook call. This will prevent many
 * folder tree operations from being started by other users and processes
 * until the current operation completes, after hooks return. Process locks
 * will also prevent code called by this hook from doing further folder
 * tree operations. Hooks should therefore do their work quickly and simply,
 * without altering the given entity or other entities in the folder tree.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   The entity being set. The entity has *not* been saved yet and should
 *   not be saved by a hook.
 * @param string $mimeType
 *   The proposed MIME type.
 *
 * @return string
 *   Returns a string containing an alternate MIME type. An empty string
 *   reverts to the default proposed MIME type.
 *
 * @see \Drupal\foldershare\Entity\FolderShare::setMimeType()
 * @see \Drupal\foldershare\Entity\FolderShare::setMimeTypeToDefault()
 */
function hook_foldershare_mime_type_alter(
  FolderShareInterface $entity = NULL,
  string $mimeType = '') {

  // A hook could use the entity's name or position within a folder
  // hierarchy to choose a special MIME type. If nothing is returned,
  // the default is used.
  return '';
}

/**
 * Alters a proposed list of filename extensions for files allowed in a parent.
 *
 * The hook is called to provide a more restrictive list of allowed filename
 * extensions than that allowed site wide. The hook is passed a FolderShare
 * parent entity (which may be NULL), an owner user ID for a user performing
 * an operation, and a default array of filename extensions allowed for the
 * operation. This default is typically the site-wide default list set by
 * the site administrator. If this list is empty, then the site administrator
 * has not set a default list and all filename extensions are allowed.
 *
 * Hooks may return a new list of filename extensions used to restrict the
 * range of file types supported as children of the indicated parent. If the
 * parent is NULL, the restriction applies to the user's root list.
 *
 * The hook is called in these situations:
 *
 * - Before a file is uploaded in order to set the list of filename
 *   extensions allowed for an upload. Client (e.g. browsers) may use this
 *   to limit the types of files that a user can select for upload.
 *
 * - After a file has been uploaded in order to validate that the file's
 *   extension is allowed in the parent.
 *
 * - Before a file is copied, moved, or renamed in order to validate that
 *   file's extension is allowed in the parent.
 *
 * - Before a file is extracted from an archive in order to validate that
 *   the file's extension is allowed in the parent.
 *
 * A module's implementation of the hook may return one of two values:
 *
 * - An empty or NULL array, indicating that the hook imposes no restrictions
 *   on filename extensions.
 *
 * - An array of allowed filename extensions, without leading dots.
 *
 * When multiple modules implement the same hook, this returned extension
 * lists are merged into a single list, removing leading dots and
 * duplicates, and converting extensions to lower case. One of three
 * cases apply:
 *
 * - If the merged hook extension list is empty, the default extension list is
 *   returned. This is also the case when there are no hook implementations.
 *
 * - If the hook merged extension list is not empty, but the default extension
 *   list is empty, then the hook extension list is returned. In this case, the
 *   site has no filename extension list and normally accepts all file types,
 *   but hooks have created context-specific limits.
 *
 * - Otherwise the default and hook extension lists are intersected and the
 *   result is returned. In this case, the site has a list of allowed
 *   filename extensions and hooks may further restrict this list, but they
 *   may not allow an extension that the site has not allowed.
 *
 * @param \Drupal\foldershare\FolderShareInterface $parent
 *   The parent folder for new files. If the parent is NULL, files are
 *   being added to the user's root list.
 * @param int $ownerUid
 *   The user ID of the intended owner of the file. If the value is (-1),
 *   the file is being added for the current user.
 * @param string[] $extensions
 *   The array of default allowed filename extensions. This is typically
 *   the site administrator's default list of allowed extensions.
 *   If this list is empty, then the site has no filename extension limits
 *   and all extensions are allowed. This list is assumed to contain
 *   only extensions without leading dots and in lower case.
 *
 * @return string[]
 *   A sorted duplicate-free lower-case array of allowed filename
 *   extensions, without leading dots.
 */
function hook_foldershare_allowed_filename_extensions_alter(
  FolderShareInterface $parent = NULL,
  int $ownerUid = (-1),
  array $extensions = []) {

  // A hook could use the parent entity to decide what filename extensions
  // are allowed for files within the entity.
  return [];
}

/**
 * Alters a proposed file upload size limit in a parent.
 *
 * This hook is called to provide a more restrictive file upload size limit
 * than that allowed site wide. The hook is passed a FolderShare parent
 * entity (which may be NULL), an owner user ID for a user performing an
 * operation, and a default limit. This default is typically the site-wide
 * default set by the site administrator.
 *
 * Hooks may return a new smaller limit to restrict the upload file size
 * supported for children of the indicated parent. If the parent is NULL,
 * the restriction applies to the user's root list.
 *
 * The hook is called in these situations:
 *
 * - Before a file is uploaded in order to set the limit for an upload.
 *   Client (e.g. browsers) may use this to limit the size of files that
 *   a user can select for upload.
 *
 * - After a file has been uploaded in order to validate that the file's
 *   size is at or below the limit in the parent.
 *
 * A module's implementation of the hook may return one of two values:
 *
 * - A non-positive integer value indicating that the hook imposes no new
 *   limit on the file size.
 *
 * - A new positive integer value for a limit in the current context.
 *
 * When multiple modules implement the same hook, this method uses the
 * lowest value set by all hooks.
 *
 * The returned value will be no larger than the given default limit.
 *
 * @param \Drupal\foldershare\FolderShareInterface $parent
 *   The parent folder for new files. If the parent is NULL, files are
 *   being added to the user's root list.
 * @param int $ownerUid
 *   The user ID of the intended owner of the file. If the value is (-1),
 *   the file is being added for the current user.
 * @param int $limit
 *   The default file upload size limit. This is typically the site
 *   administrator's default, or the configured PHP limit. The value must
 *   be positive.
 *
 * @return int
 *   Returns a positive integer for the file upload size limit.
 */
function hook_foldershare_file_upload_size_limit_alter(
  FolderShareInterface $parent = NULL,
  int $ownerUid = (-1),
  int $limit = 0) {

  // A hook could use the parent entity or the user ID to decide what
  // file size limit to imposed for files uploaded within the entity.
  return $limit;
}

/**
 * Validates that a file is suitable before it is added to the folder tree.
 *
 * The hook is called when a file is ready to be added to a folder tree.
 * The folder the file will be added to, and the owner's user ID are passed
 * to the hook, along with the file to be added. The hook may check the
 * file name, extension, size, and content to confirm that the file is in
 * a valid file format and valid for adding to the folder tree. If not
 * valid, the hook should return FALSE or throw an exception that explains
 * while the file is not valid.
 *
 * The hook is called in multiple situations:
 * - On a file upload via a form.
 * - On a file upload via REST and an HTTP input stream.
 * - On extraction of a file from an archive (e.g. ZIP).
 * - On a file addition via the FolderShare entity API.
 *
 * If the parent folder is a NULL, then the file is being added to the
 * indicated owner's root list.
 *
 * If the owner user ID is (-1), then the file is being added to the
 * current user's root list.
 *
 * The given parent and file entities should not be changed by the hook.
 * Any changes made are not guaranteed to be retained.
 *
 * The file referenced by the file entity will be a local file. The file
 * may be read, but it should not be written or deleted. Any changes made
 * to the local file are not guaranteed to be retained.
 *
 * @param \Drupal\foldershare\FolderShareInterface $parent
 *   The parent folder for the new file. If the parent is NULL, the file is
 *   being added to the user's root list.
 * @param int $ownerUid
 *   The user ID of the intended owner of the file. If the value is (-1),
 *   the file is being added for the current user.
 * @param \Drupal\file\FileInterface $file
 *   The file being added to the folder.
 *
 * @return bool
 *   Returns FALSE if the file is not valid. Any other return value is
 *   taken as valid.
 *
 * @throws \Drupal\foldershare\Entity\Exception\ValidationException
 *   May throw an exception if the file is not valid. The exception should
 *   be a ValidationException, but any other exception type may be thrown
 *   and it will be converted to a ValidationException by the hook caller.
 *   Exceptions may include a message explaining why the file is not valid.
 *   The user interface may present the message to the user.
 */
function hook_foldershare_validate_file(
  FolderShareInterface $parent = NULL,
  int $ownerUid = (-1),
  FileInterface $file = NULL) {

  // A hook could open the file and check that it is properly formatted
  // and complete. Returning FALSE or throwing an exception indicates the
  // file is not valid.
  return TRUE;
}

/*----------------------------------------------------------------------
 *
 * User interface.
 *
 *----------------------------------------------------------------------*/
/**
 * Checks if the user interface command menu should be enabled.
 *
 * This hook is called in multiple situations:
 * - When an entity is viewed.
 * - When a root list is viewed.
 *
 * The hook may return TRUE or FALSE to control the visibility of the
 * command menu button and its menu of commands when presenting the
 * indicated entity or root list for the indicated user.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 *
 * @return bool
 *   Returns TRUE if the menu should be shown, and FALSE otherwise.
 */
function hook_foldershare_ui_command_menu_enable(
  FolderShareInterface $entity = NULL,
  int $userUid = (-1)) {

  return TRUE;
}

/**
 * Checks if the user interface ancestor menu should be enabled.
 *
 * This hook is called in multiple situations:
 * - When an entity is viewed.
 * - When a root list is viewed.
 *
 * The hook may return TRUE or FALSE to control the visibility of the
 * ancestor menu button and its menu of ancestor folders when presenting the
 * indicated entity or root list for the indicated user.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 *
 * @return bool
 *   Returns TRUE if the menu should be shown, and FALSE otherwise.
 */
function hook_foldershare_ui_ancestor_menu_enable(
  FolderShareInterface $entity = NULL,
  int $userUid = (-1)) {

  return TRUE;
}

/**
 * Checks if the user interface search box should be enabled.
 *
 * This hook is called in multiple situations:
 * - When an entity is viewed.
 * - When a root list is viewed.
 *
 * The hook may return TRUE or FALSE to control the visibility of the
 * search field when presenting the indicated entity or root list for the
 * indicated user. The hook will not be called if the Drupal core search
 * module is not enabled.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 *
 * @return bool
 *   Returns TRUE if the menu should be shown, and FALSE otherwise.
 */
function hook_foldershare_ui_search_box_enable(
  FolderShareInterface $entity = NULL,
  int $userUid = (-1)) {

  return TRUE;
}

/**
 * Checks if the user interface search box should be enabled.
 *
 * This hook is called in multiple situations:
 * - When an entity is viewed.
 * - When a root list is viewed.
 *
 * The hook may return TRUE or FALSE to control the visibility of the
 * sidebar when presenting the indicated entity or root list for the
 * indicated user.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 *
 * @return bool
 *   Returns TRUE if the menu should be shown, and FALSE otherwise.
 */
function hook_foldershare_ui_sidebar_enable(
  FolderShareInterface $entity = NULL,
  int $userUid = (-1)) {

  return TRUE;
}

/**
 * Checks if the user interface sidebar show/hide button should be enabled.
 *
 * This hook is called in multiple situations:
 * - When an entity is viewed.
 * - When a root list is viewed.
 *
 * The hook may return TRUE or FALSE to control the visibility of the
 * toolbar button to show/hide the sidebar when presenting the indicated
 * entity or root list for the indicated user.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 *
 * @return bool
 *   Returns TRUE if the menu should be shown, and FALSE otherwise.
 */
function hook_foldershare_ui_sidebar_button_enable(
  FolderShareInterface $entity = NULL,
  int $userUid = (-1)) {

  return TRUE;
}

/**
 * Filters FolderShare menu command definitions.
 *
 * This hook is called following menu command plugin discovery and before
 * those plugins are processed for use. The hook may alter the definition
 * list to hide specific commands. While it is not advised, it is also
 * possible to modify the definitions of existing commands or add new
 * definitions.
 *
 * @param \Drupal\Component\Plugin\Definition\PluginDefinitionInterface[]|array[] $definitions
 *   The array of plugin definitions.
 * @param mixed[] $extra
 *   (optional, default = NULL) An associative array containing additional
 *   information provided by the code requesting the filtered definitions.
 * @param string $consumer
 *   (optional, default = NULL) A string identifying the consumer of these
 *   plugin definitions.
 */
function hook_foldersharecommand_info_alter(
  array &$definitions,
  array $extra = NULL,
  $consumer = NULL) {

  // Block a command site-wide, unset the command definition:
  $definitions->unset('foldershare_new_folder');
}

/**
 * Checks if a menu command should be allowed for a parent entity and user.
 *
 * This hook is called when building a culled list of plugin menu commands
 * to be included on a page, if they apply. Command annotation already provides
 * a set of constraints that must be met in order for a command to be
 * available (see the FolderShareCommand annotation class). This hook is
 * called *before* checking those constraints, enabling it to block commands
 * based upon more specific constraint for the page's entity and user.
 *
 * The entity passed to the hook is the *parent* entity for purposes of
 * command constraints. It is the entity primarily viewed on a page and it
 * provides the default operand for many commands unless the user selects
 * entities from a file/folder list. This hook can block a command for that
 * parent.
 *
 * However, because this hook is *not* called for the selection, this hook
 * cannot block use of a command on a specific selected entity. Those
 * constraints are applied within the Javascript front-end. Modules wishing
 * to make more detailed control over command constraints should use command
 * annotation, or modify that annotation using the
 * "hook_foldersharecommand_info_alter()" hook.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 * @param array $definition
 *   (optional, default = []) The plugin definition. See the
 *   FolderShareCommand annotation class for a description of fields.
 */
function hook_foldershare_ui_command_enable(
  FolderShareInterface $entity = NULL,
  int $userUid = (-1),
  array $definition = []) {
}

/**
 * Returns additional links for the folder browser sidebar.
 *
 * Additional links, if any, are included in the folder browser's sidebar
 * in the "Favorites" section, below the well-known favorites links.
 *
 * @param \Drupal\foldershare\FolderShareInterface $entity
 *   (optional, default = NULL = root list) The entity being viewed.
 * @param int $userUid
 *   (optional, default = (-1) = current user) The user ID of the current user.
 *
 * @return array
 *   Returns an array of Link objects that each have title text and a URL.
 */
function hook_foldershare_ui_sidebar_links(
  FolderShareInterface $entity = NULL,
  int $userUid) {

  // Add links to specific destinations within the site or outside of it.
  return [
    new Link("Example.com", Url::fromUri("http://example.com")),
  ];
}

/**
 * @}
 */
