<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\foldershare\ManageFileSystem;
use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Entity\Exception\LockException;
use Drupal\foldershare\Entity\Exception\ValidationException;

/**
 * Rename FolderShare entities.
 *
 * This trait includes methods to rename FolderShare entities and
 * wrapped File entities.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait OperationRenameTrait {

  /*---------------------------------------------------------------------
   *
   * Rename FolderShare entity.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function rename(string $newName) {
    //
    // Validate
    // --------
    // The new name must be different and legal.
    $oldName = $this->getName();
    if ($oldName === $newName) {
      // No change.
      return;
    }

    $currentUserId = self::getCurrentUserId()[0];

    $extensions = [];
    if ($this->isFileOrImage() === TRUE) {
      // Prepare to check for allowed filename extensions by getting the
      // site's allowed list, altered by module hooks.
      $extensions = ManageHooks::callHookAllowedFilenameExtensionsAlter(
        $this->getParentFolder(),
        $currentUserId,
        []);
    }

    // Throw an exception on an illegal name or bad filename extension.
    self::validateNameAndFilenameExtension(
      $newName,
      $extensions);

    // START CRITICAL REGION.
    $originalUserAbort = @ignore_user_abort(TRUE);

    //
    // Lock root folder tree.
    // ----------------------
    // Lock the item's root's folder tree to insure no other operation can
    // modify the folder tree while this item is being updated.
    //
    // LOCK ROOT FOLDER TREE.
    $rootId = $this->getRootItemId();
    if (self::acquireRootOperationLock($rootId) === FALSE) {
      // END CRITICAL REGION.
      @ignore_user_abort($originalUserAbort);

      // User-facing exception message.
      $operator = $this->t('renamed');
      throw new LockException(
        self::getStandardLockExceptionMessage($operator, $this->getName()));
    }

    //
    // Lock owner's root list, if needed.
    // ----------------------------------
    // If this item is a root item, then lock the root list of the item's
    // owner (which is normally this user). This is needed in order to keep
    // the root list unchanged while we check for name collisions.
    //
    // LOCK OWNER'S ROOT LIST.
    if ($this->isRootItem() === TRUE) {
      if (self::acquireUserRootListLock($this->getOwnerId()) === FALSE) {
        // UNLOCK ROOT FOLDER TREE.
        self::releaseRootOperationLock($rootId);

        // END CRITICAL REGION.
        @ignore_user_abort($originalUserAbort);
        // User-facing exception message.
        $operator = $this->t('renamed');
        throw new LockException(
          self::getStandardLockExceptionMessage($operator, $this->getName()));
      }
    }

    //
    // Check name.
    // -----------
    // Check that the new name is unique within either the parent folder
    // (if any) or the owner's root list (if there is no parent).
    if ($this->isRootItem() === TRUE) {
      $uid = (int) $this->getOwnerId();
      if (self::isRootNameUnique($newName, (int) $this->id(), $uid) === FALSE) {
        // UNLOCK OWNER'S ROOT LIST.
        self::releaseUserRootListLock($uid);

        // UNLOCK ROOT FOLDER TREE.
        self::releaseRootOperationLock($rootId);

        throw new ValidationException(
          self::getStandardNameInUseExceptionMessage($newName));
      }
    }
    else {
      $parentFolder = $this->getParentFolder();

      if ($parentFolder->isNameUnique($newName, (int) $this->id()) === FALSE) {
        // UNLOCK ROOT FOLDER TREE.
        self::releaseRootOperationLock($rootId);

        // END CRITICAL REGION.
        @ignore_user_abort($originalUserAbort);

        throw new ValidationException(
          self::getStandardNameInUseExceptionMessage($newName));
      }
    }

    //
    // Change the name.
    // ----------------
    // Set the name, update the MIME type, update the wrapped file (if any),
    // and save.
    $this->setName($newName);

    // Change the wrapped file's name too, if any.
    $this->renameWrappedFile($newName);

    // Update the MIME type for this and the wrapped file/image/media, if any,
    // then update the file/image kind and fields if needed.
    $this->setMimeType($this->getMimeType());
    $this->updateFileAndImageKinds();

    $this->save();

    //
    // Unlock everything.
    // ------------------
    // Unlock the owner's root list, if it was locked, and unlock the
    // root folder tree containing the item.
    //
    if ($this->isRootItem() === TRUE) {
      // UNLOCK OWNER'S ROOT LIST.
      self::releaseUserRootListLock($this->getOwnerId());
    }

    // UNLOCK ROOT FOLDER TREE.
    self::releaseRootOperationLock($rootId);

    // END CRITICAL REGION.
    @ignore_user_abort($originalUserAbort);

    //
    // Hook & log.
    // -----------
    // Note the change.
    ManageHooks::callHookPostOperation(
      'rename',
      [
        $this,
        $oldName,
        $newName,
        $currentUserId,
      ]);
    ManageLog::activity(
      "Renamed @kind '@oldName' (# @id) to '@newName'.",
      [
        '@id'      => $this->id(),
        '@kind'    => $this->getKind(),
        '@oldName' => $oldName,
        '@newName' => $newName,
        'entity'   => $this,
        'uid'      => $currentUserId,
      ]);
  }

  /*---------------------------------------------------------------------
   *
   * Rename wrapped File entity.
   *
   *---------------------------------------------------------------------*/
  /**
   * Renames an entity's underlying file, image, and media entity, if any.
   *
   * After a FolderShare entity has been renamed, this method updates any
   * underlying entities to share the same name. This includes File objects
   * underneath 'file' and 'image' kinds, and Media objects underneath
   * 'media' kinds.
   *
   * This method has no effect if the current entity is not a file, image,
   * or media wrapper.
   *
   * @param string $newName
   *   The new name for the underlying entities.
   */
  private function renameWrappedFile(string $newName) {
    // If the item is not wrapping a file or media item, do nothing.
    if ($this->isFile() === FALSE &&
        $this->isImage() === FALSE &&
        $this->isMedia() === FALSE) {
      // No wrapped file.
      return;
    }

    // For a wrapped media item, update it's name.
    if ($this->isMedia() === TRUE) {
      $media = $this->getMedia();
      if ($media !== NULL) {
        $media->setName($newName);
        self::saveMediaWithRetry($media);
      }

      return;
    }

    // For a file or image, update the corresponding File entity's name
    // and then, if needed, update the name of the stored file.
    if ($this->isFile() === TRUE) {
      $file = $this->getFile();
    }
    else {
      $file = $this->getImage();
    }

    if ($file === NULL) {
      // There is no wrapped file? The entity is corrupted!
      return;
    }

    // Set the File entity name first. This is used by the FileUtilities
    // call below to compute a new URI based upon the name (really, just
    // the extension) and the file's entity ID.
    $file->setFilename($newName);

    $oldUri = $file->getFileUri();
    $newUri = ManageFileSystem::getFileUri($file);

    // If the URIs differ, then something about the new name has caused
    // the underlying saved file to change its name. This is probably the
    // filename extension. Move the file.
    if ($oldUri !== $newUri) {
      // It should not be possible for the following move to fail. The
      // old and new URIs differ and both are based upon the file
      // entity's ID, which is unique. This prevents name collisions.
      // Only the filename extensions can differ.
      //
      // The only errors that can occur are problems with the underlying
      // server file system. And there's nothing we can do about them.
      // The above function will report those to the server log.
      FileUtilities::rename($oldUri, $newUri);

      // Update the URI to point to the moved file.
      $file->setFileUri($newUri);
    }

    self::saveFileWithRetry($file);
  }

}
