<?php

namespace Drupal\foldershare;

/**
 * Defines constants use through the module.
 *
 * This class defines constants for often-used text, services, libraries,
 * themes, permissions, routes, and so forth. In many cases, these values
 * repeat values defined in YML files.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @internal
 * Routes, permissions, themes, libraries, and many other names are
 * defined in the module's many ".yml" files. These same names are needed
 * at run-time to refer to the defined routes, permissions, etc.
 *
 * While many YML-defined values are available at run-time via the
 * Drupal API, they are often referenced via name. This forces these
 * name strings to be embedded in the code.
 *
 * Frequent use of important hard-coded strings invites typos that can
 * cause routes, permissions, settings, and other values to not line up
 * with their intended targets. To reduce the effect of typos, and centralize
 * common names, these are all defined here in a module-wide list of
 * constants. Module code uses these constants by name, rather than
 * hard-coding typo-risky strings. This turns these strings into syntactic
 * objects in PHP, which the PHP parser can catch and report as errors if
 * typos creep in.
 * @endinternal
 *
 * @ingroup foldershare
 */
final class Constants {

  /*--------------------------------------------------------------------
   *
   * Administration.
   *
   *--------------------------------------------------------------------*/
  /**
   * The module machine name.
   *
   * This must match all uses of the module name:
   *
   * - The name of the module directory.
   * - The name of the "MODULE.module" file.
   * - The machine name in "MODULE.info.yml".
   * - The name of the \Drupal\MODULE namespace for the module.
   *
   * This module name should be used as the prefix for names that
   * have global scope, such as tables, settings, and routes.
   *
   * The module name should be used as a prefix for CSS classes
   * and IDs, and as the name of the module's function group in
   * Javascript.
   *
   * @var string
   */
  const MODULE = 'foldershare';

  /**
   * The module's settings (configuration) name.
   *
   * This must match the configuration name in "MODULE.info.yml'.
   *
   * This must match the file name and settings group in
   * 'config/schema/MODULE.settings.yml'. It also must match the
   * file name in 'install/MODULE.settings.yml'.
   *
   * @var string
   */
  const SETTINGS = 'foldershare.settings';

  /**
   * The module's search index name.
   *
   * This must match the name in the "FolderSearch" plugin's annotation.
   *
   * This must match the name of the search page configuration in
   * 'config/optional/search.page.SEARCH_INDEX.yml'.
   *
   * This search index only exists if the Drupal core search module
   * is enabled.
   *
   * @var string
   */
  const SEARCH_INDEX = 'foldershare_search';

  /*--------------------------------------------------------------------
   *
   * Routes and route parameters.
   *
   * These constants define well-known module routes. All values must
   * match those in 'MODULE.routing.yml'.
   *
   *--------------------------------------------------------------------*/
  /**
   * The route to a FolderShare entity view page.
   *
   * The route has a numeric FolderShare ID parameter.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   * @see self::ROUTE_FOLDERSHARE_ID
   */
  const ROUTE_FOLDERSHARE = 'entity.foldershare.canonical';

  /**
   * The route to the user's personal root item list.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_ROOT_ITEMS_PERSONAL = 'entity.foldershare.rootitems';

  /**
   * The route to the user's shared-with-them root item list.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_ROOT_ITEMS_SHARED = 'entity.foldershare.rootitems.shared';

  /**
   * The route to the site's public root item list.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_ROOT_ITEMS_PUBLIC = 'entity.foldershare.rootitems.public';

  /**
   * The route to the site admin's root item list of everything.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_ROOT_ITEMS_ALL = 'entity.foldershare.rootitems.all';

  /**
   * The route to a FolderShare entity edit page.
   *
   * The route has a numeric FolderShare ID parameter.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   * @see self::ROUTE_FOLDERSHARE_ID
   */
  const ROUTE_FOLDERSHARE_EDIT = 'entity.foldershare.edit';

  /**
   * The FolderShare view and edit route parameter for an entity ID.
   *
   * @var string
   */
  const ROUTE_FOLDERSHARE_ID = 'foldershare';

  /**
   * The UI's command route to command-specific forms.
   *
   * @var string
   */
  const ROUTE_FOLDERSHARE_COMMAND_FORM = 'entity.foldersharecommand.plugin';

  /**
   * The route to the settings page.
   *
   * This must match the route in 'MODULE.routing.yml',
   * 'foldershare.links.menu.yml', and 'MODULE.links.task.yml'.
   *
   * @var string
   */
  const ROUTE_SETTINGS = 'entity.foldershare.settings';

  /**
   * The route to the file download handler.
   *
   * An argument 'file' must contain the entity ID of a File object wrapped
   * by a FolderShare entity.
   *
   * The route may include one or more query arguments. If present, the
   * 'foldershareprefix' query argument contains a string containing the
   * URL path prefix to prepend to the incoming file URI to generate a
   * redirect URL and return to whatever URL processing another
   * module (such as Image) may require for derived paths.
   *
   * Any other query and fragment arguments are passed along on the
   * redirect.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_DOWNLOADFILE = 'entity.foldershare.file';

  /**
   * The query name for a path prefix for the file download handler.
   *
   * @var string
   */
  const ROUTE_DOWNLOADFILE_PREFIX = 'foldershareprefix';

  /**
   * The route to the entity download handler.
   *
   * The argument 'encoded' must contain a JSON base64 array containing
   * FolderShare entity IDs to download. They must all be children of
   * the same parent.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_DOWNLOAD = 'entity.foldershare.download';

  /**
   * The route to the usage page.
   *
   * This must match the route in 'MODULE.routing.yml'.
   *
   * @var string
   */
  const ROUTE_USAGE = 'foldershare.reports.usage';

  /**
   * The route to the views UI page listing views.
   *
   * @var string
   */
  const ROUTE_VIEWS_UI = 'entity.view.collection';

  /**
   * The base route to the views UI page for a specific view.
   *
   * The name of the view to edit must be in the "view" parameter.
   *
   * @var string
   */
  const ROUTE_VIEWS_UI_VIEW = 'entity.view.edit_form';

  /*--------------------------------------------------------------------
   *
   * Services.
   *
   *--------------------------------------------------------------------*/
  /**
   * The service that keeps track of folder command plugins.
   *
   * This must match the route in 'MODULE.services.yml'.
   *
   * @var string
   */
  const SERVICE_FOLDERSHARECOMMANDS = 'foldershare.plugin.manager.foldersharecommand';

  /*--------------------------------------------------------------------
   *
   * Plugins.
   *
   *--------------------------------------------------------------------*/

  /**
   * The name of the module's search plugin for Drupal core Search.
   *
   * This name must match the ID for the search plugin.
   *
   * @var string
   */
  const SEARCH_PLUGIN = 'foldershare_search';

  /**
   * The name of the module's internal name field formatter plugin.
   *
   * This name must match the ID of the internal plugin.
   *
   * @var string
   */
  const INTERNAL_NAME_FORMATTER = 'foldershare_internal_name';

  /**
   * The name of the module's internal folder name field formatter plugin.
   *
   * This name must match the ID of the internal plugin.
   *
   * @var string
   */
  const INTERNAL_FOLDER_NAME_FORMATTER = 'foldershare_internal_folder_name';

  /*--------------------------------------------------------------------
   *
   * Views and their displays.
   *
   * These constants define the names of module-installed views.
   *
   *--------------------------------------------------------------------*/
  /**
   * The view creating all lists of files and folders.
   *
   * The name must match the machine name of the "FolderShare Lists" view.
   *
   * @var string
   */
  const VIEW_LISTS = 'foldershare_lists';

  /**
   * The view display listing all root items for any user.
   *
   * Used for the 'All' list available to admins.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "List all" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_LIST_ALL = 'list_all';

  /**
   * The view display listing root items owned by or shared with a user.
   *
   * Used for the 'Home' list.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "List personal" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_LIST_PERSONAL = 'list_personal';

  /**
   * The view display listing root items shared with a user.
   *
   * Used for the 'Shared' list.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "List shared" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_LIST_SHARED = 'list_shared';

  /**
   * The view display listing root items owned by or shared with anonymous.
   *
   * Used for the 'Public' list.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "List public" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_LIST_PUBLIC = 'list_public';

  /**
   * The view display listing folder contents available to the user.
   *
   * Used for the folder contents list on entity view pages.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "List folder" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_LIST_FOLDER = 'list_folder';

  /**
   * The view display dialog listing all root items for any user.
   *
   * Used for the 'All' list in dialogs.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "Dialog all" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_DIALOG_ALL = 'dialog_all';

  /**
   * The view display dialog listing root items owned by a user.
   *
   * Used for the 'Home' list in dialogs.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "Dialog personal" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_DIALOG_PERSONAL = 'dialog_personal';

  /**
   * The view display dialog listing root items shared with a user.
   *
   * Used for the 'Shared' list in dialogs.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "Dialog shared" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_DIALOG_SHARED = 'dialog_shared';

  /**
   * The view display dialog listing root items owned by/shared with anonymous.
   *
   * Used for the 'Public' list in dialogs.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "Dialog public" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_DIALOG_PUBLIC = 'dialog_public';

  /**
   * The view display dialog listing folder contents available to the user.
   *
   * Used for the folder contents list in dialogs.
   *
   * The name must match the machine name of the "FolderShare Lists" view
   * and its "Dialog folder" display.
   *
   * @var string
   */
  const VIEW_DISPLAY_DIALOG_FOLDER = 'dialog_folder';

  /*--------------------------------------------------------------------
   *
   * Tokens.
   *
   *--------------------------------------------------------------------*/
  /**
   * The name of the token group for folder fields.
   *
   * The name should match FolderShare::ENTITY_TYPE_ID.
   *
   * @var string
   */
  const FOLDERSHARE_TOKENS = 'foldershare';

  /*--------------------------------------------------------------------
   *
   * Themes.
   *
   *--------------------------------------------------------------------*/
  /**
   * The folder page theme.
   *
   * This must match a theme name in the module's templates directory.
   * The theme file must be named:
   *  THEME_FOLDER . 'html.twig'.
   *
   * This name also must match the theme template function
   * template_preprocess_THEME_ROOT_LIST in MODULE.module.
   *
   * @var string
   */
  const THEME_FOLDER = 'foldershare';

  /**
   * The view (list) page theme.
   *
   * This must match a theme name in the module's templates directory.
   * The theme file must be named:
   *  THEME_VIEW . 'html.twig'.
   *
   * This name also must match the theme template function
   * template_preprocess_THEME_ROOT_LIST in MODULE.module.
   *
   * @var string
   */
  const THEME_VIEW = 'foldershare_view';

  /*--------------------------------------------------------------------
   *
   * Role-based permissions.
   *
   * These constants define well-known module permissions names. Their
   * values must match those in 'MODULE.permissions.yml'.
   *
   *--------------------------------------------------------------------*/
  /**
   * The module's administrative permission.
   *
   * Users may create, delete, and modify all content by any user, change
   * ownership, and adjust share settings. They cannot change module settings.
   *
   * This must much the administer permission in 'MODULE.permissions.yml'.
   *
   * @var string
   */
  const ADMINISTER_PERMISSION = 'administer foldershare';

  /**
   * The module's share with other users permission.
   *
   * Users may share root items with other specific users, granting them
   * view and/or author access.
   *
   * This must much the author permission in 'MODULE.permissions.yml'.
   *
   * @var string
   */
  const SHARE_PERMISSION = 'share foldershare';

  /**
   * The module's share with public permission.
   *
   * Users may share root items with the anonymous user, making the content
   * public and accessible by site visitors without accounts. Anonymous users
   * may only be granted view access.
   *
   * This must much the author permission in 'MODULE.permissions.yml'.
   *
   * @var string
   */
  const SHARE_PUBLIC_PERMISSION = 'share public foldershare';

  /**
   * The module's author permission.
   *
   * Users may create, delete, and modify their own content, and
   * content owned by others if they have been granted author access.
   *
   * This must much the author permission in 'MODULE.permissions.yml'.
   *
   * @var string
   */
  const AUTHOR_PERMISSION = 'author foldershare';

  /**
   * The module's view permission.
   *
   * Users may view their own content, and content owned by others if
   * granted view access.
   *
   * This must much the view permission in 'MODULE.permissions.yml'.
   *
   * @var string
   */
  const VIEW_PERMISSION = 'view foldershare';

  /*--------------------------------------------------------------------
   *
   * UI feature flags.
   *
   *--------------------------------------------------------------------*/
  /**
   * Indicates whether to enable AJAX command dialogs in the UI.
   *
   * When TRUE, the user interface uses AJAX to enable on-page command forms
   * within dialog boxes. When FALSE, command forms are on separate pages,
   * instead of on-page dialogs.
   *
   * This flag is normally TRUE, but it can be set to FALSE to help
   * debug the user interface.
   *
   * <B>The UI is not guaranteed to fully work without dialogs. The dialog
   * version of the UI is the primary version and the most fully supported.</B>
   *
   * @var bool
   */
  const ENABLE_UI_COMMAND_DIALOGS = TRUE;

}
