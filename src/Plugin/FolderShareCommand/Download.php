<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Url;

use Drupal\foldershare\Constants;

/**
 * Defines a command plugin to download files or folders.
 *
 * The command downloads a single selected entity.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to download.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_download",
 *  label           = @Translation("Download"),
 *  menuNameDefault = @Translation("Download"),
 *  menuName        = @Translation("Download"),
 *  description     = @Translation("Download selected files and folders. Folders and lists of files are automatically downloaded as a newly-created ZIP archive."),
 *  category        = "import & export",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "any",
 *    },
 *    "access"  = "view",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "parent",
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "file",
 *      "image",
 *      "folder",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class Download extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*---------------------------------------------------------------------
   *
   * Execute behavior.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getExecuteBehavior() {
    // Prior to (and thus instead of) executing, redirect to
    // another page.
    return self::PRE_EXECUTE_PAGE_REDIRECT;
  }

  /**
   * {@inheritdoc}
   */
  public function getExecuteRedirectUrl() {
    $ids = $this->getSelectionIds();
    if (empty($ids) === TRUE) {
      $ids[] = $this->getParentId();
    }

    // Redirect to the download controller, passing it the list of
    // entity IDs to download.
    return Url::fromRoute(
      Constants::ROUTE_DOWNLOAD,
      [
        'encoded' => implode(',', $ids),
      ]);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Do nothing. This is never called because of the pre-execute redirect.
  }

}
