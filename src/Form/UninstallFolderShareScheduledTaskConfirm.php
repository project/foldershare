<?php

namespace Drupal\foldershare\Form;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Entity\FolderShareScheduledTask;
use Drupal\foldershare\Settings;

/**
 * Provides a form for removing all scheduled tasks before an uninstall.
 *
 * <B>Access control</B>
 * The route to this form should restrict access to those with administration
 * permission.
 *
 * @ingroup foldershare
 */
class UninstallFolderShareScheduledTaskConfirm extends ConfirmFormBase {

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The entity type manager, set at construction time.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new page.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /*--------------------------------------------------------------------
   *
   * Form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return mb_strtolower(str_replace('\\', '_', get_class($this)));
  }

  /**
   * Returns the title for the form.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function getTitle() {
    return $this->getQuestion();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete all FolderShare module background tasks?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // The description is filled in during form build.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete all tasks');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('system.modules_uninstall');
  }

  /*--------------------------------------------------------------------
   *
   * Form build.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState = NULL) {
    $form = parent::buildForm($form, $formState);

    // Get the number of tasks scheduled.
    $n = FolderShareScheduledTask::findNumberOfTasks();
    $nRepeating = FolderShareScheduledTask::findNumberOfRepeatingTasks();

    if ($n === 0) {
      // There are no tasks to delete.
      $form['description']['#markup'] = $this->t(
        '<p>There are no FolderShare module tasks to delete.</p>');

      // Hide the delete button if there is nothing to delete.
      $form['actions']['submit']['#access'] = FALSE;
    }
    elseif ($nRepeating === $n) {
      // All of the tasks are repeating tasks. This is the most common case.
      $form['description']['#markup'] = $this->formatPlural(
        $nRepeating,
        '<p>There is @n repeating background task to delete for the FolderShare module.</p><p>A repeating task is used to automatically update information, such as the search index and usage statistics. Deleting this task disables automatic updates. If needed, you can enable it agan using the module\'s configuration page.</p>',
        '<p>There are @n repeating background tasks to delete for the FolderShare module.</p><p>Repeating tasks are used to automatically update information, such as the search index and usage statistics. Deleting these tasks disables automatic updates. If needed, you can enable them agan using the module\'s configuration page.</p>',
        [
          '@n' => $nRepeating,
        ]);
      $form['actions']['submit']['#value'] = $this->formatPlural(
        $nRepeating,
        'Disable task',
        'Disable all tasks');
    }
    elseif ($nRepeating === 0) {
      // All of the tasks are for pending operations.
      $form['description']['#markup'] = $this->formatPlural(
        $n,
        '<p>There is @n background operation task scheduled for the FolderShare module.</p><p>This task is working on a large pending operation, like a delete, copy, and move. Deleting this task cannot be undone and will leave the operation incomplete. This may corrupt the FolderShare file system. Make a backup of your database if you want to be able to restore content.</p>',
        '<p>There are @n background operation tasks scheduled for the FolderShare module.</p><p>These tasks are working on large pending operations, like delete, copy, and move. Deleting these tasks cannot be undone and will leave these operations incomplete. This may corrupt the FolderShare file system. Make a backup of your database if you want to be able to restore content.</p>',
        [
          '@n' => $n,
        ]);
      $form['actions']['submit']['#value'] = $this->formatPlural(
        $n,
        'Delete task',
        'Delete all tasks');
    }
    else {
      // Some of the tasks are for pending operations and some are for
      // repeating tasks.
      $form['description']['#markup'] = $this->t(
        '<p>There are @n background tasks to delete for the FolderShare module.</p><p>Some of these tasks may automatically update information, such as the search index and usage statistics. Other tasks may be working on large pending operations, like delete, copy, and move. Deleting these tasks cannot be undone and will leave these operations incomplete. This may corrupt the FolderShare file system. Make a backup of your database if you want to be able to restore content.</p>',
        [
          '@n' => $n,
        ]);
      $form['actions']['submit']['#value'] = $this->t(
        'Delete all tasks');
    }

    return $form;
  }

  /*--------------------------------------------------------------------
   *
   * Form validate.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    // Nothing to do.
  }

  /*--------------------------------------------------------------------
   *
   * Form submit.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    // Stop all background tasks first.
    // - Stop the task scheduler.
    // - Stop the search indexer.
    // - Stop the usage table updater.
    $taskEnabled = FolderShareScheduledTask::isTaskExecutionEnabled();
    FolderShareScheduledTask::setTaskExecutionEnabled(FALSE);

    Settings::setSearchIndexInterval('system');

    Settings::setUsageUpdateInterval('manual');

    // Delete all tasks.
    FolderShareScheduledTask::deleteAllTasks();

    // It is possible that the user will NOT continue the uninstall after
    // running this form to delete tasks. In that case we need to leave
    // the module in a working state.
    // - Restore activity logging.
    // - Restore the task scheduler.
    FolderShareScheduledTask::setTaskExecutionEnabled($taskEnabled);

    $formState->setRedirect('system.modules_uninstall');
  }

}
