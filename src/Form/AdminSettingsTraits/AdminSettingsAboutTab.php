<?php

namespace Drupal\foldershare\Form\AdminSettingsTraits;

use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\Branding;
use Drupal\foldershare\Constants;
use Drupal\foldershare\ManageSearch;
use Drupal\foldershare\Utilities\LinkUtilities;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Manages the "About" tab for the module's settings form.
 *
 * <B>Warning:</B> This is an internal trait that is strictly used by
 * the AdminSettings form class. It is a mechanism to group functionality
 * to improve code management.
 *
 * @ingroup foldershare
 */
trait AdminSettingsAboutTab {

  /*---------------------------------------------------------------------
   *
   * Build.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds the about tab.
   *
   * @param array $form
   *   An associative array containing the structure of the form. The form
   *   is modified to include additional render elements for the tab.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   * @param string $tabGroup
   *   The name of the tab group.
   */
  private function buildAboutTab(
    array &$form,
    FormStateInterface $formState,
    string $tabGroup) {

    //
    // Find installed modules.
    // -----------------------
    // Tab sections vary their presentation based upon what modules are
    // installed at the site.
    $helpInstalled            = $this->moduleHandler->moduleExists('help');
    $coreSearchInstalled      = ManageSearch::isCoreSearchAvailable();
    $searchAPIInstalled       = ManageSearch::isSearchAPIAvailable();
    $viewsUiInstalled         = $this->moduleHandler->moduleExists('views_ui');
    $fieldUiInstalled         = $this->moduleHandler->moduleExists('field_ui');
    $restUiInstalled          = $this->moduleHandler->moduleExists('restui');
    $foldershareRestInstalled = $this->moduleHandler->moduleExists('foldershare_rest');

    //
    // Set up tab names.
    // -----------------
    // Create the machine name for the tab and the tab's title.
    $tabMachineName = 'about';
    $tabTitle = $this->t('About');
    $moduleTitle = $this->moduleHandler->getName(Constants::MODULE);
    $tabName = 'foldershare_' . $tabMachineName . '_tab';

    //
    // Create the tab
    // --------------
    // Start the tab with a title, subtitle, and description.
    $form[$tabName] = [
      '#type'           => 'details',
      '#open'           => FALSE,
      '#group'          => $tabGroup,
      '#title'          => $tabTitle,

      '#description'    => [
        'branding'      => Branding::getBannerBranding(),
        'description'   => [
          '#type'       => 'html_tag',
          '#tag'        => 'p',
          '#value'      => $this->t(
            "<strong>@moduletitle</strong> manages files and folders for each user and sharable among users.",
            [
              '@moduletitle' => $moduleTitle,
            ]),
          '#attributes' => [
            'class'     => [
              'foldershare-settings-description',
            ],
          ],
        ],
      ],
      '#attributes'     => [
        'class'         => [
          'foldershare-settings-tab ',
          'foldershare-about-tab',
        ],
      ],
    ];

    // Content.
    $form[$tabName]['user'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'p',
      '#value'          => $this->t('Files & folders:'),

      'pages'           => [
        '#type'         => 'container',
        '#prefix'       => '<ul>',
        '#suffix'       => '</ul>',
      ],
    ];

    $form[$tabName]['user']['pages']['see-personalfiles'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        "@personalFilesLink - the current user's home folder (named after the user).",
        [
          '@personalFilesLink' => LinkUtilities::createRouteLink(
            Constants::ROUTE_ROOT_ITEMS_PERSONAL),
        ]),
    ];

    $form[$tabName]['user']['pages']['see-sharedfiles'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        "@sharedFilesLink - content shared with the current user.",
        [
          '@sharedFilesLink' => LinkUtilities::createRouteLink(
            Constants::ROUTE_ROOT_ITEMS_SHARED),
        ]),
    ];

    $form[$tabName]['user']['pages']['see-publicfiles'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        "@publicFilesLink - public content available to all users.",
        [
          '@publicFilesLink' => LinkUtilities::createRouteLink(
            Constants::ROUTE_ROOT_ITEMS_PUBLIC),
        ]),
    ];

    $form[$tabName]['user']['pages']['see-allfiles'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        "@allFilesLink - aggregated home folders for all users (admin only).",
        [
          '@allFilesLink' => LinkUtilities::createRouteLink(
            Constants::ROUTE_ROOT_ITEMS_ALL),
        ]),
    ];

    // Help.
    if ($helpInstalled === TRUE) {
      $form[$tabName]['help'] = [
        '#type'           => 'html_tag',
        '#tag'            => 'p',
        '#value'          => $this->t('Help:'),

        'pages'           => [
          '#type'         => 'container',
          '#prefix'       => '<ul>',
          '#suffix'       => '</ul>',
        ],
      ];
      $form[$tabName]['help']['pages']['see-help'] = [
        '#type'         => 'html_tag',
        '#tag'          => 'li',
        '#value'        => $this->t(
          "@helpLink - module features, use, and configuration.",
          [
            '@helpLink' => LinkUtilities::createHelpLink(
              'foldershare',
              $this->t('FolderShare help')),
          ]),
      ];
      if ($foldershareRestInstalled === TRUE) {
        $form[$tabName]['help']['pages']['see-resthelp'] = [
          '#type'         => 'html_tag',
          '#tag'          => 'li',
          '#value'        => $this->t(
            "@helpLink - REST web services features, use, and configuration.",
            [
              '@helpLink' => LinkUtilities::createHelpLink(
                'foldershare_rest',
                $this->t('FolderShare REST help')),
            ]),
        ];
      }
    }

    // Configuration.
    $form[$tabName]['configure'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'p',
      '#value'          => $this->t('Configuration:'),

      'pages'           => [
        '#type'         => 'container',
        '#prefix'       => '<ul>',
        '#suffix'       => '</ul>',
      ],
    ];

    $form[$tabName]['configure']['pages']['see-permissions'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        "@permissionsLink - role-based access.",
        [
          '@permissionsLink' => LinkUtilities::createRouteLink(
            'user.admin_permissions',
            'module-foldershare',
            $this->t('Permissions')),
        ]),
    ];

    if ($foldershareRestInstalled === TRUE && $restUiInstalled === TRUE) {
      $form[$tabName]['configure']['pages']['see-rest'] = [
        '#type'           => 'html_tag',
        '#tag'            => 'li',
        '#value'          => $this->t(
          "@restUiLink - REST web services.",
          [
            '@restUiLink' => LinkUtilities::createRouteLink(
              'restui.list',
              '',
              $this->moduleHandler->getName('restui')),
          ]),
      ];
    }

    if ($coreSearchInstalled === TRUE) {
      $form[$tabName]['configure']['pages']['see-core-search'] = [
        '#type'           => 'html_tag',
        '#tag'            => 'li',
        '#value'          => $this->t(
          '@searchPluginLink - core @searchName module indexing and search forms.',
          [
            '@searchPluginLink' => ManageSearch::createCoreSearchPluginLink(),
            '@searchName' => ManageSearch::getCoreSearchModuleName(),
          ]),
      ];
    }
    if ($searchAPIInstalled === TRUE) {
      $form[$tabName]['configure']['pages']['see-search-api'] = [
        '#type'           => 'html_tag',
        '#tag'            => 'li',
        '#value'          => $this->t(
          '@searchPluginLink - @searchName module indexing.',
          [
            '@searchPluginLink' => ManageSearch::createSearchAPIConfigurationLink(),
            '@searchName' => ManageSearch::getSearchAPIModuleName(),
          ]),
      ];
    }

    // Customization.
    if ($fieldUiInstalled === TRUE || $viewsUiInstalled === TRUE) {
      $form[$tabName]['customize'] = [
        '#type'           => 'html_tag',
        '#tag'            => 'p',
        '#value'          => $this->t('Customization:'),

        'pages'           => [
          '#type'         => 'container',
          '#prefix'       => '<ul>',
          '#suffix'       => '</ul>',
        ],
      ];

      if ($fieldUiInstalled === TRUE) {
        $form[$tabName]['customize']['pages']['see-fields'] = [
          '#type'           => 'html_tag',
          '#tag'            => 'li',
          '#value'          => $this->t(
            '@fieldUiFieldLink - file and folder fields.',
            [
              '@fieldUiFieldLink'    => LinkUtilities::createRouteLink(
                'entity.' . FolderShare::ENTITY_TYPE_ID . '.field_ui_fields',
                '',
                $this->t('Fields')),
            ]),
        ];
        $form[$tabName]['customize']['pages']['see-forms'] = [
          '#type'           => 'html_tag',
          '#tag'            => 'li',
          '#value'          => $this->t(
            '@fieldUiFormLink - file and folder edit forms.',
            [
              '@fieldUiFormLink'     => LinkUtilities::createRouteLink(
                'entity.entity_form_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
                '',
                $this->t('Forms')),
            ]),
        ];
        $form[$tabName]['customize']['pages']['see-displays'] = [
          '#type'           => 'html_tag',
          '#tag'            => 'li',
          '#value'          => $this->t(
            '@fieldUiDisplayLink - file and folder pages.',
            [
              '@fieldUiDisplayLink'  => LinkUtilities::createRouteLink(
                'entity.entity_view_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
                '',
                $this->t('Displays')),
            ]),
        ];
      }

      if ($viewsUiInstalled === TRUE) {
        $form[$tabName]['customize']['pages']['see-views'] = [
          '#type'           => 'html_tag',
          '#tag'            => 'li',
          '#value'          => $this->t(
            '@viewLink - file and folder lists.',
            [
              '@viewLink'        => LinkUtilities::createRouteLink(
                'entity.view.edit_form',
                '',
                $this->t('Views'),
                [
                  'view'         => Constants::VIEW_LISTS,
                ]),
            ]),
        ];
      }
    }

    // Monitoring.
    $form[$tabName]['monitor'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'p',
      '#value'          => $this->t('Monitoring:'),

      'pages'           => [
        '#type'         => 'container',
        '#prefix'       => '<ul>',
        '#suffix'       => '</ul>',
      ],
    ];

    $form[$tabName]['monitor']['pages']['see-usage'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        '@usageReportLink - per-user usage.',
        [
          '@usageReportLink' => LinkUtilities::createRouteLink(
            Constants::ROUTE_USAGE,
            '',
            $this->t('Usage')),
        ]),
    ];

    $form[$tabName]['monitor']['pages']['see-allfiles'] = [
      '#type'           => 'html_tag',
      '#tag'            => 'li',
      '#value'          => $this->t(
        "@allFilesLink - all files and folders.",
        [
          '@allFilesLink' => LinkUtilities::createRouteLink(
            Constants::ROUTE_ROOT_ITEMS_ALL),
        ]),
    ];
  }

  /*---------------------------------------------------------------------
   *
   * Validate.
   *
   *---------------------------------------------------------------------*/
  /**
   * Validates form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  private function validateAboutTab(
    array &$form,
    FormStateInterface $formState) {

    // Nothing to do.
  }

  /*---------------------------------------------------------------------
   *
   * Submit.
   *
   *---------------------------------------------------------------------*/
  /**
   * Stores submitted form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  private function submitAboutTab(
    array &$form,
    FormStateInterface $formState) {

    // Nothing to do.
  }

}
