<?php

namespace Drupal\foldershare\EventSubscriber;

use Symfony\Component\HttpKernel\Event\TerminateEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\KernelEvents;

use Drupal\foldershare\Entity\FolderShareScheduledTask;

/**
 * Handles executing scheduled tasks after user requests are serviced.
 *
 * HTTP requests to the server are processed by Drupal and a response
 * posted back to the user. Upon completion, the user gets their next
 * page and a "terminate" event is sent. This class responds to such
 * events and uses them to service the scheduled task queue. Because
 * these events occur very often (once per page), the task queue is
 * serviced very frequently, which allows for fairly fine-grained task
 * scheduling.
 *
 * Task servicing is disabled automatically when the site is in maintenance
 * mode. This prevents activity that could interfer with maintenance
 * activity, such as installing modules or performing module updates.
 *
 * To register this event subscriber, an entry in "foldershare.services.yml"
 * is required:
 *
 * @code
 * services:
 *   foldershare.scheduledtask.subscriber:
 *     class: Drupal\foldershare\EventSubscriber\FolderShareScheduledTaskHandler
 *     arguments: []
 *     tags:
 *       - { name: event_subscriber }
 * @endcode
 *
 * @ingroup foldershare
 *
 * @see foldershare.services.yml
 * @see \Drupal\foldershare\Entity\FolderShareScheduledTask
 */
class FolderShareScheduledTaskHandler implements EventSubscriberInterface {

  /**
   * Constructs the handler.
   */
  public function __construct() {
  }

  /*---------------------------------------------------------------------
   *
   * Event handling.
   *
   *---------------------------------------------------------------------*/
  /**
   * Responds to a terminate event after a posted response.
   *
   * The scheduled task list of the FolderShare module is checked and
   * ready tasks run.
   *
   * @param \Symfony\Component\HttpKernel\Event\TerminateEvent $event
   *   The termination event.
   *
   * @see ::getSubscribedEvents()
   * @see \Drupal\foldershare\Entity\FolderShareScheduledTask::executeTasks()
   *
   * @todo When Drupal 8 and pre-Symfony 4.4 compatibility is no longer
   * required, change the argument type to TerminateEvent. And change the
   * use statement above to Symfony\Component\HttpKernel\Event\TerminateEvent.
   */
  public function onTerminate(TerminateEvent $event) {
    FolderShareScheduledTask::executeTasks(
      (int) $event->getRequest()->server->get('REQUEST_TIME'));
  }

  /**
   * Subscribes to the terminate event.
   *
   * @return array
   *   Returns an array of event listener definitions, including one that
   *   invokes this class's terminate event handler.
   *
   * @see ::onTerminate()
   */
  public static function getSubscribedEvents() : array {
    return [
      KernelEvents::TERMINATE => [
        [
          'onTerminate',
          100,
        ],
      ],
    ];
  }

}
