<?php

namespace Drupal\foldershare\Entity\FolderShareScheduledTaskTraits;

use Drupal\Core\Database\Database;

/**
 * Find FolderShareScheduledTask entities.
 *
 * This trait includes find methods that query across all
 * FolderShareScheduledTask entities and return IDs or entities
 * that match criteria.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShareScheduledTask entity class. It is a
 * mechanism to group functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait FindTrait {

  /*---------------------------------------------------------------------
   *
   * Find number of tasks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the number of scheduled tasks ready to be executed.
   *
   * Ready tasks are those with a task timestamp that is equal to
   * the current time or in the past.
   *
   * @param int $timestamp
   *   The timestamp used to select which tasks are ready.
   *
   * @see ::findNumberOfTasks()
   * @see ::findReadyTaskIds()
   * @see ::executeTasks()
   */
  public static function findNumberOfReadyTasks(int $timestamp) {
    $connection = Database::getConnection();
    $select = $connection->select(self::BASE_TABLE, "st");
    $select->condition('scheduled', $timestamp, '<=');

    return (int) $select->countQuery()->execute()->fetchField();
  }

  /**
   * Returns the number of scheduled tasks.
   *
   * If a callback name is provided, the returned number only counts
   * tasks with that name. If no name is given, all tasks are counted.
   *
   * @param string $callback
   *   (optional, default = '' = any) When set, returns the number of
   *   scheduled tasks with the given callback in the form "class::method".
   *   Otherwise returns the number of all scheduled tasks.
   *
   * @return int
   *   Returns the number of tasks.
   *
   * @see ::findNumberOfReadyTasks()
   * @see ::findReadyTaskIds()
   */
  public static function findNumberOfTasks(string $callback = '') {
    $connection = Database::getConnection();
    $select = $connection->select(self::BASE_TABLE, "st");
    if (empty($callback) === FALSE) {
      $select->condition('operation', $callback, '=');
    }

    return (int) $select->countQuery()->execute()->fetchField();
  }

  /**
   * Returns the number of scheduled repeating tasks.
   *
   * Repeating tasks are for operations like search indexing or usage table
   * updates that must occurr over and over.
   *
   * @return int
   *   Returns the number of repeating tasks.
   *
   * @see ::findNumberOfReadyTasks()
   * @see ::findNumberOfTasks()
   */
  public static function findNumberOfRepeatingTasks() {
    $connection = Database::getConnection();
    $select = $connection->select(self::BASE_TABLE, "st");
    $select->condition('flags', self::REPEATING_FLAG, '=');

    return (int) $select->countQuery()->execute()->fetchField();
  }

  /*---------------------------------------------------------------------
   *
   * Find task IDs.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns an ordered array of scheduled and ready task IDs.
   *
   * Ready tasks are those with a task timestamp that is equal to
   * the current time or in the past. The returned array is ordered
   * from oldest to newest.
   *
   * @param int $timestamp
   *   The timestamp used to select which tasks are ready.
   *
   * @return int[]
   *   Returns an array of task IDs for ready tasks, ordered from
   *   oldest to newest.
   *
   * @see ::findNumberOfReadyTasks()
   * @see ::findNumberOfTasks()
   * @see ::executeTasks()
   */
  public static function findReadyTaskIds(int $timestamp) {
    $connection = Database::getConnection();
    $select = $connection->select(self::BASE_TABLE, "st");
    $select->addField('st', 'id', 'id');
    $select->condition('scheduled', $timestamp, '<=');
    $select->orderBy('scheduled');

    return $select->execute()->fetchCol(0);
  }

  /**
   * Returns an ordered array of all scheduled task IDs.
   *
   * If a callback name is provided, the returned list only includes
   * tasks with that callback. If no callback is given, all tasks are included.
   *
   * The returned array is ordered from oldest to newest.
   *
   * @param string $callback
   *   (optional, default = '' = any) When set, returns the scheduled tasks
   *   with the given callback in the form "class::method". Otherwise returns
   *   all scheduled tasks.
   *
   * @return int[]
   *   Returns an array of task IDs, ordered from oldest to newest.
   *
   * @see ::findReadyTaskIds()
   * @see ::findNumberOfTasks()
   */
  public static function findTaskIds(string $callback = '') {
    $connection = Database::getConnection();
    $select = $connection->select(self::BASE_TABLE, "st");
    $select->addField('st', 'id', 'id');
    if (empty($callback) === FALSE) {
      $select->condition('operation', $callback, '=');
    }

    $select->orderBy('scheduled');

    return $select->execute()->fetchCol(0);
  }

}
