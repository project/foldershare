<?php

namespace Drupal\foldershare\Entity\FolderShareScheduledTaskTraits;

use Drupal\Core\Database\Database;

use Drupal\foldershare\Entity\FolderShareScheduledTask;

/**
 * Get/set FolderShareScheduledTask entity fields.
 *
 * This trait includes get and set methods that access task field values.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShareScheduledTask entity class. It is a
 * mechanism to group functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait DeleteTrait {

  /*---------------------------------------------------------------------
   *
   * Delete.
   *
   *---------------------------------------------------------------------*/
  /**
   * Deletes all tasks.
   *
   * Any task already executing will continue to execute until it finishes.
   * That execution may add new tasks, which will not be deleted.
   *
   * <B>Warning:</B> Deleting all tasks ends any pending operations, such as
   * those to delete, copy, or move content. This can leave these operations
   * in an indetermine state, with parts of the operation incomplete, locks
   * still locked, and entities marked hidden or disabled. This should only
   * be done as a last resort.
   *
   * @see ::deleteTask()
   * @see ::deleteTasks()
   * @see ::findNumberOfTasks()
   */
  public static function deleteAllTasks() {
    // Truncate the task table to delete everything.
    //
    // Since this entity type does not support a persistent cache, a static
    // cache, or a render cache, we do not have to worry about caches getting
    // out of sync with the database.
    $connection = Database::getConnection();
    $truncate = $connection->truncate(self::BASE_TABLE);
    $truncate->execute();
  }

  /**
   * Deletes a task.
   *
   * The task is deleted. If it is already executing, it will continue to
   * execute until it finishes. That execution may add new tasks, which
   * will not be deleted.
   *
   * <B>Warning:</B> Deleting a task ends any pending operation, such as one
   * to delete, copy, or move content. This can leave an operation in an
   * indetermine state, with parts of the operation incomplete, locks
   * still locked, and entities marked hidden or disabled. This should only
   * be done as a last resort.
   *
   * @param \Drupal\foldershare\Entity\FolderShareScheduledTask $task
   *   The task to delete.
   *
   * @see ::deleteAllTasks()
   * @see ::deleteTasks()
   * @see ::findNumberOfTasks()
   */
  public static function deleteTask(FolderShareScheduledTask $task) {
    if ($task === NULL) {
      return;
    }

    // It is possible that the task object has been loaded by more than one
    // process, then deleted by more than one process. The first delete
    // actually removes it from the entity table. The second delete does
    // nothing.
    try {
      $task->delete();
    }
    catch (\Exception $e) {
      // Do nothing.
    }
  }

  /**
   * Deletes tasks for a specific callback.
   *
   * All tasks with the indicated callback are deleted. If a task is
   * already executing, it will continue to execute until it finishes. That
   * execution may add new tasks, which will not be deleted.
   *
   * <B>Warning:</B> Deleting a task ends any pending operation, such as one
   * to delete, copy, or move content. This can leave an operation in an
   * indetermine state, with parts of the operation incomplete, locks
   * still locked, and entities marked hidden or disabled. This should only
   * be done as a last resort.
   *
   * @param mixed $callback
   *   (optional, default = '') The callback for the task following standard
   *   PHP callable formats (e.g. "class::method" or [object, "method"], etc.).
   *   If the callback is empty, all tasks are deleted.
   *
   * @see ::deleteAllTasks()
   * @see ::deleteTask()
   * @see ::findNumberOfTasks()
   */
  public static function deleteTasks($callback = '') {
    if (empty($callback) === TRUE) {
      self::deleteAllTasks();
    }

    // Throws an exception if the callback does not validate.
    $callback = self::verifyCallback($callback);

    // Delete all entries with the indicated callback.
    $connection = Database::getConnection();
    $query = $connection->delete(self::BASE_TABLE);
    $query->condition('operation', $callback, '=');
    $query->execute();
  }

}
