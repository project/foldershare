<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Defines a command plugin to upload files for a folder.
 *
 * The command uploads files and adds them to the parent folder.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_upload_files",
 *  label           = @Translation("Upload"),
 *  menuNameDefault = @Translation("Upload..."),
 *  menuName        = @Translation("Upload..."),
 *  description     = @Translation("Upload files to this location."),
 *  category        = "import & export",
 *  weight          = 1,
 *  specialHandling = {
 *    "upload",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "none",
 *    },
 *  },
 * )
 */
class UploadFiles extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    // A configuration form is only needed if there are no uploaded
    // files already available.  Check.
    $configuration = $this->getConfiguration();
    if (empty($configuration) === TRUE) {
      // No command configuration? Need a form.
      return TRUE;
    }

    if (empty($configuration['uploadClass']) === TRUE) {
      // No command upload class specified? Need a form.
      return TRUE;
    }

    $uploadClass = $configuration['uploadClass'];
    $pendingFiles = $this->getRequest()->files->get('files', []);
    if (isset($pendingFiles[$uploadClass]) === FALSE) {
      // No pending files? Need a form.
      return TRUE;
    }

    foreach ($pendingFiles[$uploadClass] as $fileInfo) {
      if ($fileInfo !== NULL) {
        // At least one good pending file. No form.
        return FALSE;
      }
    }

    // No good pending files. Need a form.
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    // Build a form to prompt for files to upload.
    $uploadClass = 'foldershare-command-upload';

    $configuration = $this->getConfiguration();
    $configuration['uploadClass'] = $uploadClass;
    $this->setConfiguration($configuration);

    // Get the parent, if any.
    $parent = $this->getParent();

    // Call module hooks to get a list of allowed filename extensions.
    $extensions = ManageHooks::callHookAllowedFilenameExtensionsAlter(
      $parent,
      $this->getCurrentUser()->id(),
      []);

    if (empty($extensions) === FALSE) {
      // The extensions list does not have leading dots. But the form
      // needs them.
      $list = [];
      foreach ($extensions as $ext) {
        $list[] = '.' . $ext;
      }

      $extensionsString = implode(',', $list);
    }
    else {
      $extensionsString = '';
    }

    // Provide a file field. Browsers automatically add a button to
    // invoke a platform-specific file dialog to select files.
    $form[$uploadClass] = [
      '#type'        => 'file',
      '#multiple'    => TRUE,
      '#description' => $this->t(
        "Select one or more @kinds to upload.",
        [
          '@kinds'  => FolderShare::translateKinds(FolderShare::FILE_KIND),
        ]),
      '#attributes'  => [
        'accept'     => $extensionsString,
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    $this->execute();
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {

    // Get the parent, if any.
    $parent = $this->getParent();

    // Attach the uploaded files to the parent. PHP has already uploaded
    // the files. It remains to convert the files to File objects, then
    // wrap them with FolderShare entities and add them to the parent folder.
    $configuration = $this->getConfiguration();
    $uploadClass = $configuration['uploadClass'];

    try {
      if ($parent === NULL) {
        $results = FolderShare::addFilesToRootFromFormUpload(
          $uploadClass,
          FALSE,
          (-1));
      }
      else {
        $results = $parent->addFilesFromFormUpload(
          $uploadClass,
          FALSE,
          (-1));
      }
    }
    catch (\Exception $e) {
      $this->getMessenger()->addError($e->getMessage());
      return;
    }

    // The returned array has one entry for each uploaded file. Each entry
    // is an associative array containing:
    // - 'filename': The client original filename for the file.
    // - 'error': The error message, if any.
    // - 'foldershare': The created FolderShare entity, if any.
    $nUploaded = 0;
    $firstItem = NULL;
    foreach ($results as $entry) {
      if (isset($entry['foldershare']) === FALSE) {
        // This file had an upload error.
        if (isset($entry['error']) === TRUE) {
          $this->getMessenger()->addError($entry['error']);
        }
        else {
          $filename = $entry['filename'];
          $this->getMessenger()->addError(
            "An unknown error occurred for $filename.");
        }
      }
      else {
        if ($nUploaded === 0) {
          $firstItem = $entry;
        }

        $nUploaded++;
      }
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      // Report on results, if any.
      if ($nUploaded === 1) {
        // Single file. There may have been other files that had errors though.
        $name = $firstItem->getFilename();
        $this->getMessenger()->addStatus(
          $this->t(
            "The @kind '@name' has been uploaded.",
            [
              '@kind' => FolderShare::translateKind(FolderShare::FILE_KIND),
              '@name' => $name,
            ]));
      }
      elseif ($nUploaded > 1) {
        // Multiple files. There may have been other files that had errors tho.
        $this->getMessenger()->addStatus(
          $this->t(
            "@number @kinds have been uploaded.",
            [
              '@number' => $nUploaded,
              '@kinds'  => FolderShare::translateKinds(FolderShare::FILE_KIND),
            ]));
      }
    }
  }

}
