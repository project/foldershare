<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\Core\Database\Database;

use Drupal\foldershare\Entity\FolderShareScheduledTask;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\Utilities\CacheUtilities;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;

/**
 * File system check all FolderShare entities.
 *
 * This trait includes methods to check the file system and repair it.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait OperationFsckTrait {

  /*---------------------------------------------------------------------
   *
   * File system check.
   *
   *---------------------------------------------------------------------*/
  /**
   * Runs a file system check to find and fix problems.
   *
   * <B>It is strongly recommended that the website be in maintenance
   * mode when this function is called. This prevents file system activity
   * that can collide with this function and create unexpected and
   * possibly destructive results.</B>
   *
   * This method scans the file system looking for problems. These primarily
   * include parent and root linkage problems, such as parent or root IDs
   * that do not refer to existing items, or where the parent and root IDs
   * are not consistent with each other. This method also checks for hidden
   * and disabled items that do not appear to have pending tasks that justify
   * them.
   *
   * This method clears all memory caches before it starts. If $fix is TRUE,
   * it also clears the FolderShare entity caches before any checks, and
   * clears ALL Drupal caches upon completion.
   *
   * <B>Activity log</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * <B>Resource use</B>
   * Depending upon the size of the folder tree, this method may take a
   * long time to check everything and it may consume a lot of memory.
   * For this reason, this method sets PHP execution time and memory use
   * limits to be unlimited.
   *
   * <B>Interrupts</B>
   * During file system fixes, an interrupt has the potential to corrupt
   * the file system when a fix is not allowed to complete. For this
   * reason, this method blocks user aborts.
   *
   * @param bool $fix
   *   (optional, default = FALSE) When TRUE, fix file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   *
   * @see ::clearAllSystemHidden()
   * @see ::clearAllSystemDisabled()
   * @see \Drupal\foldershare\Utilities\LimitUtilities::setUnlimited()
   *
   * @todo Recurse through every folder tree and check that all children
   * share the same root ID.
   */
  public static function fsck(bool $fix = FALSE) {
    LimitUtilities::setUnlimited();

    // Flush everything from static (memory) caches to remove the vestiges
    // of whatever operations occurred before this task began.
    CacheUtilities::flushAllMemoryCaches();

    if ($fix === TRUE) {
      // Flush all FolderShare entities from caches everywhere. Below we
      // modify the database directly. Once changed, all cached copies are
      // out of date.
      CacheUtilities::flushAllEntityCaches(FolderShareInterface::ENTITY_TYPE_ID);
    }

    $fixed = FALSE;

    // START CRITICAL REGION.
    $originalUserAbort = @ignore_user_abort(TRUE);

    //
    // Fix parent/root linkage.
    // ------------------------
    // The parent ID and root ID have the following possibilities:
    //
    // |#|parentid|rootid  |Validity|Fix by...                               |
    // |-|--------|--------|--------|----------------------------------------|
    // |1|NULL    |NULL    |good    |                                        |
    // |2|NULL    |valid   |bad     |Set parentid = rootid                   |
    // |3|NULL    |invalid |bad     |Set parentid = rootid = NULL            |
    // |4|valid   |NULL    |bad     |Set parentid = rootid = NULL            |
    // |5|valid   |valid   |good    |                                        |
    // |6|valid   |invalid |bad     |Set parentid = rootid = NULL            |
    // |7|invalid |NULL    |bad     |Set parentid = rootid = NULL            |
    // |8|invalid |valid   |bad     |Set parentid = rootid                   |
    // |9|invalid |invalid |bad     |Set parentid = rootid = NULL            |
    //
    // In choosing fixes, we have to watch for circular fixes that leave
    // content still invalid. For instance, when the root ID is NULL but
    // the parent ID is valid, we could set the root ID to the parent's
    // root ID. Except we don't know if that root ID is valid. If not,
    // we're just propagating a bad root ID into another item. And if we
    // then update descendants with that bad root ID, we're making a mess.
    //
    // So, we choose to prioritize the root ID over the parent ID. If the
    // root ID is NULL, the parent ID is forced to NULL without checking
    // if it is or is not valid.
    //
    // Cases (1) and (5) do not require any fixes.
    //
    // Cases (4) & (7). NULL rootid and non-NULL parentid.
    // - Set parentid to NULL.
    // - Cases (4) & (7) become case (1).
    $status = self::fsckFixNullRootNonNullParent($fix);
    $fixed = $fixed || $status;

    // Cases (3), (6) & (9). Non-NULL invalid rootid.
    // - Set parentid and rootid to NULL and update descendants.
    // - Cases (3), (6) & (9) become case (1).
    $status = self::fsckFixInvalidRoot($fix);
    $fixed = $fixed || $status;

    // Case (2). Non-NULL valid rootid and NULL parentid.
    // - Set parentid to rootid.
    // - Case (2) becomes case (5).
    $status = self::fsckFixValidRootNullParent($fix);
    $fixed = $fixed || $status;

    // Case (8). Non-NULL valid rootid and non-NULL invalid parentid.
    // - Set parentid to rootid.
    // - Case (8) becomes case (5).
    $status = self::fsckFixValidRootInvalidParent($fix);
    $fixed = $fixed || $status;

    //
    // Fix root consistency.
    // ---------------------
    // The root ID for every descendant of a root item must be that root
    // item's ID.
    $status = self::fsckFixConsistentRoot($fix);
    $fixed = $fixed || $status;

    //
    // Fix hidden & disabled.
    // ----------------------
    // Items should not be marked hidden or disabled if there are no
    // associated tasks pending.
    $status = self::fsckFixHidden($fix);
    $fixed = $fixed || $status;

    $status = self::fsckFixDisabled($fix);
    $fixed = $fixed || $status;

    //
    // Fix files and media.
    // --------------------
    // File, image, and media items should not have NULL or invalid file
    // or media IDs.
    $status = self::fsckFixFileOrMediaIds($fix);
    $fixed = $fixed || $status;

    $status = self::fsckFixFileOrMediaNames($fix);
    $fixed = $fixed || $status;

    $status = self::fsckFixFileEmptyUris($fix);
    $fixed = $fixed || $status;

    $status = self::fsckFixFileInvalidUris($fix);
    $fixed = $fixed || $status;

    // END CRITICAL REGION.
    @ignore_user_abort($originalUserAbort);

    //
    // Finish.
    // -------
    // Flush ALL caches everywhere.
    //
    // The activities above may have changed any number of FolderShare
    // entities. Because they do direct database operations, it is not
    // easy to track exactly which items have changed. If we did, it could
    // be a huge array of entity IDs that would be awkward to create and
    // use within PHP's memory limits.
    //
    // Without tracking every entity changed, it is not possibly to
    // selectively invalidate cache entries relating just to those entities.
    // Further, Drupal does not provide any way to flush all caches of all
    // entries for entities of this entity type. For instance, there is no
    // way to flush out of the render cache everything for all FolderShare
    // entities. The only way to flush out of any cache is to provide an
    // explicit list of entity IDs to flush.
    //
    // The only safe way to be sure we have flushed all out of date cache
    // entries from every cache is to flush everything.
    if ($fix === TRUE && $fixed === TRUE) {
      drupal_flush_all_caches();
    }

    if ($fixed === FALSE) {
      ManageLog::activity(
        'Folder tree: Integrity check complete. No problems found.');
    }

    return $fixed;
  }

  /**
   * Fixes items with a NULL root ID and non-NULL parent ID.
   *
   * <B>Problem</B>
   * If a root ID is NULL, the item has no parent and is supposed to be
   * a root item. In that case its parent ID should be NULL too. If this
   * is not the case, the item has become corrupted.
   *
   * <B>Fix</B>
   * Leave the root ID as NULL and set the parent ID to NULL too. There is
   * no need to update descendants.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixNullRootNonNullParent(bool $fix = FALSE) {
    $connection = Database::getConnection();

    //
    // Count problems.
    // ---------------
    // Issue a query to find items where there is a NULL root ID, and
    // a non-NULL parent ID.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->isNull('rootid');
    $select->isNotNull('parentid');
    $ids = $select->execute()->fetchCol(0);
    $count = count($ids);

    if ($count === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      ManageLog::critical(
        "Folder tree: Integrity check found @count invalid items with NULL root but non-NULL parent.\nThese items are incorrectly connected into the folder tree. This will cause them to be listed within a folder when they should be listed in a root list. Repair is needed.\nEntity IDs: @ids",
        [
          '@count' => $count,
          '@ids'   => implode(', ', $ids),
        ]);
      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // Issue an update to find all items where the root ID is NULL, and
    // the parent ID is not NULL. Set the parent ID to NULL.
    $update = $connection->update(FolderShareInterface::BASE_TABLE);
    $update->isNull('rootid');
    $update->isNotNull('parentid');
    $update->fields([
      'parentid' => NULL,
    ]);
    $count = $update->execute();

    if ($count === 0) {
      return FALSE;
    }

    ManageLog::notice(
      "Folder tree: Corrected @count items with NULL root but non-NULL parent.\nEach invalid item's parent has been set to NULL to be consistent with its NULL root. This corrects their folder tree connections and insures that they are listed properly in a root list.\nEntity IDs: @ids",
      [
        '@count' => $count,
        '@ids'   => implode(', ', $ids),
      ]);

    return TRUE;
  }

  /**
   * Fixes items with an invalid root ID.
   *
   * <B>Problem</B>
   * If the root ID refers to a non-existent entity, it is invalid and
   * the item has become corrupted.
   *
   * <B>Fix</B>
   * Set the root ID and parent ID to NULL. Update descendants.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixInvalidRoot(bool $fix = FALSE) {
    $connection = Database::getConnection();

    //
    // Count problems.
    // ---------------
    // Issue a query to find cases with a non-NULL rootid that does not
    // match any other entity.
    $subquery = $connection->select(FolderShareInterface::BASE_TABLE, 'subfs');
    $subquery->addField('subfs', 'id', 'subfsid');
    $subquery->where('fs.rootid = subfs.id');

    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->isNotNull('rootid');
    $select->notExists($subquery);

    $ids = $select->execute()->fetchCol(0);
    $count = count($ids);

    if ($count === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      ManageLog::critical(
        "Folder tree: Integrity check found @count invalid items with non-existent roots.\nThese items are incorrectly connected into the folder tree. They are listed as children of a parent folder, but they indicate roots that do not exist. This will cause attempts to view or change these items to fail with error messages and page crashes because the root is bad and access grants are not available. Repair is needed.\nEntity IDs: @ids",
        [
          '@count' => $count,
          '@ids'   => implode(', ', $ids),
        ]);
      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // Issue an update to set all of the found items to a NULL parent ID
    // and root ID, which makes them roots.
    //
    // Then use the first query's ID list to loop to update all
    // descendants to set their root ID to the item.
    $subquery = $connection->select(FolderShareInterface::BASE_TABLE, 'subfs');
    $subquery->addField('subfs', 'id', 'subfsid');
    $subquery->where('{' . FolderShareInterface::BASE_TABLE . '}.rootid = subfs.id');

    $update = $connection->update(FolderShareInterface::BASE_TABLE);
    $update->isNotNull('rootid');
    $update->notExists($subquery);
    $update->fields([
      'rootid'   => NULL,
      'parentid' => NULL,
    ]);
    $update->execute();

    foreach ($ids as $id) {
      // Set the root of all descendants to be the item itself.
      self::setDescendantsRootItemId($id, $id);
    }

    ManageLog::notice(
      "Folder tree: Corrected @count items with non-existent root.\nEach item's parent and root have been set to NULL. This reconnects them into the folder tree so that they are listed in a root list. All of their descendants also have been updated to use the corrected item as their root.\nEntity IDs (root-level only): @ids",
      [
        '@count' => $count,
        '@ids'   => implode(', ', $ids),
      ]);

    return TRUE;
  }

  /**
   * Fixes items with valid root ID and NULL parent ID.
   *
   * <B>Problem</B>
   * If a root ID is not NULL and valid, the item is a root and it is
   * supposed to have a NULL parent. If the parent is not NULL, the item
   * has become corrupted.
   *
   * <B>Fix</B>
   * Set the parent ID to the root ID. There is no need to update descendants.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixValidRootNullParent(bool $fix = FALSE) {
    $connection = Database::getConnection();

    //
    // Count problems.
    // ---------------
    // Issue a query to find items with a NULL parent ID, and a
    // non-NULL root ID.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->isNotNull('rootid');
    $select->isNull('parentid');
    $ids = $select->execute()->fetchCol(0);
    $count = count($ids);

    if ($count === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      ManageLog::critical(
        "Folder tree: Integrity check found @count invalid items with valid root but NULL parent.\nThese items are incorrectly connected into the folder tree. This will cause them to be listed in a root list, but use access grants as if they are children of a folder in another root list. Repair is needed.\nEntity IDs: @ids",
        [
          '@count' => $count,
          '@ids'   => implode(', ', $ids),
        ]);
      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // Issue an update to find all items where the root ID is not NULL,
    // but the parent ID is NULL. Set the parent ID to the root ID.
    $update = $connection->update(FolderShareInterface::BASE_TABLE);
    $update->isNotNull('rootid');
    $update->isNull('parentid');
    $update->expression('parentid', 'rootid');
    $count = $update->execute();

    if ($count === 0) {
      return FALSE;
    }

    ManageLog::notice(
      "Folder tree: Corrected @count items with valid root but NULL parent.\nEach item's parent has been set to it's root. This will retain their access grants and cause them to be correctly listed as children of the parent instead of incorrectly showing them in a root list.\nEntity IDs: @ids",
      [
        '@count' => $count,
        '@ids'   => implode(', ', $ids),
      ]);

    return TRUE;
  }

  /**
   * Fixes items with valid root IDs and invalid parent IDs.
   *
   * <B>Problem</B>
   * If the root ID is valid, but the parent ID refers to a non-existent
   * entity, it is invalid and the item has become corrupted.
   *
   * <B>Fix</B>
   * Set the parent ID to the root ID. There is no need to update descendants.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixValidRootInvalidParent(bool $fix = FALSE) {
    $connection = Database::getConnection();

    //
    // Count problems.
    // ---------------
    // Issue a query to find items with an invalid parent ID.
    $subquery = $connection->select(FolderShareInterface::BASE_TABLE, 'subfs');
    $subquery->addField('subfs', 'id', 'subfsid');
    $subquery->where('fs.parentid = subfs.id');

    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->isNotNull('parentid');
    $select->isNotNull('rootid');
    $select->notExists($subquery);

    $ids = $select->execute()->fetchCol(0);
    $count = count($ids);

    if ($count === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      ManageLog::critical(
        "Folder tree: Integrity check found @count invalid items with non-existent parents.\nThese items are incorrectly connected into the folder tree. They are inaccessible because they are marked as children of a non-existent parent folder. Repair is needed.\nEntity IDs: @ids",
        [
          '@count' => $count,
          '@ids'   => implode(', ', $ids),
        ]);
      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // Issue an update to find all invalid parent IDs and set them to
    // the root ID.
    $subquery = $connection->select(FolderShareInterface::BASE_TABLE, 'subfs');
    $subquery->addField('subfs', 'id', 'subfsid');
    $subquery->where('{' . FolderShareInterface::BASE_TABLE . '}.parentid = subfs.id');

    $update = $connection->update(FolderShareInterface::BASE_TABLE);
    $update->isNotNull('parentid');
    $update->isNotNull('rootid');
    $update->notExists($subquery);
    $update->expression('parentid', 'rootid');
    $count = $update->execute();

    if ($count === 0) {
      return FALSE;
    }

    ManageLog::notice(
      "Folder tree: Corrected @count items with invalid parents.\nEach item's parent has been set to it's root. This reconnects them into the folder tree so that they are listed in owner root lists.\nEntity IDs: @ids",
      [
        '@count' => $count,
        '@ids'   => implode(', ', $ids),
      ]);

    return TRUE;
  }

  /**
   * Fixes items with incorrect root IDs.
   *
   * <B>Problem</B>
   * If an item is a descendant of a root item, but its root ID is not set
   * to that root item's ID, then it is incorrect and the item has become
   * corrupted.
   *
   * <B>Fix</B>
   * Set the root ID to the highest ancestor's ID.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixConsistentRoot(bool $fix = FALSE) {
    $connection = Database::getConnection();

    //
    // Get all root IDs.
    // -----------------
    // Get a list of all root IDs owned by anybody. We'll be looping
    // over these. Get IDs for any user and any name, and include all
    // disabled AND hidden items.
    $rootIds = self::findAllRootItemIds(
      FolderShareInterface::ANY_USER_ID,
      '',
      TRUE,
      TRUE);

    //
    // Count problems.
    // ---------------
    // Iteratively recurse down through each root folder tree and count
    // the number of descendants that have the wrong root ID.
    $count = 0;

    if ($fix === FALSE) {
      foreach ($rootIds as $rootId) {
        $rootCount = 0;
        $pending = [$rootId];

        while (empty($pending) === FALSE) {
          // Get the next pending ID.
          $id = (int) array_shift($pending);

          // Count all children that have the wrong root ID.
          $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
          $select->condition('parentid', $id, '=');
          $select->condition('rootid', $rootId, '<>');
          $rootCount += $select->countQuery()->execute()->fetchField();

          // Get folder children and add them to the pending list.
          $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
          $select->addField('fs', 'id', 'id');
          $select->condition('parentid', $id, '=');
          $select->condition('kind', FolderShareInterface::FOLDER_KIND, '=');

          $pending = array_merge($pending, $select->execute()->fetchCol(0));
        }

        $count += $rootCount;
      }

      if ($count === 0) {
        return FALSE;
      }

      ManageLog::critical(
        "Folder tree: Integrity check found @count invalid descendant items with incorrect root.\nThese items are incorrectly connected into the folder tree. They are children of a parent folder, but their root is not the same as the parent's. This will cause incorrect access grants. Repair is needed.",
        [
          '@count' => $count,
        ]);
      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // Issue an update to find all invalid parent IDs and set them to
    // the root ID.
    foreach ($rootIds as $rootId) {
      $pending = [$rootId];

      while (empty($pending) === FALSE) {
        // Get the next pending ID.
        $id = (int) array_shift($pending);

        // Update all children that have the wrong root ID.
        $update = $connection->update(FolderShareInterface::BASE_TABLE);
        $update->condition('parentid', $id, '=');
        $update->condition('rootid', $rootId, '<>');
        $update->fields([
          'rootid' => $rootId,
        ]);
        $count += $update->execute();

        // Get folder children and add them to the pending list.
        $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
        $select->addField('fs', 'id', 'id');
        $select->condition('parentid', $id, '=');
        $select->condition('kind', FolderShareInterface::FOLDER_KIND, '=');

        $pending = array_merge($pending, $select->execute()->fetchCol(0));
      }
    }

    if ($count === 0) {
      return FALSE;
    }

    ManageLog::notice(
      "Folder tree: Corrected @count descendant items with incorrect root.\nEach item's root has been set to the highest ancestor's ID. This does not move items in the folder tree. It just corrects their access grants to be based on the correct root.",
      [
        '@count' => $count,
      ]);

    return TRUE;
  }

  /**
   * Fixes hidden items.
   *
   * <B>Problem</B>
   * If an item is marked as hidden, it is in the process of being deleted.
   * But if there are no delete tasks pending, the delete may have crashed.
   * This leaves hidden to-be-deleted items not deleted.
   *
   * <B>Fix</B>
   * Delete the hidden items.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixHidden(bool $fix = FALSE) {

    //
    // Find hidden.
    // ------------
    // Find all hidden items.
    $ids = self::findAllHiddenIds();
    $count = count($ids);
    if ($count === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      //
      // Find tasks.
      // -----------
      // Look for pending delete tasks. If there are hidden items, but no
      // delete tasks, there may be a problem.
      $nTasks =
        FolderShareScheduledTask::findNumberOfTasks('delete-hide') +
        FolderShareScheduledTask::findNumberOfTasks('delete-delete');

      if ($nTasks === 0) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count hidden items but no pending delete tasks.\nThese items are hidden from view and inaccessible. This normally means that they are about to be deleted, but there are no delete tasks pending. Repair is needed.\nEntity IDs: @ids",
          [
            '@count' => $count,
            '@ids'   => implode(', ', $ids),
          ]);
        return TRUE;
      }

      ManageLog::notice(
        "Folder tree: Found @count hidden items and @taskCount pending delete tasks.\nHidden items are normal while a delete task prepares to delete them. The pending tasks should be allowed to complete. Repair is probably not needed.\nEntity IDs: @ids",
        [
          '@count'     => $count,
          '@taskCount' => $nTasks,
          '@ids'       => implode(', ', $ids),
        ]);
      return FALSE;
    }

    //
    // Delete hidden.
    // --------------
    // Hidden items became hidden because a user tried to delete them and
    // something failed. Try to delete them again. We cannot make them visible
    // again because they aren't supposed to be present at all, and because
    // a deleted item made visible could have the same name as another newer
    // item created after the hidden item was "deleted".
    self::deleteMultiple($ids, FALSE);

    ManageLog::notice(
      "Folder tree: Corrected @count hidden items by deleting them.\nHidden items were originally marked hidden pending deletion anyway.",
      [
        '@count' => $count,
      ]);
    return TRUE;
  }

  /**
   * Fixes disabled items.
   *
   * <B>Problem</B>
   * If an item is marked as disabled, it is in use by a copy or move.
   * But if there are no copy or move tasks pending, there is no reason for an
   * item to be disabled. A prior copy or move may have crashed.
   *
   * <B>Fix</B>
   * Set the systemdisabled flag to FALSE to show it again.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixDisabled(bool $fix = FALSE) {

    if ($fix === FALSE) {
      //
      // Find disabled & tasks.
      // ----------------------
      // Find all disabled items and then look for pending copy and move
      // tasks. If there are disabled items, but no tasks, there may be
      // a problem.
      $ids = self::findAllDisabledIds();
      $count = count($ids);

      if ($count === 0) {
        return FALSE;
      }

      // Get the number of copy and delete tasks.
      $nTasks =
        FolderShareScheduledTask::findNumberOfTasks('copy-to-root') +
        FolderShareScheduledTask::findNumberOfTasks('copy-to-folder') +
        FolderShareScheduledTask::findNumberOfTasks('move-to-root') +
        FolderShareScheduledTask::findNumberOfTasks('move-to-folder');

      if ($nTasks === 0) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count disabled items but no pending copy or move tasks.\nThese items are disabled, visible in lists, but they cannot be changed or deleted. Disabled items are normal during copy and move operations, but there are no copy or move tasks pending. Repair is needed.\nEntity IDs: @ids",
          [
            '@count' => $count,
            '@ids'   => implode(', ', $ids),
          ]);
        return TRUE;
      }

      ManageLog::notice(
        "Folder tree: Found @count disabled items and @taskCount pending copy and/or move tasks.\nDisabled items are normal during copy and move operations in order to block user activity until the operation completes. The pending tasks should be allowed to complete. Repair is probably not needed.\nEntity IDs: @ids",
        [
          '@count'     => $count,
          '@taskCount' => $nTasks,
          '@ids'       => implode(', ', $ids),
        ]);
      return FALSE;
    }

    //
    // Clear disabled.
    // ---------------
    // Reset the disabled flag so that items can be shown normally.
    $count = self::clearAllSystemDisabled();

    if ($count === 0) {
      return FALSE;
    }

    ManageLog::notice(
      "Folder tree: Corrected @count disabled items by forcing them to be enabled.\nThis will cause these items to be listed normally and changeable. However, since these items were original disabled during a copy or move that apparently did not complete, users may be confused by items that don't seem complete.",
      [
        '@count' => $count,
      ]);
    return TRUE;
  }

  /**
   * Fixes missing or invalid file/media IDs.
   *
   * <B>Problems</B>
   * If an item's kind indicates it is a file, image, or media, then there
   * must be a file or media ID set for a wrapped File or Media entity.
   * If the ID is NULL or invalid, then the item is corrupted.
   *
   * <B>Fixes</B>
   * If an ID is missing or invalid, then the item is corrupted and cannot
   * be repaired. It is deleted.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixFileOrMediaIds(bool $fix = FALSE) {
    $errorCount = 0;
    $connection = Database::getConnection();

    //
    // Count missing ID problems.
    // --------------------------
    // Issue query to find file items with NULL file IDs.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::FILE_KIND, "=");
    $select->isNull('fs.file__target_id');
    $filesWithNullFileIds = $select->execute()->fetchCol(0);
    $errorCount += count($filesWithNullFileIds);

    // Issue query to find image items with NULL file IDs.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::IMAGE_KIND, "=");
    $select->isNull('fs.image__target_id');
    $imagesWithNullFileIds = $select->execute()->fetchCol(0);
    $errorCount += count($imagesWithNullFileIds);

    // Issue query to find media items with NULL media IDs.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::MEDIA_KIND, "=");
    $select->isNull('fs.media');
    $mediaWithNullMediaIds = $select->execute()->fetchCol(0);
    $errorCount += count($mediaWithNullMediaIds);

    //
    // Count invalid ID problems.
    // --------------------------
    // Issue query to find file items with invalid file IDs.
    $subquery = $connection->select('file_managed', 'fm');
    $subquery->addField('fm', 'fid', 'fid');
    $subquery->where('fs.file__target_id = fm.fid');

    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::FILE_KIND, "=");
    $select->isNotNull('fs.file__target_id');
    $select->notExists($subquery);
    $filesWithInvalidFileIds = $select->execute()->fetchCol(0);
    $errorCount += count($filesWithInvalidFileIds);

    // Issue query to find image items with invalid file IDs.
    $subquery = $connection->select('file_managed', 'fm');
    $subquery->addField('fm', 'fid', 'fid');
    $subquery->where('fs.image__target_id = fm.fid');

    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::IMAGE_KIND, "=");
    $select->isNotNull('fs.image__target_id');
    $select->notExists($subquery);
    $imagesWithInvalidFileIds = $select->execute()->fetchCol(0);
    $errorCount += count($imagesWithInvalidFileIds);

    // Issue query to find media items with invalid media IDs.
    $subquery = $connection->select('media', 'm');
    $subquery->addField('m', 'mid', 'mid');
    $subquery->where('fs.media = m.mid');

    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::MEDIA_KIND, "=");
    $select->isNotNull('fs.media');
    $select->notExists($subquery);
    $mediaWithInvalidMediaIds = $select->execute()->fetchCol(0);
    $errorCount += count($mediaWithInvalidMediaIds);

    if ($errorCount === 0) {
      // Nothing to fix or report.
      return FALSE;
    }

    if ($fix === FALSE) {
      if (empty($filesWithNullFileIds) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid items marked as being files, but with NULL file IDs.\nThese items are broken and refer to files entities that do not exist. This will cause them to be listed, but attempts to view or download the file will fail. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($filesWithNullFileIds),
            '@ids'   => implode(', ', $filesWithNullFileIds),
          ]);
      }

      if (empty($imagesWithNullFileIds) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid items marked as being images, but with NULL file IDs.\nThese items are broken and refer to files entities that do not exist. This will cause them to be listed, but attempts to view or download the image will fail. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($imagesWithNullFileIds),
            '@ids'   => implode(', ', $imagesWithNullFileIds),
          ]);
      }

      if (empty($mediaWithNullMediaIds) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid items marked as being media, but with NULL media IDs.\nThese items are broken and refer to media entities that does not exist. This will cause them to be listed, but attempts to view or download the media will fail. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($mediaWithNullMediaIds),
            '@ids'   => implode(', ', $mediaWithNullMediaIds),
          ]);
      }

      if (empty($filesWithInvalidFileIds) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid items marked as being files, but with invalid file IDs.\nThese items are broken and refer to files entities that do not exist. This will cause them to be listed, but attempts to view or download the file will fail. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($filesWithInvalidFileIds),
            '@ids'   => implode(', ', $filesWithInvalidFileIds),
          ]);
      }

      if (empty($imagesWithInvalidFileIds) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid items marked as being images, but with invalid file IDs.\nThese items are broken and refer to files entities that do not exist. This will cause them to be listed, but attempts to view or download the image will fail. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($imagesWithInvalidFileIds),
            '@ids'   => implode(', ', $imagesWithInvalidFileIds),
          ]);
      }

      if (empty($mediaWithInvalidMediaIds) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid items marked as being media, but with invalid media IDs.\nThese items are broken and refer to media entities that does not exist. This will cause them to be listed, but attempts to view or download the media will fail. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($mediaWithInvalidMediaIds),
            '@ids'   => implode(', ', $mediaWithInvalidMediaIds),
          ]);
      }

      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // The only fix for items with NULL or invalid entity reference IDs is
    // to delete the items. We don't know what File or Media entity should
    // have been referred to, so we cannot hook things up again.
    //
    // In order for all hooks to get a chance to run, and all deleted items
    // to be logged, we have to load and delete each item individually.
    // This also insures that parent folder sizes get updated.
    $ids = array_merge(
      $filesWithNullFileIds,
      $imagesWithNullFileIds,
      $mediaWithNullMediaIds,
      $filesWithInvalidFileIds,
      $imagesWithInvalidFileIds,
      $mediaWithInvalidMediaIds);

    foreach ($ids as $id) {
      $item = self::load($id);
      if ($item !== NULL) {
        $item->delete();
      }
    }

    ManageLog::notice(
      "Folder tree: Deleted @count file, image, or media items with NULL or invalid file or media IDs.\nThere is no way to fix these since the intended file or media is not known. All invalid items have been deleted.",
      [
        '@count' => $errorCount,
      ]);

    return TRUE;
  }

  /**
   * Fixes file/media items where the underlying file/media name doesn't match.
   *
   * <B>Problems</B>
   * If an item's kind indicates it is a file, image, or media, then the
   * name of the wrapped File or Media entity should match that of the
   * item that is wrapping them.
   *
   * <B>Fixes</B>
   * If the name does not match, set it.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixFileOrMediaNames(bool $fix = FALSE) {
    $errorCount = 0;
    $connection = Database::getConnection();

    //
    // Count mismatched name problems.
    // -------------------------------
    // Issue query to find file items with mismatched names.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::FILE_KIND, "=");
    $select->join(
      'file_managed',
      'fm',
      '(fm.fid = fs.file__target_id)');
    $select->where('fs.name <> fm.filename');
    $filesWithWrongNames = $select->execute()->fetchCol(0);
    $errorCount += count($filesWithWrongNames);

    // Issue query to find image items with mismatched names.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::IMAGE_KIND, "=");
    $select->join(
      'file_managed',
      'fm',
      '(fm.fid = fs.image__target_id)');
    $select->where('fs.name <> fm.filename');
    $imagesWithWrongNames = $select->execute()->fetchCol(0);
    $errorCount += count($imagesWithWrongNames);

    // Issue query to find media items with mismatched names.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::MEDIA_KIND, "=");
    $select->join(
      'media_field_data',
      'm',
      '(m.mid = fs.media)');
    $select->where('fs.name <> m.name');
    $mediaWithWrongNames = $select->execute()->fetchCol(0);
    $errorCount += count($mediaWithWrongNames);

    if ($errorCount === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      if (empty($filesWithWrongNames) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid file items where their names do not match the underlying file's name.\nThis will cause these items to be shown with one name, but file downloads to use a different name. Repair is needed.\nEntity IDs: @ids",
          [
            '@count' => count($filesWithWrongNames),
            '@ids'   => implode(', ', $filesWithWrongNames),
          ]);
      }

      if (empty($imagesWithWrongNames) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid image items where their names do not match the underlying file's name.\nThis will cause these items to be shown with one name, but file downloads to use a different name. Repair is needed.\nEntity IDs: @ids",
          [
            '@count' => count($imagesWithWrongNames),
            '@ids'   => implode(', ', $imagesWithWrongNames),
          ]);
      }

      if (empty($mediaWithWrongNames) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid media items where their names do not match the underlying media's name.\nThis will cause these items to be shown with one name, but media downloads to use a different name. Repair is needed.\nEntity IDs: @ids",
          [
            '@count' => count($mediaWithWrongNames),
            '@ids'   => implode(', ', $mediaWithWrongNames),
          ]);
      }

      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // The fix is to set the wrapped File or Media entity's name to that
    // of the FolderShare entity. The FolderShare entity's name is what the
    // user has been seeing in the UI or REST, so it is what the user thinks
    // the item should be named.
    //
    // In order for File and Media entity hooks to run, if any, we need to
    // make changes by explicitly loading and saving each entity.
    $errorCount = 0;

    foreach ($filesWithWrongNames as $itemId) {
      $item = self::load($itemId);
      if ($item !== NULL) {
        $file = $item->getFile();
        if ($file !== NULL) {
          $file->setFilename($item->getName());
          self::saveFileWithRetry($file);
          ++$errorCount;
        }
      }
    }

    foreach ($imagesWithWrongNames as $itemId) {
      $item = self::load($itemId);
      if ($item !== NULL) {
        $file = $item->getImage();
        if ($file !== NULL) {
          $file->setFilename($item->getName());
          self::saveFileWithRetry($file);
          ++$errorCount;
        }
      }
    }

    foreach ($mediaWithWrongNames as $itemId) {
      $item = self::load($itemId);
      if ($item !== NULL) {
        $media = $item->getMedia();
        if ($media !== NULL) {
          $media->setName($item->getName());
          self::saveMediaWithRetry($media);
          ++$errorCount;
        }
      }
    }

    $ids = array_merge(
      $filesWithWrongNames,
      $imagesWithWrongNames,
      $mediaWithWrongNames);

    ManageLog::notice(
      "Folder tree: Corrected @count file, image, or media items where their names do not match the underlying file or media entity's name.\nThe underlying entity names have been updated to match the user-visible file, image, and media item names.\nEntity IDs: @ids",
      [
        '@count' => $errorCount,
        '@ids'   => implode(', ', $ids),
      ]);

    return TRUE;
  }

  /**
   * Fixes file items where the underlying File entity has an empty URI.
   *
   * <B>Problems</B>
   * If an item's kind indicates it is a file or image, then there is a
   * wrapped File entity. That entity has a URI that must not be empty.
   *
   * <B>Fixes</B>
   * If the URI is bad, there is no way to fix it. Delete the item.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixFileEmptyUris(bool $fix = FALSE) {
    $errorCount = 0;
    $connection = Database::getConnection();

    //
    // Count empty URI problems.
    // -------------------------
    // Issue query to find file items with empty URIs.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::FILE_KIND, "=");
    $select->join(
      'file_managed',
      'fm',
      '(fm.fid = fs.file__target_id)');
    $select->isNull('fm.uri');
    $filesWithEmptyUris = $select->execute()->fetchCol(0);
    $errorCount += count($filesWithEmptyUris);

    // Issue query to find image items with mismatched names.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::IMAGE_KIND, "=");
    $select->join(
      'file_managed',
      'fm',
      '(fm.fid = fs.image__target_id)');
    $select->isNull('fm.uri');
    $imagesWithEmptyUris = $select->execute()->fetchCol(0);
    $errorCount += count($imagesWithEmptyUris);

    if ($errorCount === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      if (empty($filesWithEmptyUris) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid file items where the underlying file has an empty URI.\nThis will cause these items to be shown in folder lists, but there is no associated file to view or download. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($filesWithEmptyUris),
            '@ids'   => implode(', ', $filesWithEmptyUris),
          ]);
      }

      if (empty($imagesWithEmptyUris) === FALSE) {
        ManageLog::critical(
          "Folder tree: Integrity check found @count invalid image items where the underlying file has an empty URI.\nThis will cause these items to be shown in folder lists, but there is no associated file to view or download. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
          [
            '@count' => count($imagesWithEmptyUris),
            '@ids'   => implode(', ', $imagesWithEmptyUris),
          ]);
      }

      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // The only fix for items referring to File entities with empty URIs is
    // to delete the items. We don't know what the URI should have been,
    // so we cannot hook things up again.
    //
    // In order for all hooks to get a chance to run, and all deleted items
    // to be logged, we have to load and delete each item individually.
    // This also insures that parent folder sizes get updated.
    $ids = array_merge(
      $filesWithEmptyUris,
      $imagesWithEmptyUris);

    foreach ($ids as $id) {
      $item = self::load($id);
      if ($item !== NULL) {
        $item->delete();
      }
    }

    ManageLog::notice(
      "Folder tree: Deleted @count file or image items where the underlying file had an empty URI.\nThere is no way to fix these since the intended file URI is not known. All invalid items have been deleted.",
      [
        '@count' => $errorCount,
      ]);

    return TRUE;
  }

  /**
   * Fixes file items where the underlying File entity does not exist.
   *
   * <B>Problems</B>
   * If an item's kind indicates it is a file or image, then there is a
   * wrapped File entity. That entity has a URI that must reference an
   * existing file.
   *
   * <B>Fixes</B>
   * If the URI is bad, there is no way to fix it. Delete the item.
   *
   * <B>Logged messages</B>
   * If problems are encountered and $fix is FALSE, critical error messages
   * are posted to the site's log and the problems are not fixed. But if
   * $fix is TRUE, an attempt is made to fix the problems and notice messages
   * about the problems are posted to the site's log.
   *
   * @param bool $fix
   *   (optional, default = TRUE) When TRUE, fix and log file system problems
   *   automatically. When FALSE, just report them to the log.
   *
   * @return bool
   *   Returns TRUE if anything in the file system was fixed or would
   *   have been fixed, and FALSE if no changes are needed.
   */
  private static function fsckFixFileInvalidUris(bool $fix = FALSE) {
    $count = 0;
    $connection = Database::getConnection();

    //
    // Count files and images.
    // -----------------------
    // Issue query to find all file items.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::FILE_KIND, "=");
    $select->join(
      'file_managed',
      'fm',
      '(fm.fid = fs.file__target_id)');
    $select->addField('fm', 'uri', 'uri');
    $filesWithUris = [];
    foreach ($select->execute() as $result) {
      $filesWithUris[$result->id] = $result->uri;
    }

    $count += count($filesWithUris);

    // Issue query to find image items with mismatched names.
    $select = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $select->addField('fs', 'id', 'id');
    $select->condition('kind', FolderShareInterface::IMAGE_KIND, "=");
    $select->join(
      'file_managed',
      'fm',
      '(fm.fid = fs.image__target_id)');
    $select->addField('fm', 'uri', 'uri');
    $imagesWithUris = [];
    foreach ($select->execute() as $result) {
      $imagesWithUris[$result->id] = $result->uri;
    }

    $count += count($imagesWithUris);

    if ($count === 0) {
      // No files or images.
      return FALSE;
    }

    //
    // Count problems.
    // ---------------
    // The only way to determine if a URI is valid is to try and use it.
    // This is very expensive.
    $badIds = [];

    foreach ($filesWithUris as $id => $uri) {
      if (FileUtilities::realpath($uri) === FALSE) {
        $badIds[] = $id;
      }
    }
    foreach ($imagesWithUris as $id => $uri) {
      if (FileUtilities::realpath($uri) === FALSE) {
        $badIds[] = $id;
      }
    }

    if (count($badIds) === 0) {
      return FALSE;
    }

    if ($fix === FALSE) {
      ManageLog::critical(
        "Folder tree: Integrity check found @count invalid items where the underlying file has an invalid URI that does not reference an existing file.\nThis will cause these items to be shown in folder lists, but there is no associated file to view or download. Repair is needed to delete these invalid items.\nEntity IDs: @ids",
        [
          '@count' => count($badIds),
          '@ids'   => implode(', ', $badIds),
        ]);

      return TRUE;
    }

    //
    // Fix problems.
    // -------------
    // The only fix for items with invalid URIs is to delete them.
    // We don't know what URI should have been so we cannot fix it.
    //
    // In order for all hooks to get a chance to run, and all deleted items
    // to be logged, we have to load and delete each item individually.
    // This also insures that parent folder sizes get updated.
    foreach ($badIds as $id) {
      $item = self::load($id);
      if ($item !== NULL) {
        $item->delete();
      }
    }

    ManageLog::notice(
      "Folder tree: Deleted @count file or image items where the underlying file had an invalid URI that did not reference an existing file.\nThere is no way to fix these since the intended file URI is not known. All invalid items have been deleted.",
      [
        '@count' => count($badIds),
      ]);

    return TRUE;
  }

}
