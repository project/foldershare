<?php

namespace Drupal\foldershare\Form\AdminSettingsTraits;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\Constants;
use Drupal\foldershare\ManageSearch;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Utilities\ConfigurationUtilities;
use Drupal\foldershare\Utilities\LinkUtilities;

/**
 * Manages the "Interface" tab for the module's settings form.
 *
 * <B>Warning:</B> This is an internal trait that is strictly used by
 * the AdminSettings form class. It is a mechanism to group functionality
 * to improve code management.
 *
 * @ingroup foldershare
 */
trait AdminSettingsInterfaceTab {

  /*---------------------------------------------------------------------
   *
   * Build.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds the user interface tab.
   *
   * @param array $form
   *   An associative array containing the structure of the form. The form
   *   is modified to include additional render elements for the tab.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   * @param string $tabGroup
   *   The name of the tab group.
   */
  private function buildInterfaceTab(
    array &$form,
    FormStateInterface $formState,
    string $tabGroup) {

    //
    // Find installed modules.
    // -----------------------
    // Tab sections vary their presentation based upon what modules are
    // installed at the site.
    $helpInstalled     = $this->moduleHandler->moduleExists('help');
    $viewsUiInstalled  = $this->moduleHandler->moduleExists('views_ui');
    $realnameInstalled = $this->moduleHandler->moduleExists('realname');
    $fieldUiInstalled  = $this->moduleHandler->moduleExists('field_ui');

    //
    // Set up tab names.
    // -----------------
    // Create the machine name for the tab and the tab's title.
    $tabMachineName = 'interface';
    $tabName        = 'foldershare_' . $tabMachineName . '_tab';
    $tabTitle       = $this->t('Interface');
    $tabPaneTitle   = $this->t('Manage the user interface');

    //
    // Set up class names.
    // -------------------
    // Use a set of standard class names for tab sections.
    $tabSubtitleClass        = 'foldershare-settings-subtitle';
    $sectionWrapperClass     = 'foldershare-settings-section-wrapper';
    $sectionClass            = 'foldershare-settings-section';
    $sectionDescriptionClass = 'foldershare-settings-section-description';
    $itemDefaultClass        = 'foldershare-settings-item-default';
    $itemNoteClass           = 'foldershare-settings-item-note';

    $menuAllowAllCheck       = 'foldershare_menu_allow_all';
    $menuAllowedCommandsList = 'foldershare_menu_allowed_commands';
    $restoreMenuButton       = 'foldershare_menu_restore';
    $restoreViewButton       = 'foldershare_view_restore';
    $restoreFormsButton      = 'foldershare_forms_restore';
    $restoreDisplaysButton   = 'foldershare_displays_restore';
    $searchBoxCheck          = 'foldershare_searchbox';
    $preferredSearchSelect   = 'foldershare_preferred_search';
    $searchAPIPathText       = 'foldershare_search_api_path';
    $searchAPIURLStyleText   = 'foldershare_search_api_url_style';
    $searchAPIFilterIdText   = 'foldershare_search_api_filter_id';
    $userAutoCompleteClass   = 'foldershare-settings-userautocomplete';
    $userAutoComplete        = 'foldershare_userautocomplete';
    $viewMaximumHeightClass  = 'foldershare_view_maximum_height';

    //
    // Create the tab
    // --------------
    // Start the tab with a title, subtitle, and description.
    $form[$tabName] = [
      '#type'                    => 'details',
      '#open'                    => FALSE,
      '#group'                   => $tabGroup,
      '#title'                   => $tabTitle,

      '#description'             => [
        'subtitle'               => [
          '#type'                => 'html_tag',
          '#tag'                 => 'h2',
          '#value'               => $tabPaneTitle,
          '#attributes'          => [
            'class'              => [$tabSubtitleClass],
          ],
        ],

        'description'            => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            'Control forms, pages, menus, folder browsers, and autocomplete prompts.'),
          '#attributes'          => [
            'class'              => [
              'foldershare-settings-description',
            ],
          ],
        ],
      ],
      '#attributes'              => [
        'class'                  => [
          'foldershare-settings-tab ',
          'foldershare-interface-tab',
        ],
      ],
    ];

    //
    // Manage fields, forms, and displays.
    // -----------------------------------
    //
    // FIELD UI  HELP  MESSAGE
    // no        no    basic message
    // no        yes   basic message + field help
    // yes       no    basic message + field UI links
    // yes       yes   basic message + field UI links + field and field UI help
    //
    // The Field module is always installed since it is a required part of core.
    if ($fieldUiInstalled === TRUE) {
      $fieldUiName = $this->moduleHandler->getName('field_ui');
    }
    else {
      $fieldUiName = 'Field UI';
    }

    if ($fieldUiInstalled === TRUE &&
        $helpInstalled === TRUE) {
      // Field UI is installed and help is available.
      $description = $this->t(
        'Files and folders may be given additional @fieldUiFieldLink, and their @fieldUiFormLink and @fieldUiDisplayLink configured using the %fieldUiName module (see @fieldHelpLink and @fieldUiHelpLink).',
        [
          '%fieldUiName'         => $fieldUiName,
          '@fieldUiFieldLink'    => LinkUtilities::createRouteLink(
            'entity.' . FolderShare::ENTITY_TYPE_ID . '.field_ui_fields',
            '',
            $this->t('Fields')),
          '@fieldUiFormLink'     => LinkUtilities::createRouteLink(
            'entity.entity_form_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('forms')),
          '@fieldUiDisplayLink'  => LinkUtilities::createRouteLink(
            'entity.entity_view_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('Page displays')),
          '@fieldHelpLink'       => LinkUtilities::createHelpLink(
            'field',
            $this->t('Field help')),
          '@fieldUiHelpLink'     => LinkUtilities::createHelpLink(
            'field_ui',
            $this->t('Field UI help')),
        ]);
      $restoreFormDescription = $this->t(
        'Restore @fieldUiFormLink to default values.',
        [
          '@fieldUiFormLink'     => LinkUtilities::createRouteLink(
            'entity.entity_form_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('Forms')),
        ]);
      $restoreDisplaysDescription = $this->t(
        'Restore @fieldUiDisplayLink to default values.',
        [
          '@fieldUiDisplayLink'  => LinkUtilities::createRouteLink(
            'entity.entity_view_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('Page displays')),
        ]);
    }
    elseif ($fieldUiInstalled === TRUE &&
        $helpInstalled === FALSE) {
      // Field UI is installed but there is no help.
      $description = $this->t(
        'Files and folders may be given additional @fieldUiFieldLink, and their @fieldUiFormLink and @fieldUiDisplayLink configured using the %fieldUiName module.',
        [
          '%fieldUiName'         => $fieldUiName,
          '@fieldUiFieldLink'    => LinkUtilities::createRouteLink(
            'entity.' . FolderShare::ENTITY_TYPE_ID . '.field_ui_fields',
            '',
            $this->t('Fields')),
          '@fieldUiFormLink'     => LinkUtilities::createRouteLink(
            'entity.entity_form_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('Forms')),
          '@fieldUiDisplayLink'  => LinkUtilities::createRouteLink(
            'entity.entity_view_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('page displays')),
        ]);
      $restoreFormDescription = $this->t(
        'Restore @fieldUiFormLink to default values.',
        [
          '@fieldUiFormLink'     => LinkUtilities::createRouteLink(
            'entity.entity_form_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('Forms')),
        ]);
      $restoreDisplaysDescription = $this->t(
        'Restore @fieldUiDisplayLink to default values.',
        [
          '@fieldUiDisplayLink'  => LinkUtilities::createRouteLink(
            'entity.entity_view_display.' . FolderShare::ENTITY_TYPE_ID . '.default',
            '',
            $this->t('Page displays')),
        ]);
    }
    else {
      // All other cases.
      $description = $this->t(
        'Files and folders may be given additional fields, and their forms and page displays configured by installing the %fieldUiEnableLink module.',
        [
          '%fieldUiEnableLink'   => LinkUtilities::createRouteLink(
            'system.modules_list',
            'module-field-ui',
            $fieldUiName),
        ]);
      $restoreFormDescription = $this->t(
        'Restore forms to default values.');
      $restoreDisplaysDescription = $this->t(
        'Restore page displays to default values.');
    }

    $form[$tabName]['manage-forms'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('Forms and pages'),
      '#open'                    => FALSE,
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],

      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],

        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $description,
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        'forms-pair'             => [
          '#type'                => 'container',
          // A button to restore the default form configuration.
          $restoreFormsButton    => [
            '#type'              => 'submit',
            '#button_type'       => 'warning',
            '#value'             => $this->t('Restore'),
            '#name'              => $restoreFormsButton,
            '#attributes'        => [
              'class'            => [
                $restoreFormsButton,
              ],
            ],
          ],

          // A description for the button.
          'forms-restore-description' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $restoreFormDescription,
            '#attributes'        => [
              'class'            => [
                $restoreFormsButton . '_description',
              ],
            ],
          ],
        ],

        'displays-pair'          => [
          '#type'                => 'container',
          // A button to restore the default page display configuration.
          $restoreDisplaysButton => [
            '#type'              => 'submit',
            '#button_type'       => 'warning',
            '#value'             => $this->t('Restore'),
            '#name'              => $restoreDisplaysButton,
            '#attributes'        => [
              'class'            => [
                $restoreDisplaysButton,
              ],
            ],
          ],

          // A description for the button.
          'displays-restore-description' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $restoreDisplaysDescription,
            '#attributes'        => [
              'class'            => [
                $restoreDisplaysButton . '_description',
              ],
            ],
          ],
        ],
      ],
    ];

    //
    // Manage folder browser.
    // ----------------------
    // Add a restore button to reset the view configuration.
    //
    // The description changes a bit for different configurations:
    //
    // VIEWS  VIEWS UI  HELP  MESSAGE
    // yes    no        no    basic message + views ui enable
    // yes    no        yes   basic message + views ui enable + help
    // yes    yes       no    basic message + views form
    // yes    yes       yes   basic message + views form + help
    //
    // The Views module is always enabled in these choices because FolderShare
    // requires it.
    if ($viewsUiInstalled === TRUE) {
      $viewsUiName = $this->moduleHandler->getName('views_ui');
      $viewsUiLink = LinkUtilities::createRouteLink(
        'entity.view.collection',
        '',
        $viewsUiName);
    }
    else {
      $viewsUiName = 'Views UI';
      $viewsUiLink = $viewsUiName;
    }

    $viewsName = $this->moduleHandler->getName('views');

    $viewsUiEnableLink = LinkUtilities::createRouteLink(
      'system.modules_list',
      'module-viewsui',
      $viewsUiName);

    if ($viewsUiInstalled === TRUE && $helpInstalled === TRUE) {
      // Views UI installed and help available.
      $description = $this->t(
        'The folder browser lists files and folders using a @viewLink for the %viewsName module (see @viewsHelpLink). Use the %viewsUiLink module (see @viewsUiHelpLink) to configure the view.',
        [
          '%viewsName'           => $viewsName,
          '@viewLink'            => LinkUtilities::createRouteLink(
            'entity.view.edit_form',
            '',
            $this->t('view'),
            [
              'view'             => Constants::VIEW_LISTS,
            ]),
          '%viewsUiLink'         => $viewsUiLink,
          '@viewsHelpLink'       => LinkUtilities::createHelpLink(
            'views',
            $this->t('help')),
          '@viewsUiHelpLink'     => LinkUtilities::createHelpLink(
            'views_ui',
            $this->t('help')),
        ]);

      $buttonDescription = $this->t(
        'Restore @viewLink to default values.',
        [
          '@viewLink'            => LinkUtilities::createRouteLink(
            'entity.view.edit_form',
            '',
            $this->t('view'),
            [
              'view'             => Constants::VIEW_LISTS,
            ]),
        ]);
    }
    elseif ($viewsUiInstalled === TRUE && $helpInstalled === FALSE) {
      // Views UI installed but no help.
      $description = $this->t(
        'The folder browser lists files and folders using a @viewLink for the %viewsName module. Use the %viewsUiLink module to configure the view.',
        [
          '%viewsName'           => $viewsName,
          '@viewLink'            => LinkUtilities::createRouteLink(
            'entity.view.edit_form',
            '',
            $this->t('view'),
            [
              'view' => Constants::VIEW_LISTS,
            ]),
          '%viewsUiLink'         => $viewsUiLink,
        ]);

      $buttonDescription = $this->t(
        'Restore @viewLink to default values.',
        [
          '@viewLink'            => LinkUtilities::createRouteLink(
            'entity.view.edit_form',
            '',
            $this->t('view'),
            [
              'view'             => Constants::VIEW_LISTS,
            ]),
        ]);
    }
    elseif ($viewsUiInstalled === FALSE && $helpInstalled === TRUE) {
      // Views UI is not installed, but there is help.
      $description = $this->t(
        'The folder browser lists files and folders using a view for the %viewsName module (see @viewsHelpLink). Install the %viewsUiEnableLink module to configure lists.',
        [
          '%viewsName'           => $viewsName,
          '%viewsUiEnableLink'   => $viewsUiEnableLink,
          '@viewsHelpLink'       => LinkUtilities::createHelpLink(
            'views',
            $this->t('help')),
        ]);

      $buttonDescription = $this->t('Restore view to default values.');
    }
    else {
      // All other cases.
      $description = $this->t(
        'The folder browser lists files and folders using a view for the %viewsName module. Install the %viewsUiEnableLink module to configure lists.',
        [
          '%viewsName'           => $viewsName,
          '%viewsUiEnableLink'   => $viewsUiEnableLink,
        ]);

      $buttonDescription = $this->t('Restore view to default values.');
    }

    $viewMaximumHeight = Settings::getUserInterfaceFolderBrowserMaximumHeight();
    $viewMaximumHeightDefault = Settings::getUserInterfaceFolderBrowserMaximumHeightDefault();
    $viewMaximumHeightDescription = $this->t(
      'An optional maximum browser height limits its size on a page. When there are more rows, the browser scrolls. A zero disables the maximum height and the browser shows all rows without scrolling.');
    if ($viewMaximumHeightDefault === 0) {
      $viewMaximumHeightDefaultDescription = $this->t(
        'Default: Zero = no maximum.');
    }
    else {
      $viewMaximumHeightDefaultDescription = $this->t(
        'Default: @size ems.',
        [
          '@size' => $viewMaximumHeightDefault,
        ]);
    }

    $form[$tabName]['manage-views-ui'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('Folder browser list'),
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],

      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],
        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $description,
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        // A number field for the maximum browser height on a page.
        'view-maximum-height-group' => [
          '#type'                => 'fieldset',
          '#title'               => $this->t('Maximum browser height'),

          'view-maximum-height-description' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $viewMaximumHeightDescription,
            '#attributes'        => [
              'class'            => [$sectionDescriptionClass],
            ],
          ],
          $viewMaximumHeightClass => [
            '#type'              => 'number',
            '#default_value'     => $viewMaximumHeight,
            '#size'              => 5,
            '#min'               => 0,
            '#step'              => 1,
            '#title'             => $this->t('Maximum height (ems):'),
            '#name'              => $viewMaximumHeightClass,
            '#attributes'        => [
              'class'            => [
                $viewMaximumHeightClass,
              ],
            ],
          ],
          'view-maximum-height-default' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $viewMaximumHeightDefaultDescription,
            '#attributes'        => [
              'class'            => [
                'description',
                $itemDefaultClass,
                $itemNoteClass,
              ],
            ],
          ],
        ],

        // A button to restore the default configuration.
        $restoreViewButton       => [
          '#type'                => 'submit',
          '#button_type'         => 'warning',
          '#value'               => $this->t('Restore'),
          '#name'                => $restoreViewButton,
          '#attributes'          => [
            'class'              => [
              $restoreViewButton,
            ],
          ],
        ],

        // A description for the button.
        'view-restore-description' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $buttonDescription,
          '#attributes'          => [
            'class'              => [
              $restoreViewButton . '_description',
            ],
          ],
        ],

        // A warning of ViewsUI is not installed.
        'viewsui-disabled-warning' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            'View editing features are disabled because the @viewsUiEnableLink module is not installed.',
            [
              '@viewsUiEnableLink' => $viewsUiEnableLink,
            ]),
          '#attributes'          => [
            'class'              => [
              'color-warning',
            ],
          ],
        ],

      ],
    ];

    // If the view_ui module is not installed, links to the views config
    // pages will not work. Create a warning.
    if ($viewsUiInstalled === TRUE) {
      // Views UI is installed. Remove the warning about it not being installed.
      unset($form[$tabName]['manage-views-ui']['section']['viewsui-disabled-warning']);
    }

    //
    // Manage menus.
    // -------------
    // Add a checkbox to enable/disable menu restrictions, and a list of
    // checkboxes to enable/disable specific menu items.
    //
    // Get the unsorted list of all menu command definitions. Array keys are
    // IDs and values are definitions.
    $defs = Settings::getAllCommandDefinitions();

    // Sort them, reordering the keys to sort by definition label.
    usort(
      $defs,
      function ($a, $b) {
        if ($a['label'] == $b['label']) {
          return ($a['id'] < $b['id']) ? (-1) : 1;
        }

        return ($a['label'] < $b['label']) ? (-1) : 1;
      });

    // Build a sorted list of menu commands, with descriptions, for use in
    // creating checkboxes.
    $allNames = [];
    $allChosen = [];

    foreach ($defs as $def) {
      $id          = $def['id'];
      $label       = $def['label'];
      $provider    = $def['provider'];
      $description = $def['description'];

      $allNames[$id] = "<strong>$label</strong> (<em>$id</em>)<p>$description <em>($provider module)</em></p>";
      $allChosen[$id] = NULL;
    }

    foreach (Settings::getCommandMenuAllowed() as $id) {
      $allChosen[$id] = $id;
    }

    $form[$tabName]['manage-menus'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('Folder browser menu'),
      '#open'                    => FALSE,
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],

      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],

        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            'Menu item plugins support upload, download, copy, move, delete, and so forth.'),
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        // Checkbox to enable/disable restrictions.
        $menuAllowAllCheck       => [
          '#type'                => 'checkbox',
          '#title'               => $this->t('Allow all menu items'),
          '#default_value'       => (Settings::getCommandMenuRestrict() === FALSE),
          '#return_value'        => 'enabled',
          '#required'            => FALSE,
          '#name'                => $menuAllowAllCheck,
        ],

        // The default setting.
        'menu-restrictions-default' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t('Default: Allow all menu items'),
          '#attributes'          => [
            'class'              => [
              'description',
              $itemDefaultClass,
              $itemNoteClass,
            ],
          ],
        ],

        // List of menu commands.
        'foldershare_command_menu_choices' => [
          '#type'                => 'fieldset',
          '#title'               => $this->t('Allowed menu items'),
          '#attributes'          => [
            'class'              => ['foldershare_command_menu_choices'],
          ],
          '#states'              => [
            'invisible'          => [
              'input[name="' . $menuAllowAllCheck . '"]' => [
                'checked'        => TRUE,
              ],
            ],
          ],

          // A prompt before the list of checkboxes.
          'description'          => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t('Select the menu items to allow:'),
          ],

          // The list of checkboxes.
          $menuAllowedCommandsList => [
            '#type'              => 'checkboxes',
            '#options'           => $allNames,
            '#default_value'     => $allChosen,
            '#prefix'            => '<div class="' . $menuAllowedCommandsList . '">',
            '#suffix'            => '</div>',
          ],
        ],

        // A button to restore the default configuration.
        $restoreMenuButton => [
          '#type'                => 'submit',
          '#button_type'         => 'warning',
          '#value'               => $this->t('Restore'),
          '#name'                => $restoreMenuButton,
          '#attributes'          => [
            'class'              => [
              $restoreMenuButton,
            ],
          ],
        ],

        // A description for the button.
        'menu-restore-description' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t('Restore menus to default values.'),
          '#attributes'          => [
            'class'              => [
              $restoreMenuButton . '_description',
            ],
          ],
        ],
      ],
    ];

    //
    // Manage search box on folder browser.
    // ------------------------------------
    // Select whether to include a search box on the toolbar of the folder
    // browser. This is only available if a search provider is available.
    if (ManageSearch::isSearchAvailable() === TRUE) {
      // A search provider is available.
      $description = $this->t(
        'An optional search box on the folder browser toolbar provides users quick access to file and folder search.');

      $form[$tabName]['manage-searchbox'] = [
        '#type'                  => 'details',
        '#title'                 => $this->t('Folder browser search box'),
        '#open'                  => FALSE,
        '#attributes'            => [
          'class'                => [$sectionWrapperClass],
        ],

        'section'                => [
          '#type'                => 'container',
          '#attributes'          => [
            'class'              => [$sectionClass],
          ],

          'section-description'  => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#weight'            => 1,
            '#value'             => $description,
            '#attributes'        => [
              'class'            => [$sectionDescriptionClass],
            ],
          ],

          // Checkbox to enable/disable search box.
          $searchBoxCheck        => [
            '#type'              => 'checkbox',
            '#title'             => $this->t('Enable the search box'),
            '#default_value'     => Settings::getUserInterfaceSearchBoxEnable(),
            '#return_value'      => 'enabled',
            '#required'          => FALSE,
            '#name'              => $searchBoxCheck,
            '#weight'            => 2,
          ],

          // The default setting.
          'searchbox-default' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t('Default: Enable the search box'),
            '#weight'            => 3,
            '#attributes'        => [
              'class'            => [
                'description',
                $itemDefaultClass,
                $itemNoteClass,
              ],
            ],
          ],
        ],
      ];

      $preferredSearch = ManageSearch::updatePreferredSearch();

      $hasCoreSearch = ManageSearch::isCoreSearchAvailable();
      $hasSearchAPI = ManageSearch::isSearchAPIAvailable();

      if ($hasCoreSearch && $hasSearchAPI) {
        // Two search modules are installed. Add a menu to choose one
        // for use with the search box.
        $form[$tabName]['manage-searchbox']['section']['preferred-search-group'] = [
          '#type'                => 'fieldset',
          '#title'               => $this->t('Search module for search box'),
          '#states'              => [
            'visible'            => [
              'input[name="' . $searchBoxCheck . '"]' => [
                'checked'        => TRUE,
              ],
            ],
          ],
          '#weight'              => 4,

          'preferred-search-description' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t(
              "The core %coreSearchConfigLink module and the contributed %searchAPIConfigLink module are both installed. Select one for use with the folder browser search box:",
              [
                '%coreSearchConfigLink' => ManageSearch::createCoreSearchConfigurationLink(),
                '%searchAPIConfigLink'  => ManageSearch::createSearchAPIConfigurationLink(),
              ]),
            '#weight'            => 1,
          ],

          $preferredSearchSelect => [
            '#type'              => 'select',
            '#options'           => [
              'search'           => ManageSearch::getCoreSearchModuleName(),
              'search_api'       => ManageSearch::getSearchAPIModuleName(),
            ],
            '#default_value'     => $preferredSearch,
            '#name'              => $preferredSearchSelect,
            '#weight'            => 2,
          ],
        ];
      }

      if ($hasSearchAPI) {
        // Search API is installed. Add a text field for the search path.
        $urlStyles = [
          'query' => $this->t('A query URL'),
          'path'  => $this->t('A path URL'),
        ];

        if (ManageSearch::isSearchAPIPagesAvailable() === TRUE) {
          $urlStyleDescription = $this->t(
            'Use a query style when using a view. Use a path style when using the @searchAPIPagesName module. Use either style for a custom form.',
            [
              '@searchAPIPagesName' => ManageSearch::getSearchAPIPagesModuleName(),
            ]);
        }
        else {
          $urlStyleDescription = $this->t(
            'Use a query style when using a view. Use the query or path style for a custom form.');
        }

        $groupVisible = [
          'input[name="' . $searchBoxCheck . '"]' => [
            'checked'        => TRUE,
          ],
        ];
        if ($hasCoreSearch) {
          $groupVisible['select[name="' . $preferredSearchSelect . '"]'] = [
            'value'          => 'search_api',
          ];
        }

        $form[$tabName]['manage-searchbox']['section']['search-api-group'] = [
          '#type'                => 'fieldset',
          '#title'               => $this->t('Search API search form'),
          '#weight'              => 5,
          '#states'              => [
            'visible'            => $groupVisible,
          ],

          // A description for the search API path.
          'search-api-url-description' => [
            '#type'              => 'html_tag',
            '#tag'               => 'div',
            '#value'             => '<p>' . $this->t(
              'The folder browser search box sends entered keywords to the site\'s @searchAPIName search form. The URL for that form has a path (e.g. "/search/index") and one of two common styles:',
              [
                '@searchAPIName' => ManageSearch::getSearchAPIModuleName(),
              ]) . '</p><ul><li>' .
              $this->t('A query (e.g. "/search/index?query=keywords")') .
              '</li><li>' .
              $this->t('A path (e.g. "/search/index/keywords")') .
              '</li></ul></p>',
          ],

          $searchAPIURLStyleText => [
            '#type'              => 'select',
            '#title'             => $this->t('Search URL style:'),
            '#options'           => $urlStyles,
            '#default_value'     => Settings::getSearchAPISearchURLStyle(),
            '#name'              => $searchAPIURLStyleText,
            '#description'       => $urlStyleDescription,
          ],

          // The search path.
          $searchAPIPathText => [
            '#type'              => 'textfield',
            '#title'             => $this->t('Search page path:'),
            '#default_value'     => Settings::getSearchAPISearchPath(),
            '#name'              => $searchAPIPathText,
            '#description'       => $this->t(
              "The path must start with a '/' (e.g. '/search/index'). A blank path disables the search box."),
          ],

          // The query argument (exposed filter ID).
          $searchAPIFilterIdText => [
            '#type'              => 'textfield',
            '#title'             => $this->t('Query name:'),
            '#default_value'     => Settings::getSearchAPISearchFilterId(),
            '#name'              => $searchAPIFilterIdText,
            '#description'       => $this->t(
              "When using a view, the query name is the filter ID for the view's exposed filter (e.g. 'keywords'). A blank name disables the search box."),
            '#states'            => [
              'invisible'          => [
                'select[name="' . $searchAPIURLStyleText . '"]' => [
                  'value'        => 'path',
                ],
              ],
            ],
          ],

          'search-api-note' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t(
              "While the search page you select above can be configured to search for any content at the site, recommended practice is to dedicate a search page to finding files and folders. Use that page for the folder browser's search box. Use a general search page for a search block elsewhere on your page layout."),
          ],
        ];
      }
    }

    //
    // Manage user autocomplete.
    // -------------------------
    // Add information and a menu to select the user autocomplete style.
    //
    // The select menu options remain the same, but the text changes a bit
    // depending upon what modules are installed. The descriptions also
    // vary a bit.
    //
    // REALNAME  OPTIONS
    // no        Account names...
    // yes       User names...
    //
    // REALNAME  HELP  OPTIONS
    // no        no    basic message.
    // no        yes   basic message.
    // yes       no    basic message.
    // yes       yes   basic message + help.
    if ($realnameInstalled === TRUE) {
      // Realname module installed.
      $realnameName = $this->moduleHandler->getName('realname');
      $userautocompleteOptions = [
        'none'              => $this->t('Disable user autocomplete'),
        'name-only'         => $this->t('Show user names'),
        'name-email'        => $this->t('Show user names and email addresses'),
        'name-masked-email' => $this->t('Show user names and partially masked email addresses'),
      ];

      $description = $this->t(
        'Autocomplete menus help to select among users.');
      $defaultDescription = t('Default: Show user names');

      $realnameLink = LinkUtilities::createRouteLink(
        'realname.admin_settings_form',
        '',
        $realnameName);

      if ($helpInstalled === TRUE) {
        // Help is installed.
        $realnameHelpLink = LinkUtilities::createHelpLink(
          'realname',
          $this->t('help'));

        $nameOnlyDescription = t(
          'Autocomplete menus show full user names configured by the %realnameLink module (see @realnameHelpLink).',
          [
            '%realnameLink'      => $realnameLink,
            '@realnameHelpLink'  => $realnameHelpLink,
          ]);
        $nameEmailDescription = t(
          'Autocomplete menus full user names configured by the %realnameLink module (see @realnameHelpLink). Full email addresses are included',
          [
            '%realnameLink'      => $realnameLink,
            '@realnameHelpLink'  => $realnameHelpLink,
          ]);
        $nameMaskedEmailDescription = t(
          'Autocomplete menus show full user names configured by the %realnameLink module (see @realnameHelpLink). Partially masked email addresses are included (e.g. "a****b@example.com").',
          [
            '%realnameLink'      => $realnameLink,
            '@realnameHelpLink'  => $realnameHelpLink,
          ]);
      }
      else {
        $nameOnlyDescription = t(
          'Autocomplete menus show full user names configured by the %realnameLink module.',
          [
            '%realnameLink'      => $realnameLink,
          ]);
        $nameEmailDescription = t(
          'Autocomplete menus full user names configured by the %realnameLink module. Full email addresses are included',
          [
            '%realnameLink'      => $realnameLink,
          ]);
        $nameMaskedEmailDescription = t(
          'Autocomplete menus show full user names configured by the %realnameLink module. Partially masked email addresses are included (e.g. "a****b@example.com").',
          [
            '%realnameLink'      => $realnameLink,
          ]);
      }
    }
    else {
      // Realname module is not installed.
      $realnameName = 'Real name';
      $userautocompleteOptions = [
        'none'                   => $this->t('Disable user autocomplete'),
        'name-only'              => $this->t('Show account names'),
        'name-email'             => $this->t('Show account names and email addresses'),
        'name-masked-email'      => $this->t('Show account names and partially masked email addresses'),
      ];
      $description = $this->t(
        'Autocomplete menus help to select among users. Install the %realnameProjectLink contributed module to show full user names.',
        [
          '%realnameProjectLink' => LinkUtilities::createProjectLink(
            'realname',
            $realnameName),
        ]);
      $defaultDescription = t('Default: Show account names');

      $nameOnlyDescription = t('Autocomplete menus show user account names.');
      $nameEmailDescription = t('Autocomplete menus show user account names along with full email addresses.');
      $nameMaskedEmailDescription = t('Autocomplete menus show user account names along with partially masked email addresses (e.g. "a****b@example.com").');
    }

    $form[$tabName]['manage-user-autocomplete'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('User autocomplete'),
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],

      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],

        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $description,
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        // Autocomplete menu.
        $userAutoComplete        => [
          '#type'                => 'select',
          '#options'             => $userautocompleteOptions,
          '#default_value'       => Settings::getUserAutocompleteStyle(),
          '#name'                => $userAutoComplete,
          '#attributes'          => [
            'class'              => [
              $userAutoCompleteClass,
            ],
          ],
        ],

        // The default setting.
        'user-autocomplete-default' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $defaultDescription,
          '#attributes'          => [
            'class'              => [
              'description',
              $itemDefaultClass,
              $itemNoteClass,
            ],
          ],
        ],

        // A note when names only.
        'userautocompleteNameOnly' => [
          '#type'                => 'container',
          '#attributes'          => [
            'class'              => [
              $itemNoteClass,
            ],
          ],
          '#states'              => [
            'visible'            => [
              'select[name="' . $userAutoComplete . '"]' => [
                'value'          => 'name-only',
              ],
            ],
          ],

          'note'                 => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $nameOnlyDescription,
          ],
        ],

        // A note when showing users with email.
        'userautocompleteEmail'  => [
          '#type'                => 'container',
          '#attributes'          => [
            'class'              => [
              $itemNoteClass,
            ],
          ],
          '#states'              => [
            'visible'            => [
              'select[name="' . $userAutoComplete . '"]' => [
                'value'          => 'name-email',
              ],
            ],
          ],

          'note'                 => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $nameEmailDescription,
          ],

          'warning'              => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t(
              'Email addresses may be considered protected private information, depending upon the governing law in your area (e.g. the GDPR in Europe, and the CCPA in California). Users also may object to the display of their email addresses. Use this option with caution.'),
            '#attributes'        => [
              'class'            => [
                'color-warning',
              ],
            ],
          ],
        ],

        // A note when showing users with masked email.
        'userautocompleteMaskedEmail' => [
          '#type'                => 'container',
          '#attributes'          => [
            'class'              => [
              $itemNoteClass,
            ],
          ],
          '#states'              => [
            'visible'            => [
              'select[name="' . $userAutoComplete . '"]' => [
                'value'          => 'name-masked-email',
              ],
            ],
          ],

          'note'                 => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $nameMaskedEmailDescription,
          ],

          'warning'              => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t(
              'Email addresses may be considered protected private information, depending upon the governing law in your area (e.g. the GDPR in Europe, and the CCPA in California). Users also may object to the display of their email addresses, even when partially masked. Masking may not be sufficient to protect user privacy. Use this option with caution.'),
            '#attributes'        => [
              'class'            => [
                'color-warning',
              ],
            ],
          ],
        ],
      ],
    ];
  }

  /*---------------------------------------------------------------------
   *
   * Validate.
   *
   *---------------------------------------------------------------------*/
  /**
   * Validates form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  private function validateInterfaceTab(
    array &$form,
    FormStateInterface &$formState) {

    $trigger   = $formState->getTriggeringElement();
    $userInput = $formState->getUserInput();

    $menuAllowAllCheck       = 'foldershare_menu_allow_all';
    $menuAllowedCommandsList = 'foldershare_menu_allowed_commands';
    $restoreMenuButton       = 'foldershare_menu_restore';
    $restoreViewButton       = 'foldershare_view_restore';
    $restoreFormsButton      = 'foldershare_forms_restore';
    $restoreDisplaysButton   = 'foldershare_displays_restore';
    $searchAPIPathText       = 'foldershare_search_api_path';
    $preferredSearchSelect   = 'foldershare_preferred_search';

    //
    // Restore menu commands.
    // ----------------------
    // Restore command restrictions to an original configuration.
    if ($trigger['#name'] === $restoreMenuButton) {
      // Reset menu command configuration.
      Settings::setCommandMenuRestrict(
        Settings::getCommandMenuRestrictDefault());
      Settings::setCommandMenuAllowed(
        Settings::getCommandMenuAllowedDefault());

      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      // Update form state.
      $allChosen = [];
      foreach (Settings::getCommandMenuAllowed() as $id) {
        $allChosen[$id] = $id;
      }

      $userInput[$menuAllowedCommandsList] = $allChosen;
      $userInput[$menuAllowAllCheck] = (Settings::getCommandMenuRestrict() === FALSE);
      unset($userInput[$restoreMenuButton]);
      $formState->setUserInput($userInput);

      $this->messenger()->addStatus(
        $this->t('Menus have been restored to their defaults.'));
      return;
    }

    //
    // Restore view.
    // -------------
    // Restore the view to an original configuration.
    if ($trigger['#name'] === $restoreViewButton) {
      // Restore the view.
      $status = ConfigurationUtilities::revertConfiguration(
        'view',
        Constants::VIEW_LISTS);

      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      if ($status === TRUE) {
        $this->messenger()->addStatus(
          $this->t('File and folder views have been restored to their defaults.'));
      }
      else {
        $this->messenger()->addError(
          $this->t('File and folder views could not be restored to their defaults due to an unexpected problem.'));
      }

      return;
    }

    //
    // Restore forms.
    // --------------
    // Restore the configuration.
    if ($trigger['#name'] === $restoreFormsButton) {
      // Revert form configuration.
      $status = ConfigurationUtilities::revertConfiguration(
        'core',
        'entity_form_display.foldershare.foldershare.default');

      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      if ($status === TRUE) {
        $this->messenger()->addStatus(
          $this->t('File and folder forms have been restored to their defaults.'));
      }
      else {
        $this->messenger()->addError(
          $this->t('File and folder forms could not be restored to their defaults due to an unexpected problem.'));
      }

      return;
    }

    //
    // Restore displays.
    // -----------------
    // Restore the configuraiton.
    if ($trigger['#name'] === $restoreDisplaysButton) {
      // Revert form configuration.
      $status = ConfigurationUtilities::revertConfiguration(
        'core',
        'entity_view_display.foldershare.foldershare.default');

      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      if ($status === TRUE) {
        $this->messenger()->addStatus(
          $this->t('File and folder page displays have been restored to their defaults.'));
      }
      else {
        $this->messenger()->addError(
          $this->t('File and folder page displays could not be restored to their defaults due to an unexpected problem.'));
      }

      return;
    }

    //
    // Check search path.
    // ------------------
    // The Search API path must start with /, unless it is empty or
    // Search API is not in use.
    $preferred = $formState->getValue($preferredSearchSelect);
    if ($preferred === 'search_api' &&
        ManageSearch::isSearchAPIAvailable() === TRUE) {
      $value = $formState->getValue($searchAPIPathText);
      if (empty($value) === FALSE && $value[0] !== '/') {
        $formState->setErrorByName(
          $searchAPIPathText,
          $this->t("The search path must start with a slash ('/')."));
        return;
      }
    }
  }

  /*---------------------------------------------------------------------
   *
   * Submit.
   *
   *---------------------------------------------------------------------*/
  /**
   * Stores submitted form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  private function submitInterfaceTab(
    array &$form,
    FormStateInterface &$formState) {

    $menuAllowAllCheck       = 'foldershare_menu_allow_all';
    $menuAllowedCommandsList = 'foldershare_menu_allowed_commands';
    $searchBoxCheck          = 'foldershare_searchbox';
    $preferredSearchSelect   = 'foldershare_preferred_search';
    $searchAPIPathText       = 'foldershare_search_api_path';
    $searchAPIURLStyleText   = 'foldershare_search_api_url_style';
    $searchAPIFilterIdText   = 'foldershare_search_api_filter_id';
    $userAutoComplete        = 'foldershare_userautocomplete';
    $viewMaximumHeightClass  = 'foldershare_view_maximum_height';

    // Get whether the command menu is restricted.
    $value = $formState->getValue($menuAllowAllCheck);
    $enabled = ($value === 'enabled') ? TRUE : FALSE;
    Settings::setCommandMenuRestrict($enabled === FALSE);

    // Get allowed menu commands.
    $allChosen = $formState->getValue($menuAllowedCommandsList);
    Settings::setCommandMenuAllowed(array_keys(array_filter($allChosen)));

    // Get search box enable.
    $value = $formState->getValue($searchBoxCheck);
    $enabled = ($value === 'enabled') ? TRUE : FALSE;
    Settings::setUserInterfaceSearchBoxEnable($enabled);

    // Preferred search.
    $preferred = $formState->getValue($preferredSearchSelect);
    if (empty($preferred) === FALSE) {
      Settings::setPreferredSearchModuleId($preferred);
    }
    $preferred = Settings::getPreferredSearchModuleId();

    if ($preferred === 'search_api' &&
        ManageSearch::isSearchAPIAvailable() === TRUE) {
      // Search API path.
      $value = $formState->getValue($searchAPIPathText);
      Settings::setSearchAPISearchPath($value);

      // Search API URL style.
      $value = $formState->getValue($searchAPIURLStyleText);
      Settings::setSearchAPISearchURLStyle($value);

      // Search API filter ID.
      $value = $formState->getValue($searchAPIFilterIdText);
      Settings::setSearchAPISearchFilterId($value);
    }

    // Get user autocomplete style.
    $userautocompleteStyle = $formState->getValue($userAutoComplete);
    Settings::setUserAutocompleteStyle($userautocompleteStyle);

    // Get view maximum height.
    $viewHeight = $formState->getValue($viewMaximumHeightClass);
    Settings::setUserInterfaceFolderBrowserMaximumHeight($viewHeight);

    Cache::invalidateTags(['rendered']);
  }

}
