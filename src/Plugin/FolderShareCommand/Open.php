<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Url;

use Drupal\foldershare\Constants;

/**
 * Defines a command plugin to open an entity.
 *
 * The command is a dummy that does not use a configuration and does not
 * execute to change the entity. Instead, this command is used solely to
 * create an entry in command menus and support a redirect to the entity's
 * view page.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to edit.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_open",
 *  label           = @Translation("Open"),
 *  menuNameDefault = @Translation("Open..."),
 *  menuName        = @Translation("Open..."),
 *  description     = @Translation("Open a selected file or folder to view its contents."),
 *  category        = "open",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "view",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class Open extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*---------------------------------------------------------------------
   *
   * Execute behavior.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getExecuteBehavior() {
    // Prior to (and thus instead of) executing, redirect to
    // another page.
    return self::PRE_EXECUTE_PAGE_REDIRECT;
  }

  /**
   * {@inheritdoc}
   */
  public function getExecuteRedirectUrl() {
    $ids = $this->getSelectionIds();
    if (empty($ids) === TRUE) {
      $id = $this->getParentId();
    }
    else {
      $id = reset($ids);
    }

    // Redirect to the view controller for the entity.
    return Url::fromRoute(
      Constants::ROUTE_FOLDERSHARE,
      [
        Constants::ROUTE_FOLDERSHARE_ID => $id,
      ]);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Do nothing. This is never called because of the pre-execute redirect.
  }

}
