<?php

namespace Drupal\foldershare\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Constants;
use Drupal\foldershare\ManageSearch;
use Drupal\foldershare\Settings;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Creates a form for the search user interface associated with a view.
 *
 * While a site may have a site-wide generic search feature (often found in
 * the site's page header), THIS search UI provides localized search through
 * a folder tree. The search starts on the folder currently being displayed.
 *
 * The search form includes a search text field and hidden search submit
 * button. On a carriage-return in the search text field, the search is
 * submitted.
 *
 * The search form is disabled if:
 * - Drupal's core "search" module is not enabled.
 * - FolderShare's search plugin is not available.
 * - The current user does not have "search content" permission.
 * - The page's entity is disabled or hidden.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
final class UISearchBox extends FormBase {

  use RedirectDestinationTrait;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new form.
   */
  public function __construct() {
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static();
  }

  /*--------------------------------------------------------------------
   *
   * Form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return mb_strtolower(str_replace('\\', '_', get_class($this)));
  }

  /*--------------------------------------------------------------------
   *
   * Form build.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState = NULL) {
    //
    // Validate search possible
    // ------------------------
    // Check that the Drupal core Search module is enabled and that this
    // module's own search plugin is available. If not, there is no search
    // form.
    if (ManageSearch::isSearchAvailable() === FALSE) {
      // When no search module is enabled, do not return a search form.
      return NULL;
    }

    // Update the preferred search based upon what search modules are
    // currently installed.
    ManageSearch::updatePreferredSearch();

    $canDoAutocomplete = FALSE;
    switch (Settings::getPreferredSearchModuleId()) {
      case 'search':
        if (ManageSearch::isCoreSearchAvailable() === FALSE) {
          return NULL;
        }
        break;

      case 'search_api':
        if (ManageSearch::isSearchAPIConfigured() === FALSE) {
          return NULL;
        }
        $path  = Settings::getSearchAPISearchPath();
        $style = Settings::getSearchAPISearchURLStyle();
        $id    = Settings::getSearchAPISearchFilterId();

        if (empty($path) === TRUE) {
          // There is no search path. Do not return a search form.
          return NULL;
        }

        if ($style === 'query' && empty($id) === TRUE) {
          // Using a query URL, but the filter ID is not set. Do not
          // return a search form.
          return NULL;
        }

        if (ManageSearch::isSearchAPIAutocompleteAvailable() === TRUE &&
            $style === 'query') {
          // The autocomplete mechanism requires that the search be based
          // upon a view, which uses a "query" style URL.
          $canDoAutocomplete = TRUE;
        }
        break;

      default:
        // Unknown search module, or none. Do not return a search form.
        return NULL;
    }

    if (ManageSearch::userHasSearchPermission() === FALSE) {
      // When the user does not have search permission, do not return a
      // search form.
      return NULL;
    }

    // Get parent entity ID, if any.
    $args = $formState->getBuildInfo()['args'];
    if (empty($args) === TRUE || $args[0] === '') {
      // No parent entity. Default to showing a root list.
      $parentId = FolderShareInterface::USER_ROOT_LIST;
      $parent   = NULL;
      $disabled = FALSE;
    }
    else {
      // Parent entity ID should be the sole argument. Load it.
      // Loading could fail and return a NULL if the ID is bad.
      $parentId = (int) $args[0];
      $parent   = FolderShare::load($parentId);
      $disabled = $parent->isSystemDisabled() || $parent->isSystemHidden();
    }

    //
    // Define form classes
    // -------------------
    // Define classes used to mark the form and its items. These classes
    // are then used in CSS to style the form.
    $form['#attributes']['class'][] = 'foldershare-searchbox-form';
    $form['#attributes']['role'] = 'search';

    $uiClass       = 'foldershare-searchbox';
    $submitClass   = $uiClass . '-submit';
    $keywordsClass = $uiClass . '-keywords';

    //
    // Create UI
    // ---------
    // The UI is wrapped in a container annotated with parent attributes.
    // Children of the container include a search field and a submit button.
    $form[$uiClass] = [
      '#type'              => 'container',
      '#weight'            => -100,
      '#name'              => $uiClass,
      '#attributes'        => [
        'class'            => [
          $uiClass,
        ],
      ],

      // Add a search field.
      $keywordsClass       => [
        '#type'            => 'search',
        '#size'            => 15,
        '#name'            => $keywordsClass,
        '#default_value'   => '',
        '#disabled'        => $disabled,
        '#attributes'      => [
          'placeholder'    => $this->t('Search...'),
          // Adding 'results' triggers addition of the magnifying glass
          // icon on Webkit-based browsers (e.g. Safari).
          'results'        => '',
          'aria-label'     => $this->t('Search for a word or phrase in files and folders.'),
          'title'          => $this->t('Search for a word or phrase in files and folders.'),
          'class'          => [
            $keywordsClass,
          ],
        ],
      ],

      // Add a submit button. The button is always hidden and is triggered
      // automatically by a carriage-return on the search field.
      $submitClass         => [
        '#type'            => 'submit',
        '#value'           => $this->t('Search'),
        '#name'            => $submitClass,
        '#disabled'        => $disabled,
        '#attributes'      => [
          'class'          => [
            'hidden',
            $submitClass,
          ],
        ],
      ],
    ];

    //
    // Attach autocomplete
    // -------------------
    // When the Search API is used for the form AND the Search API Autocomplete
    // module is installed, then we have an autocomplete plugin that needs to
    // be attached to the form's keyword field.
    if ($canDoAutocomplete === TRUE) {
      // Find our plugin and hook it in.
      try {
        $storage = \Drupal::entityTypeManager()->getStorage(
          'search_api_autocomplete_search');
        $searchPlugin = $storage->loadBySearchPlugin(
          'UISearchBoxAutocomplete');

        if ($searchPlugin !== NULL && $searchPlugin->status() === TRUE) {
          // Search plugin found and it is enabled.
          $helper = \Drupal::getContainer()->get(
            'search_api_autocomplete.helper');

          if ($helper !== NULL) {
            // Altering the keyword entry element attaches the plugin.
            // A side-effect is that it changes the element type from
            // 'search' to 'text'. If we force it back, then autocomplete
            // fails.
            $helper->alterElement(
              $form[$uiClass][$keywordsClass],
              $searchPlugin);
          }
        }
      }
      catch (\Exception $e) {
        // Cannot find view storage or autocomplete helper. Skip autocomplete.
      }
    }

    return $form;
  }

  /*--------------------------------------------------------------------
   *
   * Form validate
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    // Do nothing.
  }

  /*--------------------------------------------------------------------
   *
   * Form submit
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    //
    // Validate
    // --------
    // If the search module is not enabled or the module's search plugin
    // cannot be found, then abort.
    if (ManageSearch::isSearchAvailable() === FALSE) {
      // No search module. This form should not have been created if there
      // was no search module enabled. Silently do nothing.
      return;
    }

    if (ManageSearch::userHasSearchPermission() === FALSE) {
      // When the user does not have search permission, do not return a
      // search form.
      return;
    }

    //
    // Search
    // ------
    // Redirect to a search results page depending upon the search
    // module in use. Pass keywords in the URL.
    $keywords = $formState->getValue('foldershare-searchbox-keywords');
    if (empty($keywords) === TRUE) {
      // No search keywords. Do nothing.
      return;
    }

    switch (Settings::getPreferredSearchModuleId()) {
      case 'search':
        // Core Search is preferred.
        if (ManageSearch::isCoreSearchAvailable() === TRUE) {
          // Send the search keywords to the core Search plugin.
          $query = [
            'keys' => $keywords,
          ];

          // LEGACY FEATURE. In earlier versions of this module, the search
          // box supported folder-constrained search that only searched through
          // the descendants of the current folder.
          //
          // THIS FEATURE IS NORMALLY DISABLED AND IS NOT RECOMMENDED.
          // The implementation must get a list of all descendant entity IDs,
          // which requires a recursive traversal that can be very slow.
          // The resulting ID list can be very large. That makes the search
          // index query very large. This can overload database query size
          // limits and is always a very slow query for the database to
          // implement.
          if (Settings::getSearchFoldersEnable() === TRUE) {
            // When the search folders constraint is enabled, the search
            // is constrained to the current folder and its descendents.
            // Get current folder entity ID, if any.
            $args = $formState->getBuildInfo()['args'];

            if (empty($args) === FALSE && $args[0] !== '') {
              // Parent entity ID should be the sole argument. Load it.
              // Loading could fail and return a NULL if the ID is bad.
              $parentId = (int) $args[0];
              $parent   = FolderShare::load($parentId);

              if ($parent === NULL ||
                  $parent->isSystemDisabled() === TRUE ||
                  $parent->isSystemHidden() === TRUE) {
                // Unknown parent ID or parent is not showable.
                return;
              }

              $query['parentId'] = $parentId;
            }
          }

          $formState->setRedirect(
            'search.view_' . Constants::SEARCH_PLUGIN,
            [],
            [
              'query' => $query,
            ]
          );
        }
        break;

      case 'search_api':
        // Search API is preferred.
        if (ManageSearch::isSearchAPIConfigured() === TRUE) {
          // Send the search keywords to the Search API module.
          $path  = Settings::getSearchAPISearchPath();
          $style = Settings::getSearchAPISearchURLStyle();
          $id    = Settings::getSearchAPISearchFilterId();

          switch ($style) {
            default:
            case 'query':
              // The query style creates a redirect URL of the form:
              // SEARCHPATH?FILTERID=keywords. For this to work, the
              // search path and filter ID must be non-empty.
              if (empty($path) === FALSE && empty($id) === FALSE) {
                if (preg_match('/\s/', $keywords) === 1) {
                  // The keywords string includes white space. Surround it
                  // in double-quotes.
                  $formState->setRedirectUrl(Url::fromUri(
                    'internal:' . $path . '?' . $id . '="' . $keywords . '"'));
                }
                else {
                  // The keywords string does not include white space.
                  // Skip the double-quotes.
                  $formState->setRedirectUrl(Url::fromUri(
                    'internal:' . $path . '?' . $id . '=' . $keywords));
                }
              }
              break;

            case 'path':
              // The path style creates a redirect URL of the form:
              // SEARCHPATH/keywords where the keywords are URL encoded.
              // For this to work, the search path must be non-empty.
              if (empty($path) === FALSE) {
                $formState->setRedirectUrl(Url::fromUri(
                  'internal:' . $path . '/' . urlencode($keywords)));
              }
              break;
          }
        }
        break;

      default:
        // Unknown or no search provider. Do nothing.
        break;
    }
  }

}
