<?php

namespace Drupal\foldershare\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines the annotation for command plugins.
 *
 * A command is similar to a Drupal action. It has a configuration containing
 * operands for the command, and an execute() function to apply the command
 * to the configuration. Some commands also have configuration forms to
 * prompt for operands.
 *
 * Commands differ from Drupal actions by including extensive annotation
 * that governs how that command appears in menus and under what circumstances
 * the command may be applied. For instance, annotation may indicate if
 * the command applies to files or folders or both, whether it requires a
 * selection and if that selection can include more than one item, and
 * what access permissions the user must have on the parent folder and on
 * the selection.
 *
 * Annotation for a command can be broken down into groups of information:
 *
 * - Identification:
 *   - the unique machine name for the plugin.
 *
 * - User interface:
 *   - the labels for the plugin.
 *   - the user interface category and weight for presenting the command.
 *
 * - Constraints and access controls:
 *   - the parent folder constraints and access controls required, if needed.
 *   - the selection constraints and access controls required, if needed.
 *   - the destination constraints and access controls required, if needed.
 *   - any special handling needed.
 *
 * There are several plugin labels that may be specified. Each is used in
 * a different context:
 *
 * - "label" is a generic name for the command, such as "Edit" or "Delete".
 *   This label may be used in error messages, such as "The Delete command
 *   requires a selection."
 *
 * - "menuNameDefault" is the command name used in menus when no better text
 *   is available in "menuName". The default text is primarily used when the
 *   menu item is disabled. If not given, this defaults to the value of "label".
 *
 * - "menuName" is the command name to use in menus. This is primarily used
 *   for selectable menu items and the text may include the "@operand" marker,
 *   which will be replaced with the name of an item or a kind, such as
 *   "Edit @operand...". If not given, this defaults to the value of
 *   "menuNameDefault".
 *
 * "label", "menuNameDefault", and "menuName" should all use
 * title-case except for small connecting words like "of", "in", and "and".
 *
 * An optional "@operand" is replaced with text that varies depending upon
 * use.  Possibilities include:
 *
 * - Replaced with a singular name of the kind of operand, such as
 *   "Delete file" or "Delete folder". If the context is the current item,
 *   the word "this" may be inserted, such as "Delete this file".
 *
 * - Replaced with a plural name of the kind for all operands, such as
 *   "Delete files" or "Delete folders".
 *
 * - Replaced with a plural "items" if the kinds of the operands is mixed,
 *   such as "Delete items".
 *
 * - Replaced with the quoted singular name of the operand when there is
 *   just one item (typically for form titles), such as "Delete "Bob's folder"".
 *
 * The plugin namespace for commands is "Plugin\FolderShareCommand".
 *
 * @ingroup foldershare
 *
 * @Annotation
 */
class FolderShareCommand extends Plugin {

  /*--------------------------------------------------------------------
   *
   * Identification.
   *
   *--------------------------------------------------------------------*/
  /**
   * The unique machine-name plugin ID.
   *
   * The value is taken from the plugin's 'id' annotation.
   *
   * The ID must be a unique string used to identify the plugin. Often it
   * is closely related to a module namespace and class name.
   *
   * @var string
   */
  public $id = '';

  /*--------------------------------------------------------------------
   *
   * User interface.
   *
   *--------------------------------------------------------------------*/
  /**
   * The generic command label for user interfaces.
   *
   * The label is used by user interface code for buttons and error messages.
   * The value is required.
   *
   * The text should be translated and use title-case where each word is
   * capitalized, except for small connecting words like "of" or "in".
   *
   * Examples:
   * - "Change Owner".
   * - "Delete".
   * - "Edit".
   * - "Rename".
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label = '';

  /**
   * The name for disabled user interface menus.
   *
   * The menu name is used for user interface code to create a menu item for
   * the command when the command is disabled and not available to the user.
   * If no menu name is given, the value defaults to the value of the
   * 'label' field.
   *
   * The text should be translated and use title-case where each word is
   * capitalized, except for small connecting words like "of" or "in".
   *
   * Examples:
   * - "Change Owner...".
   * - "Delete...".
   * - "Edit...".
   * - "Rename...".
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $menuNameDefault = '';

  /**
   * The name for enabled user interface menus.
   *
   * The menu name is used for user interface code to create a menu item for
   * the command when the command is enabled and available to the user. If
   * no menu name is given, the value defaults to the value of the
   * 'menuNameDefault' field.
   *
   * The text should be translated and use title-case where each word is
   * capitalized, except for small connecting words like "of" or "in".
   *
   * The text may include "@operand" to mark where the name of an item or
   * item kind should be inserted.
   *
   * Examples:
   * - "Change Owner of @operand...".
   * - "Delete @operand...".
   * - "Edit @operand...".
   * - "Rename @operand...".
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $menuName = '';

  /**
   * A brief description of the command.
   *
   * @var string
   */
  public $description = '';

  /**
   * The command's category within user interfaces.
   *
   * The category gives the name of a group into which the command should be
   * sorted when it is presented in a menu. Category names must be lower case.
   *
   * The module defines a set of well-known categories that may be used.
   * Any category name not on this list is appended to the end of menus
   * based upon these categories.
   *
   * Well-known categories are (in order):
   * - "open".
   * - "import & export".
   * - "close".
   * - "edit".
   * - "delete".
   * - "copy & move".
   * - "save".
   * - "archive".
   * - "message".
   * - "settings".
   * - "administer".
   *
   * @var string
   *
   * @see \Drupal\Core\Plugin\CategorizingPluginManagerTrait
   * @see \Drupal\Component\Plugin\CategorizingPluginManagerInterface
   */
  public $category = '';

  /**
   * The commands weight among other commands in the same category.
   *
   * The value is taken from the plugin's 'weight' annotation and should
   * be a positive or negative integer.
   *
   * Weights are used to sort commands within a category before they
   * are presented within a menu, toobar of buttons, etc. Higher weights
   * are listed later in the category.
   *
   * @var int
   */
  public $weight = 0;

  /*--------------------------------------------------------------------
   *
   * Constraints and access controls.
   *
   *--------------------------------------------------------------------*/
  /**
   * The command's user constraints.
   *
   * Defined by the 'userConstraints' annotation, this value is an array
   * of user type requirements:
   * - 'any' (default).
   * - 'anonymous'.
   * - 'authenticated'.
   * - 'adminpermission'.
   * - 'noadminpermission'.
   * - 'authorpermission'.
   * - 'sharepermission'.
   * - 'sharepublicpermission'.
   * - 'viewpermission'.
   *
   * The constraint is met if any choice is met (i.e. they are ORed together).
   *
   * @var array
   */
  public $userConstraints = NULL;

  /**
   * The menu command's parent constraints.
   *
   * Defined by the 'parentConstraints' annotation, this value is an
   * associative array with keys:
   * - 'access'.
   * - 'filenameextensions'.
   * - 'kinds'.
   * - 'mimetypes'.
   * - 'ownership'.
   *
   * Each key's value is an optional array that lists values the parent
   * may have. If the array is not given or empty, the default is used.
   *
   * An entity is considered valid for use with the menu command if
   * it meets all constraints ANDed together, where accepted values within a
   * constraint are ORed together. For instance, let a menu command's 'kinds'
   * list include ['file', 'image'] and its MIME type list include
   * ['image/png', 'image/jpeg']. An entity meets these constraints if:
   * - (kind is 'file' OR 'image) AND
   * - (MIME type is 'image/png' OR 'image/jpeg').
   *
   * <B>Kinds</B><BR>
   * The 'kinds' field lists the entity kinds supported:
   * - 'any' (default).
   * - 'rootlist'.
   * - Any kind supported by FolderShare (e.g. 'file', 'folder').
   *
   * <B>MIME types</B><BR>
   * The 'mimetypes' field lists entity MIME types supported:
   * - 'any' (default).
   * - MAJOR/MINOR (a specific major and minor type).
   * - MAJOR/* (a specific major, but any minor).
   *
   * MINOR parts may include a list of variants, separated by "+".
   *
   * MIME type constraints are only applied for entities that are not
   * folders or rootlists (e.g. they are files, images, media, or objects).
   * Contraints are always checked with lower case forms of MIME types.
   *
   * <B>Filename extensions</B><BR>
   * The 'filenameextensions' field lists filename extensions supported:
   * - 'any' or empty list (default = any extension).
   * - 'none'.
   * - .EXT (a specific extension, WITH a leading dot).
   *
   * <B>Ownership</B><BR>
   * The 'ownership' field lists the ownership states supported:
   * - 'any' (default).
   * - 'ownedbyanonymous'.
   * - 'ownedbyanother'.
   * - 'ownedbyuser'.
   * - 'sharedbyuser'.
   * - 'sharedwithanonymoustoview'.
   * - 'sharedwithanonymoustoauthor'.
   * - 'sharedwithusertoview'.
   * - 'sharedwithusertoauthor'.
   *
   * <B>Access</B><BR>
   * The 'access' field names ONE access operation the entity must support:
   * - 'chown'.
   * - 'create'.
   * - 'delete'.
   * - 'share'.
   * - 'update'.
   * - 'view' (default).
   *
   * @var array
   */
  public $parentConstraints = NULL;

  /**
   * The command's selection constraints, if any.
   *
   * Defined by the 'selectionConstraints' annotation, this value is an
   * associative array with keys:
   * - 'access'.
   * - 'filenameextensions'.
   * - 'kinds'.
   * - 'mimetypes'.
   * - 'ownership'.
   * - 'types'.
   *
   * Each key's value is an optional array that lists values the selected
   * entities may have. If the array is not given or empty, the default is used.
   *
   * An entity is considered valid for use with the menu command if
   * it meets all constraints ANDed together, where accepted values within a
   * constraint are ORed together. For instance, let a menu command's 'kinds'
   * list include ['file', 'image'] and its MIME type list include
   * ['image/png', 'image/jpeg']. An entity meets these constraints if:
   * - (kind is 'file' OR 'image) AND
   * - (MIME type is 'image/png' OR 'image/jpeg').
   *
   * <B>Selection types</B><BR>
   * The 'types' field lists types of selection supported:
   * - 'none' (default).
   * - 'parent'.
   * - 'one'.
   * - 'many'.
   *
   * <B>Kinds</B><BR>
   * The 'kinds' field lists the entity kinds supported:
   * - 'any' (default).
   * - Any kind supported by FolderShare (e.g. 'file', 'folder').
   *
   * The 'rootlist' kind available for other constraints is not available for
   * selections.
   *
   * <B>MIME types</B><BR>
   * The 'mimetypes' field lists entity MIME types supported:
   * - 'any' (default).
   * - MAJOR/MINOR (a specific major and minor type).
   * - MAJOR/* (a specific major, but any minor).
   *
   * MINOR parts may include a list of variants, separated by "+".
   *
   * MIME type constraints are only applied for entities that are not
   * folders or rootlists (e.g. they are files, images, media, or objects).
   * Contraints are always checked with lower case forms of MIME types.
   *
   * <B>Filename extensions</B><BR>
   * The 'filenameextensions' field lists filename extensions supported:
   * - 'any' or empty list (default = any extension).
   * - 'none'.
   * - .EXT (a specific extension, WITH a leading dot).
   *
   * <B>Ownership</B><BR>
   * The 'ownership' field lists the ownership states supported:
   * - 'any' (default).
   * - 'ownedbyanonymous'.
   * - 'ownedbyanother'.
   * - 'ownedbyuser'.
   * - 'sharedbyuser'.
   * - 'sharedwithanonymoustoview'.
   * - 'sharedwithanonymoustoauthor'.
   * - 'sharedwithusertoview'.
   * - 'sharedwithusertoauthor'.
   *
   * <B>Access</B><BR>
   * The 'access' field names ONE access operation every selected entity
   * must support:
   * - 'chown'.
   * - 'create'.
   * - 'delete'.
   * - 'share'.
   * - 'update'.
   * - 'view' (default).
   *
   * @var array
   */
  public $selectionConstraints = NULL;

  /**
   * The command's destination constraints, if any.
   *
   * Defined by the 'destinationConstraints' annotation, this value is an
   * associative array with keys:
   * - 'access'.
   * - 'filenameextensions'.
   * - 'kinds'.
   * - 'mimetypes'.
   * - 'ownership'.
   *
   * Each key's value is an optional array that lists values the destination
   * may have. If the array is not given or empty, the default is used.
   *
   * An entity is considered valid for use with the menu command if
   * it meets all constraints ANDed together, where accepted values within a
   * constraint are ORed together. For instance, let a menu command's 'kinds'
   * list include ['file', 'image'] and its MIME type list include
   * ['image/png', 'image/jpeg']. An entity meets these constraints if:
   * - (kind is 'file' OR 'image) AND
   * - (MIME type is 'image/png' OR 'image/jpeg').
   *
   * <B>Kinds</B><BR>
   * The 'kinds' field lists the entity kinds supported:
   * - 'none' (default).
   * - 'any'.
   * - 'rootlist'.
   * - Any kind supported by FolderShare (e.g. 'file', 'folder').
   *
   * The 'none' kind indicates the command does not require a destination.
   *
   * <B>MIME types</B><BR>
   * The 'mimetypes' field lists entity MIME types supported:
   * - 'any' (default).
   * - MAJOR/MINOR (a specific major and minor type).
   * - MAJOR/* (a specific major, but any minor).
   *
   * MINOR parts may include a list of variants, separated by "+".
   *
   * MIME type constraints are only applied for entities that are not
   * folders or rootlists (e.g. they are files, images, media, or objects).
   * Contraints are always checked with lower case forms of MIME types.
   *
   * <B>Filename extensions</B><BR>
   * The 'filenameextensions' field lists filename extensions supported:
   * - 'any' or empty list (default = any extension).
   * - 'none'.
   * - .EXT (a specific extension, WITH a leading dot).
   *
   * <B>Ownership</B><BR>
   * The 'ownership' field lists the ownership states supported:
   * - 'any' (default).
   * - 'ownedbyanonymous'.
   * - 'ownedbyanother'.
   * - 'ownedbyuser'.
   * - 'sharedbyuser'.
   * - 'sharedwithanonymoustoview'.
   * - 'sharedwithanonymoustoauthor'.
   * - 'sharedwithusertoview'.
   * - 'sharedwithusertoauthor'.
   *
   * <B>Access</B><BR>
   * The 'access' field names ONE access operation the entity must support:
   * - 'chown'.
   * - 'create'.
   * - 'delete'.
   * - 'share'.
   * - 'update' (default).
   * - 'view'.
   *
   * @var array
   */
  public $destinationConstraints = NULL;

  /**
   * The command's special needs, if any.
   *
   * Defined by the 'specialHandling' annotation, this value is an array
   * with known values:
   * - 'copy': the command copies items on a row drag-and-drop.
   * - 'move': the command moves items on a row drag-and-drop.
   * - 'upload': the command uploads files, including on a file drag-and-drop.
   *
   * After parent, selection, and destination constraints, there should be
   * at most one menu command that is marked with each of the above so that
   * there is no ambiguity about what command to execute for a drag-and-drop
   * for file upload. If there is ambiguity because there is more than one
   * menu command marked, then the one with the lowest weight will be used.
   *
   * @var array
   */
  public $specialHandling = NULL;

}
