<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Provides the base class for command plugins that delete files or folders.
 *
 * The command deletes all selected entities. Deletion recurses and
 * deletes all folder content as well.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to delete.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_release_share",
 *  label           = @Translation("Release Share"),
 *  menuNameDefault = @Translation("Release Share..."),
 *  menuName        = @Translation("Release Share..."),
 *  description     = @Translation("Release selected shared files and folders."),
 *  category        = "settings",
 *  weight          = 0,
 *  userConstraints = {
 *    "authenticated",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *    },
 *    "access"  = "update",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "ownership" = {
 *      "sharedwithusertoview",
 *      "sharedwithusertoauthor",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class ReleaseShare extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    $ids = $this->getSelectionIds();
    $kindsAndIds = FolderShare::findKindsForIds($ids);

    if (count($ids) === 1) {
      $kinds = array_keys($kindsAndIds);
      return $this->t(
        'Once released, this @kind will no longer be accessible by you.',
        [
          '@kind' => FolderShare::translateKind($kinds[0]),
        ]);
    }

    return $this->t(
      'Once released, these @kinds will no longer be accessible by you.',
      [
        '@kinds' => FolderShare::translateKinds('items'),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    $ids = $this->getSelectionIds();

    if (count($ids) === 1) {
      return $this->t(
        'Release shared access to "@name"?',
        [
          '@name' => FolderShare::findNameForId($ids[0]),
        ]);
    }

    $kinds = FolderShare::translateKindsForIds($ids);
    return $this->t(
      "Release shared access to @kinds?",
      [
        '@kinds' => $kinds['translation'],
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Release');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    // The command wrapper provides form basics:
    // - Attached libraries.
    // - Page title (if not an AJAX dialog).
    // - Description (from ::getDescription()).
    // - Submit buttion (labeled with ::getSubmitButtonName()).
    // - Cancel button (if AJAX dialog).
    $form['#attributes']['class'][] = 'confirmation';
    $form['#attributes']['class'][] = 'foldershare-release-share';
    $form['#theme'] = 'confirm_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState) {
    // Nothing to do.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    $this->execute();
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $ids = $this->getSelectionIds();

    FolderShare::unshareMultiple($ids, $this->getCurrentUser()->id(), '');

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $count = count($ids);
      if ($count <= 1) {
        $message = $this->t("Sharing for the item has been released.");
      }
      else {
        $message = $this->t(
          "Sharing for @count items has been released.",
          [
            '@count' => $count,
          ]);
      }
      $this->getMessenger()->addStatus($message);
    }
  }

}
