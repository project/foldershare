<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Defines a command plugin to unarchive (uncompress) files and folders.
 *
 * The command extracts all contents of a ZIP archive and adds them to
 * the current folder.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to Archive.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_unarchive",
 *  label           = @Translation("Uncompress"),
 *  menuNameDefault = @Translation("Uncompress"),
 *  menuName        = @Translation("Uncompress"),
 *  description     = @Translation("Uncompress a selected ZIP file into the current location. (This command is in development and not recommended for production sites.)"),
 *  category        = "archive",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *    },
 *    "kinds"   = {
 *      "file",
 *    },
 *    "filenameextensions" = {
 *      ".zip",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class Uncompress extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $selectionIds = $this->getSelectionIds();
    $item = FolderShare::load(reset($selectionIds));

    try {
      $item->unarchiveFromZip();
    }
    catch (\Exception $e) {
      $this->getMessenger()->addError($e->getMessage());
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $this->getMessenger()->addStatus(
        $this->t(
          "The @kind '@name' has been uncompressed.",
          [
            '@kind' => FolderShare::translateKind($item->getKind()),
            '@name' => $item->getName(),
          ]));
    }
  }

}
