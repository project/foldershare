<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Settings;

/**
 * Defines a command plugin to duplicate a file or folder.
 *
 * The command copies all selected entities and adds them back into the
 * same parent folder or root folder list. Duplication recurses through
 * all folder content as well.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to duplicate.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_duplicate",
 *  label           = @Translation("Duplicate"),
 *  menuNameDefault = @Translation("Duplicate"),
 *  menuName        = @Translation("Duplicate"),
 *  description     = @Translation("Duplicate selected files and folders to create a copy in the current location."),
 *  category        = "copy & move",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class Duplicate extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $ids = $this->getSelectionIds();
    try {
      FolderShare::duplicateMultiple($ids);
    }
    catch (\Exception $e) {
      $this->getMessenger()->addError($e->getMessage());
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $count = count($ids);
      if ($count <= 1) {
        $message = $this->t("The item has been duplicated.");
      }
      else {
        $message = $this->t(
          "@count items have been duplicated.",
          [
            '@count' => $count,
          ]);
      }
      $this->getMessenger()->addStatus($message);
    }
  }

}
