<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Settings;

/**
 * Defines a command plugin to create a new object.
 *
 * The command creates a new object in the current parent folder, if any.
 * If there is no parent folder, the command creates a new root object.
 * The new object has a default name.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_new_object",
 *  label           = @Translation("New Object"),
 *  menuNameDefault = @Translation("New Object"),
 *  menuName        = @Translation("New Object"),
 *  description     = @Translation("Create a new object."),
 *  category        = "open",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "none",
 *    },
 *  },
 * )
 */
class NewObject extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {

    $parent = $this->getParent();
    try {
      if ($parent === NULL) {
        $newObject = FolderShare::createRootObject('');
      }
      else {
        $newObject = $parent->createObject('');
      }
    }
    catch (\Exception $e) {
      throw $e;
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $this->getMessenger()->addStatus(
        $this->t(
          "A @kind named '@name' has been created.",
          [
            '@kind' => FolderShare::translateKind($newObject->getKind()),
            '@name' => $newObject->getName(),
          ]));
    }
  }

}
