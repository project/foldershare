<?php

namespace Drupal\foldershare\Commands;

use Symfony\Component\Console\Input\InputOption;

use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\State\StateInterface;
use Drupal\Core\Entity\EntityTypeManager;

use Drush\Commands\DrushCommands;

use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareScheduledTask;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\LimitUtilities;

/**
 * Defines Drush site administrator commands for the FolderShare module.
 *
 * In addition to this file, drush.services.yml lists this file, and
 * composer.json provides the name of the services file to use.
 *
 * See these files for an example of injecting Drupal services:
 *   - http://cgit.drupalcode.org/devel/tree/src/Commands/DevelCommands.php
 *   - http://cgit.drupalcode.org/devel/tree/drush.services.yml
 */
final class FolderShareCommands extends DrushCommands {

  /*---------------------------------------------------------------------
   *
   * Constants.
   *
   *---------------------------------------------------------------------*/
  /**
   * The return code for an error.
   *
   * @var int
   */
  private const RETURN_ERROR = 1;

  /**
   * The return code for success.
   *
   * @var int
   */
  private const RETURN_OK = 0;

  /*---------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *---------------------------------------------------------------------*/
  /**
   * Access to system state, set at construction time.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  private $state;

  /**
   * Access the list of installed modules, set at construction time.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  private $moduleList;

  /**
   * Access User entity storage, set at construction time.
   *
   * @var \Drupal\Core\Entity\EntityStorageInterface
   */
  private $userStorage;

  /*---------------------------------------------------------------------
   *
   * Construct.
   *
   *---------------------------------------------------------------------*/
  /**
   * Constructs a Drush command set.
   *
   * @param \Drupal\Core\State\StateInterface $state
   *   The system state.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleList
   *   The list of installed modules.
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(
    StateInterface $state,
    ModuleExtensionList $moduleList,
    EntityTypeManager $entityTypeManager) {

    parent::__construct();
    $this->state       = $state;
    $this->moduleList  = $moduleList;
    $this->userStorage = $entityTypeManager->getStorage('user');
  }

  /*---------------------------------------------------------------------
   *
   * Utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Shows an entity table for the indicated entities.
   *
   * @param int[] $ids
   *   An array of integer entity IDs.
   */
  private function showEntityTable(array $ids) {
    $header = [
      dt('Owner'),
      dt('Entity ID'),
      dt('Path'),
    ];
    $data = [];

    foreach ($ids as $id) {
      $item = FolderShare::load($id);
      if ($item === NULL) {
        // The item does not exist.
        continue;
      }

      $ownerId = $item->getOwnerId();
      $path = $item->getPath();
      if ($item->isFolder() === TRUE) {
        $path .= '/';
      }

      $u = $this->userStorage->load($ownerId);
      if ($u === NULL) {
        $ownerName = dt('Unknown (@id)', ['@id' => $ownerId]);
      }
      else {
        $ownerName = $u->getDisplayName();
      }

      $data[] = [
        $ownerName,
        $id,
        $path,
      ];
    }

    $this->io()->table($header, $data);
  }

  /*---------------------------------------------------------------------
   *
   * Fsck.
   *
   *---------------------------------------------------------------------*/
  /**
   * Checks the file system.
   *
   * @param string[] $options
   *   Use --fix to fix problems.
   *
   * @command foldershare:fsck
   * @usage drush foldershare::fsck [--fix]
   *   Check the file system.
   * @option fix Fix the file system.
   *
   * @validate-module-enabled foldershare
   */
  public function fsck(array $options) {
    $fix = (empty($options['fix']) === FALSE);

    // Intro.
    $this->io()->section('foldershare:fsck');
    if ($fix === TRUE) {
      $this->io()->text(dt('Fixing the file system...'));
    }
    else {
      $this->io()->text(dt('Checking the file system...'));
    }

    FolderShareScheduledTask::setTaskExecutionEnabled(FALSE);

    if ($fix === TRUE) {
      $originalMode = $this->state->get('system.maintenance_mode');
      if ($originalMode !== 1) {
        $this->io()->text(dt('  Entering maintenance mode.'));
        $this->state->set('system.maintenance_mode', 1);
      }

      $this->io()->text('');
      $wereFixed = FolderShare::fsck(TRUE);

      if ($originalMode !== 1) {
        $this->io()->text(dt('  Exiting maintenance mode.'));
        $this->state->set('system.maintenance_mode', $originalMode);
      }

      $this->io()->text('');
      if ($wereFixed === TRUE) {
        $this->io()->text(dt('Repairs were made.'));
      }
      else {
        $this->io()->text(dt('No repairs were needed or made.'));
      }
    }
    else {
      $this->io()->text('');
      $needToBeFixed = FolderShare::fsck(FALSE);

      if ($needToBeFixed === TRUE) {
        $this->io()->error(dt("Repairs need to be made. To make repairs, run:\n  drush foldershare:fsck --fix"));
        return self::RETURN_ERROR;
      }
      else {
        $this->io()->text(dt('No repairs are needed.'));
      }
    }

    return self::RETURN_OK;
  }

  /*---------------------------------------------------------------------
   *
   * Delete.
   *
   *---------------------------------------------------------------------*/
  /**
   * Deletes all FolderShare entities.
   *
   * @command foldershare:deleteall
   * @usage drush foldershare:deleteall
   *   Delete all FolderShare entities.
   *
   * @validate-module-enabled foldershare
   */
  public function deleteAll() {
    // Intro.
    $this->io()->section('foldershare:deleteall');
    $this->io()->text(dt('Delete all FolderShare entities.'));

    // Confirm.
    $answer = $this->io()->confirm(dt('Are you sure?'), FALSE);
    if ($answer === FALSE) {
      $this->io()->text(dt('Canceled.'));
      return 1;
    }

    // Execute.
    $enableActivityLoggingWhenDone = FALSE;
    if (ManageLog::isActivityLoggingEnabled() === TRUE) {
      Settings::setActivityLogEnable(FALSE);
      $enableActivityLoggingWhenDone = TRUE;
    }

    $this->io()->text(dt('Deleting...'));

    FolderShare::deleteAll(FolderShare::ANY_USER_ID);

    if ($enableActivityLoggingWhenDone === TRUE) {
      Settings::setActivityLogEnable(TRUE);
      ManageLog::activity(dt('Deleted all files and folders.'));
    }

    return self::RETURN_OK;
  }

  /*---------------------------------------------------------------------
   *
   * Root entity locks.
   *
   *---------------------------------------------------------------------*/
  /**
   * List all FolderShare locks.
   *
   * @command foldershare:locks
   * @option unlockall Unlock all locks.
   * @usage drush foldershare::locks
   *   List all FolderShare locks.
   *
   * @validate-module-enabled foldershare
   */
  public function locks(
    array $options = [
      'unlockall' => FALSE,
    ]) {

    $unlockall = (empty($options['unlockall']) === FALSE);

    // Intro.
    $this->io()->section('foldershare:locks');
    if ($unlockall === TRUE) {
      $this->io()->text(dt('Unlock all FolderShare root entities.'));
    }
    else {
      $this->io()->text(dt('List all FolderShare locks.'));
    }
    $this->io()->text('');

    // Get a list of all locks.
    $rootIds = FolderShare::findAllRootItemIds();
    $lockedRootIds = [];
    foreach ($rootIds as $rootId) {
      if (FolderShare::isRootOperationLockAvailable($rootId) === FALSE) {
        $lockedRootIds[] = $rootId;
      }
    }

    // Execute.
    if ($unlockall === TRUE) {
      foreach ($lockedRootIds as $id) {
        FolderShare::releaseRootOperationLock($id);
      }
    }

    $this->showEntityTable($lockedRootIds);
    return self::RETURN_OK;
  }

  /*---------------------------------------------------------------------
   *
   * Scheduled tasks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Lists all FolderShare tasks.
   *
   * @command foldershare:tasks
   * @option delete Delete a specific task.
   * @option deleteall Delete all tasks.
   * @option finish Finish all tasks.
   * @option run Run tasks ready to run.
   * @usage drush foldershare::tasks
   *   List all FolderShare tasks.
   * @usage drush foldershare::tasks --delete=123
   *   Delete waiting tasks 123, then list all FolderShare tasks.
   * @usage drush foldershare::tasks --deletell
   *   Delete all waiting tasks.
   * @usage drush foldershare::tasks --delete=all
   *   Delete all waiting tasks.
   *
   * @validate-module-enabled foldershare
   */
  public function tasks(
    array $options = [
      'delete'    => InputOption::VALUE_REQUIRED,
      'deleteall' => FALSE,
      'run'       => FALSE,
      'finish'    => FALSE,
    ]) {

    // Get and validate options.
    $delete    = (empty($options['delete']) === FALSE);
    $deleteall = (empty($options['deleteall']) === FALSE);
    $finish    = (empty($options['finish']) === FALSE);
    $run       = (empty($options['run']) === FALSE);

    $deleteid = (-1);
    if ($delete === TRUE) {
      $deleteid = $options['delete'];
      if (mb_strtolower($deleteid) === 'all') {
        $deleteall = TRUE;
        $delete = FALSE;
      }
      else {
        $deleteid = intval($deleteid);
      }
    }

    if ($delete === TRUE && $deleteall === TRUE) {
      $this->io()->error(dt('Use just one of --delete or --deleteall.'));
      return self::RETURN_ERROR;
    }

    if ($delete === TRUE && $finish === TRUE) {
      $this->io()->error(dt('Use just one of --delete or --finish.'));
      return self::RETURN_ERROR;
    }

    if ($deleteall === TRUE && $finish === TRUE) {
      $this->io()->error(dt('Use just one of --deleteall or --finish.'));
      return self::RETURN_ERROR;
    }

    if ($run === TRUE && $finish === TRUE) {
      $this->io()->error(dt('Use just one of --run or --finish.'));
      return self::RETURN_ERROR;
    }

    // Intro.
    $this->io()->section('foldershare:tasks');
    if ($delete === TRUE) {
      if ($run === TRUE) {
        $this->io()->text(dt(
          'Run waiting FolderShare tasks then delete task @id.',
          [
            '@id' => $deleteid,
          ]));
      }
      else {
        $this->io()->text(dt(
          'Delete FolderShare task @id.',
          [
            '@id' => $deleteid,
          ]));
      }
    }
    elseif ($deleteall === TRUE) {
      if ($run === TRUE) {
        $this->io()->text(dt('Run waiting FolderShare tasks then delete all remaining tasks.'));
      }
      else {
        $this->io()->text(dt('Delete all FolderShare tasks.'));
      }
    }
    elseif ($finish === TRUE) {
      $this->io()->text(dt('Finish running all waiting FolderShare tasks then list all remaining tasks.'));
    }
    elseif ($run === TRUE) {
      $this->io()->text(dt('Run waiting FolderShare tasks then list all remaining tasks.'));
    }
    else {
      $this->io()->text(dt('List all FolderShare tasks.'));
    }
    $this->io()->text('');

    // Get the number of tasks.
    $nTasks = FolderShareScheduledTask::findNumberOfTasks();

    // Execute finish.
    $returnCode = self::RETURN_OK;
    if ($finish === TRUE) {
      if ($nTasks <= 0) {
        $this->io()->text(dt('There are no tasks to finish.'));
        return self::RETURN_ERROR;
      }

      $this->io()->text(dt('Finishing tasks...'));

      FolderShareScheduledTask::setTaskExecutionEnabled(TRUE);

      $status = FolderShareScheduledTask::finishTasks();
      if ($status !== TRUE) {
        $this->io()->error(dt(
          "One or more tasks could not be finished. These tasks may be working on\nlarge activities that require more time or memory, they may be blocked\nbecause they need a resource in use elsewhere, or they may be crashing.\nPlease check the site log for possible error messages. If necessary,\nyou can delete a problematic task using:\n  drush foldershare:tasks --delete=TASKID"));
        $returnCode = self::RETURN_ERROR;
      }
      $this->io()->text('');

      // Fall through to list remaining tasks.
    }

    // Execute run.
    if ($nTasks > 0 && $run === TRUE) {
      $this->io()->text(dt('Running tasks...'));

      LimitUtilities::setUnlimited();
      FolderShareScheduledTask::setTaskExecutionEnabled(TRUE);

      FolderShareScheduledTask::executeTasks(time());
      $this->io()->text('');

      // Fall through to list remaining tasks.
    }

    // Execute delete.
    if ($delete === TRUE) {
      $task = FolderShareScheduledTask::load($deleteid);
      if ($task === NULL) {
        $this->io()->error(dt(' Unknown task "@id".', ['@id' => $deleteid]));
        return self::RETURN_ERROR;
      }

      // Confirm.
      $this->io()->text([
        dt('Deleting a task abruptly ends an operation in progress.'),
        dt('This can corrupt the file system.'),
      ]);

      $answer = $this->io()->confirm(dt('Are you sure?'), FALSE);
      if ($answer === FALSE) {
        $this->io()->text(dt('Canceled.'));
        return self::RETURN_ERROR;
      }

      $task->delete();
      $this->io()->text([
        dt('Done. You should run "drush foldershare:fsck --fix" immediately to'),
        dt('fix file system corruption.'),
      ]);

      return self::RETURN_OK;
    }

    // Execute delete all.
    if ($deleteall === TRUE) {
      if ($nTasks <= 0) {
        $this->io()->text(dt('There are no tasks to delete.'));
        return self::RETURN_OK;
      }

      $this->io()->text([
        dt('Deleting tasks abruptly ends operations in progress.'),
        dt('This can corrupt the file system.'),
      ]);

      $answer = $this->io()->confirm(dt('Are you sure?'), FALSE);
      if ($answer === FALSE) {
        $this->io()->text(dt('Canceled.'));
        return self::RETURN_ERROR;
      }

      FolderShareScheduledTask::deleteAllTasks();
      $this->io()->text([
        dt('Done. You should run "drush foldershare:fsck --fix" immediately to'),
        dt('fix file system corruption.'),
      ]);

      return self::RETURN_OK;
    }

    // Execute list tasks.
    $taskIds = FolderShareScheduledTask::findTaskIds();
    $nTasks = count($taskIds);

    $header = [
      dt('Task ID'),
      dt('Requester'),
      dt('Flags'),
      dt('Started / Run next'),
      dt('Comments / Callback'),
    ];
    $data = [];

    if ($nTasks === 0) {
      $data[] = [
        dt('No tasks.'),
        '',
        '',
        '',
        '',
      ];
    }
    else {
      foreach ($taskIds as $id) {
        $task = FolderShareScheduledTask::load($id);
        if ($task === NULL) {
          // Task does not exist.
          continue;
        }

        $requester     = $task->getRequester();
        $comments      = $task->getComments();
        $startTime     = date('Y-m-d H:i:s', intval($task->getStartedTime()));
        $callback      = $task->getCallback();
        $scheduledTime = date('Y-m-d H:i:s', intval($task->getScheduledTime()));
        $flags         = ($task->isRepeating() === TRUE) ? 'repeating' : '';

        $u = $this->userStorage->load($requester);
        if ($u === NULL) {
          $requesterName = sprintf('%s (%d)', dt('unknown'), $requester);
        }
        else {
          $requesterName = sprintf('%s (%d)', $u->getDisplayName(), $requester);
        }

        $data[] = [
          $id,
          $requesterName,
          $flags,
          $startTime,
          $comments,
        ];
        $data[] = [
          '',
          '',
          '',
          $scheduledTime,
          $callback,
        ];

        // Additional task information available:
        // - $task->getCallback();
        // - $task->getParameters();
        // - $task->getAccumulatedExecutionTime();
        // - date('Y-m-d H:i:s', intval($task->getCreatedTime()));
        // - date('Y-m-d H:i:s', intval($task->getScheduledTime()));
      }
    }

    $this->io()->table($header, $data);
    return $returnCode;
  }

  /*---------------------------------------------------------------------
   *
   * Version.
   *
   *---------------------------------------------------------------------*/
  /**
   * Shows FolderShare version number.
   *
   * @command foldershare:version
   * @usage drush foldershare::version
   *   Shows FolderShare version number.
   *
   * @validate-module-enabled foldershare
   */
  public function version() {
    try {
      $moduleInfo = $this->moduleList->getExtensionInfo('foldershare');
      $version = $moduleInfo['version'];
      $this->io()->text("FolderShare $version");
    }
    catch (\Exception $e) {
      $this->io()->text(dt('FolderShare unknown version'));
    }

    return self::RETURN_OK;
  }

}
