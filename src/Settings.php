<?php

namespace Drupal\foldershare;

use Drupal;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;

/**
 * Defines functions to get/set the module's configuration.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * The module's settings (configuration) schema is defined in
 * config/schema/MODULE.settings.yml, with install-time defaults set
 * in config/install/MODULE.settings.yml. The defaults provided
 * in this module match those in the YML files.
 *
 * @internal
 * Get/set of a module configuration setting requires two strings: (1) the
 * name of the module's configuration, and (2) the name of the setting.
 * When used frequently in module code, these strings invite typos that
 * can cause the wrong setting to be set or retrieved.
 *
 * This class centralizes get/set for settings and turns all settings
 * accesses into class method calls. The PHP parser can then catch typos
 * in method calls and report them as errors.
 *
 * This class also centralizes and makes accessible the default values
 * for all settings.
 * @endinternal
 *
 * @ingroup foldershare
 */
class Settings
{
    /**
     * Returns the current file storage scheme.
     *
     * Some sample known values are:
     *
     * - 'public' = store files in the site's public file system.
     * - 'private' = store files in the site's private file system.
     *
     * @return string
     *   Returns the file storage scheme
     *
     * @see ::getFileSchemeDefault()
     * @see ::setFileScheme()
     */
    public static function getFileScheme()
    {
        $config = Drupal::config(Constants::SETTINGS);
        $scheme = $config->get('file_scheme');
        $streamWrapper = (!$scheme) ? NULL : FileUtilities::getStreamWrapper($scheme);

        // Return the default file scheme if no scheme is configured,
        // or if the configured scheme doesn't map to a supported stream wrapper that exists.
        if (!$streamWrapper || !FileUtilities::isStreamWrapperTypeSupported($streamWrapper->getType())) {
            return self::getFileSchemeDefault();
        }

        return $scheme;
    }

    /**
     * This function will return the raw "file_scheme" value found in the settings configuration, without any post-processing.
     * @return mixed
     */
    public static function getRawFileScheme() {
        $config = Drupal::config(Constants::SETTINGS);
        return $config->get('file_scheme');
    }

    /**
     * Returns the default public/private file storage scheme.
     *
     * Known values are:
     *
     * - 'public' = store files in the site's public file system.
     * - 'private' = store files in the site's private file system.
     *
     * @return string
     *   Returns the default file storage scheme as either 'public' or 'private'.
     *
     * @see ::getFileScheme()
     * @see ::setFileScheme()
     */
    public static function getFileSchemeDefault()
    {
        // Try to use the Core File module's default choice, if it is
        // supported for Foldershare. If not, revert to 'public'.
        $defaultScheme = Drupal::config('system.file')->get('default_scheme');
        $defaultStreamWrapper = Drupal::service('stream_wrapper_manager')->getViaScheme($defaultScheme);
        if (!$defaultStreamWrapper || !FileUtilities::isStreamWrapperTypeSupported($defaultStreamWrapper->getType())) {
            return "public";
        }
        return $defaultScheme;
    }

    /**
     * Sets the current file storage scheme.
     *
     * Some sample known values are:
     *
     * - 'public' = store files in the site's public file system.
     * - 'private '= store files in the site's private file system.
     *
     * Unrecognized values are silently ignored.
     *
     * @param string $scheme
     *   The file storage scheme
     *
     * @see ::getFileScheme()
     * @see ::getFileSchemeDefault()
     */
    public static function setFileScheme(string $scheme)
    {
        $streamWrapper = FileUtilities::getStreamWrapper($scheme);
        if (!$streamWrapper || !FileUtilities::isStreamWrapperTypeSupported($streamWrapper->getType())) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('file_scheme', $scheme);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Files - extension restriction enable.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current filename extension restrictions flag.
     *
     * Legal values are TRUE or FALSE.
     *
     * @return bool
     *   Returns TRUE if filename extensions are restricted, and false otherwise.
     *
     * @see ::getAllowedFilenameExtensions()
     * @see ::getFileRestrictFilenameExtensionsDefault()
     * @see ::setFileRestrictFilenameExtensions()
     */
    public static function getFileRestrictFilenameExtensions()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('file_restrict_extensions') === NULL) {
            return self::getFileRestrictFilenameExtensionsDefault();
        }

        return boolval($config->get('file_restrict_extensions'));
    }

    /**
     * Returns the default filename extension restrictions flag.
     *
     * @return bool
     *   Returns the default filename extensions flag. The value is TRUE if
     *   filename extensions are restricted, and FALSE otherwise.
     *
     * @see ::getFileRestrictFilenameExtensions()
     * @see ::setFileRestrictFilenameExtensions()
     */
    public static function getFileRestrictFilenameExtensionsDefault()
    {
        return FALSE;
    }

    /**
     * Sets the current filename extension restrictions flag.
     *
     * @param bool $value
     *   TRUE if filename extensions are restricted, and FALSE otherwise.
     *
     * @see ::getAllowedFilenameExtensions()
     * @see ::getFileRestrictFilenameExtensions()
     * @see ::getFileRestrictFilenameExtensionsDefault()
     */
    public static function setFileRestrictFilenameExtensions(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('file_restrict_extensions', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Files - extension restriction list.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current list of allowed filename extensions.
     *
     * The return list is a single string with space-separated dot-free
     * filename extensions allowed for file uploads and renames.
     *
     * The list is only meaningful if filename extension restrictions are
     * enabled.
     *
     * @return string
     *   Returns a string containing a space-separated list of file
     *   extensions (without the leading dot).
     *
     * @see ::getAllowedFilenameExtensionsDefault()
     * @see ::getFileRestrictFilenameExtensions()
     * @see ::setAllowedFilenameExtensions()
     * @see ::setFileRestrictFilenameExtensions()
     */
    public static function getAllowedFilenameExtensions()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('file_allowed_extensions') === NULL) {
            return self::getAllowedFilenameExtensionsDefault();
        }

        return (string)$config->get('file_allowed_extensions');
    }

    /**
     * Returns the default list of allowed filename extensions.
     *
     * This list is intentionally broad and includes a large set of
     * well-known extensions for text, web, image, video, audio,
     * graphics, data, archive, office, and programming documents.
     *
     * @return string
     *   Returns a string containing a space-separated default
     *   list of file extensions (without the leading dot).
     *
     * @see ::getAllowedFilenameExtensions()
     * @see ::setAllowedFilenameExtensions()
     */
    public static function getAllowedFilenameExtensionsDefault()
    {
        return implode(' ', ManageFilenameExtensions::getAllFilenameExtensions());
    }

    /**
     * Sets the current list of allowed filename extensions.
     *
     * The given list must be a single string with space-separated dot-free
     * filename extensions allowed for file uploads and renames.
     *
     * The list is only meaningful if filename extension restrictions are
     * enabled.
     *
     * @param string $ext
     *   A string containing a space-separated list of file
     *   extensions (without the leading dot).
     *
     * @see ::getAllowedFilenameExtensions()
     * @see ::getAllowedFilenameExtensionsDefault()
     * @see ::getFileRestrictFilenameExtensions()
     * @see ::setFileRestrictFilenameExtensions()
     */
    public static function setAllowedFilenameExtensions(string $ext)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('file_allowed_extensions', $ext);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Files - size limit.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current file upload size limit.
     *
     * A zero value means there is no specified limit, though upload sizes
     * may still be limited by PHP settings.
     *
     * @return int
     *   Returns the file upload size limit.
     *
     * @see ::getAllowedFilenameExtensions()
     * @see ::getFileRestrictFilenameExtensionsDefault()
     * @see ::setFileRestrictFilenameExtensions()
     */
    public static function getFileUploadSizeLimit()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('file_upload_size_limit') === NULL) {
            return self::getFileUploadSizeLimitDefault();
        }

        $i = intval($config->get('file_upload_size_limit'));
        if ($i < 0) {
            return 0;
        }
        return $i;
    }

    /**
     * Returns the default file upload size limit.
     *
     * A zero value means there is no specified limit, though upload sizes
     * may still be limited by PHP settings.
     *
     * @return int
     *   Returns the default file upload size limit.
     *
     * @see ::getFileUploadSizeLimit()
     * @see ::setFileUploadSizeLimit()
     */
    public static function getFileUploadSizeLimitDefault()
    {
        return 0;
    }

    /**
     * Sets the current file upload size limit.
     *
     * A zero value means there is no specified limit, though upload sizes
     * may still be limited by PHP settings.
     *
     * @param int $value
     *   The file upload size limit. Negative values and values larger than
     *   or requal to the PHP limit are converted to a zero, indicating
     *   that the PHP limit should be used.
     *
     * @see ::getAllowedFilenameExtensions()
     * @see ::getFileRestrictFilenameExtensions()
     * @see ::getFileRestrictFilenameExtensionsDefault()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getPhpFileUploadSizeLimit()
     */
    public static function setFileUploadSizeLimit(int $value)
    {
        if ($value < 0 ||
            $value >= LimitUtilities::getPhpFileUploadSizeLimit()) {
            $value = 0;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('file_upload_size_limit', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Command menu - restrictions enable.
     *
     *---------------------------------------------------------------------*/

    /**
     * Sets the current command menu restrictions flag.
     *
     * When FALSE, all available plugin commands are allowed on the user
     * interface's command menu. When TRUE, this list is restricted to only
     * the commands selected by the site administrator.
     *
     * @param bool $value
     *   TRUE if the command menu is restricted, and FALSE otherwise.
     *
     * @see ::getCommandMenuRestrict()
     * @see ::getCommandMenuRestrictDefault()
     */
    public static function setCommandMenuRestrict(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('command_menu_restrict', $value);
        $config->save(TRUE);
    }

    /**
     * Returns a list of allowed plugin command definitions.
     *
     * The returned list includes all currently installed plugin commands,
     * regardless of the module source for the commands. This list is then
     * filtered to only include those commands that are currently allowed,
     * based upon module settings for command menu restrictions and content.
     *
     * @param FolderShareInterface $parent
     *   The parent entity for the commands. A NULL indicates a root list.
     * @param int $userId
     *   The current user ID.
     *
     * @return array
     *   Returns an array of plugin command definitions.  Array keys are
     *   command keys, and array values are command definitions.
     *
     * @see ::getAllCommandDefinitions()
     */
    public static function getAllowedCommandDefinitions(
        FolderShareInterface $parent = NULL,
        int $userId = (-1))
    {

        // Get a list of all installed commands. This list may already have been
        // filtered by command plugin hooks that can unset or alter definitions
        // as they are loaded.
        $defs = self::getAllCommandDefinitions();

        // If the command menu is restricted, filter the list based upon a
        // list of allowed commands set by the site administrator.
        if (self::getCommandMenuRestrict() === TRUE) {
            // Get the current list of allowed command IDs.
            $ids = self::getCommandMenuAllowed();

            // Cull the definitions to only those allowed.
            $allowed = [];
            foreach ($defs as $id => $def) {
                if (in_array($id, $ids) === TRUE) {
                    $allowed[$id] = $def;
                }
            }

            $defs = $allowed;
        }

        // Let hooks disable commands.
        if (empty(ManageHooks::getHookCommandEnable()) === FALSE) {
            $allowed = [];
            foreach ($defs as $id => $def) {
                $enable = ManageHooks::callHookCommandEnable($parent, $userId, $def);
                if ($enable === TRUE) {
                    $allowed[$id] = $def;
                }
            }

            $defs = $allowed;
        }

        return $defs;
    }

    /**
     * Returns a list of all available plugin command definitions.
     *
     * The returned list includes all currently installed plugin commands,
     * regardless of the module source for the commands. Depending upon
     * module settings, some of these commands are allowed on the command
     * menu, while others are disabled from the user interface.
     *
     * @return array
     *   Returns an array of plugin command definitions.  Array keys are
     *   command keys, and array values are command definitions.
     *
     * @see ::getAllowedCommandDefinitions()
     */
    public static function getAllCommandDefinitions()
    {
        // Get the command plugin manager.
        $container = Drupal::getContainer();
        $manager = $container->get('foldershare.plugin.manager.foldersharecommand');
        if ($manager === NULL) {
            // No manager? Return nothing.
            return [];
        }

        // Get all the command definitions. The returned array has array keys
        // as plugin IDs, and array values as definitions.
        return $manager->getDefinitions();
    }

    /*---------------------------------------------------------------------
     *
     * Command menu - restriction list.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current command menu restrictions flag.
     *
     * When FALSE, all available plugin commands are allowed on the user
     * interface's command menu. When TRUE, this list is restricted to only
     * the commands selected by the site administrator.
     *
     * @return bool
     *   Returns TRUE if the command menu is restricted, and FALSE otherwise.
     *
     * @see ::getCommandMenuRestrictDefault()
     * @see ::setCommandMenuRestrict()
     */
    public static function getCommandMenuRestrict()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('command_menu_restrict') === NULL) {
            return self::getCommandMenuRestrictDefault();
        }

        return boolval($config->get('command_menu_restrict'));
    }

    /**
     * Returns the default command menu restrictions flag.
     *
     * @return bool
     *   Returns TRUE if the command menu is restricted by default, and
     *   FALSE otherwise.
     *
     * @see ::getCommandMenuRestrict()
     * @see ::setCommandMenuRestrict()
     */
    public static function getCommandMenuRestrictDefault()
    {
        return FALSE;
    }

    /**
     * Returns the current list of allowed plugin commands for menus.
     *
     * The returned list is an array of command plugin IDs.
     *
     * Plugin IDs are not validated and could list plugins that are no
     * longer installed at the site.
     *
     * @return array
     *   Returns an array of plugin command IDs.
     *
     * @see ::getCommandMenuAllowedDefault()
     * @see ::setCommandMenuAllowed()
     */
    public static function getCommandMenuAllowed()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('command_menu_allowed') === NULL) {
            // Nothing set yet. Revert to default.
            return self::getCommandMenuAllowedDefault();
        }

        $ids = $config->get('command_menu_allowed');
        if (is_array($ids) === TRUE) {
            return $ids;
        }

        // The stored value is bogus. Reset it to the default.
        $ids = self::getCommandMenuAllowedDefault();
        self::setCommandMenuAllowed($ids);
        return $ids;
    }

    /**
     * Returns the default list of allowed plugin commands for menus.
     *
     * The returned list is an array of command plugin IDs. By default,
     * this list includes all command plugins currently installed.
     *
     * @return array
     *   Returns an array of plugin command IDs.
     *
     * @see ::getAllCommandDefinitions()
     * @see ::getCommandMenuAllowed()
     * @see ::setCommandMenuAllowed()
     */
    public static function getCommandMenuAllowedDefault()
    {
        return array_keys(self::getAllCommandDefinitions());
    }

    /**
     * Sets the current list of allowed plugin commands for menus.
     *
     * The given list is an array of command plugin IDs.
     *
     * Plugin IDs are not validated and could list plugins that are no
     * longer installed at the site.
     *
     * @param array $ids
     *   An array of plugin command IDs.
     *
     * @see ::getCommandMenuAllowed()
     * @see ::getCommandMenuAllowedDefault()
     */
    public static function setCommandMenuAllowed(array $ids)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);

        if (empty($ids) === TRUE || is_array($ids) === FALSE) {
            // Set the menu to be an empty list.
            $config->set('command_menu_allowed', []);
            $config->save(TRUE);
        } else {
            $config->set('command_menu_allowed', $ids);
            $config->save(TRUE);
        }
    }

    /*---------------------------------------------------------------------
     *
     * Command menu - submenu threshold.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current command submenu threshold.
     *
     * @return int
     *   Returns the submenu threshold.
     *
     * @see ::getCommandMenuSubmenuThresholdDefault()
     * @see ::setCommandMenuSubmenuThreshold()
     */
    public static function getCommandMenuSubmenuThreshold()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('command_menu_submenu_threshold') === NULL) {
            return self::getCommandMenuSubmenuThresholdDefault();
        }

        return $config->get('command_menu_submenu_threshold');
    }

    /**
     * Returns the default command submenu threshold.
     *
     * @return int
     *   Returns the submenu threshold.
     *
     * @see ::getCommandMenuSubmenuThreshold()
     * @see ::setCommandMenuSubmenuThreshold()
     */
    public static function getCommandMenuSubmenuThresholdDefault()
    {
        return 3;
    }

    /**
     * Sets the current command submenu threshold.
     *
     * The submenu threshold indicates the maximum number of adjacent
     * commands in a command category on the main menu before the commands
     * are moved to a submenu. Having a threshold helps insure the command
     * menu does not get long and unwieldy when there are large number of
     * command plugins installed.
     *
     * @param int $threshold
     *   The submenu threshold. Values <= 2 are ignored.
     *
     * @see ::getCommandMenuSubmenuThreshold()
     * @see ::getCommandMenuSubmenuThresholdDefault()
     */
    public static function setCommandMenuSubmenuThreshold(int $threshold)
    {
        if ($threshold <= 2) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('command_menu_submenu_threshold', $threshold);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Commands - report normal completion.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current command normal completion report flag.
     *
     * @return bool
     *   Returns the command normal completion report flag.
     *
     * @see ::getCommandNormalCompletionReportEnableDefault()
     * @see ::setCommandNormalCompletionReportEnable()
     */
    public static function getCommandNormalCompletionReportEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('command_normal_completion_report') === NULL) {
            return self::getCommandNormalCompletionReportEnableDefault();
        }

        return boolval($config->get('command_normal_completion_report'));
    }

    /**
     * Returns the default command normal completion report flag.
     *
     * @return bool
     *   Returns the default command normal completion report flag.
     *
     * @see ::getCommandNormalCompletionReportEnable()
     * @see ::setCommandNormalCompletionReportEnable()
     */
    public static function getCommandNormalCompletionReportEnableDefault()
    {
        return FALSE;
    }

    /**
     * Sets the current command normal completion report flag.
     *
     * Commands always report errors to Drupal's messenger, which are then
     * often picked up and shown in an error dialog. Optionally, commands
     * also can report normal completion such as "Item deleted" or
     * "Item copied".
     *
     * While normal completion messages are common in Drupal for nodes and
     * other entities, they are not conventional in file systems. Windows
     * Explorer, macOS Finder, and the various Linux file browsers do not
     * put up a dialog or show a message every time a file or folder is
     * created, deleted, moved, copied, or renamed.
     *
     * When this flag is TRUE, normal completion messages are reported to
     * the user. When FALSE (which is recommended), these messages are
     * suppressed.
     *
     * @param bool $enable
     *   The command normal completion report flag.
     *
     * @see ::getCommandNormalCompletionReportEnable()
     * @see ::getCommandNormalCompletionReportEnableDefault()
     */
    public static function setCommandNormalCompletionReportEnable(bool $enable)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('command_normal_completion_report', $enable);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - preferred search module.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current module ID of the preferred search module.
     *
     * @return string
     *   Returns the module ID.
     *
     * @see ::getPreferredSearchModuleIdDefault()
     * @see ::setPreferredSearchModuleId()
     */
    public static function getPreferredSearchModuleId()
    {
        // Return the ID of Drupal's core Search.
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_preferred_module') === NULL) {
            return self::getPreferredSearchModuleIdDefault();
        }
        return $config->get('search_preferred_module');
    }

    /**
     * Returns the module ID of the default preferred search module.
     *
     * @return string
     *   Returns the module ID default.
     *
     * @see ::getPreferredSearchModuleId()
     * @see ::setPreferredSearchModuleId()
     */
    public static function getPreferredSearchModuleIdDefault()
    {
        // Return the ID of Drupal's core Search.
        return 'search';
    }

    /**
     * Sets the current module ID of the preferred search module.
     *
     * There can be multiple search providers enabled. Only one of them
     * is supported at a time for the folder browser toolbar's search box
     * and for search indexing of file and folder content.
     *
     * @param string $value
     *   The module ID of the preferred search module.
     *
     * @see ::getPreferredSearchModuleId()
     * @see ::getPreferredSearchModuleIdDefault()
     */
    public static function setPreferredSearchModuleId(string $value = NULL)
    {
        if (empty($value) === TRUE) {
            $value = '';
        }
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_preferred_module', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - index file content.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current file search indexing enable flag.
     *
     * Search indexing always includes the name of a file or folder, its
     * description, and other field content. Optionally, it may also include
     * the content of underlying file.
     *
     * @return bool
     *   Returns if TRUE if search indexing of file content is enabled,
     *   and FALSE otherwise.
     *
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::setSearchIndexFileContentEnable()
     */
    public static function getSearchIndexFileContentEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_file_content') === NULL) {
            return self::getSearchIndexFileContentEnableDefault();
        }

        return boolval($config->get('search_file_content'));
    }

    /**
     * Returns the default file search indexing enable flag.
     *
     * @return bool
     *   Returns TRUE if search indexing of file content is enabled by default,
     *   and FALSE otherwise.
     *
     * @see ::getSearchIndexFileContentEnable()
     * @see ::setSearchIndexFileContentEnable()
     */
    public static function getSearchIndexFileContentEnableDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current file search indexing enable flag.
     *
     * Search indexing always includes the name of a file or folder, its
     * description, and other field content. Optionally, it may also include
     * the content of underlying file.
     *
     * @param bool $value
     *   TRUE to enable search indexing of file content, and FALSE to disable.
     *
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     */
    public static function setSearchIndexFileContentEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_file_content', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - index file content maximum size.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current file search indexing maximum size.
     *
     * When search indexing includes file content, the maximum size determines
     * the maximum number of bytes read from the start of the file and included
     * in the search index.
     *
     * This feature is only used when file search indexing is enabled and
     * a search module is enabled for file and folder content.
     *
     * @return int
     *   Returns the maximum size, in bytes, for file content included in
     *   the search index. A 0 means everything.
     *
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::getSearchIndexMaximumFileSizeDefault()
     * @see ::setSearchIndexFileContentEnable()
     * @see ::setSearchIndexMaximumFileSize()
     */
    public static function getSearchIndexMaximumFileSize()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_file_maximum_size') === NULL) {
            return self::getSearchIndexMaximumFileSizeDefault();
        }

        return intval($config->get('search_file_maximum_size'));
    }

    /**
     * Returns the default file search indexing maximum size.
     *
     * This feature is only used when file search indexing is enabled and
     * a search module is enabled for file and folder content.
     *
     * @return int
     *   Returns the maximum size, in bytes, for file content included in
     *   the search index. A 0 means everything.
     *
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::getSearchIndexMaximumFileSize()
     * @see ::setSearchIndexFileContentEnable()
     * @see ::setSearchIndexMaximumFileSize()
     */
    public static function getSearchIndexMaximumFileSizeDefault()
    {
        return (1 << 20);
    }

    /**
     * Sets the current file search indexing maximum size.
     *
     * When search indexing includes file content, the maximum size determines
     * the maximum number of bytes read from the start of the file and included
     * in the search index.
     *
     * This feature is only used when file search indexing is enabled and
     * a search module is enabled for file and folder content.
     *
     * @param int $value
     *   The maximum size, in bytes, for file content included in
     *   the search index. A 0 means everything.
     *
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::getSearchIndexMaximumFileSize()
     * @see ::setSearchIndexFileContentEnable()
     * @see ::getSearchIndexMaximumFileSizeDefault()
     */
    public static function setSearchIndexMaximumFileSize(int $value)
    {
        if ($value < 0) {
            $value = 0;
        }
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_file_maximum_size', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - index file content extensions.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current list of filename extensions for file content indexing.
     *
     * The return list is a single string with space-separated dot-free
     * filename extensions allowed for file content inclusion in the search
     * index.
     *
     * This feature is only used when file search indexing is enabled and
     * a search module is enabled for file and folder content.
     *
     * @return string
     *   Returns a string containing a space-separated list of file
     *   extensions (without the leading dot).
     *
     * @see ::getAllowedSearchIndexFilenameExtensionsDefault()
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::setAllowedSearchIndexFilenameExtensions()
     * @see ::setSearchIndexFileContentEnable()
     */
    public static function getAllowedSearchIndexFilenameExtensions()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_file_allowed_extensions') === NULL) {
            return self::getAllowedSearchIndexFilenameExtensionsDefault();
        }

        return (string)$config->get('search_file_allowed_extensions');
    }

    /**
     * Returns the default list of filename extensions for file content indexing.
     *
     * This list is intentionally broad and includes a large set of
     * well-known extensions for text, web, image, video, audio,
     * graphics, data, archive, office, and programming documents.
     *
     * This feature is only used when file search indexing is enabled and
     * a search module is enabled for file and folder content.
     *
     * @return string
     *   Returns a string containing a space-separated default
     *   list of file extensions (without the leading dot).
     *
     * @see ::getAllowedSearchIndexFilenameExtensions()
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::setAllowedSearchIndexFilenameExtensions()
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     */
    public static function getAllowedSearchIndexFilenameExtensionsDefault()
    {
        return implode(' ', ManageFilenameExtensions::getAllTextFilenameExtensions());
    }

    /**
     * Sets the current list of filename extensions for file content indexing.
     *
     * The given list must be a single string with space-separated dot-free
     * filename extensions allowed for file content inclusion in the
     * search index.
     *
     * This feature is only used when file search indexing is enabled and
     * a search module is enabled for file and folder content.
     *
     * @param string $ext
     *   A string containing a space-separated list of file
     *   extensions (without the leading dot).
     *
     * @see ::getAllowedSearchIndexFilenameExtensions()
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::getSearchIndexFileContentEnable()
     * @see ::getSearchIndexFileContentEnableDefault()
     * @see ::getAllowedSearchIndexFilenameExtensionsDefault()
     */
    public static function setAllowedSearchIndexFilenameExtensions(string $ext)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_file_allowed_extensions', $ext);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - core Search indexing interval.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current core search indexing interval.
     *
     * Known values are:
     *
     * - 'system' = the index is serviced by the system (e.g. CRON).
     * - '5min' = the index is serviced every 5 minutes.
     * - '10min' = the index is serviced every 10 minutes.
     * - '15min' = the index is serviced every 15 minutes.
     * - '30min' = the index is serviced every 30 minutes.
     * - 'hourly' = the index is serviced every hour.
     * - 'daily' = the index is serviced every day.
     *
     * The legacy value '1min' is no longer supported and is automatically
     * mapped to '5min'.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @return string
     *   The indexing interval.
     *
     * @see ::getSearchIndexIntervalDefault()
     * @see ::setSearchIndexInterval()
     */
    public static function getSearchIndexInterval()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_index_interval') === NULL) {
            return self::getSearchIndexIntervalDefault();
        }

        $interval = (string)$config->get('search_index_interval');
        switch ($interval) {
            case 'system':
            case '5min':
            case '10min':
            case '15min':
            case '30min':
            case 'hourly':
            case 'daily':
                return $interval;

            case '1min':
                // Legacy interval is too short and can cause high system load.
                // Map it to 5 minutes.
                return '5min';

            default:
                return self::getSearchIndexIntervalDefault();
        }
    }

    /**
     * Returns the default core search indexing interval.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @return string
     *   The default indexing interval.
     *
     * @see ::getSearchIndexInterval()
     * @see ::setSearchIndexInterval()
     */
    public static function getSearchIndexIntervalDefault()
    {
        return 'system';
    }

    /**
     * Sets the core search indexing interval.
     *
     * Known values are:
     *
     * - 'system' = the index is serviced by the system (e.g. CRON).
     * - '5min' = the index is serviced every 5 minutes.
     * - '10min' = the index is serviced every 10 minutes.
     * - '15min' = the index is serviced every 15 minutes.
     * - '30min' = the index is serviced every 30 minutes.
     * - 'hourly' = the index is serviced every hour.
     * - 'daily' = the index is serviced every day.
     *
     * The legacy value '1min' is no longer supported and is automatically
     * mapped to '5min'.
     *
     * Updating the search index is normally done by CRON, but CRON run times
     * are often at long time intervals. To keep the search index more uptodate,
     * indexing can be done by a scheduled task that runs more often than CRON.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @param string $interval
     *   The indexing interval.
     *
     * @see ::getSearchIndexInterval()
     * @see ::getSearchIndexIntervalDefault()
     * @see \Drupal\foldershare\ManageSearch
     */
    public static function setSearchIndexInterval(string $interval)
    {
        $interval = mb_convert_case($interval, MB_CASE_LOWER);

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        if ($config->get('search_index_interval') === $interval) {
            // No change.
            return;
        }

        switch ($interval) {
            case 'system':
            case '5min':
            case '10min':
            case '15min':
            case '30min':
            case 'hourly':
            case 'daily':
                break;

            case '1min':
                // Legacy interval is too short and can cause high system load.
                // Map it to 5 minutes.
                $interval = '5min';
                break;

            default:
                $interval = self::getSearchIndexIntervalDefault();
                break;
        }

        $config->set('search_index_interval', $interval);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - core Search folder-constrained search.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current file search indexing enable flag.
     *
     * When core Search is used, and this feature is enabled, the search plugin
     * supports a parent folder ID constraint that limits a search to that
     * folder and its descendents. When the search is initiated from a folder
     * browser search box, the ID is automatically set to the current folder.
     *
     * Enabling this feature is not recommended. Creating a search query using
     * the ID requires a recursive traversal of the folder tree, which generates
     * a huge number of queries on a deep tree and can take a noticable amount
     * of time. The resulting ID list can be large, which can overflow memory
     * constraints and query structure constraints, depending upon the
     * database. Finally, there is no UI to set the parent folder ID in a
     * search results page used to continue a search started from the folder
     * browser search box.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @return bool
     *   Returns TRUE if the folder browser search box, when used with core
     *   Search, supports constraining a search to the current folder and its
     *   descendents, and FALSE otherwise.
     *
     * @see ::getSearchFoldersEnableDefault()
     * @see ::setSearchFoldersEnable()
     */
    public static function getSearchFoldersEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_folders_constraint') === NULL) {
            return self::getSearchFoldersEnableDefault();
        }

        return boolval($config->get('search_folders_constraint'));
    }

    /**
     * Returns the default folder-constrained search enable flag.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @return bool
     *   Returns TRUE if the folder browser search box, when used with core
     *   Search, supports constraining a search to the current folder and its
     *   descendents by default, and FALSE otherwise.
     *
     * @see ::getSearchFoldersEnable()
     * @see ::setSearchFoldersEnable()
     */
    public static function getSearchFoldersEnableDefault()
    {
        return FALSE;
    }

    /**
     * Sets the current file search indexing enable flag.
     *
     * When core Search is used, and this feature is enabled, the search plugin
     * supports a parent folder ID constraint that limits a search to that
     * folder and its descendents. When the search is initiated from a folder
     * browser search box, the ID is automatically set to the current folder.
     *
     * Enabling this feature is not recommended. Creating a search query using
     * the ID requires a recursive traversal of the folder tree, which generates
     * a huge number of queries on a deep tree and can take a noticable amount
     * of time. The resulting ID list can be large, which can overflow memory
     * constraints and query structure constraints, depending upon the
     * database. Finally, there is no UI to set the parent folder ID in a
     * search results page used to continue a search started from the folder
     * browser search box.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @param bool $value
     *   TRUE if the folder browser search box, when used with core
     *   Search, supports constraining a search to the current folder and its
     *   descendents, and FALSE otherwise.
     *
     * @see ::getSearchFoldersEnable()
     * @see ::getSearchFoldersEnableDefault()
     */
    public static function setSearchFoldersEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_folders_constraint', $value);
        $config->save(TRUE);
    }


    /*---------------------------------------------------------------------
     *
     * Search - search API search path.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the Search API search page path.
     *
     * This feature is only used when the Search API module is in use.
     *
     * @return string
     *   Returns the search page path.
     *
     * @see ::getSearchAPISearchPathDefault()
     * @see ::setSearchAPISearchPath()
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function getSearchAPISearchPath()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_api_search_path') === NULL) {
            return self::getSearchAPISearchPathDefault();
        }

        return $config->get('search_api_search_path');
    }

    /**
     * Returns the default Search API search page path.
     *
     * This feature is only used when the Search API module is in use.
     *
     * @return string
     *   Returns the default search page path.
     *
     * @see ::getSearchAPISearchPath()
     * @see ::setSearchAPISearchPath()
     */
    public static function getSearchAPISearchPathDefault()
    {
        return '';
    }

    /**
     * Sets the current search API search page path.
     *
     * When using the Search API module, the folder browser search box can
     * redirect to a search form that uses the module. There are three common
     * options:
     * - Create the form using Views and an exposed filter.
     * - Create the form using the Search API Pages module.
     * - Create a custom form.
     *
     * The redirect needs to know how to assemble the URL for each of the
     * above cases. There are two styles:
     *
     * - "query": The URL has the form "HOST/SEARCHPATH?FILTERID=keywords".
     *   This form is used by Views and an exposed filter and is the default.
     *
     * - "path": The URL has the form "HOST/SEARCHPATH/keywords". This form
     *   is used by Search API Pages and by Views using a contextual filter.
     *
     * A custom form might use either style.
     *
     * This method sets the SEARCHPATH part of the URL. When using Views,
     * this is the path set for the view and display. When using the
     * Search API Pages module, this is the path set in that module. And
     * when using a custom form, this is the path in the route to that form.
     *
     * This feature is only used when the Search API module is in use and
     * the search box on the folder browser toolbar is enabled.
     *
     * @param string $value
     *   The search page path.
     *
     * @see ::getSearchAPISearchPath()
     * @see ::getSearchAPISearchPathDefault()
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function setSearchAPISearchPath(string $value = NULL)
    {
        if (empty($value) === TRUE) {
            $value = '';
        }
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_api_search_path', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - search API search URL style.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the Search API URL style.
     *
     * This feature is only used when the Search API module is in use.
     *
     * @return string
     *   Returns the search URL style. One of "query" or "path".
     *
     * @see ::getSearchAPISearchURLStyle()
     * @see ::setSearchAPISearchURLStyleDefault()
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function getSearchAPISearchURLStyle()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_api_search_url_style') === NULL) {
            return self::getSearchAPISearchURLStyleDefault();
        }

        switch ($config->get('search_api_search_url_style')) {
            default:
            case 'query':
                return 'query';

            case 'path':
                return 'path';
        }
    }

    /**
     * Returns the default Search API URL style.
     *
     * This feature is only used when the Search API module is in use.
     *
     * @return string
     *   Returns the default search API URL style.
     *
     * @see ::getSearchAPISearchURLStyle()
     * @see ::setSearchAPISearchURLStyle()
     */
    public static function getSearchAPISearchURLStyleDefault()
    {
        return 'query';
    }

    /**
     * Sets the current search API URL style.
     *
     * When using the Search API module, the folder browser search box can
     * redirect to a search form that uses the module. There are three common
     * options:
     * - Create the form using Views and an exposed filter.
     * - Create the form using the Search API Pages module.
     * - Create a custom form.
     *
     * The redirect needs to know how to assemble the URL for each of the
     * above cases. There are two styles:
     *
     * - "query": The URL has the form "HOST/SEARCHPATH?FILTERID=keywords".
     *   This form is used by Views and an exposed filter and is the default.
     *
     * - "path": The URL has the form "HOST/SEARCHPATH/keywords". This form
     *   is used by Search API Pages and by Views using a contextual filter.
     *
     * A custom form might use either style.
     *
     * This method sets the URL style to either "query" or "path".
     *
     * This feature is only used when the Search API module is in use and
     * the search box on the folder browser toolbar is enabled. The "query"
     * style also requires a filter ID.
     *
     * @param string $value
     *   The search full URL style. One of "query" or "path".
     *
     * @see ::getSearchAPISearchURLStyle()
     * @see ::getSearchAPISearchURLStyleDefault()
     * @see ::getSearchAPISearchFilterId()
     * @see ::setSearchAPISearchFilterId()
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function setSearchAPISearchURLStyle(string $value = NULL)
    {
        switch ($value) {
            default:
            case 'query':
                $value = 'query';
                break;

            case 'path':
                $value = 'path';
                break;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_api_search_url_style', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - search API exposed filter ID.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the Search API full text exposed filter ID.
     *
     * This feature is only used when the Search API module is in use.
     *
     * @return string
     *   Returns the search page path.
     *
     * @see ::getSearchAPISearchFilterIdDefault()
     * @see ::setSearchAPISearchFilterId()
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function getSearchAPISearchFilterId()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('search_api_search_filter_id') === NULL) {
            return self::getSearchAPISearchFilterIdDefault();
        }

        return $config->get('search_api_search_filter_id');
    }

    /**
     * Returns the default Search API full text exposed filter ID.
     *
     * This feature is only used when the Search API module is in use and
     * the search URL style is "query".
     *
     * @return string
     *   Returns the default search full text exposed filter ID.
     *
     * @see ::getSearchAPISearchFilterId()
     * @see ::setSearchAPISearchFilterId()
     */
    public static function getSearchAPISearchFilterIdDefault()
    {
        return '';
    }

    /**
     * Sets the current search API full text exposed filter ID.
     *
     * When using the Search API module, the site uses the core Views module
     * to set up one or more search pages. An essential part of that view is
     * an exposed filter that accepts keywords from the user and initiates a
     * search through full text fields in the search index. In order for
     * keywords to be passed to that filter via a URL, the filter needs a
     * "Filter identifier". This can be anything and is up to the site admin.
     *
     * When using the Search API module, the folder browser search box can
     * redirect to a search form that uses the module. There are three common
     * options:
     * - Create the form using Views and an exposed filter.
     * - Create the form using the Search API Pages module.
     * - Create a custom form.
     *
     * The redirect needs to know how to assemble the URL for each of the
     * above cases. There are two styles:
     *
     * - "query": The URL has the form "HOST/SEARCHPATH?FILTERID=keywords".
     *   This form is used by Views and an exposed filter and is the default.
     *
     * - "path": The URL has the form "HOST/SEARCHPATH/keywords". This form
     *   is used by Search API Pages and by Views using a contextual filter.
     *
     * A custom form might use either style.
     *
     * When using the "query" style, the URL query needs an argument name
     * FILTERID. When using Views, this is the text ID of the exposed filter
     * for the search form.
     *
     * This feature is only used when the Search API module is in use, the
     * search URL style is "query", and the search box on the folder browser
     * toolbar is enabled.
     *
     * @param string $value
     *   The search full text exposed filter ID.
     *
     * @see ::getSearchAPISearchFilterId()
     * @see ::getSearchAPISearchFilterIdDefault()
     * @see ::getSearchAPISearchURLStyle()
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setSearchAPISearchURLStyle()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function setSearchAPISearchFilterId(string $value = NULL)
    {
        if (empty($value) === TRUE) {
            $value = '';
        }
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('search_api_search_filter_id', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Search - core search index lock duration.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current search index lock duration in seconds.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @return float
     *   Returns the search index lock duration in seconds.
     *
     * @see ::setSearchIndexLockDuration()
     * @see ::getSearchIndexLockDurationDefault()
     */
    public static function getSearchIndexLockDuration()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('lock_search_index_duration') === NULL) {
            return self::getSearchIndexLockDurationDefault();
        }

        return floatval($config->get('lock_search_index_duration'));
    }

    /**
     * Returns the default search index lock duration in seconds.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @return float
     *   Returns the default search index lock duration in seconds.
     *
     * @see ::getSearchIndexLockDuration()
     * @see ::setSearchIndexLockDuration()
     */
    public static function getSearchIndexLockDurationDefault()
    {
        return 60.0;
    }

    /**
     * Sets the current search index lock duration in seconds.
     *
     * When updating the search index, server load is reduced if only one
     * process updates it at a time. To enforce this, indexing holds a
     * process lock that automatically expires as a fail-safe. This time
     * is chosen to be long enough for indexing of a few items to complete
     * and the indexing process to exit. Typically this is around the same
     * length as the PHP or web server fail-safe timeout to prevent run-away
     * processes.
     *
     * This feature is only used by the module's search plugin for the
     * core Search module.
     *
     * @param float $value
     *   The search index lock duration in seconds.
     *
     * @see ::getSearchIndexLockDuration()
     * @see ::getSearchIndexLockDurationDefault()
     */
    public static function setSearchIndexLockDuration(float $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('lock_search_index_duration', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Usage statistics - update interval.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current update interval for the usage table update.
     *
     * Known values are:
     *
     * - 'manual' = the table is only updated manually.
     * - 'hourly' = the table is updated hourly.
     * - 'daily' = the table is updated daily.
     * - 'weekly' = the table is updated weekly.
     *
     * @return string
     *   The update interval.
     *
     * @see ::getUsageUpdateIntervalDefault()
     * @see ::setUsageUpdateInterval()
     */
    public static function getUsageUpdateInterval()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('usage_report_rebuild_interval') === NULL) {
            return self::getUsageUpdateIntervalDefault();
        }

        $interval = (string)$config->get('usage_report_rebuild_interval');
        switch ($interval) {
            case 'manual':
            case 'hourly':
            case 'daily':
            case 'weekly':
                return $interval;

            default:
                return self::getUsageUpdateIntervalDefault();
        }
    }

    /**
     * Returns the default update interval for the usage table.
     *
     * @return string
     *   The default update interval.
     *
     * @see ::getUsageUpdateInterval()
     * @see ::setUsageUpdateInterval()
     */
    public static function getUsageUpdateIntervalDefault()
    {
        return 'manual';
    }

    /**
     * Sets the update interval for the usage table update.
     *
     * Known values are:
     *
     * - 'manual' = the table is only updated manually.
     * - 'hourly' = the table is updated hourly.
     * - 'daily' = the table is updated daily.
     * - 'weekly' = the table is updated weekly.
     *
     * Rebuilding the usage table is fairly expensive. For production
     * environments, some administrators will check the table frequently
     * to monitor usage, so they will need a rapid update. Other administrators
     * may rarely check the table, or never check it, so using a less rapid
     * update makes sense.
     *
     * Regardless of this setting, the usage table may be manually updated
     * from the administrator's user interface.
     *
     * @param string $interval
     *   The update interval.
     *
     * @see ::getUsageUpdateInterval()
     * @see ::getUsageUpdateIntervalDefault()
     * @see \Drupal\foldershare\ManageUsageStatistics
     */
    public static function setUsageUpdateInterval(string $interval)
    {
        $interval = mb_convert_case($interval, MB_CASE_LOWER);

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        if ($config->get('usage_report_rebuild_interval') === $interval) {
            // No change.
            return;
        }

        switch ($interval) {
            case 'manual':
            case 'hourly':
            case 'daily':
            case 'weekly':
                break;

            default:
                $interval = self::getUsageUpdateIntervalDefault();
                break;
        }

        $config->set('usage_report_rebuild_interval', $interval);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Logging - enable.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current activity logging enable flag.
     *
     * @return bool
     *   Returns if TRUE if activity logging is enabled, and FALSE otherwise.
     *
     * @see ::getActivityLogEnableDefault()
     * @see ::setActivityLogEnable()
     */
    public static function getActivityLogEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('activity_log') === NULL) {
            return self::getActivityLogEnableDefault();
        }

        return boolval($config->get('activity_log'));
    }

    /**
     * Returns the default activity logging enable flag.
     *
     * @return bool
     *   Returns TRUE if activity logging is enabled by default, and
     *   FALSE otherwise.
     *
     * @see ::getActivityLogEnable()
     * @see ::setActivityLogEnable()
     */
    public static function getActivityLogEnableDefault()
    {
        return FALSE;
    }

    /**
     * Sets the current activity logging enable flag.
     *
     * After each operation that copies, deletes, moves, otherwise changes the
     * file and folder tree, a log message can be posted. During production
     * use, this is usually disabled. But during development, or if close tracking
     * of activity is needed, this can be enabled.
     *
     * @param bool $value
     *   TRUE to enable activity logging, and FALSE to disable.
     *
     * @see ::getActivityLogEnable()
     * @see ::getActivityLogEnableDefault()
     */
    public static function setActivityLogEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('activity_log', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * ZIP archive - name.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current name for new ZIP archives.
     *
     * @return string
     *   Returns the name.
     *
     * @see ::getNewZipArchiveNameDefault()
     * @see ::setNewZipArchiveName()
     */
    public static function getNewZipArchiveName()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('new_zip_archive_name') === NULL) {
            return self::getNewZipArchiveNameDefault();
        }

        return (string)$config->get('new_zip_archive_name');
    }

    /**
     * Returns the default name for new ZIP archives.
     *
     * @return string
     *   Returns 'Archive.zip'.
     *
     * @see ::getNewZipArchiveName()
     * @see ::setNewZipArchiveName()
     */
    public static function getNewZipArchiveNameDefault()
    {
        return 'Archive.zip';
    }

    /**
     * Sets the current name for new ZIP archives.
     *
     * When a new ZIP archive is created, it nees a name. The default name
     * is something like 'Archive.zip', but it could be anything. Other
     * possible names are 'Compressed.zip', 'Data.zip', 'FilesAndFolders.zip',
     * etc.
     *
     * @param string $value
     *   The name for new ZIP archives.
     *
     * @see ::getNewZipArchiveName()
     * @see ::getNewZipArchiveNameDefault()
     */
    public static function setNewZipArchiveName(string $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('new_zip_archive_name', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * ZIP archive - comment.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current comment for new ZIP archives.
     *
     * @return string
     *   Returns the name.
     *
     * @see ::getNewZipArchiveCommentDefault()
     * @see ::setNewZipArchiveComment()
     */
    public static function getNewZipArchiveComment()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('new_zip_archive_comment') === NULL) {
            return self::getNewZipArchiveCommentDefault();
        }

        return (string)$config->get('new_zip_archive_comment');
    }

    /**
     * Returns the default comment for new ZIP archives.
     *
     * @return string
     *   Returns 'Created by FolderShare.'
     *
     * @see ::getNewZipArchiveComment()
     * @see ::setNewZipArchiveComment()
     */
    public static function getNewZipArchiveCommentDefault()
    {
        return 'Created by FolderShare.';
    }

    /**
     * Sets the current comment for new ZIP archives.
     *
     * When a new ZIP archive is created, a short comment is added to the
     * archive that indicates where the archive came from. This defaults
     * to saying the archive came from this module, but a site could change
     * it to include the website's name.
     *
     * @param string $value
     *   The name for new ZIP archives.
     *
     * @see ::getNewZipArchiveComment()
     * @see ::getNewZipArchiveCommentDefault()
     */
    public static function setNewZipArchiveComment(string $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('new_zip_archive_comment', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * ZIP archive - unarchive behavior.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current ZIP unarchive multiple to subfolder flag.
     *
     * @return bool
     *   Returns TRUE if unarchiving multiple items should place them in
     *   a subfolder, and FALSE if they should be placed in the current folder.
     *
     * @see ::getZipUnarchiveMultipleToSubfolderDefault()
     * @see ::setZipUnarchiveMultipleToSubfolder()
     */
    public static function getZipUnarchiveMultipleToSubfolder()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('zip_unarchive_multiple_to_subfolder') === NULL) {
            return self::getZipUnarchiveMultipleToSubfolderDefault();
        }

        return boolval($config->get('zip_unarchive_multiple_to_subfolder'));
    }

    /**
     * Returns the default ZIP unarchive multiple to subfolder flag.
     *
     * @return bool
     *   Returns TRUE.
     *
     * @see ::getZipUnarchiveMultipleToSubfolder()
     * @see ::setZipUnarchiveMultipleToSubfolder()
     */
    public static function getZipUnarchiveMultipleToSubfolderDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current ZIP unarchive multiple to subfolder flag.
     *
     * When a ZIP archive holds multiple files and folders, there are two
     * possible behaviors when the archive is un-ZIPed:
     *
     * - Create a subfolder named after the archive to contain the items (TRUE).
     * - Unarchive the items directly into the current location (FALSE).
     *
     * @param bool $value
     *   Returns TRUE if unarchiving multiple items should place them in
     *   a subfolder, and FALSE if they should be placed in the current folder.
     *
     * @see ::getZipUnarchiveMultipleToSubfolder()
     * @see ::getZipUnarchiveMultipleToSubfolderDefault()
     */
    public static function setZipUnarchiveMultipleToSubfolder(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('zip_unarchive_multiple_to_subfolder', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Locks - content lock duration.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current content lock duration in seconds.
     *
     * @return float
     *   Returns the content lock duration in seconds.
     *
     * @see ::setContentLockDuration()
     * @see ::getContentLockDurationDefault()
     * @see ::setOperationLockDuration()
     * @see ::getOperationLockDurationDefault()
     */
    public static function getContentLockDuration()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('lock_content_duration') === NULL) {
            return self::getContentLockDurationDefault();
        }

        return floatval($config->get('lock_content_duration'));
    }

    /**
     * Returns the default content lock duration in seconds.
     *
     * @return float
     *   Returns the default content lock duration in seconds.
     *
     * @see ::getContentLockDuration()
     * @see ::setContentLockDuration()
     * @see ::getOperationLockDuration()
     * @see ::setOperationLockDuration()
     */
    public static function getContentLockDurationDefault()
    {
        return 60.0;
    }

    /**
     * Sets the current content lock duration in seconds.
     *
     * For single item locks, this duration sets the length of time before
     * the lock automatically expires. This time is chosen to be long enough
     * for typical single-item operations to complete, and short enough that
     * if something crashes, users and developers don't have too long to wait
     * until the lock expires and work can continue.
     *
     * @param float $value
     *   The content lock duration in seconds.
     *
     * @see ::getContentLockDuration()
     * @see ::getContentLockDurationDefault()
     * @see ::getOperationLockDuration()
     * @see ::getOperationLockDurationDefault()
     */
    public static function setContentLockDuration(float $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('lock_content_duration', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Locks - operation lock duration.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current operation lock duration in seconds.
     *
     * @return float
     *   Returns the operation lock duration in seconds.
     *
     * @see ::setContentLockDuration()
     * @see ::getContentLockDurationDefault()
     * @see ::setOperationLockDuration()
     * @see ::getOperationLockDurationDefault()
     */
    public static function getOperationLockDuration()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('lock_operation_duration') === NULL) {
            return self::getOperationLockDurationDefault();
        }

        return floatval($config->get('lock_operation_duration'));
    }

    /**
     * Returns the default operation lock duration in seconds.
     *
     * @return float
     *   Returns the default operation lock duration in seconds.
     *
     * @see ::getContentLockDuration()
     * @see ::setContentLockDuration()
     * @see ::getOperationLockDuration()
     * @see ::setOperationLockDuration()
     */
    public static function getOperationLockDurationDefault()
    {
        return 3600.0;
    }

    /**
     * Sets the current operation lock duration in seconds.
     *
     * For large operations that operate on an entire folder tree, locks
     * are acquired on the root of the folder tree and they need to last
     * long enough that the entire operation can complete before the folder
     * tree is unlocked. Since such operations could span multiple scheduled
     * task executions, and the rate of those executions depends upon system
     * activity and CRON intervals, it is possible that an operation could
     * span minutes or even hours.
     *
     * @param float $value
     *   The operation lock duration in seconds.
     *
     * @see ::getContentLockDuration()
     * @see ::getContentLockDurationDefault()
     * @see ::getOperationLockDuration()
     * @see ::getOperationLockDurationDefault()
     */
    public static function setOperationLockDuration(float $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('lock_operation_duration', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Scheduling - initial task delay.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current initial scheduled task delay in seconds.
     *
     * @return float
     *   Returns the initial scheduled task delay in seconds.
     *
     * @see ::getScheduledTaskInitialDelayDefault()
     * @see ::setScheduledTaskInitialDelay()
     */
    public static function getScheduledTaskInitialDelay()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('scheduled_task_initial_delay') === NULL) {
            return self::getScheduledTaskInitialDelayDefault();
        }

        return floatval($config->get('scheduled_task_initial_delay'));
    }

    /**
     * Returns the default initial scheduled task delay in seconds.
     *
     * @return float
     *   Returns the default initial scheduled task delay.
     *
     * @see ::getScheduledTaskInitialDelay()
     * @see ::setScheduledTaskInitialDelay()
     */
    public static function getScheduledTaskInitialDelayDefault()
    {
        return 3.0;
    }

    /**
     * Sets the current initial scheduled task delay in seconds.
     *
     * The initial scheduled task delay is the time offset from the current
     * time to the time at which a newly scheduled task should execute.
     * This initial delay is only used for new operations, such as the first
     * task in a copy, delete, or move. Such operations are typically started
     * by the user interface and a delay is needed to give the interface a
     * chance to refresh its page, including any AJAX requests involved.
     *
     * The delay for an initial task should be pretty short, such as a
     * few seconds.
     *
     * Values less than 3 seconds are ignored.
     *
     * @param float $value
     *   The initial scheduled task delay in seconds.
     *
     * @see ::getScheduledTaskInitialDelay()
     * @see ::getScheduledTaskInitialDelayDefault()
     */
    public static function setScheduledTaskInitialDelay(float $value)
    {
        if ($value < 3) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('scheduled_task_initial_delay', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Scheduling - continuation task delay.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current continuation scheduled task delay in seconds.
     *
     * @return float
     *   Returns the continuation scheduled task delay in seconds.
     *
     * @see ::getScheduledTaskContinuationDelayDefault()
     * @see ::setScheduledTaskContinuationDelay()
     */
    public static function getScheduledTaskContinuationDelay()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('scheduled_task_continuation_delay') === NULL) {
            return self::getScheduledTaskContinuationDelayDefault();
        }

        return floatval($config->get('scheduled_task_continuation_delay'));
    }

    /**
     * Returns the default continuation scheduled task delay in seconds.
     *
     * @return float
     *   Returns the default continuation scheduled task delay.
     *
     * @see ::getScheduledTaskContinuationDelay()
     * @see ::setScheduledTaskContinuationDelay()
     */
    public static function getScheduledTaskContinuationDelayDefault()
    {
        return 3.0;
    }

    /**
     * Sets the current continuation scheduled task delay in seconds.
     *
     * The continuation scheduled task delay is the time offset from the current
     * time to the time at which a continuing scheduled task should execute.
     * Continuation tasks are intermediate tasks that perform phase after
     * phase of a multi-step operation. This could be multiple steps in doing
     * a large copy, delete, or move. A delay is needed to let page updates
     * happen, including any AJAX requests involved.
     *
     * The delay for a continuation task should be pretty short, such as a
     * few seconds.
     *
     * Values less than 3 seconds are ignored.
     *
     * @param float $value
     *   The continuation scheduled task delay in seconds.
     *
     * @see ::getScheduledTaskContinuationDelay()
     * @see ::getScheduledTaskContinuationDelayDefault()
     */
    public static function setScheduledTaskContinuationDelay(float $value)
    {
        if ($value < 3) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('scheduled_task_continuation_delay', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Scheduling - safety net task delay.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current safety net scheduled task delay in seconds.
     *
     * @return float
     *   Returns the safety net scheduled task delay in seconds.
     *
     * @see ::getScheduledTaskSafetyNetDelayDefault()
     * @see ::setScheduledTaskSafetyNetDelay()
     */
    public static function getScheduledTaskSafetyNetDelay()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('scheduled_task_safety_net_delay') === NULL) {
            return self::getScheduledTaskSafetyNetDelayDefault();
        }

        return floatval($config->get('scheduled_task_safety_net_delay'));
    }

    /**
     * Returns the default safety net scheduled task delay in seconds.
     *
     * @return float
     *   Returns the default safety net scheduled task delay.
     *
     * @see ::getScheduledTaskSafetyNetDelay()
     * @see ::setScheduledTaskSafetyNetDelay()
     */
    public static function getScheduledTaskSafetyNetDelayDefault()
    {
        return 120.0;
    }

    /**
     * Sets the current safety net scheduled task delay in seconds.
     *
     * The safety net scheduled task delay is the time offset from the current
     * time to the time at which a failsafe "safety net" scheduled task should
     * execute. Safety net tasks are automatically scheduled at the *start* of
     * any operation phase, such as the start of a copy, delete, or move.
     * The task's job is to continue the operation if the current process is
     * killed due to a crash, timeout, or other interrupt.
     *
     * The delay for the safety net task should be large enough to give the
     * task a chance to get some work done, yet short enough that the user
     * doesn't have to wait a long time after a crash before the task resumes.
     *
     * Values less than 3 seconds are ignored.
     *
     * @param float $value
     *   The safety net scheduled task delay in seconds.
     *
     * @see ::getScheduledTaskSafetyNetDelay()
     * @see ::getScheduledTaskSafetyNetDelayDefault()
     */
    public static function setScheduledTaskSafetyNetDelay(float $value)
    {
        if ($value < 3) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('scheduled_task_safety_net_delay', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Limits - memory use limit percentage.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current memory use limit percentage.
     *
     * @return float
     *   Returns the memory use limit percentage as a faction
     *   (e.g. 50% = 0.5).
     *
     * @see ::getMemoryUseLimitPercentageDefault()
     * @see ::setMemoryUseLimitPercentage()
     */
    public static function getMemoryUseLimitPercentage()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('memory_use_limit_percentage') === NULL) {
            return self::getMemoryUseLimitPercentageDefault();
        }

        return floatval($config->get('memory_use_limit_percentage'));
    }

    /**
     * Returns the default memory use limit percentage.
     *
     * @return float
     *   Returns the default memory use limit percentage as a faction
     *   (e.g. 50% = 0.5).
     *
     * @see ::getMemoryUseLimitPercentage()
     */
    public static function getMemoryUseLimitPercentageDefault()
    {
        return 0.80;
    }

    /**
     * Sets the current memory use limit percentage.
     *
     * Large operations, such as copying a big folder tree, may require long
     * run times and loading and storing many entities. Drupal core, and
     * third-party modules, have memory leaks that cause memory use to
     * gradually increase as an active process runs. If memory use reaches
     * PHP's configured "memory_limit", the process will be aborted. This
     * abort can leave operations incomplete and in an indetermine state.
     * It can also corrupt the file system.
     *
     * To avoid memory limit caused aborts, this module's code may monitor
     * its memory use and intentially stop and schedule a background task
     * to continue the work. When that task runs, it may again get near
     * the memory limit and intentionally stop and reschedule, and so on until
     * the work is done.
     *
     * PHP's memory use limit is measured in bytes. Site administrators may
     * set this in the PHP configuration file or in website-specific files,
     * such as Apache's .htaccess. The default value is 128 Mbytes.
     *
     * The memory limit is computed as a percentage of PHP's memory limit.
     * This allows the limit to float up or down based on whatever the
     * website administrator has chosen. The percentage should never be 100%,
     * or code will be aborted. A maximum practical value is probably 90%,
     * which leaves a small margin for the task to clean up and schedule a
     * new task.
     *
     * While this memory limit is primarily used by background tasks, it may
     * be used anywhere in module code.
     *
     * @param float $value
     *   The memory use limit percentage as a faction (e.g. 50% = 0.5).
     *   Values less than 0.3 or greater than 0.9 are ignored.
     *
     * @see ::getMemoryUseLimitPercentage()
     * @see ::getMemoryUseLimitPercentageDefault()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getPhpMemoryUseLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getMemoryUseLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::aboveMemoryUseLimit()
     */
    public static function setMemoryUseLimitPercentage(float $value)
    {
        if ($value > 0.9 || $value < 0.3) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('memory_use_limit_percentage', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Limits - execution time limit percentage.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current execution time limit percentage.
     *
     * @return float
     *   Returns the execution time limit percentage.
     *
     * @see ::getExecutionTimeLimitPercentageDefault()
     * @see ::setExecutionTimeLimitPercentage()
     * @see ::getResponseExecutionTimeLimitDefault()
     * @see ::getResponseExecutionTimeLimit()
     * @see ::setResponseExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getPhpExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::aboveExecutionTimeLimit()
     */
    public static function getExecutionTimeLimitPercentage()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('execution_time_limit_percentage') === NULL) {
            return self::getExecutionTimeLimitPercentageDefault();
        }

        return floatval($config->get('execution_time_limit_percentage'));
    }

    /**
     * Returns the default execution time limit percentage.
     *
     * @return float
     *   Returns the default execution time limit percentage
     *
     * @see ::getExecutionTimeLimitPercentage()
     * @see ::setExecutionTimeLimitPercentage()
     * @see ::getResponseExecutionTimeLimitDefault()
     * @see ::getResponseExecutionTimeLimit()
     * @see ::setResponseExecutionTimeLimit()
     */
    public static function getExecutionTimeLimitPercentageDefault()
    {
        return 0.80;
    }

    /**
     * Sets the current execution time limit percentage.
     *
     * Large operations, such as a deep folder tree copy, could run for minutes
     * or even an hour or more, but trying to do so can easily exceed two
     * execution time limits:
     *
     * - PHP's 'max_execution_time', set by site administrators in PHP's
     *   configuration file or in website-specific files, such as Apache's
     *   .htaccess. This defaults to 30 seconds.
     *
     * - A web server's maximum time, such as Apache's 'TimeOut' from the
     *   server's configuration file. Other servers have equivalent features.
     *   This often defaults to 300 seconds.
     *
     * If execution time excedes these limits, the process will be aborted.
     * This will cause the current operation to stop immediately and leave
     * things in an indetermine state that can corrupt the file system.
     *
     * To avoid this abort, this module's code may monitor execution time
     * and intentionally stop and schedule a task to continue the operation
     * later. That task will start later in a new process and again do work
     * until it gets close to the execution time limit.
     *
     * The execution time limit is computed as a percentage of PHP's execution
     * time limit. This allows the limit to float up or down based on whatever the
     * website administrator has chosen. The percentage should never be 100%,
     * or code will be aborted. A maximum practical value is probably 90%,
     * which leaves a small margin for the task to clean up and schedule a
     * new task.
     *
     * While this exeuction time limit is primarily used by background tasks,
     * it may be used anywhere in module code.
     *
     * @param float $value
     *   The execution time limit percentage.
     *
     * @see ::getExecutionTimeLimitPercentage()
     * @see ::getExecutionTimeLimitPercentageDefault()
     * @see ::getResponseExecutionTimeLimitDefault()
     * @see ::getResponseExecutionTimeLimit()
     * @see ::setResponseExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getPhpExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::aboveExecutionTimeLimit()
     */
    public static function setExecutionTimeLimitPercentage(float $value)
    {
        if ($value > 0.9 || $value < 0.3) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('execution_time_limit_percentage', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * Limits - immediate response execution time limit.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current immediate response execution time limit, in seconds.
     *
     * @return float
     *   Returns the immediate response execution time limit, in seconds.
     *
     * @see ::getExecutionTimeLimitPercentage()
     * @see ::getExecutionTimeLimitPercentageDefault()
     * @see ::setExecutionTimeLimitPercentage()
     * @see ::getResponseExecutionTimeLimitDefault()
     * @see ::setResponseExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getPhpExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::aboveExecutionTimeLimit()
     */
    public static function getResponseExecutionTimeLimit()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('response_execution_time_limit') === NULL) {
            return self::getResponseExecutionTimeLimitDefault();
        }

        return floatval($config->get('response_execution_time_limit'));
    }

    /**
     * Returns the default immediate response execution time limit, in seconds.
     *
     * @return float
     *   Returns the default immediate response execution time limit, in seconds.
     *
     * @see ::getExecutionTimeLimitPercentage()
     * @see ::getExecutionTimeLimitPercentageDefault()
     * @see ::setExecutionTimeLimitPercentage()
     * @see ::getResponseExecutionTimeLimit()
     * @see ::setResponseExecutionTimeLimit()
     */
    public static function getResponseExecutionTimeLimitDefault()
    {
        return 5.0;
    }

    /**
     * Sets the current immediate response execution time limit, in seconds.
     *
     * The immediate response time limit is the maximum time an operation may
     * take while responding immediately to a request. Any work that takes
     * longer than this should be deferred into a scheduled task.
     *
     * This time limit, in seconds, is typically quite short so that a response
     * can be returned to the user quickly enough for the user to feel that
     * the website is responding properly. A good value is about 5 seconds.
     * Shorter values leave very little time for the operation to do any real
     * work before scheduling a task. Longer values over about 10 seconds
     * reduce the interactivity of the site and annoy users.
     *
     * This time limit differs from the PHP execution time limit, which is
     * usually 30 seconds or more and far too long to maintain an interactive
     * feel for the website. This time limit also differs from this module's
     * execution time limit percentage, which computes a maximum time based
     * on PHP's time limit. Such a maximum time is useful for background tasks,
     * but far too long for responding quickly to a user.
     *
     * @param float $value
     *   The immediate response execution time limit, in seconds. Values
     *   less than 1.0 second are ignored.
     *
     * @see ::getExecutionTimeLimitPercentage()
     * @see ::getExecutionTimeLimitPercentageDefault()
     * @see ::setExecutionTimeLimitPercentage()
     * @see ::getResponseExecutionTimeLimit()
     * @see ::getResponseExecutionTimeLimitDefault()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getPhpExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::getExecutionTimeLimit()
     * @see \Drupal\foldershare\Utilities\LimitUtilities::aboveExecutionTimeLimit()
     */
    public static function setResponseExecutionTimeLimit(float $value)
    {
        if ($value < 1.0) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('response_execution_time_limit', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - Javascript polling interval.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current status polling interval in seconds.
     *
     * @return float
     *   Returns the status polling interval in seconds.
     *
     * @see ::getStatusPollingIntervalDefault()
     * @see ::setStatusPollingInterval()
     */
    public static function getStatusPollingInterval()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('status_polling_interval') === NULL) {
            return self::getStatusPollingIntervalDefault();
        }

        return floatval($config->get('status_polling_interval'));
    }

    /**
     * Returns the default status polling interval in seconds.
     *
     * @return float
     *   Returns the default status polling interval in seconds.
     *
     * @see ::getStatusPollingInterval()
     * @see ::setStatusPollingInterval()
     */
    public static function getStatusPollingIntervalDefault()
    {
        return 5.0;
    }

    /**
     * Sets the current status polling interval in seconds.
     *
     * @param float $value
     *   The status polling interval in seconds.
     *
     * @see ::getStatusPollingInterval()
     * @see ::getStatusPollingIntervalDefault()
     */
    public static function setStatusPollingInterval(float $value)
    {
        if ($value < 3) {
            return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('status_polling_interval', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - User autocomplete style.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current user autocomplete style.
     *
     * @return string
     *   Returns the current style.
     *
     * @see ::getUserAutocompleteStyleDefault()
     * @see ::setUserAutocompleteStyle()
     */
    public static function getUserAutocompleteStyle()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('user_autocomplete_style') === NULL) {
            return self::getUserAutocompleteStyleDefault();
        }

        return $config->get('user_autocomplete_style');
    }

    /**
     * Returns the default user autocomplete style.
     *
     * @return string
     *   Returns the default style name.
     *
     * @see ::getUserAutocompleteStyle()
     * @see ::setUserAutocompleteStyle()
     */
    public static function getUserAutocompleteStyleDefault()
    {
        return "name-only";
    }

    /**
     * Sets the current user autocomplete style.
     *
     * The value must be one of:
     * * 'none' to disable auto-complete.
     * * 'name-only' to only show user names.
     * * 'name-email' to show user names and email addresses.
     * * 'name-masked-email' to show user names and partly masked email
     *   addresses.
     *
     * Any other value is silently ignored.
     *
     * @param string $value
     *   The autocomplete style name.
     *
     * @see ::getUserAutocompleteStyle()
     * @see ::getUserAutocompleteStyleDefault()
     */
    public static function setUserAutocompleteStyle(string $value)
    {
        // Validate to known styles.
        switch ($value) {
            case 'none':
            case 'name-only':
            case 'name-email':
            case 'name-masked-email':
                break;

            default:
                return;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('user_autocomplete_style', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - command menu.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current command menu enable flag.
     *
     * The command menu button is shown on a toolbar above the file/folder list.
     * Pressing the button shows a menu of available commands, such as those
     * to create a new folder, delete, move, and copy. When disabled, the button
     * and menu are not shown in the user interface.
     *
     * @return bool
     *   Returns if TRUE if the command menu is enabled, and FALSE otherwise.
     *
     * @see ::getUserInterfaceCommandMenuEnableDefault()
     * @see ::setUserInterfaceCommandMenuEnable()
     */
    public static function getUserInterfaceCommandMenuEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('ui_command_menu') === NULL) {
            return self::getUserInterfaceCommandMenuEnableDefault();
        }

        return boolval($config->get('ui_command_menu'));
    }

    /**
     * Returns the default command menu enable flag.
     *
     * @return bool
     *   Returns TRUE if the command menu is enabled by default, and
     *   FALSE otherwise.
     *
     * @see ::getUserInterfaceCommandMenuEnable()
     * @see ::setUserInterfaceCommandMenuEnable()
     */
    public static function getUserInterfaceCommandMenuEnableDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current command menu enable flag.
     *
     * The command menu button is shown on a toolbar above the file/folder list.
     * Pressing the button shows a menu of available commands, such as those
     * to create a new folder, delete, move, and copy. When disabled, the button
     * and menu are not shown in the user interface.
     *
     * Without the command menu, there is no built-in way to issue commands to
     * upload, download, create folders, copy, move, delete, etc. Disabling
     * the command menu should only be done if an alternate user interface
     * is made available.
     *
     * @param bool $value
     *   TRUE to enable the command menu, and FALSE to disable.
     *
     * @see ::getUserInterfaceCommandMenuEnable()
     * @see ::getUserInterfaceCommandMenuEnableDefault()
     */
    public static function setUserInterfaceCommandMenuEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('ui_command_menu', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - ancestor menu.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current ancestor menu enable flag.
     *
     * The ancestor menu button is shown on a toolbar above the file/folder list.
     * Pressing the button shows a menu of ancestor folders above the current
     * page's file/folder. Selecting one jumps to a page showing that ancestor.
     * When disabled, the button and menu are not shown in the user interface.
     *
     * @return bool
     *   Returns if TRUE if the ancestor menu is enabled, and FALSE otherwise.
     *
     * @see ::getUserInterfaceAncestorMenuEnableDefault()
     * @see ::setUserInterfaceAncestorMenuEnable()
     */
    public static function getUserInterfaceAncestorMenuEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('ui_ancestor_menu') === NULL) {
            return self::getUserInterfaceAncestorMenuEnableDefault();
        }

        return boolval($config->get('ui_ancestor_menu'));
    }

    /**
     * Returns the default ancestor menu enable flag.
     *
     * @return bool
     *   Returns TRUE if the ancestor menu is enabled by default, and
     *   FALSE otherwise.
     *
     * @see ::getUserInterfaceAncestorMenuEnable()
     * @see ::setUserInterfaceAncestorMenuEnable()
     */
    public static function getUserInterfaceAncestorMenuEnableDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current ancestor menu enable flag.
     *
     * The ancestor menu button is shown on a toolbar above the file/folder list.
     * Pressing the button shows a menu of ancestor folders above the current
     * page's file/folder. Selecting one jumps to a page showing that ancestor.
     * When disabled, the button and menu are not shown in the user interface.
     *
     * @param bool $value
     *   TRUE to enable the ancestor menu, and FALSE to disable.
     *
     * @see ::getUserInterfaceAncestorMenuEnable()
     * @see ::getUserInterfaceAncestorMenuEnableDefault()
     */
    public static function setUserInterfaceAncestorMenuEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('ui_ancestor_menu', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - search box.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current search box enable flag.
     *
     * The search box is a textfield shown on a toolbar above the file/folder
     * list. Entering search text and pressing return initiates a search from
     * the current folder down through the folder tree. If the Drupal core
     * search module is not enabled, the search plugin is not enabled, the user
     * does not have search permission, or the search box is disabled, the search
     * box is not shown in the user interface.
     *
     * @return bool
     *   Returns if TRUE if the search box is enabled, and FALSE otherwise.
     *
     * @see ::getUserInterfaceSearchBoxEnableDefault()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function getUserInterfaceSearchBoxEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('ui_search_box') === NULL) {
            return self::getUserInterfaceSearchBoxEnableDefault();
        }

        return boolval($config->get('ui_search_box'));
    }

    /**
     * Returns the default search box enable flag.
     *
     * @return bool
     *   Returns TRUE if the search box is enabled by default, and
     *   FALSE otherwise.
     *
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::setUserInterfaceSearchBoxEnable()
     */
    public static function getUserInterfaceSearchBoxEnableDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current search box enable flag.
     *
     * The search box is a textfield shown on a toolbar above the file/folder
     * list. Entering search text and pressing return initiates a search from
     * the current folder down through the folder tree. If the Drupal core
     * search module is not enabled, the search plugin is not enabled, the user
     * does not have search permission, or the search box is disabled, the search
     * box is not shown in the user interface.
     *
     * @param bool $value
     *   TRUE to enable the search box, and FALSE to disable.
     *
     * @see ::getUserInterfaceSearchBoxEnable()
     * @see ::getUserInterfaceSearchBoxEnableDefault()
     */
    public static function setUserInterfaceSearchBoxEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('ui_search_box', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - folder browser sidebar.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current folder browser sidebar enable flag.
     *
     * The sidebar is a panel beside the folder browser's list.
     * The panel contains favorites links and temporary information,
     * such as file upload progress and details of the current selection.
     *
     * @return bool
     *   Returns if TRUE if the sidebar is enabled, and FALSE otherwise.
     *
     * @see ::getUserInterfaceSidebarEnableDefault()
     * @see ::setUserInterfaceSidebarEnable()
     */
    public static function getUserInterfaceSidebarEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('ui_folder_browser_sidebar') === NULL) {
            return self::getUserInterfaceSidebarEnableDefault();
        }

        return boolval($config->get('ui_folder_browser_sidebar'));
    }

    /**
     * Returns the default folder browser sidebar enable flag.
     *
     * @return bool
     *   Returns TRUE if the sidebar is enabled by default, and
     *   FALSE otherwise.
     *
     * @see ::getUserInterfaceSidebarEnable()
     * @see ::setUserInterfaceSidebarEnable()
     */
    public static function getUserInterfaceSidebarEnableDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current folder browser sidebar enable flag.
     *
     * The sidebar is a panel beside the folder browser's list.
     * The panel contains favorites links and temporary information,
     * such as file upload progress and details of the current selection.
     *
     * @param bool $value
     *   TRUE to enable the sidebar, and FALSE to disable.
     *
     * @see ::getUserInterfaceSidebarEnable()
     * @see ::getUserInterfaceSidebarEnableDefault()
     */
    public static function setUserInterfaceSidebarEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('ui_folder_browser_sidebar', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - folder browser sidebar show/hide button.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current folder browser sidebar button enable flag.
     *
     * The sidebar is a panel beside the folder browser's list.
     * The panel contains favorites links and temporary information,
     * such as file upload progress and details of the current selection.
     *
     * The sidebar button is a toolbar button that shows/hides the sidebar.
     *
     * @return bool
     *   Returns if TRUE if the sidebar button is enabled, and FALSE otherwise.
     *
     * @see ::getUserInterfaceSidebarButtonEnableDefault()
     * @see ::setUserInterfaceSidebarButtonEnable()
     */
    public static function getUserInterfaceSidebarButtonEnable()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('ui_folder_browser_sidebar_button') === NULL) {
            return self::getUserInterfaceSidebarButtonEnableDefault();
        }

        return boolval($config->get('ui_folder_browser_sidebar_button'));
    }

    /**
     * Returns the default folder browser sidebar button enable flag.
     *
     * @return bool
     *   Returns TRUE if the sidebar button is enabled by default, and
     *   FALSE otherwise.
     *
     * @see ::getUserInterfaceSidebarButtonEnable()
     * @see ::setUserInterfaceSidebarButtonEnable()
     */
    public static function getUserInterfaceSidebarButtonEnableDefault()
    {
        return TRUE;
    }

    /**
     * Sets the current folder browser sidebar button enable flag.
     *
     * The sidebar is a panel beside the folder browser's list.
     * The panel contains favorites links and temporary information,
     * such as file upload progress and details of the current selection.
     *
     * The sidebar button is a toolbar button that shows/hides the sidebar.
     *
     * @param bool $value
     *   TRUE to enable the sidebar button, and FALSE to disable.
     *
     * @see ::getUserInterfaceSidebarButtonEnable()
     * @see ::getUserInterfaceSidebarButtonEnableDefault()
     */
    public static function setUserInterfaceSidebarButtonEnable(bool $value)
    {
        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('ui_folder_browser_sidebar_button', $value);
        $config->save(TRUE);
    }

    /*---------------------------------------------------------------------
     *
     * User interface - maximum folder browser height.
     *
     *---------------------------------------------------------------------*/

    /**
     * Returns the current folder browser maximum height (ems).
     *
     * The folder browser shows a list of files and folders within a folder
     * or root list. The maximum height sets the folder browser's size on a
     * page. When there is no maximum, the size is as large as needed to show
     * the list of files and folders. When there is a maximum, the browser's
     * size is constrained on the page and it scrolls internally to show more.
     *
     * @return int
     *   Returns the maximum height in ems. A zero means there is no maximum.
     *
     * @see ::getUserInterfaceFolderBrowserMaximumHeightDefault()
     * @see ::setUserInterfaceFolderBrowserMaximumHeight()
     */
    public static function getUserInterfaceFolderBrowserMaximumHeight()
    {
        $config = Drupal::config(Constants::SETTINGS);
        if ($config->get('ui_folder_browser_maximum_height') === NULL) {
            return self::getUserInterfaceFolderBrowserMaximumHeightDefault();
        }

        return intval($config->get('ui_folder_browser_maximum_height'));
    }

    /**
     * Returns the default folder browser maximum height (ems).
     *
     * @return int
     *   Returns the default maximum height in ems. A zero means there is
     *   no maximum.
     *
     * @see ::getUserInterfaceFolderBrowserMaximumHeight()
     * @see ::setUserInterfaceFolderBrowserMaximumHeight()
     */
    public static function getUserInterfaceFolderBrowserMaximumHeightDefault()
    {
        return 0;
    }

    /**
     * Sets the current folder browser maximum height (ems).
     *
     * The folder browser shows a list of files and folders within a folder
     * or root list. The maximum height sets the folder browser's size on a
     * page. When there is no maximum, the size is as large as needed to show
     * the list of files and folders. When there is a maximum, the browser's
     * size is constrained on the page and it scrolls internally to show more.
     *
     * @param int $value
     *   The maximum height in ems. A zero means there is no maximum. Negative
     *   values are treated as zero.
     *
     * @see ::getUserInterfaceFolderBrowserMaximumHeight()
     * @see ::getUserInterfaceFolderBrowserMaximumHeight()
     */
    public static function setUserInterfaceFolderBrowserMaximumHeight(int $value)
    {
        if ($value < 0) {
            $value = 0;
        }

        $config = Drupal::configFactory()->getEditable(Constants::SETTINGS);
        $config->set('ui_folder_browser_maximum_height', $value);
        $config->save(TRUE);
    }

}
