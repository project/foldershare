<?php

namespace Drupal\foldershare;

use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareScheduledTask;
use Drupal\foldershare\Utilities\ConfigurationUtilities;
use Drupal\foldershare\Utilities\LinkUtilities;

/**
 * Manages search features for FolderShare entities.
 *
 * This class provides static methods to manage search indexing for
 * FolderShare entities. Supported operations include:
 * - Clearing the index.
 * - Deleting index entries for specific entities.
 * - Marking index entries as out of date for specific entities.
 * - Updating the index.
 *
 * This method also provides methods to query configuration settings for
 * indexing and check the amount of indexing work pending.
 *
 * The following search modules are supported:
 * - "search" in Drupal core. See also FolderShare's search plugin.
 *
 * <B>Access control</B>
 * This class's methods do not do access control. The caller should check
 * access as needed by their situation.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\foldershare\Settings
 * @see \Drupal\foldershare\Entity\FolderShare
 * @see \Drupal\foldershare\Entity\FolderShare\Plugin\Search\FolderShareSearch
 */
final class ManageSearch {

  /*--------------------------------------------------------------------
   *
   * Search provider queries.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns TRUE if there is a search provider.
   *
   * @return bool
   *   Returns TRUE if a search provider is available.
   *
   * @see ::isCoreSearchAvailable()
   */
  public static function isSearchAvailable() {
    return self::isCoreSearchAvailable() ||
      self::isSearchAPIAvailable();
  }

  /**
   * Returns TRUE if the Drupal core Search module is enabled.
   *
   * @return bool
   *   Returns TRUE if core Search is available.
   */
  public static function isCoreSearchAvailable() {
    return \Drupal::moduleHandler()->moduleExists('search');
  }

  /**
   * Returns TRUE if the Drupal Search API module is enabled.
   *
   * @return bool
   *   Returns TRUE if Search API is available.
   */
  public static function isSearchAPIAvailable() {
    return \Drupal::moduleHandler()->moduleExists('search_api');
  }

  /**
   * Returns TRUE if the Drupal Search API module is configured.
   *
   * The module is configured if:
   * - It is installed.
   * - It has at least one database server configured.
   * - It has at least one database index supporting FolderShare entities.
   *
   * @return bool
   *   Returns TRUE if Search API is configured.
   */
  public static function isSearchAPIConfigured() {
    if (\Drupal::moduleHandler()->moduleExists('search_api') === FALSE) {
      return FALSE;
    }

    // The module is installed. But are there any database servers
    // configured for it? If not, then it isn't really available.
    try {
      $serverStorage = \Drupal::entityTypeManager()->getStorage(
        'search_api_server');
      $servers = $serverStorage->loadMultiple(NULL);
      if (count($servers) === 0) {
        // No servers yet.
        return FALSE;
      }
    }
    catch (\Exception $e) {
      // Failure to load server entities?
      return FALSE;
    }

    // The module is installed and there is at least one server. But
    // is there any server configured to index FolderShare entities yet?
    $indexInfo = self::searchAPIFindFolderShareIndexes();
    if (empty($indexInfo) === TRUE) {
      // No relevant indexes yet.
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Returns TRUE if the Drupal Search API Autocomplete module is enabled.
   *
   * @return bool
   *   Returns TRUE if Search API Autocomplete is available.
   */
  public static function isSearchAPIAutocompleteAvailable() {
    return \Drupal::moduleHandler()->moduleExists('search_api_autocomplete');
  }

  /**
   * Returns TRUE if the Drupal Search API Pages module is enabled.
   *
   * @return bool
   *   Returns TRUE if Search API Pages is available.
   */
  public static function isSearchAPIPagesAvailable() {
    return \Drupal::moduleHandler()->moduleExists('search_api_page');
  }

  /**
   * Returns the internal module id of the search provider, if any.
   *
   * The module ID is the name in code for the module. The id is in the
   * module's namespace and directory name. This differs from the public
   * module name, but usually only in case. For instance, the Drupal core
   * search module's ID is "search" and the module's name is "Search".
   *
   * It is poossible for multiple search providers to be enabled (though
   * this is awkward and not recommended). In this case, providers are
   * checked in this order and the first one found is returned:
   * - Search API
   * - core Search
   *
   * @return string
   *   Returns the id of the search module in use, or an empty string
   *   if there is none.
   */
  public static function getSearchModuleId() {
    if (self::isSearchAPIAvailable() === TRUE) {
      return 'search_api';
    }
    if (self::isCoreSearchAvailable() === TRUE) {
      return 'search';
    }
    return '';
  }

  /**
   * Returns the public name of the search provider, if any.
   *
   * The module name is the name in shown in module lists for a module.
   * The name is often just a title case version of the module's ID.
   * For instance, the Drupal core search module's ID is "search" and the
   * module's name is "Search".
   *
   * @return string
   *   Returns the name of the search module in use, or an empty string
   *   if there is none.
   *
   * @see ::isSearchAvailable()
   * @see ::isCoreSearchAvailable()
   */
  public static function getSearchModuleName() {
    $id = self::getSearchModuleId();
    if (empty($id) === TRUE) {
      return '';
    }
    return \Drupal::moduleHandler()->getName($id);
  }

  /**
   * Returns the public name of the core Search module.
   *
   * @return string
   *   Returns the name of the core Search module.
   */
  public static function getCoreSearchModuleName() {
    if (self::isCoreSearchAvailable() === TRUE) {
      return \Drupal::moduleHandler()->getName('search');
    }
    return 'Search';
  }

  /**
   * Returns the public name of the Search API module.
   *
   * @return string
   *   Returns the name of the Search API module.
   */
  public static function getSearchAPIModuleName() {
    if (self::isSearchAPIAvailable() === TRUE) {
      return \Drupal::moduleHandler()->getName('search_api');
    }
    return "Search API";
  }

  /**
   * Returns the public name of the Search API Pages module.
   *
   * @return string
   *   Returns the name of the Search API Pages module.
   */
  public static function getSearchAPIPagesModuleName() {
    if (self::isSearchAPIPagesAvailable() === TRUE) {
      return \Drupal::moduleHandler()->getName('search_api_page');
    }
    return "Search API Pages";
  }

  /*--------------------------------------------------------------------
   *
   * Preferred search.
   *
   *-------------------------------------------------------------------*/
  /**
   * Updates the preferred search module based upon what is installed.
   *
   * The site may have the Drupal core Search or contributed Search API
   * modules both installed, just one, or neither. When there are two
   * modules installed (which is hopefully rare), then the site admin
   * must select which one to use with the folder browser's toolbar
   * search box. That module is the "preferred" search module.
   *
   * But as modules are installed and uninstalled, the preferred search
   * module may no longer be available. This method checks availability
   * and, if needed, updates the preferred search choice.
   *
   * @return string
   *   Returns the module ID of the preferred search module.
   *
   * @see Settings::getPreferredSearchModuleId()
   * @see Settings::setPreferredSearchModuleId()
   * @see self::isCoreSearchAvailable()
   * @see self::isSearchAPIAvailable()
   * @see foldershare_modules_installed()
   * @see foldershare_modules_uninstalled()
   */
  public static function updatePreferredSearch() {
    // Get the current preference, if any.
    $id = Settings::getPreferredSearchModuleId();

    if (empty($id) === TRUE) {
      // There is no preferred search yet. Pick one.
      if (\Drupal::moduleHandler()->moduleExists('search_api') === TRUE) {
        $id = 'search_api';
      }
      elseif (\Drupal::moduleHandler()->moduleExists('search') === TRUE) {
        $id = 'search';
      }

      Settings::setPreferredSearchModuleId($id);
      return $id;
    }

    if (\Drupal::moduleHandler()->moduleExists($id) === TRUE) {
      // There is a preferred search and that module is installed.
      return $id;
    }

    // The preferred module is not still installed. Look for an
    // alternative.
    switch ($id) {
      case 'search':
        // Core search is preferred, but not installed. Switch to
        // Search API if it is installed. However, just because it is
        // installed does not mean it has been configured yet.
        if (\Drupal::moduleHandler()->moduleExists('search_api') === TRUE) {
          $id = 'search_api';
        }
        else {
          $id = '';
        }
        break;

      case 'search_api':
        // Search API is preferred, but not installed. Switch to
        // core Search if it is installed.
        if (\Drupal::moduleHandler()->moduleExists('search') === TRUE) {
          $id = 'search';
        }
        else {
          $id = '';
        }
        break;

      default:
        // Unknown preferrence.
        $id = '';
        break;
    }

    Settings::setPreferredSearchModuleId($id);
    return $id;
  }

  /*--------------------------------------------------------------------
   *
   * Search permissions queries.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns TRUE if the current user has permission to search.
   *
   * If the Drupal core search module is installed, the "search content"
   * permission is checked for the current user.
   *
   * Otherwise the function returns FALSE.
   *
   * @return bool
   *   Returns TRUE if the current user has search permission, and FALSE
   *   otherwise.
   */
  public static function userHasSearchPermission() {
    if (self::isCoreSearchAvailable() === TRUE ||
        self::isSearchAPIAvailable() === TRUE) {
      // Core Search and the Search API module both use the same
      // search permission.
      return \Drupal::currentUser()->hasPermission('search content');
    }
    return FALSE;
  }

  /*--------------------------------------------------------------------
   *
   * Search module links.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns a link to the current or default search module list entry.
   *
   * If there is a search module enabled, the returned link goes to
   * that module's entry in the system module's list (i.e. the admin's
   * "Extend" page).
   *
   * If there is no search module enabled, the return link defaults to the
   * entry for Drupal core's "search" module.
   *
   * @return string
   *   The link to the current or default search entry in the site's
   *   list of modules.
   */
  public static function createCoreSearchEnableLink() {
    return LinkUtilities::createRouteLink(
      'system.modules_list',
      'module-search',
      \Drupal::moduleHandler()->getName('search'));
  }

  /**
   * Returns a link to the FolderShare core search plugin.
   *
   * If the Drupal core search module is enabled, the returned link goes
   * to the FolderShare search plugin's edit page.
   *
   * If the Drupal core search module is not enabled, an empty string
   * is returned.
   *
   * @return string
   *   The link to the FolderShare core search plugin, or an empty string if
   *   core search is not enabled.
   */
  public static function createCoreSearchPluginLink() {
    if (self::isCoreSearchAvailable() === FALSE) {
      return '';
    }
    return LinkUtilities::createRouteLink(
      'entity.search_page.edit_form',
      '',
      t('Search plugin'),
      [
        'search_page' => Constants::SEARCH_PLUGIN,
      ]);
  }

  /**
   * Returns a link to the search plugin list for core search.
   *
   * If the Drupal core search module is enabled, the returned link goes
   * to that module's list of search plugins.
   *
   * If the Drupal core search module is not enabled, an empty string
   * is returned.
   *
   * @return string
   *   The link to the core search plugin list, or an empty string if
   *   core search is not enabled.
   */
  public static function createCoreSearchConfigurationLink() {
    if (self::isCoreSearchAvailable() === FALSE) {
      return '';
    }
    return LinkUtilities::createRouteLink(
      'entity.search_page.collection',
      '',
      \Drupal::moduleHandler()->getName('search'));
  }

  /**
   * Returns a link to the search database list for the Search API.
   *
   * If the Search API module is enabled, the returned link goes
   * to that module's list of database plugins.
   *
   * If the Search API module is not enabled, an empty string
   * is returned.
   *
   * @return string
   *   The link to the Search API database plugin list, or an empty string if
   *   module is not enabled.
   */
  public static function createSearchAPIConfigurationLink() {
    if (self::isSearchAPIAvailable() === FALSE) {
      return '';
    }
    return LinkUtilities::createRouteLink(
      'search_api.overview',
      '',
      \Drupal::moduleHandler()->getName('search_api'));
  }

  /**
   * Returns a link to the help page of the current search provider.
   *
   * If there is a search provider, a link to that module's admin help
   * page is returned.
   *
   * Otherwise, an empty string is returned.
   *
   * @return string
   *   The link to the search provider help page, or an empty string if
   *   there is no search provider enabled.
   */
  public static function createSearchHelpLink() {
    $id = self::getSearchModuleId();
    if (empty($id) === TRUE) {
      return '';
    }
    return LinkUtilities::createHelpLink($id, t('help'));
  }

  /*--------------------------------------------------------------------
   *
   * Search configurations.
   *
   *-------------------------------------------------------------------*/
  /**
   * Reverts the FolderShare core search plugin to its initial configuration.
   *
   * If the Drupal core search module is enabled, the configuration for
   * FolderShare's search plugin is reverted to the original settings
   * shipped with the module.
   *
   * If the Drupal core search module is not enabled, no action is taken.
   *
   * @return bool
   *   Returns TRUE on success and FALSE on failure.
   */
  public static function revertCoreSearchPluginConfiguration() {
    if (self::isCoreSearchAvailable() === TRUE) {
      return ConfigurationUtilities::revertConfiguration(
        'core',
        'search.page.' . Constants::SEARCH_PLUGIN);
    }
    return TRUE;
  }

  /*--------------------------------------------------------------------
   *
   * Core search plugin operations.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns TRUE if the module's search plugin is enabled.
   *
   * If the Drupal core "search" module is not enabled, FolderShare's
   * search plugin is not enabled, this method returns FALSE.
   *
   * @return bool
   *   Returns TRUE if the search plugin is enabled.
   *
   * @see \Drupal\foldershare\Entity\FolderShare\Plugin\Search\FolderShareSearch
   */
  public static function isCoreSearchPluginEnabled() {
    return (self::findCoreSearchPlugin() !== NULL);
  }

  /**
   * Returns the Drupal core "search" plugin instance for FolderShare.
   *
   * The FolderShare module provides a custom search plugin that supports
   * search indexing of FolderShare entities and implements a
   * FolderShare-specific search form.
   *
   * If the Drupal core "search" module is not enabled or FolderShare's
   * search plugin is not enabled, this method returns a NULL.
   *
   * @return \Drupal\search\Plugin\SearchInterface
   *   Returns the module's search plugin object, or NULL if the plugin
   *   is not found or if it is currently disabled.
   *
   * @see \Drupal\foldershare\Entity\FolderShare\Plugin\Search\FolderShareSearch
   */
  private static function findCoreSearchPlugin() {
    if (self::isCoreSearchAvailable() === FALSE) {
      return NULL;
    }

    $searchPageRepository = \Drupal::service('search.search_page_repository');

    // Loop over all *indexable* search pages. A page is not indexable if
    // the page is disabled or the plugin does not support indexing.
    foreach ($searchPageRepository->getIndexableSearchPages() as $page) {
      $plugin = $page->getPlugin();
      if ($plugin->getType() === Constants::SEARCH_INDEX) {
        return $plugin;
      }
    }

    return NULL;
  }

  /**
   * Returns the list of filename extensions for indexed file content.
   *
   * @return string
   *   Returns a space-separated list of filename extensions supported for
   *   file content indexing.
   *
   * @see ::isIndexFileContentEnabled()
   * @see ::getIndexFileContentMaximumSize()
   */
  public static function getIndexFileContentFilenameExtensions() {
    if (self::isSearchAvailable() === FALSE) {
      // There is no support for search installed. Do nothing.
      return '';
    }
    return Settings::getAllowedSearchIndexFilenameExtensions();
  }

  /**
   * Returns the maximum number of bytes indexed from file content.
   *
   * @return int
   *   Returns the maximum number of bytes indexed from file content.
   *
   * @see ::isIndexFileContentEnabled()
   * @see ::getIndexFileContentFilenameExtensions()
   */
  public static function getIndexFileContentMaximumSize() {
    if (self::isSearchAvailable() === FALSE) {
      // There is no support for search installed. Do nothing.
      return 0;
    }
    return Settings::getSearchIndexMaximumFileSize();
  }

  /**
   * Returns TRUE if the search indexes file content.
   *
   * @return bool
   *   Returns TRUE if file content indexing is enabled.
   *
   * @see ::getIndexFileContentMaximumSize()
   * @see ::getIndexFileContentFilenameExtensions()
   */
  public static function isIndexFileContentEnabled() {
    if (self::isSearchAvailable() === FALSE) {
      // There is no support for search installed. Do nothing.
      return FALSE;
    }
    return Settings::getSearchIndexFileContentEnable();
  }

  /*--------------------------------------------------------------------
   *
   * Search index operations for core Search.
   *
   *-------------------------------------------------------------------*/
  /**
   * Clears the core Search index of all FolderShare entries.
   *
   * @see ::clearIndex()
   */
  public static function coreSearchClearIndex() {
    if (self::isCoreSearchAvailable() === FALSE) {
      // Core Search not installed.
      return;
    }

    // DEPRECATED. Remove this when Drupal 8.7 is no longer supported.
    $versionParts = explode('.', \Drupal::VERSION);
    $drupalVersion = floatval($versionParts[0] . '.' . $versionParts[1]);
    if ($drupalVersion < 8.8) {
      // Pre-Drupal 8.8, the 'search.index' service does not exist.
      // Call the deprecated search_index_clear() function.
      call_user_func('search_index_clear', Constants::SEARCH_INDEX);
      return;
    }
    // END DEPRECATED.
    try {
      \Drupal::service('search.index')->clear(Constants::SEARCH_INDEX);
    }
    catch (\Exception $e) {
      // No search index service?
    }
  }

  /**
   * Deletes the core Search index entries for a list of FolderShare items.
   *
   * @param int[] $ids
   *   An array of entity IDs to remove from the search index.
   */
  public static function coreSearchDeleteFromIndex(array $ids = []) {
    if (self::isCoreSearchAvailable() === FALSE) {
      // Core Search not installed.
      return;
    }

    // DEPRECATED. Remove this when Drupal 8.7 is no longer supported.
    $versionParts = explode('.', \Drupal::VERSION);
    $drupalVersion = floatval($versionParts[0] . '.' . $versionParts[1]);
    if ($drupalVersion < 8.8) {
      // Pre-Drupal 8.8, the 'search.index' service does not exist.
      // Call the deprecated search_index_clear() function.
      foreach ($ids as $id) {
        call_user_func(
          'search_index_clear',
          Constants::SEARCH_INDEX,
          $id,
          '');
      }
      return;
    }
    // END DEPRECATED.
    try {
      $searchIndex = \Drupal::service('search.index');
      foreach ($ids as $id) {
        $searchIndex->clear(
            Constants::SEARCH_INDEX,
            $id,
            '');
      }
    }
    catch (\Exception $e) {
      // No search index service?
    }
  }

  /**
   * Returns the indexing status for core Search.
   *
   * The returned status is an associative array with two values:
   * - 'total' has the total number of items.
   * - 'remaining' has the number of items to index.
   *
   * Zeros are returned if the Drupal core Search module is not installed,
   * or if this module's search plugin is not enabled.
   *
   * @return array
   *   Returns an associative array with 'total' and 'remaining' values.
   *
   * @see ::getIndexStatus()
   * @see \Drupal\search\Plugin\SearchIndexingInterface::indexStatus()
   * @see \Drupal\foldershare\Entity\FolderShare\Plugin\Search\FolderShareSearch
   */
  public static function coreSearchGetIndexStatus() {
    if (self::isCoreSearchAvailable() === TRUE) {
      $plugin = self::findCoreSearchPlugin();
      if ($plugin !== NULL) {
        return $plugin->indexStatus();
      }
    }

    return [
      'total'     => 0,
      'remaining' => 0,
    ];
  }

  /**
   * Marks a list of FolderShare items as in need of core Search reindexing.
   *
   * When an item is marked, future search indexing runs will rebuild
   * the index's entry for the item.
   *
   * This method is typically called whenever an item is changed and
   * saved back to the database.
   *
   * @param int[] $ids
   *   (optional, default = []) An array of entity IDs to mark for reindexing.
   *   If the list is empty, all FolderShare entities are marked.
   */
  public static function coreSearchMarkForReindex(array $ids = []) {
    if (self::isCoreSearchAvailable() === FALSE) {
      // Core Search not installed.
      return;
    }

    $plugin = self::findCoreSearchPlugin();
    if ($plugin === NULL) {
      // No search plugin? Nothing to do.
      return;
    }

    if (empty($ids) === TRUE) {
      // If the list is empty, mark everything in the search index.
      $plugin->markForReindex();
      return;
    }

    // DEPRECATED. Remove this when Drupal 8.7 is no longer supported.
    $versionParts = explode('.', \Drupal::VERSION);
    $drupalVersion = floatval($versionParts[0] . '.' . $versionParts[1]);
    if ($drupalVersion < 8.8) {
      // Pre-Drupal 8.8, the 'search.index' service does not exist.
      // Call the deprecated search_mark_for_reindex() function.
      foreach ($ids as $id) {
        call_user_func(
          'search_mark_for_reindex',
          Constants::SEARCH_INDEX,
          $id);
      }
      return;
    }
    // END DEPRECATED.
    try {
      $searchIndex = \Drupal::service('search.index');
      foreach ($ids as $id) {
        $searchIndex->markForReindex(Constants::SEARCH_INDEX, $id);
      }
    }
    catch (\Exception $e) {
      // No search index service?
    }
  }

  /**
   * Updates the core Search index for pending FolderShare items.
   *
   * If a list of items is provided, the index entries for those items
   * are updated. Otherwise any pending items are updated.
   *
   * @param int[] $ids
   *   (optional, default = NULL) An array of entity IDs to explicitly
   *   update in the search index. If the list is empty, all FolderShare
   *   entities are updated.
   */
  private static function coreSearchUpdateIndex(array $ids = []) {
    $plugin = self::findCoreSearchPlugin();
    if ($plugin === NULL) {
      // No search plugin? Nothing to do.
      return;
    }

    if (empty($ids) === TRUE) {
      // Update the index for everything pending.
      $plugin->updateIndex();
    }
    else {
      // A specific list of items to update was provided.
      $plugin->updateIndexById($ids);
    }
  }

  /*--------------------------------------------------------------------
   *
   * Search index operations for Search API.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns a list of Search API indexes supporting FolderShare entities.
   *
   * The Search API's list of data source indexes is searched to find
   * Index entities supporting FolderShare entities. An array of those
   * entities is returned.
   *
   * @return \Drupal\search_api\IndexInterface[]
   *   Returns an array of Search API index entities. An empty array is
   *   returned if there are none.
   */
  private static function searchAPIFindFolderShareIndexes() {
    $ret = [];
    try {
      $indexStorage = \Drupal::entityTypeManager()->getStorage(
        'search_api_index');
      $indexes = $indexStorage->loadMultiple(NULL);
      foreach ($indexes as $index) {
        $sources = $index->getEntityTypes();
        foreach ($sources as $entityTypeId) {
          if ($entityTypeId === FolderShare::ENTITY_TYPE_ID) {
            $ret[] = $index;
          }
        }
      }
    }
    catch (\Exception $e) {
      // No such entity, and yet the Search API module is installed.
      // It must not be configured for FolderShare yet.
    }
    return $ret;
  }

  /**
   * Clears the Search API index(s) of all FolderShare entries.
   *
   * @see ::clearIndex()
   */
  public static function searchAPIClearIndex() {
    if (self::isSearchAPIAvailable() === TRUE) {
      foreach (self::searchAPIFindFolderShareIndexes() as &$index) {
        $index->clear();
      }
    }
  }

  /**
   * Returns the indexing status for Search API.
   *
   * The returned status is an associative array with two values:
   * - 'total' has the total number of items.
   * - 'remaining' has the number of items to index.
   *
   * Zeros are returned if the Search API module is not installed,
   * or if this module's search plugin is not enabled.
   *
   * @return array
   *   Returns an associative array with 'total' and 'remaining' values.
   *
   * @see ::getIndexStatus()
   */
  public static function searchAPIGetIndexStatus() {
    if (self::isSearchAPIAvailable() === TRUE) {
      $total     = 0;
      $remaining = 0;
      foreach (self::searchAPIFindFolderShareIndexes() as &$index) {
        $tracker = $index->getTrackerInstance();
        $t = $tracker->getTotalItemsCount();
        $r = $t - $tracker->getIndexedItemsCount();
        $total += $t;
        $remaining += $r;
      }
      return [
        'total'     => $total,
        'remaining' => $remaining,
      ];
    }

    return [
      'total'     => 0,
      'remaining' => 0,
    ];
  }

  /*--------------------------------------------------------------------
   *
   * Search index operations.
   *
   *-------------------------------------------------------------------*/
  /**
   * Clears the search indexes of all FolderShare entries.
   *
   * When core Search module is in use, the FolderShare search plugin is
   * retrieved and used to clear its search index.
   *
   * When the Search API module is in use, the FolderShare search index
   * is found and cleared.
   *
   * @see ::coreSearchClearIndex()
   * @see ::searchAPIClearIndex()
   */
  public static function clearIndex() {
    self::coreSearchClearIndex();
    self::searchAPIClearIndex();
  }

  /**
   * Returns the indexing status.
   *
   * The returned status is an associative array with two values:
   * - 'total' has the total number of items.
   * - 'remaining' has the number of items to index.
   *
   * Zeros are returned if the Drupal core "search" module is not installed,
   * or if this module's search plugin is not enabled.
   *
   * @return array
   *   Returns an associative array with 'total' and 'remaining' values.
   */
  public static function getIndexStatus() {
    switch (Settings::getPreferredSearchModuleId()) {
      case 'search':
        return self::coreSearchGetIndexStatus();

      case 'search_api':
        return self::searchAPIGetIndexStatus();

      default:
        return [
          'total'     => 0,
          'remaining' => 0,
        ];
    }
  }

  /*--------------------------------------------------------------------
   *
   * Core search index scheduling.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns the core Search indexing task run interval in seconds.
   *
   * When core Search is in use, that module's updates to the search
   * index only occur when CRON is run. Optionally, FolderShare supports
   * a more rapid update to the search index by using a scheduled task.
   *
   * The current run interval is interpreted to compute and return the
   * interval time in seconds. A zero value indicates the interval is
   * 'system', which disables task scheduling and leaves indexing to CRON.
   *
   * @return int
   *   Returns the run interval in seconds. Zero is returned if the
   *   interval is 'system'.
   *
   * @see \Drupal\foldershare\Settings::getSearchIndexInterval()
   */
  private static function getIndexIntervalSeconds() {
    switch (Settings::getSearchIndexInterval()) {
      default:
      case 'system':
        // No indexing task.
        return 0;

      case '1min':
        // Legacy 1-minute interval is no longer supported. Map it to
        // 5 minutes.
        // 5 min * 60 seconds/minute.
        return (5 * 60);

      case '5min':
        // 5 min * 60 seconds/minute.
        return (5 * 60);

      case '10min':
        // 10 min * 60 seconds/minute.
        return (10 * 60);

      case '15min':
        // 15 min * 60 seconds/minute.
        return (15 * 60);

      case '30min':
        // 30 min * 60 seconds/minute.
        return (30 * 60);

      case 'hourly':
        // 60 minutes/hour * 60 seconds/minute.
        return (60 * 60);

      case 'daily':
        // 24 hours/day * 60 minutes/hour * 60 seconds/minute.
        return (24 * 60 * 60);
    }
  }

  /**
   * Schedules core Search indexing using the current indexing interval.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * When core Search is in use, this method schedules an optional task
   * that updates the search index on a schedule that is typically more
   * rapid than using CRON.
   *
   * This method always starts by deleting any pending search indexing tasks.
   *
   * If the core Search module is not installed, or if the search indexing
   * interval is set to 'system' (i.e. use CRON), then no indexing task is
   * scheduled.
   *
   * @see ::isCoreSearchPluginEnabled()
   * @see ::taskUpdateIndex()
   * @see \Drupal\foldershare\Settings::getSearchIndexInterval()
   */
  public static function scheduleSearchIndexing() {
    // Delete any pending search tasks.
    FolderShareScheduledTask::deleteTasks(
      '\Drupal\foldershare\ManageSearch::taskUpdateIndex');

    if (self::isCoreSearchAvailable() === FALSE) {
      // There is no support for core Search installed. Do nothing.
      return;
    }

    // Go no further if the search indexing task is not enabled.
    $seconds = self::getIndexIntervalSeconds();
    if ($seconds <= 0) {
      // There is no scheduled search index task. Do nothing.
      return;
    }

    $t = time();
    FolderShareScheduledTask::createTask(
      $t + $seconds,
      '\Drupal\foldershare\ManageSearch::taskUpdateIndex',
      (int) \Drupal::currentUser()->id(),
      NULL,
      $t,
      Settings::getSearchIndexInterval() . ' search index update',
      0,
      FolderShareScheduledTask::REPEATING_FLAG);
  }

  /*---------------------------------------------------------------------
   *
   * Core Search index update task.
   *
   *---------------------------------------------------------------------*/
  /**
   * Updates the core Search index as a scheduled task.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * When core Search is in use, this method updates the search index and
   * schedules itself to run again at the site admin's chosen indexing
   * interval.
   *
   * If the core Search module is not installed, then no indexing is done
   * and the task doe snot reschedule itself.
   *
   * If the core Search module is installed, but the indexing interval is
   * set to 'system' (i.e. use CRON), then the index is updated but the
   * task does not reschedule itself.
   *
   * @param int $requester
   *   The user ID of the user that requested the update. This is ignored.
   * @param array $parameters
   *   The queued task's parameters. This is ignored.
   * @param int $started
   *   The timestamp of the start date & time for an operation that causes
   *   a chain of tasks. This is ignored.
   * @param string $comments
   *   A comment on the current task. This is ignored.
   * @param int $executionTime
   *   The accumulated total execution time of the task chain, in seconds.
   *   This is ignored.
   *
   * @see ::isSearchAvailable()
   * @see ::scheduleSearchIndexing()
   * @see \Drupal\foldershare\Settings::getSearchIndexInterval()
   */
  public static function taskUpdateIndex(
    int $requester,
    array $parameters,
    int $started,
    string $comments,
    int $executionTime) {

    if (self::isCoreSearchAvailable() === FALSE) {
      // There is no support for core Search installed. Do nothing.
      return;
    }
    $plugin = self::findCoreSearchPlugin();
    if ($plugin === NULL) {
      // No core Search plugin? Nothing to do.
      return;
    }

    //
    // Prevent multiple search update tasks from running
    // -------------------------------------------------
    // This indexing task is invoked repeatedly every N seconds, where N
    // is the search indexing interval set by the admin. If an indexing
    // task is started and it takes longer than N to run, then another task
    // could get started, and another, and so on every N seconds. With
    // multiple tasks running, they can collide as they all try to index
    // pending entities. In the worst case, the first pending entity requires
    // an unusually long time to index. The first task stalls on it. The next
    // task starts up and stalls on it. Then the next task, and so on.
    //
    // This scenario has happened when N is short, file content is being
    // indexed, and file content is large. Gradually the server becomes
    // overloaded with redundant search index tasks and it grinds to a halt.
    //
    // We can force N to be less short, but that is not sufficient. An index
    // task could still take longer than N. We could force file content
    // indexing to be limited to smaller content, but that is not sufficient.
    // The file content could be locked in some way and the indexing task
    // still could get stalled.
    //
    // There are two fixes implemented:
    //
    // 1. If scheduled tasks are in use, check the queue to see if there is
    //    already another task there. If so, some other search indexing task
    //    is probably running so stop now.
    //
    // 2. If scheduled tasks are not in use (i.e. CRON is used instead) or if
    //    the index is being updated manually via drush or a UI for site
    //    admins, then check a process lock. If the index is locked, indexing
    //    is in progress so stop now.
    //
    // Fix 1 is implemented here. Fix 2 is implemented by the core Search
    // plugin itself in its updateIndex() method.
    $nSearchTasks = FolderShareScheduledTask::findNumberOfTasks(
      '\Drupal\foldershare\ManageSearch::taskUpdateIndex');
    if ($nSearchTasks > 0) {
      // Too many search index tasks. Do nothing.
      return;
    }

    //
    // Get scheduling interval
    // -----------------------
    // The scheduling interval is set by the admin. If it is <= 0, then
    // task scheduling has been disabled. An outside CRON is probably being
    // used instead.
    $seconds = self::getIndexIntervalSeconds();

    //
    // Schedule next task
    // ------------------
    // To keep scheduled search indexing going, immediately schedule the
    // next task. If the index update below gets interrupted, there is always
    // the next task in the queue and ready to run in the future.
    if ($seconds > 0) {
      $t = time();
      FolderShareScheduledTask::createTask(
        $t + $seconds,
        '\Drupal\foldershare\ManageSearch::taskUpdateIndex',
        $requester,
        NULL,
        $t,
        Settings::getSearchIndexInterval() . ' search index update',
        0,
        FolderShareScheduledTask::REPEATING_FLAG);
    }

    //
    // Update search index
    // -------------------
    // Update the core Search index via the plugin.
    $plugin->updateIndex();
  }

}
