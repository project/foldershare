<?php

namespace Drupal\foldershare\Form;

use Drupal\Core\Url;
use Drupal\Component\Utility\Html;

use Symfony\Component\HttpFoundation\Request;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\FolderShareInterface;

/**
 * Provides support for the ancestor menu used on multiple pages.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
final class UIAncestorMenu {

  /*--------------------------------------------------------------------
   *
   * Functions.
   *
   *-------------------------------------------------------------------*/
  /**
   * Creates a render description for an ancestor menu.
   *
   * The last item in the ancestor menu is a list of available root lists
   * for the user:
   * - Home.
   * - Shared.
   * - Public.
   * - All.
   *
   * The anonoymous user always sees only the "Public" list, regardless of
   * the value of the $allowPublicRootList argument.
   *
   * The "All" list is only included if the user has module admin permissions.
   *
   * The "Public" list is only included if $allowPublicRootList is TRUE.
   *
   * @param int $currentId
   *   (optional, default = FolderShareInterface::USER_ROOT_LIST) The integer
   *   entity ID of the current FolderShare item. If the value is negative or
   *   FolderShareInterface::USER_ROOT_LIST, if the current item is
   *   the user's root list.
   * @param bool $allowPublicRootList
   *   (optional, default = TRUE) When TRUE, the public root list is available.
   *
   * @return array
   *   Returns an array of renderable elements for an ancestor menu's
   *   entries.
   */
  public static function build(
    int $currentId = FolderShareInterface::USER_ROOT_LIST,
    bool $allowPublicRootList = TRUE) {
    //
    // Setup.
    // ------
    // Get services and the current request.
    $routeProvider = \Drupal::service('router.route_provider');
    $titleResolver = \Drupal::service('title_resolver');
    $dummyRequest = new Request();

    //
    // Load entity.
    // ------------
    // Load the current entity, if any.
    $currentItem = NULL;
    if ($currentId >= 0) {
      $currentItem = FolderShare::load($currentId);
      if ($currentItem === NULL) {
        $currentId = FolderShareInterface::USER_ROOT_LIST;
      }
    }

    $uiClass         = 'foldershare-ancestormenu';
    $menuButtonClass = 'foldershare-ancestormenu-menu-button';
    $menuClass       = 'foldershare-ancestormenu-menu';

    $menuButtonUrl   = '/' . \Drupal::service('extension.list.module')->getPath('foldershare') .
      '/images/menu-ancestor.svg';
    $menuButtonAlt   = t('Location menu');
    $menuButtonImage = "<img alt=\"$menuButtonAlt\" src=\"$menuButtonUrl\">";

    //
    // Add ancestor list markup.
    // -------------------------
    // Build a list of ancestor folders for the ancestor menu. For each one,
    // include the ancestor's URL as an attribute. Javascript will use this
    // to load the appropriate page. The URL is not included in an <a> for
    // the menu item so that menu items aren't styled as links.
    //
    // File classes are added so that themes can style the item with
    // folder icons.
    //
    // Drupal work-around: There are no render array #type's to make
    // hierarchical lists. Even if an abstract list type existed, it would
    // be routed through theme templates that could alter the structure,
    // strip classes, etc. Since we need a very specific structure with
    // specific classes and data attributes, the only practical way to
    // build this is with explicit HTML.
    $menuMarkup = "<ul class=\"hidden $menuClass\">";

    if ($currentItem !== NULL) {
      // The page is for entity. Get its ancestors.
      $folders = $currentItem->findAncestorFolders();

      // Reverse ancestors from root first to root last.
      $folders = array_reverse($folders);

      // Push the current entity onto the ancestor list so that it gets
      // included in the menu.
      array_unshift($folders, $currentItem);

      // Add ancestors to menu.
      foreach ($folders as $item) {
        // Get the URL to the folder.
        $url = rawurlencode($item->toUrl(
          'canonical',
          [
            'absolute' => TRUE,
          ])->toString());

        // Get the name for the folder.
        $name = Html::escape($item->getName());

        // Add the HTML. Include file classes that mark this as a folder
        // or file.
        if ($item->isFolder() === TRUE) {
          $fileClasses = 'file file--folder file--mime-folder-directory';
        }
        else {
          $mimes = explode('/', $item->getMimeType());
          $fileClasses = 'file file--' . $mimes[0] .
            ' file--mime-' . $mimes[0] . '-' . $mimes[1];
        }

        if ($item->isSystemDisabled() === TRUE) {
          $attr = '';
        }
        else {
          $attr = 'data-foldershare-id="' . $item->id() . '"';
        }

        $menuMarkup .= "<li $attr data-foldershare-url=\"$url\"><div><span class=\"$fileClasses\"></span>$name</div></li>";
      }
    }

    //
    // Add root list.
    // --------------
    // Show a list of available root lists in a submenu. If there is only
    // one available root list, then don't use a submenu.
    $currentUser = \Drupal::currentUser();

    // Start by assembling a list of available root lists for this user.
    $rootLists = [];
    if ($currentUser->isAnonymous() === TRUE) {
      // Anonymous user. The user doesn't have an account so they cannot
      // have a home folder. While they can have content shared with them,
      // that content will be in their Shared AND Public lists, so just
      // show the Public list.
      //
      // Home   = no.
      // Shared = no.
      // Public = yes.
      // All    = no (admins only).
      $rootLists[] = [
        "public",
        Constants::ROUTE_ROOT_ITEMS_PUBLIC,
        'file--mime-home-directory',
      ];
    }
    elseif ($currentUser->hasPermission(Constants::ADMINISTER_PERMISSION) === TRUE) {
      // Administrator.
      //
      // Home   = yes.
      // Shared = yes.
      // Public = yes.
      // All    = yes.
      $rootLists[] = [
        "home",
        Constants::ROUTE_ROOT_ITEMS_PERSONAL,
        'file--mime-home-directory',
      ];

      $rootLists[] = [
        "shared",
        Constants::ROUTE_ROOT_ITEMS_SHARED,
        'file--mime-shared-directory',
      ];

      if ($allowPublicRootList === TRUE) {
        $rootLists[] = [
          "public",
          Constants::ROUTE_ROOT_ITEMS_PUBLIC,
          'file--mime-public-directory',
        ];
      }
      $rootLists[] = [
        "all",
        Constants::ROUTE_ROOT_ITEMS_ALL,
        'file--mime-all-directory',
      ];
    }
    else {
      // Authenticated non-admin user. If the user is on a page showing
      // this ancestor menu, then they have permission to use the module
      // and have a home directory. They may have content shared with them.
      //
      // Home   = yes.
      // Shared = no.
      // Public = yes.
      // All    = no (admins only).
      $rootLists[] = [
        "home",
        Constants::ROUTE_ROOT_ITEMS_PERSONAL,
        'file--mime-home-directory',
      ];

      $rootLists[] = [
        "shared",
        Constants::ROUTE_ROOT_ITEMS_SHARED,
        'file--mime-shared-directory',
      ];

      if ($allowPublicRootList === TRUE) {
        $rootLists[] = [
          "public",
          Constants::ROUTE_ROOT_ITEMS_PUBLIC,
          'file--mime-public-directory',
        ];
      }
    }

    // Build markup for the available root lists.
    $fileClasses = 'file';
    $fileClassesFallback = 'file--folder file--mime-rootfolder-group-directory';

    if (count($rootLists) === 1) {
      $rootListName  = $rootLists[0][0];
      $rootListRoute = $rootLists[0][1];
      $mimeClass     = $rootLists[0][2];

      // Get the root list's route.
      $route = $routeProvider->getRouteByName($rootListRoute);

      // Create a route URL to the root list.
      $url = Url::fromRoute(
        $rootListRoute,
        [],
        ['absolute' => TRUE])->toString();

      // Get the route's title.
      $title = $titleResolver->getTitle($dummyRequest, $route);

      // Create markup for a menu entry to the root list.
      $attr = "data-foldershare-id=\"$rootListName\" data-foldershare-url=\"$url\"";

      $menuMarkup .= "<li $attr><div><span class=\"$fileClasses $mimeClass $fileClassesFallback\"></span>$title</div></li>";
    }
    else {
      $title = t('Favorites')->render();
      $menuMarkup .= "<li><div>$title</div><ul>";

      foreach ($rootLists as &$rootList) {
        $rootListName  = $rootList[0];
        $rootListRoute = $rootList[1];
        $mimeClass     = $rootList[2];

        // Get the root list's route.
        $route = $routeProvider->getRouteByName($rootListRoute);

        // Create a route URL to the root list.
        $url = Url::fromRoute(
          $rootListRoute,
          [],
          ['absolute' => TRUE])->toString();

        // Get the route's title.
        $title = $titleResolver->getTitle($dummyRequest, $route);

        // Create markup for a menu entry to the root list.
        $attr = "data-foldershare-id=\"$rootListName\" data-foldershare-url=\"$url\"";
        $menuMarkup .= "<li $attr><div><span class=\"$fileClasses $mimeClass $fileClassesFallback\"></span>$title</div></li>";
      }
      $menuMarkup .= "</ul></li>";
    }

    $menuMarkup .= '</ul>';

    //
    // Create UI
    // ---------
    // Everything is hidden initially, and only exposed by Javascript, if
    // the browser supports Javascript.
    $renderable = [
      '#attributes' => [
        'class'     => [
          'foldershare-ancestormenu',
        ],
      ],
      $uiClass => [
        '#type'              => 'container',
        '#weight'            => -90,
        '#attributes'        => [
          'class'            => [
            $uiClass,
            'hidden',
          ],
        ],

        // Add a hierarchical menu of ancestors. Javascript uses jQuery.ui
        // to build a menu from this and presents it from a menu button.
        // The menu was built and marked as hidden.
        //
        // Drupal work-around: The field is built using an inline template
        // to avoid Drupal's HTML cleaning that can remove classes and
        // attributes on the menu items, which we need to retain to provide
        // the URLs of ancestor folders. Those URLs are used by Javascript
        // to load the appropriate page when a menu item is selected.
        $menuClass           => [
          '#type'            => 'inline_template',
          '#template'        => '{{ menu|raw }}',
          '#context'         => [
            'menu'           => $menuMarkup,
          ],
        ],

        // Add a button to go up to an ancestor folder. Javascript binds
        // a behavior to the button to load an ancestor page. The button is
        // hidden initially and only shown if the browser supports Javascript.
        //
        // Drupal work-around: If we use the #type "button" or "image_button",
        // we'll get a button, but not necessarily using HTML's <button>.
        // This varies by theme. The core themes, based on "classy",
        // implement buttons with <input>. Some Bootstrap-based themes
        // implement buttons with <button>, while others do not.
        // Unfortunately, <input> does not support wrapping an <img> for
        // an icon, while <button> does (which makes it odd that core's
        // #type "image_button" still generates <input>, which will never
        // work).
        //
        // To guarantee that we get an <img> on the button, we have to
        // force use of <button>, and that requires using #type "html_tag".
        // Unfortunately, "html_tag" only allows a string as the value
        // within the tag, so we are forced to enter explicit HTML below.
        //
        // A downside of using "html_tag" is that the button does not get
        // processed by core's #type "button" code, a theme's button
        // template pre-processor, or a theme's button template. Since those
        // are responsible for adding button classes, we have to add them
        // here explicitly:
        //
        // - "button", "js-form-submit", and "form-submit" are added by
        //   core #type "button".
        //
        // - "button--image-button" is added by core #type "button" if the
        //   #button_type is "image_button".
        //
        // - "btn" is added by many Bootstrap-based themes.
        //
        // - "w3-button", "w3-border", and "w3-theme-border" are W3.CSS
        //   framework standards.
        //
        // While it is possible to create a custom button template for this
        // kind of button, core's themeing system always cascades from
        // simplest to most custom template, invoking ALL of them. Because
        // the core themes do not define twig blocks around <input>, it is
        // not possible for a custom theme late in the cascade to override
        // the <input> tag created earlier in the cascade. This is why
        // custom themes (like Bootstrap) do not extend the core themes.
        // But here we don't have that option - we have to work with any
        // theme, including core themes, and therefore we cannot use a
        // custom template to override button handling.
        $menuButtonClass     => [
          '#type'            => 'html_tag',
          '#tag'             => 'button',
          '#value'           => $menuButtonImage,
          '#name'            => $menuButtonClass,
          '#attributes'      => [
            'title'          => t('See the location of the current item.'),
            'class'          => [
              $menuButtonClass,
              'hidden',
              // Core classes.
              'button',
              'button--image-button',
              'js-form-submit',
              'form-submit',
              // Bootstrap classes.
              'btn',
              // W3.CSS classes.
              'w3-button',
              'w3-border',
              'w3-theme-border',
              // jQuery.UI. With some themes, these get lost when set by
              // jQuery using button(). Why?
              'ui-button',
              'ui-corner-all',
              'ui-widget',
            ],
          ],
        ],
      ],
    ];

    return $renderable;
  }

}
