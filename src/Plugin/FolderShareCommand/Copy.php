<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Defines a command plugin to copy files and folders.
 *
 * The command copies all selected files and folders to a chosen
 * destination folder or the root list.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to duplicate.
 * - 'destinationId': the destination folder, if any.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_copy",
 *  label           = @Translation("Copy"),
 *  menuNameDefault = @Translation("Copy..."),
 *  menuName        = @Translation("Copy..."),
 *  description     = @Translation("Copy selected files and folders to a new location."),
 *  category        = "copy & move",
 *  weight          = 0,
 *  specialHandling = {
 *    "copy",
 *  },
 *  userConstraints = {
 *    "authorpermission",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "view",
 *  },
 *  destinationConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class Copy extends CopyMoveBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {

    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Configuration.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateDestinationConstraints() {
    if ($this->destinationValidated === TRUE) {
      return;
    }

    if ($this->selectionValidated === FALSE) {
      $this->validateSelectionConstraints();
    }

    // Handle special cases for destination.
    $destinationId = $this->getDestinationId();
    if ($destinationId === FolderShareInterface::ALL_ROOT_LIST) {
      // User-facing exception message.
      $message1 = $this->t(
        'Items cannot be copied to the administrator\'s "@title" list.',
        [
          '@title' => $this->rootItemsAllTitle,
        ]);
      $message2 = $this->t('Please select a subfolder instead.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message1, $message2));
    }

    if ($destinationId === FolderShareInterface::PUBLIC_ROOT_LIST) {
      // User-facing exception message.
      $message1 = $this->t(
        'Items cannot be copied to the "@title" list.',
        [
          '@title' => $this->rootItemsPublicTitle,
        ]);
      $message2 = $this->t('Please select a subfolder instead.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message1, $message2));
    }

    parent::validateDestinationConstraints();
  }

  /**
   * {@inheritdoc}
   */
  public function validateParameters() {
    if ($this->parametersValidated === TRUE) {
      // Already validated.
      return;
    }

    //
    // Validate destination.
    // ---------------------
    // There must be a destination ID. It must be a valid ID. It must
    // not be one of the selected items. And it must not be a descendant
    // of the selected items.
    //
    // A positive destination ID is for a folder to receive the selected
    // items.
    //
    // A negative destination ID is for the user's root list.
    $destinationId = $this->getDestinationId();

    if ($destinationId < 0) {
      // Destination is the user's root list. Nothing further to validate.
      $this->parametersValidated = TRUE;
      return;
    }

    // Destination is a specific folder.
    $destination = FolderShare::load($destinationId);

    if ($destination === NULL) {
      // Destination ID is not valid. This should have been caught
      // well before this validation stage.
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . " was called with an invalid entity ID '$destinationId'.");
    }

    // Verify that the destination is not in the selection. That would
    // be a copy to self, which is not valid.
    $selectionIds = $this->getSelectionIds();
    if (in_array($destinationId, $selectionIds) === TRUE) {
      // User-facing exception message.
      $message = $this->t('Items cannot be copied into themselves.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message));
    }

    // Verify that the destination is not a descendant of the selection.
    // That would be a recursive tree copy into itself.
    foreach ($selectionIds as $id) {
      $item = FolderShare::load($id);
      if ($item === NULL) {
        // The item does not exist.
        continue;
      }

      if ($destination->isDescendantOfFolderId($item->id()) === TRUE) {
        // User-facing exception message.
        $message = $this->t('Items cannot be copied into their own subfolders.');
        throw new ValidationException(
          FormatUtilities::createFormattedMessage($message));
      }

      unset($item);
    }

    unset($destination);

    $this->parametersValidated = TRUE;

    // Garbage collect.
    gc_collect_cycles();
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    $ids = $this->getSelectionIds();

    if (count($ids) === 1) {
      return $this->t(
        'Copy "@name"',
        [
          '@name' => FolderShare::findNameForId($ids[0]),
        ]);
    }

    $kinds = FolderShare::translateKindsForIds($ids);
    return $this->t(
      "Copy @kinds",
      [
        '@kinds' => $kinds['translation'],
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Copy');
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $ids = $this->getSelectionIds();
    $destination = $this->getDestination();

    if ($destination === NULL) {
      FolderShare::copyToRootMultiple($ids);
    }
    else {
      FolderShare::copyToFolderMultiple($ids, $destination);
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $count = count($ids);
      if ($count <= 1) {
        $message = $this->t("The item has been copied.");
      }
      else {
        $message = $this->t(
          "@count items have been copied.",
          [
            '@count' => $count,
          ]);
      }
      $this->getMessenger()->addStatus($message);
    }
  }

}
