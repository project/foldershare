<?php

namespace Drupal\foldershare\Form\AdminSettingsTraits;

use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\LinkUtilities;
use Drupal\foldershare\ManageUsageStatistics;
use Drupal\foldershare\ManageSearch;

/**
 * The "Services" tab for the module's settings form.
 *
 * <B>Warning:</B> This is an internal trait that is strictly used by
 * the AdminSettings form class. It is a mechanism to group functionality
 * to improve code management.
 *
 * @ingroup foldershare
 */
trait AdminSettingsServicesTab
{

  /*---------------------------------------------------------------------
   *
   * Build.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds the services tab.
   *
   * @param array $form
   *   An associative array containing the structure of the form. The form
   *   is modified to include additional render elements for the tab.
   * @param FormStateInterface $formState
   *   The current state of the form.
   * @param string $tabGroup
   *   The name of the tab group.
   */
  private function buildServicesTab(array &$form, FormStateInterface $formState, string $tabGroup)
  {

    //
    // Find installed modules.
    // -----------------------
    // Tab sections vary their presentation based upon what modules are
    // installed at the site.
    $helpInstalled = $this->moduleHandler->moduleExists('help');
    $dblogInstalled = $this->moduleHandler->moduleExists('dblog');

    //
    // Set up tab names.
    // -----------------
    // Create the machine name for the tab and the tab's title.
    $tabMachineName = 'services';
    $tabName = 'foldershare_' . $tabMachineName . '_tab';
    $tabTitle = $this->t('Services');
    $tabPaneTitle = $this->t('Manage services');

    //
    // Set up class names.
    // -------------------
    // Use a set of standard class names for tab sections.
    $tabSubtitleClass = 'foldershare-settings-subtitle';
    $sectionWrapperClass = 'foldershare-settings-section-wrapper';
    $sectionClass = 'foldershare-settings-section';
    $sectionDescriptionClass = 'foldershare-settings-section-description';
    $itemDefaultClass = 'foldershare-settings-item-default';
    $itemNoteClass = 'foldershare-settings-item-note';

    $clearSearchButton = 'foldershare_search_clear';
    $restoreSearchButton = 'foldershare_search_restore';

    //
    // Create the tab
    // --------------
    // Start the tab with a title, subtitle, and description.
    $form[$tabName] = ['#type' => 'details', '#open' => FALSE, '#group' => $tabGroup, '#title' => $tabTitle,

      '#description' => ['subtitle' => ['#type' => 'html_tag', '#tag' => 'h2', '#value' => $tabPaneTitle, '#attributes' => ['class' => [$tabSubtitleClass],],],

        'description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Control activity logging, search indexing, and usage data.'), '#attributes' => ['class' => ['foldershare-settings-description',],],],], '#attributes' => ['class' => ['foldershare-settings-tab ', 'foldershare-services-tab',],],];

    //
    // Manage logging.
    // ---------------
    // Add a checkbox for logging.
    //
    // Drupal's logger API routes log messages to all installed loggers.
    // The core "dblog" module is one such logger, but there are contributed
    // modules for many more types of loggers. Unfortunately, there is no
    // direct way to ask Drupal if there are any loggers at all. If there
    // are none installed, all logging is silently ignored.
    //
    // Since we cannot determine if there is a logger installed, we cannot
    // disable this UI if there are no loggers. Just leave it enabled.
    //
    // In the very common case where core's "dblog" logger is installed,
    // add a link to its pages.
    if ($dblogInstalled === TRUE && $helpInstalled === TRUE) {
      $description = $this->t('Activity logging records each time a file or folder is created, deleted, downloaded, or changed. Activity is shown on the page of @dblogListLink (see @dblogHelpLink).', ['@dblogListLink' => LinkUtilities::createRouteLink('dblog.overview', '', $this->t('Recent error messages')), '@dblogHelpLink' => LinkUtilities::createHelpLink('dblog', $this->t('help')),]);
    } elseif ($dblogInstalled === TRUE && $helpInstalled === FALSE) {
      $description = $this->t('Activity logging records each time a file or folder is created, deleted, downloaded, or changed. Activity is shown on the page of @dblogListLink.', ['@dblogListLink' => LinkUtilities::createRouteLink('dblog.overview', '', $this->t('Recent error messages')),]);
    } else {
      $description = $this->t('Activity logging records each time a file or folder is created, deleted, downloaded, or changed.');
    }

    $form[$tabName]['manage-logging'] = ['#type' => 'details', '#title' => $this->t('Activity logging'), '#open' => FALSE, '#attributes' => ['class' => [$sectionWrapperClass],],

      'section' => ['#type' => 'container', '#attributes' => ['class' => [$sectionClass],],

        'section-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $description, '#attributes' => ['class' => [$sectionDescriptionClass],],],

        // Checkbox to enable/disable logging.
        'enable-logging' => ['#type' => 'checkbox', '#title' => $this->t('Enable activity logging'), '#default_value' => Settings::getActivityLogEnable(), '#return_value' => 'enabled', '#required' => FALSE, '#name' => 'enable-logging',],

        // The default setting.
        'enable-logging-default' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Default: Disable activity logging'), '#attributes' => ['class' => ['description', $itemDefaultClass, $itemNoteClass,],],],

        // A note about logging on busy sites.
        'enable-logging-note' => ['#type' => 'container', '#attributes' => ['class' => [$itemNoteClass, 'color-warning',],], '#states' => ['visible' => ['input[name="enable-logging"]' => ['checked' => TRUE,],],],

          'note' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t("For busy sites, activity logging can create a large number of log entries."),],],],];

    //
    // Manage search
    // -------------
    // The search configuration varies based on what search modules are
    // installed:
    // - None.
    // - core Search only.
    // - Search API only.
    // - both core Search and Search API.
    //
    // When at least one search module is enabled, present options:
    // - Enable file content inclusion in the search index.
    //   - Allowed filename extensions for inclusion.
    //   - Maximum file content.
    //
    // When two search modules are enabled, prompt for:
    // - Update search index in one or both.
    //
    // When core Search is in use, prompt for:
    // - Search indexing interval.
    //
    // When Search API is in use, and the folder browser's toolbar search
    // box is enabled, prompt for:
    // - Path to search page.
    $searchAvailable = ManageSearch::isSearchAvailable();
    $coreSearchInstalled = ManageSearch::isCoreSearchAvailable();
    $searchAPIInstalled = ManageSearch::isSearchAPIAvailable();

    // Create a section description based on what search module (if any)
    // is installed.
    if ($coreSearchInstalled === TRUE && $searchAPIInstalled === TRUE) {
      // BOTH of the supported search modules are installed.
      $description = $this->t("Drupal's standard %coreSearchConfigLink module and the contributed %searchAPIConfigLink module are both installed. Both will add file and folder content to their search index.", ['%coreSearchConfigLink' => ManageSearch::createCoreSearchConfigurationLink(), '%searchAPIConfigLink' => ManageSearch::createSearchAPIConfigurationLink(),]);
    } elseif ($coreSearchInstalled === TRUE) {
      // Just the core search module is installed.
      $description = $this->t("This module supports searching through files and folders using Drupal's standard %coreSearchConfigLink module. Below, configure how files and folders are added to the search index.", ['%coreSearchConfigLink' => ManageSearch::createCoreSearchConfigurationLink(),]);
    } elseif ($searchAPIInstalled === TRUE) {
      $description = $this->t('This module supports searching through files and folders using the contributed %searchAPIConfigLink module. Below, configure how files and folders are added to the search index.', ['%searchAPIConfigLink' => ManageSearch::createSearchAPIConfigurationLink(),]);
    } else {
      // There is no search module installed. Recommend enabling one.
      $description = $this->t("This module supports searching through files and folders. Install Drupal's standard %coreSearchEnableLink module or download and install the contributed Search API module.", ['%coreSearchEnableLink' => ManageSearch::createCoreSearchEnableLink(),]);
    }

    // Build the form.
    $form[$tabName]['manage-search'] = ['#type' => 'details', '#title' => $this->t('Search indexing'), '#open' => FALSE, '#attributes' => ['class' => [$sectionWrapperClass],],

      'section' => ['#type' => 'container', '#attributes' => ['class' => [$sectionClass],],

        'section-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $description, '#attributes' => ['class' => [$sectionDescriptionClass],],],],];

    if ($searchAvailable === TRUE) {
      // Add settings to control file content indexing.
      //
      // Map the numeric size to the text size name.
      //
      // This mapping is done specifically to insure that a hacked form
      // or YML configuration cannot be used to set an arbitrary file size.
      //
      // File sizes are also limited below to a "reasonable" size that is
      // sufficient for even very large text files. It is also sufficient
      // to get the header text (if any) out of binary files, such as those
      // for images and videos. Supporting larger sizes causes more file I/O
      // during indexing, more memory use for read in "text", more processing
      // during indexing, and more space in the database for index "text".
      switch (Settings::getSearchIndexMaximumFileSize()) {
        default:
        case (int)(1 << 10):
          $fileSizeLabel = 0;
          break;

        case (int)((1 << 10) * 10):
          $fileSizeLabel = 1;
          break;

        case (int)((1 << 10) * 100):
          $fileSizeLabel = 2;
          break;

        case (int)(1 << 20):
          $fileSizeLabel = 3;
          break;

        case (int)((1 << 20) * 10):
          $fileSizeLabel = 4;
          break;
      }

      $fileSizeOptions = [0 => $this->t('1 Kbyte'), 1 => $this->t('10 Kbytes'), 2 => $this->t('100 Kbytes'), 3 => $this->t('1 Mbyte'), 4 => $this->t('10 Mbytes'),];

      $form[$tabName]['manage-search']['section']['file-search-group'] = ['#type' => 'fieldset', '#title' => $this->t('File content indexing'), '#weight' => 1,

        'search-file-content-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('File and folder names and descriptive information is always included in the search index. Optionally, the index may include partial or full file content for text-based file types.'),],

        'search-file-content' => ['#type' => 'checkbox', '#title' => $this->t('Add file text to the search index'), '#default_value' => Settings::getSearchIndexFileContentEnable(), '#return_value' => 'enabled', '#name' => 'search-file-content',],

        'search_file_content_related' => ['#type' => 'container', '#states' => ['invisible' => ['input[name="search-file-content"]' => ['checked' => FALSE,],],],

          'search-file-extensions' => ['#type' => 'textarea', '#rows' => 3, '#default_value' => Settings::getAllowedSearchIndexFilenameExtensions(), '#required' => FALSE, '#name' => 'search-file-extensions', '#title' => $this->t('File types to add to the search index:'), '#description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Index the text content of specific file types only. Leave this blank to support all file types, or list file name extensions separated by spaces and without dots (e.g. "txt csv pdf").'),],],

          'search-file-size' => ['#type' => 'select', '#options' => $fileSizeOptions, '#default_value' => $fileSizeLabel, '#required' => FALSE, '#name' => 'search-file-size', '#title' => $this->t('Maximum size of file content indexed'), '#description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Limit file content so that it does not overwhelm the search index.'),],],],];

      // Create a note about the search index status. If there is more than
      // one search provider, there may be multiple index statuses.
      $indexStatus = '';
      $coreSearchStatus = ['total' => 0, 'remaining' => 0,];
      $searchAPIStatus = ['total' => 0, 'remaining' => 0,];

      if (ManageSearch::isCoreSearchAvailable() === TRUE) {
        $coreSearchStatus = ManageSearch::coreSearchGetIndexStatus();
      }
      if (ManageSearch::isSearchAPIAvailable() === TRUE) {
        $searchAPIStatus = ManageSearch::searchAPIGetIndexStatus();
      }

      $includeReindexButton = TRUE;

      // Nothing to index?
      if ((int)$coreSearchStatus['total'] === 0 && (int)$searchAPIStatus['total'] === 0) {
        // The total number of indexable items is zero? This could be true,
        // but for the Search API, it also could be that the module has not
        // been configured for indexing FolderShare entities.
        if (ManageSearch::isSearchAPIAvailable() === TRUE && ManageSearch::isSearchAPIConfigured() === FALSE) {
          $indexStatus = $this->t('The @searchapilink database index is not yet configured to index files and folders.', ['@searchapilink' => ManageSearch::createSearchAPIConfigurationLink(),]);
          $includeReindexButton = FALSE;
        } else {
          // Search API is not in use or it is in use and is configured.
          // Perhaps there really is nothing to index.
          $indexStatus = $this->t('There are currently no files and folders to index.');
          $includeReindexButton = FALSE;
        }
      } else {
        if (ManageSearch::isCoreSearchAvailable() === TRUE) {
          $totalForIndex = (float)$coreSearchStatus['total'];
          $remainingToIndex = (float)$coreSearchStatus['remaining'];
          if ($totalForIndex <= 0.0) {
            $percent = 100;
          } else {
            $percent = ((int)(100.0 * ($totalForIndex - $remainingToIndex) / $totalForIndex));
          }

          if ($percent === 100) {
            $indexStatus = $this->t('The core @searchName index for files and folders is complete with @totalForIndex items.', ['@searchName' => ManageSearch::getCoreSearchModuleName(), '@totalForIndex' => (int)$totalForIndex,]);
          } else {
            $indexStatus = $this->t('The core @searchName index for files and folders is @percent % complete, with @remainingToIndex out of @totalForIndex items to index.', ['@searchName' => ManageSearch::getCoreSearchModuleName(), '@percent' => $percent, '@totalForIndex' => (int)$totalForIndex, '@remainingToIndex' => (int)$remainingToIndex,]);
          }
        }

        if (ManageSearch::isSearchAPIAvailable() === TRUE) {
          $totalForIndex = (float)$searchAPIStatus['total'];
          $remainingToIndex = (float)$searchAPIStatus['remaining'];
          if ($totalForIndex <= 0.0) {
            $percent = 100;
          } else {
            $percent = ((int)(100.0 * ($totalForIndex - $remainingToIndex) / $totalForIndex));
          }

          if ($percent === 100) {
            $indexStatus .= $this->t('The @searchName index for files and folders is complete with @totalForIndex items.', ['@searchName' => ManageSearch::getSearchAPIModuleName(), '@totalForIndex' => (int)$totalForIndex,]);
          } else {
            $indexStatus .= $this->t('The @searchName index for files and folders is @percent % complete, with @remainingToIndex out of @totalForIndex items to index.', ['@searchName' => ManageSearch::getSearchAPIModuleName(), '@percent' => $percent, '@totalForIndex' => (int)$totalForIndex, '@remainingToIndex' => (int)$remainingToIndex,]);
          }
        }
      }

      $form[$tabName]['manage-search']['section']['search-clear-group'] = ['#type' => 'fieldset', '#title' => $this->t('Search index status'), '#weight' => 2,

        // A note about the current search index state.
        'search-clear-progress' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $indexStatus, '#weight' => 1,],

        // A button to clear the search index.
        $clearSearchButton => ['#type' => 'submit', '#value' => $this->t('Re-index'), '#name' => $clearSearchButton, '#weight' => 2, '#attributes' => ['class' => [$clearSearchButton,],],],

        // A description for the clear button.
        'search-clear-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Re-index files and folders.'), '#weight' => 3, '#attributes' => ['class' => [$clearSearchButton . '_description',],],],];

      if ($includeReindexButton === FALSE) {
        unset($form[$tabName]['manage-search']['section']['search-clear-group'][$clearSearchButton]);
        unset($form[$tabName]['manage-search']['section']['search-clear-group']['search-clear-description']);
      }
    }

    if ($coreSearchInstalled === TRUE) {
      // Core Search is installed. Include fields to control indexing.
      $form[$tabName]['manage-search']['section']['core-search-group'] = ['#type' => 'fieldset', '#title' => $this->t('Automatic search indexing'), '#weight' => 3,

        // A description for search indexing intervals.
        'search-interval-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t("The file and folder search index for Drupal's core @coreSearchName module can be updated automatically as pages are processed, or by using the site's @cronLink configuration.", ['@coreSearchName' => ManageSearch::getCoreSearchModuleName(), '@cronLink' => LinkUtilities::createRouteLink('system.cron_settings', '', $this->t('Cron')),]), '#weight' => 1,],

        // Menu to select the indexing interval.
        'search-index-interval' => ['#type' => 'select', '#options' => ['system' => $this->t('Use Cron for updates'), // Legacy '1min' interval is no longer supported.
          '5min' => $this->t('Update every 5 minutes'), '10min' => $this->t('Update every 10 minutes'), '15min' => $this->t('Update every 15 minutes'), '30min' => $this->t('Update every 30 minutes'), 'hourly' => $this->t('Update every hour'), 'daily' => $this->t('Update every day'),], '#default_value' => Settings::getSearchIndexInterval(), '#name' => 'search-index-interval', '#weight' => 2,],

        // The default setting.
        'search-index-interval-default' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Default: Use Cron for updates'), '#weight' => 3, '#attributes' => ['class' => ['description', $itemDefaultClass, $itemNoteClass,],],],];

      $form[$tabName]['manage-search']['section']['search-restore-group'] = ['#type' => 'container', '#weight' => 4,

        // A button to restore the search configuration.
        $restoreSearchButton => ['#type' => 'submit', '#button_type' => 'warning', '#value' => $this->t('Restore'), '#name' => $restoreSearchButton, '#weight' => 1, '#attributes' => ['class' => [$restoreSearchButton,],],],

        // A description for the restore button.
        'search-restore-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Restore the search settings to their default values.'), '#weight' => 2, '#attributes' => ['class' => [$restoreSearchButton . '_description',],],],];
    }

    //
    // Manage usage reporting.
    // -----------------------
    // Add a menu for the usage table update interval.
    $usageReportLink = LinkUtilities::createRouteLink(Constants::ROUTE_USAGE, '', $this->t('Usage report'));

    $form[$tabName]['manage-usage-statistics'] = ['#type' => 'details', '#title' => $this->t('Usage reporting'), '#attributes' => ['class' => [$sectionWrapperClass],],

      'section' => ['#type' => 'container', '#attributes' => ['class' => [$sectionClass],],

        'section-description' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t("The @usageReportLink shows the number of files and folders owned by each user. Data can be updated automatically as pages are processed, or only when needed.", ['@usageReportLink' => $usageReportLink,]), '#attributes' => ['class' => [$sectionDescriptionClass],],],

        // Menu to select the update interval.
        'usage-update-interval' => ['#type' => 'select', '#options' => ['manualy' => $this->t('Update manually'), 'hourly' => $this->t('Update every hour'), 'daily' => $this->t('Update every day'), 'weekly' => $this->t('Update every week'),], '#default_value' => Settings::getUsageUpdateInterval(), '#name' => 'usage-update-interval',],

        // The default setting.
        'usage-update-interval-default' => ['#type' => 'html_tag', '#tag' => 'p', '#value' => $this->t('Default: Update manually'), '#attributes' => ['class' => ['description', $itemDefaultClass, $itemNoteClass,],],],],];
  }

  /*---------------------------------------------------------------------
   *
   * Validate.
   *
   *---------------------------------------------------------------------*/
  /**
   * Validates form values.
   *
   * @param array $form
   *   The form configuration.
   * @param FormStateInterface $formState
   *   The entered values for the form.
   */
  private function validateServicesTab(array &$form, FormStateInterface $formState)
  {

    $trigger = $formState->getTriggeringElement();
    $coreSearchInstalled = ManageSearch::isCoreSearchAvailable();

    //
    // Restore core search plugin configuration.
    // -----------------------------------------
    // Reset the search index update interval, restore the search plugin's
    // settings, and report the change.
    $restoreSearchButton = 'foldershare_search_restore';

    if ($trigger['#name'] === $restoreSearchButton && $coreSearchInstalled === TRUE) {
      // Reset the core Search index update interval.
      $default = Settings::getSearchIndexIntervalDefault();
      Settings::setSearchIndexInterval($default);

      $formInput = $formState->getUserInput();
      $formInput['search-index-interval'] = $default;
      $formState->setUserInput($formInput);

      // Reset the search plugin configuration.
      $status = ManageSearch::revertCoreSearchPluginConfiguration();

      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      if ($status === TRUE) {
        // Clear the search index.
        ManageSearch::clearIndex();

        $this->messenger()->addStatus($this->t('Search features have been restored to their defaults and the search index cleared.'));
      } else {
        $this->messenger()->addError($this->t('Search features could not be restored to their defaults due to an unexpected problem.'));
      }
      return;
    }

    //
    // Clear search index.
    // -------------------
    // Clear the files and folders search index.
    $clearSearchButton = 'foldershare_search_clear';

    if ($trigger['#name'] === $clearSearchButton) {
      ManageSearch::clearIndex();

      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      $this->messenger()->addStatus($this->t('Files and folders search index reset for re-indexing.'));
      return;
    }
  }

  /*---------------------------------------------------------------------
   *
   * Submit.
   *
   *---------------------------------------------------------------------*/
  /**
   * Stores submitted form values.
   *
   * @param array $form
   *   The form configuration.
   * @param FormStateInterface $formState
   *   The entered values for the form.
   */
  private function submitServicesTab(array &$form, FormStateInterface $formState)
  {

    $needToClearSearchIndex = FALSE;

    // Set activity logging.
    $value = $formState->getValue('enable-logging');
    $enabled = ($value === 'enabled') ? TRUE : FALSE;
    Settings::setActivityLogEnable($enabled);

    // Set search indexing interval.
    if (ManageSearch::isCoreSearchAvailable() === TRUE) {
      $value = $formState->getValue('search-index-interval');
      if ($value !== NULL) {
        Settings::setSearchIndexInterval($value);
        ManageSearch::scheduleSearchIndexing();
      }
    }

    // Set whether file content should be indexed.
    $value = $formState->getValue('search-file-content');
    if ($value !== NULL) {
      $enabled = ($value === 'enabled') ? TRUE : FALSE;
      if (Settings::getSearchIndexFileContentEnable() !== $enabled) {
        Settings::setSearchIndexFileContentEnable($enabled);
        $needToClearSearchIndex = TRUE;
      }
    }

    // Set filename extensions for file content indexing.
    $value = $formState->getValue('search-file-extensions');
    if ($value !== NULL) {
      if (Settings::getAllowedSearchIndexFilenameExtensions() !== $value) {
        Settings::setAllowedSearchIndexFilenameExtensions($value);
        $needToClearSearchIndex = TRUE;
      }
    }

    // Set maximum file size for file content indexing.
    // Map the size choice to the numeric size. This mapping is needed to
    // insure that a hacked form cannot introduce a ridiculous file size.
    $value = $formState->getValue('search-file-size');
    if ($value !== NULL) {
      switch ($value) {
        default:
        case 0:
          $fileSize = (int)(1 << 10);
          break;

        case 1:
          $fileSize = (int)((1 << 10) * 10);
          break;

        case 2:
          $fileSize = (int)((1 << 10) * 100);
          break;

        case 3:
          $fileSize = (int)(1 << 20);
          break;

        case 4:
          $fileSize = (int)((1 << 20) * 10);
          break;
      }
      if (Settings::getSearchIndexMaximumFileSize() !== $fileSize) {
        Settings::setSearchIndexMaximumFileSize($fileSize);
        $needToClearSearchIndex = TRUE;
      }
    }


    // Set usage statistics update interval.
    $value = $formState->getValue('usage-update-interval');
    Settings::setUsageUpdateInterval($value);
    ManageUsageStatistics::scheduleUsageUpdate();

    if ($needToClearSearchIndex === TRUE) {
      ManageSearch::clearIndex();
    }
  }

}
