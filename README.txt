                        "foldershare" module for Durpal 8

                      by the San Diego Supercomputer Center
                   at the University of California at San Diego


CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Requirements
 * Recommended modules
 * Installation
 * Configuration
 * Troubleshooting
 * Maintainers


INTRODUCTION
------------
The FolderShare module for Drupal 8 manages shared folders and their files and
subfolders. Folders may be nested to create a folder tree like that provided
by Windows, Mac, and Linux systems. Access controls enable users to restrict
folder tree access to themselves or a selected group of users, or to publish
content for anonymous access.


REQUIREMENTS
------------
The FolderShare module requires Drupal 8.7 or greater.

The module does not require any third-party contributed modules or libraries.
The module only requires modules included in Drupal core and already enabled by
most websites.

See foldershare.info.yml for a list of required modules.


INSTALLATION
------------
Install the module as you would normally install a contributed Drupal module.
Visit:
  https://www.drupal.org/docs/user_guide/en/config-install.html


RECOMMENDED MODULES
-------------------
The FolderShare module provides additional features if any of these
Drupal core modules are enabled:

 * Comment - threaded comments on files and folders
 * Help - site admin help
 * Search - search through file and folder keywords and file content

Site administrators that wish to customize the layout of module pages and tables
may enable these Drupal core modules:

 * Field UI
 * Views UI

A few third-party modules are useful for configuring and using features
related to the FolderShare module:

 * Real name - configure account display names
 * Tokens - view token replacement

REST web services are available if the following is installed:

 * FolderShare REST - web services for files and folders


PRIVATE FILE SYSTEM
-------------------
The FolderShare module may store its files in the site's public or private file
system. The private file system is recommended. This needs to be set up for the
site before configuring the FolderShare module. Visit:
  https://www.drupal.org/docs/8/core/modules/file/overview


CONFIGURATION
-------------
After enabling the module, you may adjust module settings by selecting
"FolderShare" under the administrator's "Structure" menu. A few key items are
important to configure before creating any files or folders:

 * Select whether files are stored in the public or private file system.
 * Select a subdirectory into which to place stored files.
 * Enable or disable file name extension restrictions.


ADDITIONAL HELP
---------------
If the "Help" module is installed, FolderShare's help page provides additional
information on permissions, shared access, and recommended configurations.


TROUBLESHOOTING
---------------
The FolderShare module installs defaults for its fields, views, and search
features. These may be modified by a site administrator. If they are
accidentally deleted or broken, the default settings may be restored by going to
the "FolderShare" menu item under the administrator's "Structure" menu. Then
click on the appropriate "Restore original settings" buttons.


MAINTAINERS
-----------
Current maintainers:
 * Amit Chourasia
   San Diego Supercomputer Center
   University of California at San Diego

This project has been sponsored by:
 * The National Science Foundation. See NSF.txt
