<?php

namespace Drupal\foldershare\Entity\Exception;

/**
 * Defines an exception indicating that an access lock could not be acquired.
 *
 * @ingroup foldershare
 */
class LockException extends RuntimeExceptionWithMarkup {

  /*--------------------------------------------------------------------
   *
   * Constructors.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs an exception.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface $message
   *   (optional, default = NULL) The message string or an instance of
   *   \Drupal\Component\Render\MarkupInterface. If NULL, a default
   *   message is used.
   * @param int $code
   *   (optional, default = 0) An error code.
   * @param \Throwable $previous
   *   (optional, default = NULL) A previous exception that this extends.
   */
  public function __construct(
    $message = NULL,
    int $code = 0,
    \Throwable $previous = NULL) {

    if (empty($message) === TRUE) {
      $message = $this->t('Lock could not be acquired');
    }

    parent::__construct($message, $code, $previous);
  }

}
