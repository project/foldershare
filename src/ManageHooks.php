<?php

namespace Drupal\foldershare;

use Drupal\file\FileInterface;

use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Utilities\FormatUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;

/**
 * Manages module hooks.
 *
 * This class provides static methods to for calling the module's hooks.
 * Supported hooks include:
 * - hook_foldershare_post_operation_XXX (where XXX is the name of an
 *   operation, such as "add_files").
 * - hook_foldershare_validate_file.
 * - hook_foldershare_allowed_filename_extensions_alter.
 * - hook_foldershare_file_upload_size_limit_alter.
 * - hook_foldershare_mime_type_alter.
 * - hook_foldershare_ui_command_menu_enable.
 * - hook_foldershare_ui_ancestor_menu_enable.
 * - hook_foldershare_ui_search_box_enable.
 * - hook_foldershare_ui_command_enable.
 * - hook_foldershare_ui_sidebar_enable.
 * - hook_foldershare_ui_sidebar_button_enable.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\foldershare\Entity\FolderShare
 */
final class ManageHooks {

  /*---------------------------------------------------------------------
   *
   * Utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns an array of module names implementing a hook.
   *
   * @param string $hook
   *   The name of the hook.
   *
   * @return string[]
   *   Returns an array of module names. The array is empty if there are
   *   no modules implementing the hook.
   */
  private static function getHookImplementations(string $hook) {
    $implementors = [];
    \Drupal::moduleHandler()->invokeAllWith($hook, function (callable $hook, string $module) use (&$implementors) {
      // There is minimal overhead since the hook is not invoked.
      $implementors[] = $module;
    });
    return $implementors;
  }

  /*---------------------------------------------------------------------
   *
   * Post operation hook.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to announce completion of a folder tree operation.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_post_operation_XXX" hook is called, where "XXX"
   * is the name of the operation just completed (e.g. "add_files",
   * "new_folder", "delete", etc.). The hook is called with an array of
   * arguments that varies depending upon the operation. The array's
   * arguments typically indicate the entity affected by the operation and
   * the changes that occurred.
   *
   * The return values of module hook implementations are ignored. Exceptions
   * thrown by module hooks are ignored.
   *
   * If no modules implement the hook, this method returns immediately.
   *
   * @param string $operationName
   *   The name of the operation.
   * @param array|\Drupal\foldershare\FolderShareInterface $args
   *   (optional, default = NULL) Appropriate arguments for the hook, such
   *   as a FolderShare entity or an array of entity IDs.
   */
  public static function callHookPostOperation(
    string $operationName,
    $args = NULL) {

    // Convert the operation to lower case and create the hook name.
    // We assume the operation does not have any illegal method name
    // characters since the name is coming from authored code.
    $op = mb_convert_case($operationName, MB_CASE_LOWER);
    $hookName = Constants::MODULE . '_post_operation_' . $op;

    if ($args === NULL) {
      $args = [];
    }
    elseif (is_array($args) === FALSE) {
      // Arguments passed to invokeAll() must be an array. When the hook
      // is called, these are expanded into adjacent function arguments.
      $args = [$args];
    }

    try {
      \Drupal::moduleHandler()->invokeAll($hookName, $args);
    }
    catch (\Exception $e) {
      // Ignore exceptions.
    }
  }

  /*---------------------------------------------------------------------
   *
   * Validate file hook.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to validate a file is valid for use.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_validate_file" hook is called with the given
   * FolderShare parent entity (which may be NULL) and a proposed File entity
   * to be added as a child of the parent.  Hooks may return FALSE to
   * reject the file, or throw an exception.
   *
   * When multiple modules implement the hook, if any module hook throws
   * an exception or returns FALSE, this method throws an exception to
   * indicate that the file is consider invalid. If all modules return
   * non-FALSE (such as TRUE), then this method returns without throwing
   * an exception and the file is condidered valid.
   *
   * If no modules implement the hook, this method returns without throwing
   * an exception and the file is condidered valid.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   The parent folder for the new file. If the parent is NULL, the file is
   *   being added to the user's root list.
   * @param int $ownerUid
   *   The user ID of the intended owner of the file. If the value is (-1),
   *   the file is being added for the current user.
   * @param \Drupal\file\FileInterface $file
   *   The file being added to the folder.
   *
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if any of the hooks return FALSE or if any of
   *   them throw their own exception. If a hook throws an exception, the
   *   exception is converted to a ValidationException with the original
   *   exception's message.
   *
   * @see ::addFilesInternal()
   */
  public static function callHookValidateFile(
    FolderShareInterface $parent = NULL,
    int $ownerUid = (-1),
    FileInterface $file = NULL) {

    try {
      $hookName = Constants::MODULE . '_validate_file';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $parent,
          $ownerUid,
          $file,
        ]);
    }
    catch (ValidationException $e) {
      throw $e;
    }
    catch (\Exception $e) {
      throw new ValidationException($e->getMessage());
    }

    // If there is a FALSE response, throw an exception.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        // User-facing exception message.
        $message1 = t(
          'Invalid file "@filename".',
          [
            '@filename' => pathinfo($file->getFileUri(), PATHINFO_BASENAME),
          ]);
        $message2 = t(
          'The file has been checked and is not valid for this use.');
        throw new ValidationException(
          FormatUtilities::createFormattedMessage($message1, $message2));
      }
    }
  }

  /*---------------------------------------------------------------------
   *
   * Allowed filename extensions alter hook.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to alter a situation's list of filename extensions.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is
   * called and passed a FolderShare parent entity (which may be NULL),
   * an owner user ID for a user performing an operation, and a default
   * array of filename extensions allowed for the operation. This default
   * is typically the site-wide default list set by the site administrator.
   * If this list is empty, then the site administrator has not set a
   * default list and all filename extensions are allowed.
   *
   * Hooks may return a new list of filename extensions used to restrict the
   * range of file types supported as children of the indicated parent. If the
   * parent is NULL, the restriction applies to the user's root list.
   *
   * The hook is called in these situations:
   *
   * - Before a file is uploaded in order to set the list of filename
   *   extensions allowed for an upload. Client (e.g. browsers) may use this
   *   to limit the types of files that a user can select for upload.
   *
   * - After a file has been uploaded in order to validate that the file's
   *   extension is allowed in the parent.
   *
   * - Before a file is copied, moved, or renamed in order to validate that
   *   file's extension is allowed in the parent.
   *
   * - Before a file is extracted from an archive in order to validate that
   *   the file's extension is allowed in the parent.
   *
   * A module's implementation of the hook may return one of two values:
   *
   * - An empty or NULL array, indicating that the hook imposes no restrictions
   *   on filename extensions.
   *
   * - An array of allowed filename extensions, without leading dots.
   *
   * When multiple modules implement the same hook, this method merges all
   * returned extensions into a single list, removing leading dots and
   * duplicates, and converting extensions to lower case. One of three
   * results is then returned:
   *
   * - If the hook extension list is empty, the default extension list is
   *   returned. This is also the case when there are no hook implementations.
   *
   * - If the hook extension list is not empty, but the default extension list
   *   is empty, then the hook extension list is returned. In this case, the
   *   site has no filename extension list and normally accepts all file types,
   *   but hooks have created context-specific limits.
   *
   * - Otherwise the default and hook extension lists are intersected and the
   *   result is returned. In this case, the site has a list of allowed
   *   filename extensions and hooks may further restrict this list, but they
   *   may not allow an extension that the site has not allowed.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   The parent folder for new files. If the parent is NULL, files are
   *   being added to the user's root list.
   * @param int $ownerUid
   *   The user ID of the intended owner of the file. If the value is (-1),
   *   the file is being added for the current user.
   * @param string[] $extensions
   *   The array of default allowed filename extensions, in lower case and
   *   without leading dots. If empty, the site's default extensions will be
   *   used. If the default list is empty too, then the site has no filename
   *   extension limits.
   *
   * @return string[]
   *   A sorted duplicate-free lower-case array of allowed filename
   *   extensions, without leading dots.
   */
  public static function callHookAllowedFilenameExtensionsAlter(
    FolderShareInterface $parent = NULL,
    int $ownerUid = (-1),
    array $extensions = []) {

    // Get default extensions, if none were provided.
    if (empty($extensions) === TRUE) {
      $extensionsString = ManageFilenameExtensions::getAllowedFilenameExtensions();
      $extensions = [];
      if (empty($extensionsString) === FALSE) {
        $extensions = mb_split(' ', $extensionsString);
      }
    }

    if ($extensions === NULL) {
      $extensions = [];
    }

    // Sort the incoming extensions list, if any. Assume it does not have
    // leading dots and it is lower case.
    sort($extensions, SORT_NATURAL);

    // Get hook extensions, if any.
    try {
      $hookName = Constants::MODULE . '_allowed_filename_extensions_alter';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $parent,
          $ownerUid,
          $extensions,
        ]);

      if (empty($results) === TRUE) {
        // No hooks, or they didn't return any extensions. Use the default.
        return $extensions;
      }

      // Clean the hook extensions list, removing non-string values, converting
      // strings to lower case, and removing leading dots.
      $hookExtensions = [];
      foreach ($results as $r) {
        if (is_string($r) === TRUE) {
          $rr = mb_ereg_replace('^[\.]*', '', $r);
          if ($rr === FALSE) {
            $hookExtensions[] = mb_strtolower($r);
          }
          else {
            $hookExtensions[] = mb_strtolower($rr);
          }
        }
      }

      if (empty($hookExtensions) === TRUE) {
        // No valid extensions. Use the default.
        return $extensions;
      }

      $hookExtensions = array_unique($hookExtensions);
      sort($hookExtensions, SORT_NATURAL);

      if (empty($extensions) === TRUE) {
        // No default extensions. Use the hook extensions.
        return $hookExtensions;
      }

      // Remove extensions from hooks that are not in the original extension
      // list. A hook cannot override the site's limits, if any.
      $hookExtensions = array_intersect($extensions, $hookExtensions);
      sort($hookExtensions, SORT_NATURAL);
      return $hookExtensions;
    }
    catch (\Exception $e) {
      // Ignore exceptions.
    }

    return $extensions;
  }

  /**
   * Returns a list of modules that implement the allowed extensions hook.
   *
   * @return string[]
   *   Returns an array of module names for modules that implement the hook.
   *   An empty array is returned if there are no modules implementing the
   *   hook.
   */
  public static function getHookAllowedFilenameExtensionsAlter() {
    $hookName = Constants::MODULE . '_allowed_filename_extensions_alter';
    return self::getHookImplementations($hookName);
  }

  /*---------------------------------------------------------------------
   *
   * File upload size alter hook.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to alter a situation's file upload size limit.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B> This method is public so that it can be called
   * from classes throughout the module.
   *
   * The "hook_foldershare_file_upload_size_limit_alter" hook is
   * called and passed a FolderShare parent entity (which may be NULL),
   * an owner user ID for a user performing an operation, and a default
   * limit. This default is typically the site-wide default set by the
   * site administrator.
   *
   * Hooks may return a new smaller limit to restrict the upload file size
   * supported for children of the indicated parent. If the parent is NULL,
   * the restriction applies to the user's root list.
   *
   * The hook is called in these situations:
   *
   * - Before a file is uploaded in order to set the limit for an upload.
   *   Client (e.g. browsers) may use this to limit the size of files that
   *   a user can select for upload.
   *
   * - After a file has been uploaded in order to validate that the file's
   *   size is at or below the limit in the parent.
   *
   * A module's implementation of the hook may return one of two values:
   *
   * - A non-positive integer value indicating that the hook imposes no new
   *   limit on the file size.
   *
   * - A new positive integer value for a limit in the current context.
   *
   * When multiple modules implement the same hook, this method uses the
   * lowest value set by all hooks.
   *
   * The returned value will be no larger than the given default limit.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   The parent folder for new files. If the parent is NULL, files are
   *   being added to the user's root list.
   * @param int $ownerUid
   *   The user ID of the intended owner of the file. If the value is (-1),
   *   the file is being added for the current user.
   * @param int $limit
   *   The default positive integer file upload size limit. If non-positive,
   *   the site's default file upload size limit is used.
   *
   * @return int
   *   Returns a positive integer for the file upload size limit.
   */
  public static function callHookFileUploadSizeLimitAlter(
    FolderShareInterface $parent = NULL,
    int $ownerUid = (-1),
    int $limit = 0) {

    // Insure that the limit is reasonable.
    if ($limit <= 0) {
      $limit = Settings::getFileUploadSizeLimit();
      if ($limit <= 0) {
        $limit = LimitUtilities::getPhpFileUploadSizeLimit();
      }
    }

    // Get hook limits, if any.
    try {
      $hookName = Constants::MODULE . '_file_upload_size_limit_alter';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $parent,
          $ownerUid,
          $limit,
        ]);

      if (empty($results) === TRUE) {
        // No hooks, or they didn't return any limits. Use the default.
        return $limit;
      }

      // Use the smallest positive limit.
      foreach ($results as $r) {
        $val = intval($r);
        if ($val > 0 && $val < $limit) {
          $limit = $val;
        }
      }

      return $limit;
    }
    catch (\Exception $e) {
      // Ignore exceptions.
    }

    return $limit;
  }

  /**
   * Returns a list of modules that implement the file upload limit hook.
   *
   * @return string[]
   *   Returns an array of module names for modules that implement the hook.
   *   An empty array is returned if there are no modules implementing the
   *   hook.
   */
  public static function getHookFileUploadSizeLimitAlter() {
    $hookName = Constants::MODULE . '_file_upload_size_limit_alter';
    return self::getHookImplementations($hookName);
  }

  /*---------------------------------------------------------------------
   *
   * MIME type alter hook.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to alter the MIME type of an entity being set.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * entity and default MIME type.
   *
   * @param \Drupal\foldershare\FolderShareInterface $entity
   *   The entity being set. The entity has *not* been saved yet and should
   *   not be saved by a hook.
   * @param string $mimeType
   *   The proposed MIME type.
   *
   * @return string
   *   Returns the MIME type to use. If there were no hook implementations or
   *   if none of them proposed an alternative, the given MIME type is
   *   returned.
   *
   * @see ::setMimeType()
   * @see ::setMimeTypeToDefault()
   */
  public static function callHookMimeTypeAlter(
    FolderShareInterface $entity,
    string $mimeType) {

    try {
      $hookName = Constants::MODULE . '_mime_type_alter';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $entity,
          $mimeType,
        ]);
    }
    catch (\Exception $e) {
      return $mimeType;
    }

    if (empty($results) === TRUE) {
      return $mimeType;
    }

    // Use the first non-empty result, if any.
    foreach ($results as $r) {
      if (empty($r) === FALSE) {
        return (string) $r;
      }
    }

    return $mimeType;
  }

  /*---------------------------------------------------------------------
   *
   * User interface enable hooks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to check if the command menu should be enabled.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_command_menu_enable" hook is called with the
   * FolderShare item (which may be NULL). Hooks may return FALSE
   * to disable the user interface's command menu.
   *
   * When multiple modules implement the hook, if any module hook returns FALSE,
   * this method returns FALSE.
   *
   * If no modules implement the hook, this method returns TRUE.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   *
   * @see ::callHookAncestorMenuEnable()
   * @see ::callHookSearchBoxEnable()
   */
  public static function callHookCommandMenuEnable(
    FolderShareInterface $item = NULL,
    int $userUid = (-1)) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    try {
      $hookName = Constants::MODULE . '_ui_command_menu_enable';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
        ]);
    }
    catch (\Exception $e) {
      return TRUE;
    }

    // If there is any FALSE response, return FALSE.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Calls module hooks to check if the ancestor menu should be enabled.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_ancestor_menu_enable" hook is called with the
   * FolderShare item (which may be NULL). Hooks may return FALSE
   * to disable the user interface's ancestor menu.
   *
   * When multiple modules implement the hook, if any module hook returns FALSE,
   * this method returns FALSE.
   *
   * If no modules implement the hook, this method returns TRUE.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   *
   * @see ::callHookAncestorMenuEnable()
   * @see ::callHookSearchBoxEnable()
   */
  public static function callHookAncestorMenuEnable(
    FolderShareInterface $item = NULL,
    int $userUid = (-1)) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    try {
      $hookName = Constants::MODULE . '_ui_ancestor_menu_enable';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
        ]);
    }
    catch (\Exception $e) {
      return TRUE;
    }

    // If there is any FALSE response, return FALSE.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Calls module hooks to check if the search box should be enabled.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_search_box_enable" hook is called with the given
   * FolderShare item (which may be NULL). Hooks may return FALSE
   * to disable the user interface's search box.
   *
   * When multiple modules implement the hook, if any module hook returns FALSE,
   * this method returns FALSE.
   *
   * If no modules implement the hook, this method returns TRUE.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   *
   * @see ::callHookAncestorMenuEnable()
   * @see ::callHookSearchBoxEnable()
   */
  public static function callHookSearchBoxEnable(
    FolderShareInterface $item = NULL,
    int $userUid = (-1)) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    try {
      $hookName = Constants::MODULE . '_ui_search_box_enable';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
        ]);
    }
    catch (\Exception $e) {
      return TRUE;
    }

    // If there is any FALSE response, return FALSE.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Calls module hooks to check if the sidebar should be enabled.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_sidebar_enable" hook is called with the given
   * FolderShare item (which may be NULL). Hooks may return FALSE
   * to disable the user interface's folder browser sidebar.
   *
   * When multiple modules implement the hook, if any module hook returns FALSE,
   * this method returns FALSE.
   *
   * If no modules implement the hook, this method returns TRUE.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   *
   * @see ::callHookSidebarButtonEnable()
   */
  public static function callHookSidebarEnable(
    FolderShareInterface $item = NULL,
    int $userUid = (-1)) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    try {
      $hookName = Constants::MODULE . '_ui_sidebar_enable';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
        ]);
    }
    catch (\Exception $e) {
      return TRUE;
    }

    // If there is any FALSE response, return FALSE.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Calls module hooks to check if the sidebar button should be enabled.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_sidebar_button_enable" hook is called with
   * the given FolderShare item (which may be NULL). Hooks may return FALSE
   * to disable the user interface's folder browser sidebar show/hide button.
   *
   * When multiple modules implement the hook, if any module hook returns FALSE,
   * this method returns FALSE.
   *
   * If no modules implement the hook, this method returns TRUE.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   *
   * @see ::callHookSidebarEnable()
   */
  public static function callHookSidebarButtonEnable(
    FolderShareInterface $item = NULL,
    int $userUid = (-1)) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    try {
      $hookName = Constants::MODULE . '_ui_sidebar_button_enable';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
        ]);
    }
    catch (\Exception $e) {
      return TRUE;
    }

    // If there is any FALSE response, return FALSE.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Calls module hooks to check if a command should be enabled.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_command_enable" hook is called with the given
   * FolderShare item (which may be NULL), user ID, and command definition.
   * Hooks may return FALSE to disable the specific command in the user
   * interface.
   *
   * When multiple modules implement the hook, if any module hook returns FALSE,
   * this method returns FALSE.
   *
   * If no modules implement the hook, this method returns TRUE.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   * @param array $definition
   *   (optional, default = []) The command plugin's definition. See the
   *   FolderShareCommand annotation class for a description of definition
   *   fields.
   *
   * @see ::getHookCommandEnable()
   * @see \Drupal\foldershare\Annotation\FolderShareCommand
   */
  public static function callHookCommandEnable(
    FolderShareInterface $item = NULL,
    int $userUid = (-1),
    array $definition = []) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    try {
      $hookName = Constants::MODULE . '_ui_command_enable';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
          $definition,
        ]);
    }
    catch (\Exception $e) {
      return TRUE;
    }

    // If there is any FALSE response, return FALSE.
    foreach ($results as $r) {
      if (is_bool($r) === TRUE && $r === FALSE) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Returns a list of modules that implement the command enable hook.
   *
   * @return string[]
   *   Returns an array of module names for modules that implement the hook.
   *   An empty array is returned if there are no modules implementing the
   *   hook.
   *
   * @see ::callHookCommandEnable()
   */
  public static function getHookCommandEnable() {
    $hookName = Constants::MODULE . '_ui_command_enable';
    return self::getHookImplementations($hookName);
  }

  /*---------------------------------------------------------------------
   *
   * User interface additions hooks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Calls module hooks to collect additional links for the sidebar.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The "hook_foldershare_ui_sidebar_links" hook is called with the given
   * FolderShare item (which may be NULL) and an array of links. Hooks may
   * add or delete from the array. After all hooks have been called, if the
   * array is not empty, the links in the array are added to the sidebar.
   *
   * When multiple modules implement the hook. Each one can add to the
   * links array.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL = root list) The current file/folder.
   * @param int $userUid
   *   (optional, default = (-1) = current user) The current user ID.
   *
   * @return array
   *   Returns an array of link objects collected from the hooks. If
   *   there are no hook implementations or no hooks return links, the
   *   returned array is empty.
   *
   * @see ::callHookSidebarEnable()
   */
  public static function &callHookSidebarLinks(
    FolderShareInterface $item = NULL,
    int $userUid = (-1)) {

    if ($userUid < 0) {
      $userUid = \Drupal::currentUser()->id();
    }

    $results = [];
    try {
      $hookName = Constants::MODULE . '_ui_sidebar_links';
      $results = \Drupal::moduleHandler()->invokeAll(
        $hookName,
        [
          $item,
          $userUid,
        ]);
    }
    catch (\Exception $e) {
      return $results;
    }

    return $results;
  }

}
