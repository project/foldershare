<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

use Drupal\foldershare\Entity\Exception\RuntimeExceptionWithMarkup;
use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\Settings;

/**
 * Defines a command plugin to rename a file or folder.
 *
 * The command sets the name of a single selected entity.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entity to rename.
 * - 'name': the new name.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_rename",
 *  label           = @Translation("Rename"),
 *  menuNameDefault = @Translation("Rename..."),
 *  menuName        = @Translation("Rename..."),
 *  description     = @Translation("Rename a selected file or folder."),
 *  category        = "edit",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "any",
 *    },
 *    "access"  = "view",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "parent",
 *      "one",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "update",
 *  },
 * )
 */
class Rename extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Configuration.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Include room for the new name in the configuration.
    $config = parent::defaultConfiguration();
    $config['name'] = '';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function validateParameters() {
    if ($this->parametersValidated === TRUE) {
      return;
    }

    // Get the parent folder, if any.
    $parent = $this->getParent();

    // Get the selected item. If none, use the parent instead.
    $itemIds = $this->getSelectionIds();
    if (empty($itemIds) === TRUE) {
      $item = $this->getParent();
    }
    else {
      $item = FolderShare::load(reset($itemIds));
    }

    // Check if the name is legal. This throws an exception if not valid.
    $extensions = [];
    if ($item->isFileOrImage() === TRUE) {
      // Prepare to check for allowed filename extensions by getting the
      // site's allowed list, altered by module hooks.
      $extensions = ManageHooks::callHookAllowedFilenameExtensionsAlter(
        $parent,
        $this->getCurrentUser()->id(),
        []);
    }

    // Throw an exception on an illegal name or bad filename extension.
    $newName = $this->configuration['name'];
    FolderShare::validateNameAndFilenameExtension(
      $newName,
      $extensions);

    // Check if the new name is unique within the parent folder or root list.
    if ($parent !== NULL) {
      if ($parent->isNameUnique($newName, (int) $item->id()) === FALSE) {
        // User-facing exception.
        throw new ValidationException(
          FolderShare::getStandardNameInUseExceptionMessage($newName));
      }
    }
    elseif (FolderShare::isRootNameUnique($newName, (int) $item->id()) === FALSE) {
      // User-facing exception.
      throw new ValidationException(
          FolderShare::getStandardNameInUseExceptionMessage($newName));
    }

    $this->parametersValidated = TRUE;
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    $ids = $this->getSelectionIds();
    if (empty($ids) === TRUE) {
      $ids = [$this->getParentId()];
    }

    return $this->t(
      'Rename "@name"',
      [
        '@name' => FolderShare::findNameForId($ids[0]),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Rename');
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    //
    // Disable inline form errors.
    // ---------------------------
    // When the core Inline Form Errors module is enabled, it attempts to
    // distributed error messages to immediately follow the form fields they
    // relate to. While this can be a good thing, for this dialog it causes
    // entered user name errors to be inserted below the user text field
    // (added below) and before the text field's explanation and its' "Add"
    // button. This is awkward. The Inline Form Errors module also adds a
    // redundant error message to the status messages section at the bottom
    // of the dialog.
    //
    // To disable this strangeness, the Inline Form Errors module's behavior
    // is diabled for the entire dialog.
    $form['#disable_inline_form_errors'] = TRUE;

    //
    // Disable auto-complete.
    // ----------------------
    // In principal, adding 'autocomplete="off"' to a form (and/or form
    // element) will disable browser autocomplete. In practice, modern
    // browsers ignore it and provide autocomplete on any text field.
    // If the text field is for a user name, then some browsers will invoke
    // password managers (e.g. Apple's keychain) to let you select a user
    // name.
    //
    // Nevertheless, mark the form with our intent that autocomplete NOT
    // be enabled anywhere on the form.
    $form['#attributes']['autocomplete'] = 'off';
    $form['#attributes']['class'][] = 'foldershare-rename';
    $form['#attached']['library'][] = 'foldershare/foldershare.renamedialog';

    //
    // Get the item and current name.
    // ------------------------------
    // Get the current item name to use as the form default value.
    $itemIds = $this->getSelectionIds();
    if (empty($itemIds) === TRUE) {
      $item = $this->getParent();
    }
    else {
      $item = FolderShare::load(reset($itemIds));
    }

    $this->configuration['name'] = $item->getName();

    //
    // Fight auto-complete.
    // --------------------
    // Browsers often ignore attempts to disable autocomplete. Instead,
    // they try to figure out what type of autocomplete to use. Two common
    // forms are:
    // - User account autocomplete.
    // - User name autocomplete.
    //
    // A browser's user account autocomplete invokes a password manager to
    // let the user select an account name for the current user.
    //
    // A browser's user name autocomplete invokes a contact list browser to
    // let the user select another user name for the field.
    //
    // Browsers may use the "name" attribute to infer one of these. If that
    // doesn't work, they may look at the field's label. For instance, if
    // the label is "Username", a browser may infer autocomplete of user
    // account names and show a password manager.
    //
    // In the following form, we intentionally use a field name that does
    // not have the word "name" in it. And we intentionally use a label
    // that doesn't have "Name" in it either. Experimentation has found that
    // this combination defeats both of the above autocomplete inferences
    // and produces an "off" autocomplete fallback, which is what we want.
    $renameLabel = $this->t('Rename:');

    //
    // Define form element names/classes.
    // ----------------------------------
    // Most form elements have specific names/classes. Names enable values
    // to be retreived easily. Classes enable custom styling.
    //
    // The rename field is *intentionally* not named using words like
    // "name" or "rename" because some browsers use this name to decide whether
    // to show an autofill using a password manager to select an account name,
    // presuming that the form is for a login. By naming the field differently
    // we thwart a browser's guess that the field is for an account name,
    // and block an inappropriate password manager prompt. This is crude,
    // but necessary in 2019 to get around over-zelous browser attempts to
    // autofill form fields.
    $renameField = 'foldershare-replacement';

    // The command wrapper provides form basics:
    // - Attached libraries.
    // - Page title (if not an AJAX dialog).
    // - Description (from ::getDescription()).
    // - Submit buttion (labeled with ::getSubmitButtonName()).
    // - Cancel button (if AJAX dialog).
    //
    // Add a text field prompt for the new name.
    $weight = 10;
    $form[$renameField] = [
      '#type'          => 'textfield',
      '#name'          => $renameField,
      '#weight'        => $weight,
      '#title'         => $renameLabel,
      '#size'          => 30,
      '#minlength'     => 1,
      '#maxlength'     => 255,
      '#required'      => TRUE,
      '#default_value' => $this->configuration['name'],
      '#attributes'    => [
        'class'        => [$renameField],
        // Most browsers, but not iOS Safari. Move focus to text field.
        'autofocus'    => 'on',
        // No browsers, but they should disable autocomplete.
        'autocomplete' => 'off',
        // Most browsers. Disables automatic capitalization.
        'autocapitalize' => 'none',
        // Most browsers. Disables spell check.
        'spellcheck'   => 'false',
        // Safari only. Disables spell check.
        'autocorrect'  => 'off',
        // Firefox for Android only. Names the 'Enter' key on a virtual keybd.
        'mozactionhint' => $this->t('Rename'),
      ],
    ];
    ++$weight;

    //
    // Handle messages.
    // ----------------
    // This form is intended to rename items. This is not the place to
    // report warnings and status messages. Only those messages relevant to
    // this form should be shown.
    //
    // So, delete pending status and warning messages. If we don't do this,
    // Drupal's form processing automatically adds them to the top of the form.
    //
    // Then, add a field for the remaining messages.
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_STATUS);
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_WARNING);

    $form['status'] = [
      '#type'   => 'status_messages',
      '#weight' => $weight,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    //
    // Define form element names/classes.
    // ----------------------------------
    // Most form elements have specific names/classes. Names enable values
    // to be retreived easily.
    $renameField = 'foldershare-replacement';

    // Get the entered text for the item.
    $name = $formState->getValue($renameField);

    // Validate the name alone, without considering file name extensions
    // or uniqueness.
    if (FolderShare::isNameLegal($name) === FALSE) {
      // Since the field is required, Drupal adds a default error message
      // when the field is left empty. Get rid of that so we can provide a
      // more helpful message.
      $formState->clearErrors();

      $formState->setErrorByName(
        $renameField,
        strip_tags(
          (string) FolderShare::getStandardIllegalNameExceptionMessage($name)));
      return;
    }

    // Update the configuration.
    $this->configuration['name'] = $name;

    // Do a full validation.
    try {
      $this->validateParameters();
    }
    catch (RuntimeExceptionWithMarkup $e) {
      // Unfortunately, setErrorByName() will not accept complex markup,
      // such as for multi-line error messages. If we pass the markup,
      // the function strips it down to the last line of text. To avoid
      // this, we have to pass the message text instead.
      $formState->setErrorByName($renameField, $e->getMessage());
    }
    catch (\Exception $e) {
      $formState->setErrorByName($renameField, $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    if ($this->isValidated() === TRUE) {
      $this->execute();
    }
  }

  /*---------------------------------------------------------------------
   *
   * Execution behavior.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getExecuteBehavior() {
    if (empty($this->getSelectionIds()) === TRUE) {
      // When there is no selection, execution falls back to operating
      // on the current parent. After a change, the breadcrumbs or
      // other page decoration may differ. We need to refresh the page.
      return FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH;
    }

    // When there is a selection, execution changes that selection on the
    // current page. While columns may change, the page doesn't, so we
    // only need to refresh the view.
    return FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH;
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $itemIds = $this->getSelectionIds();
    if (empty($itemIds) === TRUE) {
      $item = $this->getParent();
    }
    else {
      $item = FolderShare::load(reset($itemIds));
    }

    $this->validateParameters();
    $item->rename($this->configuration['name']);

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $this->getMessenger()->addStatus(
        $this->t(
          "The @kind has been renamed.",
          [
            '@kind' => FolderShare::translateKind($item->getKind()),
          ]));
    }
  }

}
