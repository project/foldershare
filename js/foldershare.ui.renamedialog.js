/**
 * @file
 * Implements the FolderShare share dialog user interface.
 *
 * The dialog for the "Share" command presents a table of users and the
 * sharing permissions they have been granted. The dialog also contains
 * an auto-complete text field for entering the name of a new user to add
 * to the list. An "Add" button triggers the addition.
 *
 * This script supports the dialog by mapping a carriage return on the add
 * user text field into a trigger of the "Add" button.
 *
 * @ingroup foldershare
 * @see \Drupal\foldershare\Plugin\FolderShareCommand\Share
 * @see \Drupal\foldershare\Form\CommandFormWrapper
 */
(($, Drupal) => {
  Drupal.foldershare.RenameDialog = {
    /**
     * Attaches the module's UI behaviors.
     *
     * @param {Document} pageContext
     *   The page context for this call. Initially, this is the full document.
     *   Later, this is only portions of the document added via AJAX.
     *
     * @return {boolean}
     *   Always returns true.
     */
    attach(pageContext) {
      //
      // Test and exit
      // -------------
      // Quickly exit if the page context does not include the add-user form.
      const renameForm = ".foldershare-rename";
      let $renameForm;

      if (typeof pageContext.tagName === "undefined") {
        // The full document. Search down.
        $renameForm = $(renameForm, pageContext);
      } else {
        // Above or below. Search up first, then down.
        $renameForm = $(pageContext).parents(renameForm);
        if ($renameForm.length === 0) {
          $renameForm = $(renameForm, pageContext);
        }
      }

      if ($renameForm.length === 0) {
        // Fail. The document does not contain the add-user form.
        return true;
      }

      //
      // Process top elements
      // --------------------
      // Process each rename form. There should be only one.
      $renameForm.each((index, element) => {
        // Get the text field.
        const $renameField = $(".foldershare-replacement", $(element)).eq(0);
        if ($renameField.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI rename field is missing."
          );
          return false;
        }

        // The form always has a submit button.
        const $submitButton = $(".dialog-submit", $(element)).eq(0);
        if ($submitButton.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI rename submit button is missing."
          );
          return false;
        }

        // Attach a behavior to trigger the button on a carriage return.
        $renameField.off("keypress.foldershare");
        $renameField.on("keypress.foldershare", ev => {
          if (ev.keyCode === 13) {
            $submitButton.prop("disabled", false);
            $submitButton.click();
            ev.preventDefault();
            return false;
          }
        });

        // Attach a behavior to enable/disable the submit button based
        // on whether the text field is empty.
        //
        // Complicating things... Drupal wraps the form with a dialog that
        // can add duplicate action buttons to a standard location in the
        // dialog. This is done with Javascript, so we can't search for
        // the additional buttons until it is time to enable/disable them.
        $renameField.off("keyup.foldershare");
        $renameField.on("keyup.foldershare", () => {
          // Determine the new state.
          const name = $renameField.val();
          let disabled = false;
          if (name === null || name.length === 0) {
            disabled = true;
          }

          // Set the disabled property on the main form submit button.
          $submitButton.prop("disabled", disabled);

          // Look for the duplicate submit button and set its
          // disabled property too.
          const $parentDialog = $(element).closest(".ui-dialog");
          if ($parentDialog.length !== 0) {
            const $parentSubmitButton = $(
              ".ui-dialog-buttonset .dialog-submit",
              $parentDialog
            ).eq(0);
            if ($parentSubmitButton.length !== 0) {
              $parentSubmitButton.prop("disabled", disabled);
            }
          }
        });

        $renameField.focus();

        // Enable/disable the submit button initially based on whether the text
        // field is empty.
        const name = $renameField.val();
        let disabled = false;
        if (name === null || name.length === 0) {
          disabled = true;
        }
        $submitButton.prop("disabled", disabled);
      });

      return true;
    }
  };

  /*--------------------------------------------------------------------
   *
   * On Drupal ready behaviors.
   *
   * Set up behaviors to execute when the page is fully loaded, or whenever
   * AJAX sends a new page fragment.
   *
   *--------------------------------------------------------------------*/

  Drupal.behaviors.foldershare_RenameDialog = {
    attach(pageContext) {
      Drupal.foldershare.RenameDialog.attach(pageContext);
    }
  };
})(jQuery, Drupal);
