<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

use Drupal\user\Entity\User;

use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\FormatUtilities;
use Drupal\foldershare\Utilities\UserUtilities;

/**
 * Defines a command plugin to change ownership of files or folders.
 *
 * The command sets the UID for the owner of all selected entities.
 * Owenrship changes recurse through all folder content as well.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to change ownership on.
 * - 'uid': the UID of the new owner.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_change_owner",
 *  label           = @Translation("Change Owner"),
 *  menuNameDefault = @Translation("Change Owner..."),
 *  menuName        = @Translation("Change Owner..."),
 *  description     = @Translation("Change the owner of selected files and folders, and optionally for all of a folder's descendants. This command is only available for content administrators."),
 *  category        = "administer",
 *  weight          = 0,
 *  userConstraints = {
 *    "adminpermission",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "any",
 *    },
 *    "access"  = "view",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "parent",
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "chown",
 *  },
 * )
 */
class ChangeOwner extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Configuration.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Add room for the UID and a recursion flag.
    $config = parent::defaultConfiguration();
    $config['uid'] = '';
    $config['changedescendants'] = 'FALSE';
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function validateParameters() {
    if ($this->parametersValidated === TRUE) {
      return;
    }

    // Get the new UID from the configuration and check if it is valid.
    $uid = $this->configuration['uid'];
    if ($uid === NULL) {
      // When there is no UID in the configuration, it is probably because
      // the form auto-complete did not recognize the user name the user
      // typed in, and therefore could not map it to a UID. So make the
      // error message about an unknown user name, not a missing UID.
      // User-facing exception message.
      $message1 = $this->t(
        'The user name does not match any user account at this site.');
      $message2 = $this->t(
        'Please check that the name is correct and for an existing account.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message1, $message2));
    }

    $user = User::load($uid);
    if ($user === NULL) {
      // User-facing exception message.
      $message1 = $this->t(
        'The user ID "@uid" does not match any user account at this site.',
        [
          '@uid' => $uid,
        ]);
      $message2 = $this->t(
        'Please check that the ID is correct and for an existing account.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message1, $message2));
    }

    $this->parametersValidated = TRUE;
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    // If there is no selection, use the parent.
    $ids = $this->getSelectionIds();
    if (empty($ids) === TRUE) {
      $ids = [$this->getParentId()];
    }

    // If none of the selected items are shared, do not provide a description.
    $shared = $this->doesSelectionContainSharedWithOthers($ids);
    if ($shared === FALSE) {
      return NULL;
    }

    // If none of the items are root ids, do not provide a description.
    $idsAndRoots = FolderShare::findRootIdsForIds($ids);
    $rootIds = array_unique(array_values($idsAndRoots));
    $intersect = array_intersect($ids, $rootIds);
    if (empty($intersect) === TRUE) {
      return NULL;
    }

    // Otherwise, we have at least one shared root. Include a warning.
    $kindsAndIds = FolderShare::findKindsForIds($ids);

    if (count($ids) === 1) {
      $kinds = array_keys($kindsAndIds);
      return $this->t(
        'Since this @kind is shared, changing its owner will end shared access and affect other users.',
        [
          '@kind' => FolderShare::translateKind($kinds[0]),
        ]);
    }

    return $this->t(
      'Since some of these @kinds are shared, changing their owner will end shared access and affect other users.',
      [
        '@kinds' => FolderShare::translateKinds('items'),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    // If there is no selection, use the parent.
    $ids = $this->getSelectionIds();
    if (empty($ids) === TRUE) {
      $ids = [$this->getParentId()];
    }

    if (count($ids) === 1) {
      return $this->t(
        'Change the owner of "@name"',
        [
          '@name' => FolderShare::findNameForId($ids[0]),
        ]);
    }

    $kinds = FolderShare::translateKindsForIds($ids);
    return $this->t(
      "Change the owner of @kinds",
      [
        '@kinds' => $kinds['translation'],
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Change');
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    //
    // Define form element names/classes.
    // ----------------------------------
    // Most form elements have specific names/classes. Names enable values
    // to be retreived easily. Classes enable custom styling.
    $ownerField        = 'foldershare-new-owner';
    $changeDescendants = 'foldershare-change-descendants';

    //
    // Disable inline form errors.
    // ---------------------------
    // When the core Inline Form Errors module is enabled, it attempts to
    // distributed error messages to immediately follow the form fields they
    // relate to. While this can be a good thing, for this dialog it causes
    // entered user name errors to be inserted below the user text field
    // (added below) and before the text field's explanation and its' "Add"
    // button. This is awkward. The Inline Form Errors module also adds a
    // redundant error message to the status messages section at the bottom
    // of the dialog.
    //
    // To disable this strangeness, the Inline Form Errors module's behavior
    // is diabled for the entire dialog.
    $form['#disable_inline_form_errors'] = TRUE;

    //
    // Disable auto-complete.
    // ----------------------
    // In principal, adding 'autocomplete="off"' to a form (and/or form
    // element) will disable browser autocomplete. In practice, modern
    // browsers ignore it and provide autocomplete on any text field.
    // If the text field is for a user name, then some browsers will invoke
    // password managers (e.g. Apple's keychain) to let you select a user
    // name.
    //
    // Nevertheless, mark the form with our intent that autocomplete NOT
    // be enabled anywhere on the form.
    $form['#attributes']['autocomplete'] = 'off';
    $form['#attributes']['class'][] = 'foldershare-change-owner';
    $form['#attached']['library'][] = 'foldershare/foldershare.changeownerdialog';

    //
    // Decide on labeling.
    // -------------------
    // Find the kinds for each of the selection IDs.
    $selectionIds = $this->getSelectionIds();
    $selectionKinds = FolderShare::findKindsForIds($selectionIds);
    $hasFolders = (isset($selectionKinds[FolderShare::FOLDER_KIND]) === TRUE);

    // Use the UID of the current user as the default value.
    $this->configuration['uid'] = $this->getCurrentUser()->id();

    $formInput = $formState->getUserInput();

    //
    // Build the form.
    // ---------------
    // The command wrapper provides form basics:
    // - Attached libraries.
    // - Page title (if not an AJAX dialog).
    // - Description (from ::getDescription()).
    // - Submit buttion (labeled with ::getSubmitButtonName()).
    // - Cancel button (if AJAX dialog).
    //
    // Add a text field for the new owner, and optionally a checkbox for
    // recursing through folder contents.
    $title = '';
    switch (Settings::getUserAutocompleteStyle()) {
      default:
      case 'none':
      case 'name-only':
        $title = $this->t('User account name:');
        break;

      case 'name-email':
      case 'name-masked-email':
        $title = $this->t('User account name or email address:');
        break;
    }

    $defaultUser = '';
    if (isset($formInput[$ownerField]) === TRUE) {
      $defaultUser = $formInput[$ownerField];
    }

    if (empty($defaultUser) === TRUE) {
      $defaultUser = $this->getCurrentUser()->getAccountName();
    }

    $weight = 10;
    $form[$ownerField] = [
      '#type'          => 'textfield',
      '#maxlength'     => 256,
      '#default_value' => $defaultUser,
      '#name'          => $ownerField,
      '#title'         => $title,
      '#weight'        => $weight,
      '#required'      => TRUE,
      '#size'          => 30,
      '#minlength'     => 1,
      '#attributes'    => [
        'class'        => ['foldershare-change-owner-new-owner'],
        // Most browsers, but not iOS Safari. Move focus to text field.
        'autofocus'    => 'on',
        // No browsers, but they should disable autocomplete.
        'autocomplete' => 'off',
        // Most browsers. Disables automatic capitalization.
        'autocapitalize' => 'none',
        // Most browsers. Disables spell check.
        'spellcheck'   => 'false',
        // Safari only. Disables spell check.
        'autocorrect'  => 'off',
        // Firefox for Android only. Names the 'Enter' key on a virtual keybd.
        'mozactionhint' => $this->t('Change'),
      ],
    ];
    ++$weight;

    // If the site allows user autocomplete, set up the text field.
    if (Settings::getUserAutocompleteStyle() !== 'none') {
      $form[$ownerField]['#autocomplete_route_name'] =
        'entity.foldershare.userautocomplete';

      $form[$ownerField]['#autocomplete_route_parameters'] = [
        'excludeBlocked' => 0,
      ];
    }

    if ($hasFolders === TRUE) {
      $form[$changeDescendants] = [
        '#type'          => 'checkbox',
        '#name'          => $changeDescendants,
        '#weight'        => $weight,
        '#title'         => $this->t('Apply to enclosed items'),
        '#default_value' => ($this->configuration['changedescendants'] === 'TRUE'),
        '#attributes' => [
          'class'     => [
            $changeDescendants,
          ],
        ],
      ];
      ++$weight;
    }

    //
    // Handle messages.
    // ----------------
    // This form is intended to change ownership. This is not the place to
    // report warnings and status messages. Only those messages relevant to
    // this form should be shown.
    //
    // So, delete pending status and warning messages. If we don't do this,
    // Drupal's form processing automatically adds them to the top of the form.
    //
    // Then, add a field for the remaining messages.
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_STATUS);
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_WARNING);

    $form['status'] = [
      '#type'   => 'status_messages',
      '#weight' => $weight,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    //
    // Define form element names/classes.
    // ----------------------------------
    // Most form elements have specific names/classes. Names enable values
    // to be retreived easily. Classes enable custom styling.
    $ownerField = 'foldershare-new-owner';
    $changeDescendants = 'foldershare-change-descendants';

    $ownerErrors = $ownerField;

    //
    // Get new owner name.
    // -------------------
    // Get the entered text for the new owner.
    $name = $formState->getValue($ownerField);

    // Remove leading white space.
    $cleanedName = mb_ereg_replace('^\s+', '', $name);
    if ($cleanedName !== FALSE) {
      $name = $cleanedName;
    }

    // Remove trailing white space.
    $cleanedName = mb_ereg_replace('\s+$', '', $name);
    if ($cleanedName !== FALSE) {
      $name = $cleanedName;
    }

    if (empty($name) === TRUE) {
      // Empty user.
      // Since the field is required, Drupal adds a default error message
      // when the field is left empty. Get rid of that so we can provide
      // a more helpful message.
      $formState->clearErrors();

      // And now intentionally use an empty error message. This will
      // highlight the form field but not show a message. Such a message
      // would be redundant and just say "Enter an account name", which
      // is what the description under the field already says.
      $formState->setErrorByName($ownerErrors, '');

      // Clear the name field.
      $form[$ownerField]['#value'] = '';
      return;
    }

    // Use the given name to find a user then add them to the grants list.
    $uid = UserUtilities::findUser($name);
    if ($uid === (-1)) {
      // User not found.
      $formState->setErrorByName(
        $ownerErrors,
        $this->t(
          '"%name" is not a recognized account at this site.',
          [
            '%name' => $name,
          ]));

      // Insure the name field has the cleaned name.
      $form[$ownerField]['#value'] = $name;
      return;
    }

    $this->configuration['uid'] = $uid;

    // Get the descendants flag, if any.
    $this->configuration['changedescendants'] = 'FALSE';
    if ($formState->hasValue($changeDescendants) === TRUE &&
        $formState->getValue($changeDescendants) === 1) {
      $this->configuration['changedescendants'] = 'TRUE';
    }

    // Validate.
    try {
      $this->validateParameters();
    }
    catch (\Exception $e) {
      $formState->setErrorByName($ownerErrors, $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    if ($this->isValidated() === TRUE) {
      $this->execute();
    }
  }

  /*---------------------------------------------------------------------
   *
   * Execution behavior.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getExecuteBehavior() {
    if (empty($this->getSelectionIds()) === TRUE) {
      // When there is no selection, execution falls back to operating
      // on the current parent. After a change, the breadcrumbs or
      // other page decoration may differ. We need to refresh the page.
      return FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH;
    }

    // When there is a selection, execution changes that selection on the
    // current page. While columns may change, the page doesn't, so we
    // only need to refresh the view.
    return FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH;
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $ids = $this->getSelectionIds();
    if (empty($ids) === TRUE) {
      $ids[] = $this->getParentId();
    }

    FolderShare::changeOwnerIdMultiple(
      $ids,
      $this->configuration['uid'],
      ($this->configuration['changedescendants'] === 'TRUE'));

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $count = count($ids);
      if ($count <= 1) {
        $message = $this->t("The item has been changed.");
      }
      else {
        $message = $this->t(
          "@count items have been changed.",
          [
            '@count' => $count,
          ]);
      }
      $this->getMessenger()->addStatus($message);
    }
  }

}
