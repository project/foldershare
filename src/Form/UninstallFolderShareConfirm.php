<?php

namespace Drupal\foldershare\Form;

use Drupal\Core\Entity\EntityTypeManager;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareScheduledTask;
use Drupal\foldershare\ManageSearch;
use Drupal\foldershare\ManageUsageStatistics;
use Drupal\foldershare\Settings;

/**
 * Provides a form for removing all files and folders before an uninstall.
 *
 * <B>Access control</B>
 * The route to this form should restrict access to those with administration
 * permission.
 *
 * @ingroup foldershare
 */
class UninstallFolderShareConfirm extends ConfirmFormBase {

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The entity type manager, set at construction time.
   *
   * @var \Drupal\Core\Entity\EntityTypeManager
   */
  protected $entityTypeManager;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new page.
   *
   * @param \Drupal\Core\Entity\EntityTypeManager $entityTypeManager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManager $entityTypeManager) {
    $this->entityTypeManager = $entityTypeManager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static($container->get('entity_type.manager'));
  }

  /*--------------------------------------------------------------------
   *
   * Form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return mb_strtolower(str_replace('\\', '_', get_class($this)));
  }

  /**
   * Returns the title for the form.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The form title.
   */
  public function getTitle() {
    return $this->getQuestion();
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Are you sure you want to delete all files and folders?');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    // The description is filled in during form build.
    return '';
  }

  /**
   * {@inheritdoc}
   */
  public function getConfirmText() {
    return $this->t('Delete all files and folders');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return Url::fromRoute('system.modules_uninstall');
  }

  /*--------------------------------------------------------------------
   *
   * Form build.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState = NULL) {
    $form = parent::buildForm($form, $formState);

    // Get the number of items to delete. This is an expensive query,
    // so we didn't do it in getDescription().
    $n = FolderShare::countNumberOfItems();

    // Now that we have the number of items to delete, create a better
    // description.
    if ($n === 0) {
      $form['description']['#markup'] = $this->t(
        '<p>There are no files and folders to delete.</p>');

      // Hide the delete button if there is nothing to delete.
      $form['actions']['submit']['#access'] = FALSE;
    }
    else {
      $form['description']['#markup'] = $this->t(
        '<p>There are @n FolderShare module files and folders to delete. Deleting them cannot be undone. Make a backup of your database if you want to be able to restore these items.</p><p>This only affects files managed by the FolderShare module. Files used by other modules are not affected.</p>',
      [
        '@n' => $n,
      ]);
    }

    return $form;
  }

  /*--------------------------------------------------------------------
   *
   * Form validate.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    // Nothing to do.
  }

  /*--------------------------------------------------------------------
   *
   * Form submit.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    // Stop all background tasks first.
    // - Stop the task scheduler.
    // - Stop the search indexer.
    // - Stop the usage table updater.
    // - Disable activity logging.
    $taskEnabled = FolderShareScheduledTask::isTaskExecutionEnabled();
    FolderShareScheduledTask::setTaskExecutionEnabled(FALSE);

    $indexInterval = Settings::getSearchIndexInterval();
    Settings::setSearchIndexInterval('system');

    $usageInterval = Settings::getUsageUpdateInterval();
    Settings::setUsageUpdateInterval('manual');

    $loggingEnable = Settings::getActivityLogEnable();
    Settings::setActivityLogEnable(FALSE);

    // Delete all tasks.
    FolderShareScheduledTask::deleteAllTasks();

    // Delete all files and folders.
    FolderShare::deleteAll();

    // Clear the search index.
    ManageSearch::clearIndex();

    // Clear usage table.
    ManageUsageStatistics::clearUsage();

    // It is possible that the user will NOT continue the uninstall after
    // running this form to delete everything. In that case we need to leave
    // the module in a working state.
    // - Restore activity logging.
    // - Restore the usage table updater.
    // - Restore the search indexer.
    // - Restore the task scheduler.
    //
    // Let the module's preuninstall() hook finish cleanup by deleting
    // the directory tree containing local files, if the user continues
    // to uninstall the module.
    Settings::setUsageUpdateInterval($usageInterval);

    Settings::setSearchIndexInterval($indexInterval);

    Settings::setActivityLogEnable($loggingEnable);

    FolderShareScheduledTask::setTaskExecutionEnabled($taskEnabled);

    $formState->setRedirect('system.modules_uninstall');
  }

}
