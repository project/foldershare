<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

/**
 * Get/set FolderShare entity description field.
 *
 * This trait includes get and set methods for FolderShare entity
 * description field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetDescriptionTrait {

  /*---------------------------------------------------------------------
   *
   * Description field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    $value = $this->description->getValue();
    if (empty($value) === TRUE ||
        isset($value[0]) === FALSE ||
        isset($value[0]['value']) === FALSE ||
        empty($value[0]['value']) === TRUE) {
      return '';
    }

    return $value[0]['value'];
  }

  /**
   * {@inheritdoc}
   */
  public function getDescriptionFormat() {
    $value = $this->description->getValue();
    if (empty($value) === TRUE ||
        isset($value[0]) === FALSE ||
        isset($value[0]['format']) === FALSE ||
        empty($value[0]['format']) === TRUE) {
      return '';
    }

    return $value[0]['format'];
  }

  /**
   * {@inheritdoc}
   */
  public function setDescription(string $text) {
    if (empty($text) === TRUE) {
      $text = '';
    }

    $format = $this->getDescriptionFormat();

    $this->description->setValue([
      'value'  => $text,
      'format' => $format,
    ]);

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setDescriptionFormat(string $format) {
    if (empty($format) === TRUE) {
      $format = '';
    }

    $text = $this->getDescription();

    $this->description->setValue([
      'value'  => $text,
      'format' => $format,
    ]);

    return $this;
  }

}
