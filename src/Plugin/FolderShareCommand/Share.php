<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

use Drupal\user\Entity\User;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareAccessControlHandler;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\UserUtilities;

/**
 * Defines a command plugin to change share grants on a root item.
 *
 * The command sets the access grants for the root item of the selected
 * entity. Access grants enable/disable view and author access for
 * individual users.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entity who's root item is shared.
 * - 'grants': the new access grants.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_share",
 *  label           = @Translation("Share"),
 *  menuNameDefault = @Translation("Share..."),
 *  menuName        = @Translation("Share..."),
 *  description     = @Translation("Share selected top-level files and folders, and their contents."),
 *  category        = "settings",
 *  weight          = 0,
 *  userConstraints = {
 *    "authenticated",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *    },
 *    "access"  = "view",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "share",
 *  },
 * )
 */
class Share extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Configuration.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    // Include room for the new grants in the configuration.
    $config = parent::defaultConfiguration();
    $config['grants'] = NULL;
    return $config;
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    return $this->t(
      '%view access enables other users to view, copy, and download. %author access enables other users to edit, delete, move, and upload too.',
      [
        '%view'   => $this->t('View'),
        '%author' => $this->t('Author'),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    $ids  = $this->getSelectionIds();
    $name = FolderShare::findNameForId($ids[0]);

    return $this->t(
      'Adjust shared access to "@name"',
      [
        '@name' => $name,
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Save');
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    //
    // Define form element names/classes.
    // ----------------------------------
    // Most form elements have specific names/classes. Names enable values
    // to be retreived easily. Classes enable custom styling.
    //
    // The user name field is *intentionally* not named using words like
    // "name" or "user" because some browsers use this name to decide whether
    // to show an autofill using a password manager to select an account name,
    // presuming that the form is for a login. By naming the field differently
    // we thwart a browser's guess that the field is for an account name,
    // and block an inappropriate password manager prompt. This is crude,
    // but necessary in 2019 to get around over-zelous browser attempts to
    // autofill form fields.
    $shareForm             = 'foldershare_share_form';
    $shareState            = 'foldershare_share_state';
    $shareFormWrapper      = 'foldershare_share_form_wrapper';
    $shareTable            = 'foldershare_share_table';
    $shareTableDescription = 'foldershare_share_table_description';
    $addUserForm           = 'foldershare_share_add_sharer_form';
    $addUserField          = 'foldershare_share_add_sharer';
    $addUserFieldAndButton = 'foldershare_share_add_sharer_and_button';
    $addUserButton         = 'foldershare_share_add_sharer_button';
    $addUserMessages       = 'foldershare_share_add_sharer_messages';

    $formInput = $formState->getUserInput();

    //
    // Disable inline form errors.
    // ---------------------------
    // When the core Inline Form Errors module is enabled, it attempts to
    // distributed error messages to immediately follow the form fields they
    // relate to. While this can be a good thing, for this dialog it causes
    // entered user name errors to be inserted below the user text field
    // (added below) and before the text field's explanation and its' "Add"
    // button. This is awkward. The Inline Form Errors module also adds a
    // redundant error message to the status messages section at the bottom
    // of the dialog.
    //
    // To disable this strangeness, the Inline Form Errors module's behavior
    // is diabled for the entire dialog.
    $form['#disable_inline_form_errors'] = TRUE;

    //
    // Disable auto-complete.
    // ----------------------
    // In principal, adding 'autocomplete="off"' to a form (and/or form
    // element) will disable browser autocomplete. In practice, modern
    // browsers ignore it and provide autocomplete on any text field.
    // If the text field is for a user name, then some browsers will invoke
    // password managers (e.g. Apple's keychain) to let you select a user
    // name.
    //
    // Nevertheless, mark the form with our intent that autocomplete NOT
    // be enabled anywhere on the form.
    $form['#attributes']['autocomplete'] = 'off';
    $form['#attributes']['class'][] = 'foldershare-share';
    $form['#attached']['library'][] = 'foldershare/foldershare.sharedialog';

    //
    // Get current user permissions.
    // -----------------------------
    // Does the current user have permission to share with other users
    // and/or share with the public?
    $hasShareWithUsersPermission = AccessResult::allowedIfHasPermission(
      $this->getCurrentUser(),
      Constants::SHARE_PERMISSION)->isAllowed();

    $hasShareWithPublicPermission = AccessResult::allowedIfHasPermission(
      $this->getCurrentUser(),
      Constants::SHARE_PUBLIC_PERMISSION)->isAllowed();

    // If the current user doesn't have permission to share content with
    // other users or with the public, then the share form should not have
    // been presented. Since it has been, show an error message.
    if ($hasShareWithUsersPermission === FALSE &&
        $hasShareWithPublicPermission === FALSE) {
      $form['nopermission'] = [
        '#type'  => 'html_tag',
        '#tag'   => 'p',
        '#value' => $this->t('You do not have permission to share files and folders with other users or the public.'),
      ];

      unset($form['actions']['submit']);

      return $form;
    }

    //
    // Get saved form grants.
    // ----------------------
    // On the first form build, get the list of grants from the selected
    // item. On subsequent calls, use a saved list that's been updated with
    // added and removed users.
    //
    // The grants list has UID keys and values that are arrays with one
    // or more of:
    //
    // '' = no grants.
    // 'view' = granted view access.
    // 'author' = granted author access.
    //
    // The returned array cannot be empty. It always contains at least
    // the owner of the folder, who always has 'view' and 'author' access.
    $selectionIds = $this->getSelectionIds();

    // Load the item who's sharing is being adjusted.
    $item = FolderShare::load(reset($selectionIds));

    // Get any previously saved grant state.
    if (empty($formInput[$shareState]) === TRUE) {
      // No previous grants. This is the first build of this form.
      // Get the item's current grants.
      $grants = $item->getAccessGrants();
    }
    else {
      // There are previous grants. The grants are saved in form state as
      // a JSON-encoded array.
      $grants = json_decode($formInput[$shareState], TRUE);
    }

    //
    // Get user info.
    // --------------
    // Load all users referenced in the grants. We need their display names
    // for the form's list of users. Add the users into an array keyed by
    // the display name, then sort on those keys so that we can create
    // a sorted list in the form.
    $loadedUsers = User::loadMultiple(array_keys($grants));
    $users = [];
    foreach ($loadedUsers as $user) {
      if ($user === NULL) {
        // User cannot be loaded. The item's grants list probably references
        // a user ID for an account that has been deleted. Silently ignore
        // the deleted user.
        continue;
      }
      $users[$user->getDisplayName()] = $user;
    }

    ksort($users, SORT_NATURAL);

    // Get the anonymous user.
    $anonymousUser = User::getAnonymousUser();

    // Get the user ID of the special primary site admin (which is always 1).
    // There can be additional site admins, and it is even possible to
    // delete the primary site admin (though that is not advised).
    $siteAdminId = 1;

    // Get the item's owner.
    $ownerId = $item->getOwnerId();
    $owner   = User::load($ownerId);

    //
    // Table of users and grants.
    // --------------------------
    // Create a table that has a first column showing user names, and a
    // second column showing access grants.
    $form[$shareForm] = [
      '#type'         => 'container',
      '#name'         => $shareForm,
      '#prefix'       => '<div id="' . $shareFormWrapper . '">',
      '#suffix'       => '</div>',
      '#attributes'      => [
        'class'          => [$shareForm],
      ],

      $shareTable     => [
        '#type'       => 'table',
        '#name'       => $shareTable,
        '#attributes' => [
          'class'     => [$shareTable],
        ],
        '#responsive' => FALSE,
        '#sticky'     => TRUE,
        '#header'     => [
          $this->t('User'),
          $this->t(
            '<span>@none</span><span>@view</span><span>@author</span>',
            [
              '@none'   => $this->t('None'),
              '@view'   => $this->t('View'),
              '@author' => $this->t('Author'),
            ]),
        ],
      ],
    ];

    $rows = [];
    $uids = [];

    //
    // Add row for anonymous.
    // ----------------------
    // If sharing with anonymous is allowed for this user, include a row
    // to do so at the top of the table.
    if ($hasShareWithPublicPermission === TRUE) {
      if (isset($grants[(int) $anonymousUser->id()]) === FALSE) {
        // There is no entry yet for anonymous. Create one, but with neither
        // view or author access.
        $grants[0] = [];
      }

      $r = $this->buildRow(
        $anonymousUser,
        $owner,
        $grants[(int) $anonymousUser->id()]);

      // buildRow() can return an empty array if the anonymous user has not
      // been given view permission for files and folders.
      if (empty($r) === FALSE) {
        $rows[] = $r;
      }
    }

    //
    // Add rows for users granted access.
    // ----------------------------------
    // If sharing with other users is allowed for this user, include a list
    // of users currently granted access.
    if ($hasShareWithUsersPermission === TRUE) {
      foreach ($users as $user) {
        $uid = (int) $user->id();

        // Do not add table rows for:
        // - Anonymous.
        // - Blocked accounts.
        // - The primary site admin.
        // - The file/folder owner.
        //
        // Anonymous access is already handled above and requires a separate
        // permission.
        //
        // Blocked accounts can be on a grants list if the account was blocked
        // recently. But once blocked, this form no longer shows the account
        // and the account is silently removed from the grants list when the
        // form is saved.
        //
        // The primary site admin always had access, so they don't have to be
        // on a share list. Adding them looks odd, so they are silently skipped.
        //
        // Other site admins also always have access, but they usually have
        // real user accounts and other users at the site may or may not
        // know that those users are actually admins. Rather than exclude
        // them from the grants list, just include them even though as admins
        // they have access anway.
        //
        // The owner always has access to their content, so they don't need
        // to be listed here.
        if ($user->isAnonymous() === TRUE ||
            $user->isBlocked() === TRUE ||
            $uid === $siteAdminId ||
            $uid === $ownerId) {
          continue;
        }

        // Add the row.
        $r = $this->buildRow(
          $user,
          $owner,
          $grants[$uid]);

        if (empty($r) === FALSE) {
          $rows[] = $r;
          $uids[] = $uid;
        }
      }
    }

    // If the above code did not add any rows, then there is currently no one
    // granted shared access. Say so.
    //
    // If the above code added rows, excluding anonymous, then add a table
    // description.
    if (count($rows) <= 0) {
      $rows[] = [
        'User'                  => [
          '#markup'             => $this->t('No users have shared access.'),
          '#wrapper_attributes' => [
            'colspan'           => 2,
          ],
        ],
      ];
    }
    elseif ($hasShareWithUsersPermission === TRUE) {
      // Add usage note below the table.
      $form[$shareForm]['description'] = [
        '#type'         => 'html_tag',
        '#tag'          => 'p',
        '#value'        => $this->t(
          'Users without %view or %author access are automatically removed from this list when the form is saved.',
          [
            '%view'     => $this->t('View'),
            '%author'   => $this->t('Author'),
          ]),
        '#attributes'   => [
          'class'       => [
            'description',
            $shareTableDescription,
          ],
        ],
      ];
    }

    // And add the rows to the table.
    if (count($rows) != 0) {
      $form[$shareForm][$shareTable] = array_merge(
        $form[$shareForm][$shareTable],
        $rows);
    }

    //
    // Add user autocomplete field and button.
    // ---------------------------------------
    // If sharing with other users is allowed for this user, include an
    // 'Add a user' autocomplete field used to add users to the share list.
    if ($hasShareWithUsersPermission === TRUE) {
      // If the text field is empty, the 'Add' user button is disabled by
      // Javascript attached to the form.
      $routeParameters =
        $form['actions']['submit']['#ajax']['url']->getRouteParameters()['encoded'];

      $title = '';
      switch (Settings::getUserAutocompleteStyle()) {
        default:
        case 'none':
        case 'name-only':
          $title = $this->t('User account name:');
          break;

        case 'name-email':
        case 'name-masked-email':
          $title = $this->t('User account name or email address:');
          break;
      }

      $defaultUser = '';
      if (isset($formInput[$addUserField]) === TRUE) {
        $defaultUser = $formInput[$addUserField];
      }

      $form[$shareForm][$addUserForm] = [
        '#type'               => 'container',
        '#tree'               => TRUE,
        '#name'               => $addUserForm,
        '#attributes'         => [
          'class'             => [$addUserForm],
        ],
        $addUserFieldAndButton => [
          '#type'             => 'container',
          '#attributes'       => [
            'class'           => [$addUserFieldAndButton],
          ],
          $addUserField       => [
            '#type'           => 'textfield',
            '#title'            => $title,
            '#maxlength'      => 256,
            '#default_value'  => $defaultUser,
            '#required'       => FALSE,
            '#name'           => $addUserField,
            '#attributes'     => [
              'class'         => [$addUserField],
              // Most browsers, but not iOS Safari. Move focus to text field.
              'autofocus'    => 'on',
              // No browsers, but they should disable autocomplete.
              'autocomplete' => 'off',
              // Most browsers. Disables automatic capitalization.
              'autocapitalize' => 'none',
              // Most browsers. Disables spell check.
              'spellcheck'   => 'false',
              // Safari only. Disables spell check.
              'autocorrect'  => 'off',
              // Firefox for Android only. Names 'Enter' key on virtual keybd.
              'mozactionhint' => $this->t('Add'),
            ],
          ],
          $addUserButton => [
            '#type'           => 'button',
            '#button_type'    => 'secondary',
            '#value'          => $this->t('Add'),
            '#name'           => $addUserButton,
            '#attributes'     => [
              'class'         => [$addUserButton],
            ],
            '#ajax'           => [
              'callback'      => [$this, 'refreshConfigurationForm'],
              'event'         => 'click',
              'wrapper'       => $shareFormWrapper,
              'url'           => Url::fromRoute(
                'entity.foldersharecommand.plugin',
                [
                  'encoded'   => $routeParameters,
                ]),
              'options'       => [
                'query'       => [
                  'ajax_form' => 1,
                ],
              ],
            ],
          ],
        ],

        // Show error messages related to this form at the bottom of the dialog.
        $addUserMessages      => [
          '#type'             => 'status_messages',
        ],
      ];

      // If the site allows user autocomplete, set up the text field. Include
      // a list of users culled to skip:
      // - All users already with shared access.
      // - The current user, who always has access.
      // - Anonymous, who has a special row above.
      // - The primary site admin, who always has access.
      $autocompleteStyle = Settings::getUserAutocompleteStyle();

      if ($autocompleteStyle !== 'none') {
        $excludeUids = $uids;
        $excludeUids[] = (int) $this->getCurrentUser()->id();
        $excludeUids[] = (int) $anonymousUser->id();
        $excludeUids[] = $siteAdminId;

        $form[$shareForm][$addUserForm][$addUserFieldAndButton][$addUserField]['#autocomplete_route_name'] =
          'entity.foldershare.userautocomplete';

        $form[$shareForm][$addUserForm][$addUserFieldAndButton][$addUserField]['#autocomplete_route_parameters'] = [
          'excludeUids'    => $excludeUids,
          'excludeBlocked' => 1,
        ];
      }
    }

    //
    // Add saved grants.
    // -----------------
    // Save the current share grants into a hidden form field. The field's
    // value is picked up on the next build to initialize the table.
    $form[$shareForm][$shareState] = [
      '#type'  => 'hidden',
      '#value' => json_encode($grants),
      '#name'  => $shareState,
    ];

    //
    // Handle messages.
    // ----------------
    // This form is intended to change sharing. This is not the place to
    // report warnings and status messages. Only those messages relevant to
    // this form should be shown.
    //
    // So, delete pending status and warning messages. If we don't do this,
    // Drupal's form processing automatically adds them to the top of the form.
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_STATUS);
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_WARNING);

    return $form;
  }

  /**
   * Builds and returns a form row for a user.
   *
   * This method is called to create a row for a user in the share table.
   * Each row has three columns:
   * - User name.
   * - Access choice.
   * - Actions.
   *
   * The user name is the account's display name. The field value is the
   * user's ID.
   *
   * The access choice is a pair of radio buttons for "view" and "access".
   *
   * If the given user does not have appropriate module permissions, they
   * cannot have either "view" or "access" and an empty array is returned.
   *
   * @param \Drupal\user\Entity\User $user
   *   The user for whom to build the row.
   * @param \Drupal\user\Entity\User $owner
   *   The owner of the current root item.
   * @param array $grant
   *   The access grants for the user.
   *
   * @return array
   *   The form table row description for the user, or an empty row if the
   *   user cannot be granted access because their account is blocked, they
   *   don't have module permissions, or they are the item's owner.
   */
  protected function buildRow(
    User $user,
    User $owner,
    array $grant) {

    //
    // Get account attributes.
    // -----------------------
    // Watch for special users.
    $rowIsForAnonymous   = $user->isAnonymous();
    $rowIsForBlockedUser = $user->isBlocked();
    $rowIsForRootOwner   = ((int) $user->id() === (int) $owner->id());

    if ($rowIsForBlockedUser === TRUE && $rowIsForAnonymous === FALSE) {
      // Blocked non-anonymous users cannot be granted shared access.
      // Do not list them.
      return [];
    }

    if ($rowIsForRootOwner === TRUE) {
      // The owner always has access. Do not list them.
      return [];
    }

    //
    // Check row user permissions.
    // ---------------------------
    // Get what the row user is permitted to do based on permissions alone.
    $permittedToView =
      FolderShareAccessControlHandler::mayAccess('view', $user);
    $permittedToAuthor =
      FolderShareAccessControlHandler::mayAccess('update', $user);

    if ($permittedToView === FALSE) {
      // The user has not been given view access for the module's content.
      // They cannot be granted shared access then.
      return [];
    }

    //
    // Get grants for this root.
    // -------------------------
    // Check the access grants on the root item and see what the row user
    // has been granted to do, if anything.
    $currentlyGrantedView = in_array('view', $grant);
    $currentlyGrantedAuthor = in_array('author', $grant);

    // If the user is explicitly granted author access, then automatically
    // include view access.
    if ($currentlyGrantedAuthor === TRUE) {
      $currentlyGrantedView = TRUE;
    }

    if ($permittedToAuthor === FALSE) {
      // The row user doesn't have author permission, so any author grant
      // they have on the row's root is irrelevant.
      $currentlyGrantedAuthor = FALSE;
    }

    //
    // Build row
    // ---------
    // Start by creating the radio button options array based upon the grants.
    $radios = ['none' => ''];
    $default = 'none';

    if ($permittedToView === TRUE) {
      // User has view permissions, so allow a 'view' choice.
      $radios['view'] = '';
    }

    if ($permittedToAuthor === TRUE) {
      // User has author permissions, so allow a 'author' choice.
      $radios['author'] = '';
    }

    if ($currentlyGrantedView === TRUE) {
      // User has been granted view access.
      $default = 'view';
    }

    if ($currentlyGrantedAuthor === TRUE) {
      // User has been granted author access (which for our purposes
      // includes view access).
      $default = 'author';
    }

    // Create an annotated user name.
    $name = $user->getDisplayName();
    if ($rowIsForAnonymous === TRUE) {
      $nameMarkup = $this->t('Everyone that can access this website');
    }
    else {
      $nameMarkup = $this->t(
        '@name',
        [
          '@name' => $name,
        ]);
    }

    // Create the row. Provide the user's UID as the row value, which we'll
    // use later during validation and submit handling. Show a link to the
    // user's profile.
    $shareTableRow       = 'foldershare_share_table_row';
    $shareTableRowUser   = 'foldershare_share_table_row_user';
    $shareTableRowAccess = 'foldershare_share_table_row_access';

    return [
      'User' => [
        '#type'          => 'item',
        '#value'         => $user->id(),
        '#markup'        => $nameMarkup,
        '#attributes'    => [
          'class'        => [$shareTableRowUser],
        ],
      ],

      // Disable the row if the user has no permissions.
      'Access' => [
        '#type'          => 'radios',
        '#options'       => $radios,
        '#default_value' => $default,
        '#attributes'    => [
          'class'        => [$shareTableRowAccess],
        ],
      ],

      '#attributes'      => [
        'class'          => [$shareTableRow],
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    //
    // Define form element names/classes.
    // ----------------------------------
    // Most form elements have specific names/classes. Names enable values
    // to be retreived easily.
    $shareForm             = 'foldershare_share_form';
    $shareState            = 'foldershare_share_state';
    $shareTable            = 'foldershare_share_table';
    $addUserForm           = 'foldershare_share_add_sharer_form';
    $addUserFieldAndButton = 'foldershare_share_add_sharer_and_button';
    $addUserField          = 'foldershare_share_add_sharer';
    $addUserButton         = 'foldershare_share_add_sharer_button';

    $addUserErrors =
      "$shareForm][$addUserForm][$addUserFieldAndButton][$addUserField";

    //
    // Setup.
    // ------
    // Get user input.
    $trigger = $formState->getTriggeringElement();
    $formInput = $formState->getUserInput();

    // Get the selection, which is always a single root list item.
    $selectionIds = $this->getSelectionIds();
    $item = FolderShare::load(reset($selectionIds));
    $ownerId = $item->getOwnerId();

    //
    // Determine reason validation was called.
    // ---------------------------------------
    // Validation is called on a 'Save' button click to submit the form,
    // or on an 'Add' button click to add a new user.
    $formIsForAdd = FALSE;
    if ($trigger['#name'] === $addUserButton) {
      $formIsForAdd = TRUE;
    }

    //
    // Validate current grants.
    // ------------------------
    // Whether or not the user has clicked 'Save' to submit changes to
    // the grant list, we still need to validate them and save them into
    // the form's configuration.
    //
    // Get the form's grants. These may differ from the item's original
    // grants in two ways:
    // - Added users will have an entry.
    // - Removed users will not have an entry.
    //
    // All entries that are not for added users will have the view/author
    // grants originally granted. Below we'll adjust those grants based upon
    // the radio buttons in the form.
    $encodedGrants = $formInput[$shareState];
    if (empty($encodedGrants) === TRUE) {
      $grants = [];
    }
    else {
      $grants = json_decode($encodedGrants, TRUE);
    }

    // Get the original grants too.
    $originalGrants = $item->getAccessGrants($item);

    // Copy the owner's own original grants forward. The owner always has
    // access and should not have been listed in the form.
    if (isset($originalGrants[$ownerId]) === TRUE) {
      $grants[$ownerId] = $originalGrants[$ownerId];
    }

    // Get the current list of users from the table.
    $entries = $formState->getValue([$shareForm, $shareTable]);

    if (empty($entries) === FALSE) {
      if ($formIsForAdd === TRUE) {
        // The form has been submitted by clicking 'Add' to add a new user.
        // The form's table of users and grants is not final yet, so it is
        // premature to validate it.
        foreach ($entries as $entry) {
          $uid = $entry['User'];
          switch ($entry['Access']) {
            case 'view':
              $grants[$uid] = ['view'];
              break;

            case 'author':
              $grants[$uid] = [
                'view',
                'author',
              ];
              break;

            case 'none':
              $grants[$uid] = [];
              break;

            default:
              break;
          }
        }
      }
      else {
        // Validate that the new grants make sense.
        //
        // Loop through the form's table of users and access grants. For each
        // user, see if 'none', 'view', or 'author' radio buttons are set
        // and create the associated grant in a user-grant array.
        foreach ($entries as $entry) {
          $uid = $entry['User'];

          // Ignore any row for the root item owner. This shouldn't have been in
          // the table in the first place.
          if ($uid === $ownerId) {
            continue;
          }

          // Ignore blocked accounts that aren't anonymous. Blocked accounts
          // should not have been in the table in the first place.
          $user = User::load($uid);
          if ($user === NULL ||
              ($user->isBlocked() === TRUE && $user->isAnonymous() === FALSE)) {
            continue;
          }

          switch ($entry['Access']) {
            case 'view':
              // Make sure the user has view permission.
              if (FolderShareAccessControlHandler::mayAccess('view', $user) === TRUE) {
                $grants[$uid] = ['view'];
              }
              break;

            case 'author':
              // Make sure the user has author permission.
              if (FolderShareAccessControlHandler::mayAccess('update', $user) === TRUE) {
                // Author grants ALWAYS include view grants.
                $grants[$uid] = [
                  'view',
                  'author',
                ];
              }
              break;

            case 'none':
              // Remove any user that has not been granted any permission.
              unset($grants[$uid]);
              break;

            default:
              break;
          }
        }
      }
    }

    // Save the updated grants back into the form and into the command's
    // configuration.
    $encodedGrants = json_encode($grants);
    $formInput[$shareState] = $encodedGrants;
    $formState->setValue([$shareForm, $shareState], $encodedGrants);
    $formState->setUserInput($formInput);
    $form[$shareForm][$shareState]['#value'] = $encodedGrants;

    $this->configuration['grants'] = $encodedGrants;

    //
    // Finish, if not adding a user.
    // -----------------------------
    // At this point, if we're not adding a user, the grants have been
    // updated to match the form. The user has clicked 'Save'. It is time
    // to validate everything to be sure.
    if ($formIsForAdd == FALSE) {
      try {
        $this->validateParameters();
      }
      catch (\Exception $e) {
        $formState->setErrorByName($shareTable, $e->getMessage());
        $formState->setRebuild();
      }

      return;
    }

    //
    // Handle add user.
    // ----------------
    // When the trigger is the add user button, get the entered text for
    // a user to add. The text can be an account name or email address.
    // Add the user to the grants list, then rebuild the form.
    //
    // Get the entered text for the user to add. While Javascript should have
    // insured that the 'Add' button could not be clicked if the name was
    // empty, the Javascript could have been bypassed.
    $name = $formInput[$addUserField];

    // Remove leading white space, if any.
    $cleanedName = mb_ereg_replace('^\s+', '', $name);
    if ($cleanedName !== FALSE) {
      $name = $cleanedName;
    }

    // Remove trailing white space, if any.
    $cleanedName = mb_ereg_replace('\s+$', '', $name);
    if ($cleanedName !== FALSE) {
      $name = $cleanedName;
    }

    // Clear the name field. If any of the validation checks below fail,
    // we set a validation error on the form and return. Since a validation
    // error does not rebuild the form, updating the form's state does not
    // work to insure the form name field is empty.
    $form[$shareForm][$addUserForm][$addUserFieldAndButton][$addUserField]['#value'] = '';

    // If the name left after trimming white space is empty, then there
    // is no user to add.
    if (empty($name) === TRUE) {
      // Empty user.
      $formState->setErrorByName(
        $addUserErrors,
        $this->t("Enter the name of a user to add to the share list."));
      return;
    }

    //
    // Look up the user.
    // -----------------
    // Use the given name to find the user. The field's autocomplete should
    // have already insured the name is an account name, but the autocomplete
    // can be bypassed.
    $uid = UserUtilities::findUser($name);
    if ($uid === -1) {
      // Unknown user.
      $formState->setErrorByName(
        $addUserErrors,
        $this->t(
          '"%name" is not a recognized account at this site.',
          [
            '%name' => $name,
          ]));

      // Update the name field to the cleaned name.
      $form[$shareForm][$addUserForm][$addUserFieldAndButton][$addUserField]['#value'] = $name;
      return;
    }

    //
    // Validate user.
    // --------------
    // The user was found. Now make sure the user is:
    // - Not the administrator.
    // - Not the owner.
    // - Not blocked.
    // - Has at least view permission on FolderShare content.
    // - Not already on the grants list.
    //
    // If any of the above are false, issue an error message.
    if ($uid === 1) {
      // Primary site admin.
      $formState->setErrorByName(
        $addUserErrors,
        $this->t(
          'The "%name" account is for the site administrator, who always has access for administrative purposes. They cannot be added to the list.',
          [
            '%name' => $name,
          ]));
      return;
    }

    if ((int) $item->getOwnerId() === $uid) {
      // Owner.
      if ((int) $this->getCurrentUser()->id() === $uid) {
        // The current user is the owner. Tell them they don't have to be
        // added to the list.
        $formState->setErrorByName(
          $addUserErrors,
          $this->t('As the owner, you always have access and do not need to be added to the list.'));
      }
      else {
        // The current user is not the owner, but has permission to change
        // sharing on the item. They are probably an administrator. Tell
        // them they don't need to add the owner to the list.
        $formState->setErrorByName(
          $addUserErrors,
          $this->t(
            'The "%name" user is the @kind owner, who always has access. They do not need to be added to the list.',
            [
              '%name' => $name,
              '@kind' => FolderShare::translateKind($item->getKind()),
            ]));
      }
      return;
    }

    $user = User::load($uid);
    if ($user->isBlocked() === TRUE && $user->isAnonymous() === FALSE) {
      // Blocked user that is not anonymous.
      $formState->setErrorByName(
        $addUserErrors,
        $this->t(
          'The "%name" account is disabled and cannot be added to the list.',
          [
            '%name' => $name,
          ]));
      return;
    }

    if (FolderShareAccessControlHandler::mayAccess('view', $user) === FALSE) {
      // User without view permission.
      if ($uid === 0) {
        // Anonymous.
        $formState->setErrorByName(
          $addUserErrors,
          $this->t('Public visitors to the web site do not have permission to use files and folders. They cannot be added to the list.'));
      }
      else {
        $formState->setErrorByName(
          $addUserErrors,
          $this->t(
            'The "%name" user does not have permission to use files and folders. They cannot be added to the list.',
            [
              '%name' => $name,
            ]));
      }
      return;
    }

    if (isset($grants[$uid]) === TRUE) {
      // User already on list.
      $formState->setErrorByName(
        $addUserErrors,
        $this->t(
          '"%name" is already on the list.',
          [
            '%name' => $name,
          ]));
      return;
    }

    //
    // Add user to pending grants.
    // ---------------------------
    // Add the new user's ID to the list of pending grants! Default the user
    // to no granted access. The form's user needs to explicitly grant them
    // view or author access on the rebuilt form.
    $grants[$uid] = [];

    $encodedGrants = json_encode($grants);
    $formInput[$shareState] = $encodedGrants;

    // Clear the current form's saved radio button state because the number
    // of table rows in that state no longer matches the number or order of
    // grants. If we don't reset the form's input, the wrong radio button
    // values will be assigned to the newly ordered users and we get a mess.
    unset($formInput[$shareForm][$shareTable]);

    // Clear the user name input.
    $formInput[$addUserField] = '';

    $formState->setUserInput($formInput);

    $formState->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    $shareState = 'foldershare_share_state';

    $formInput = $formState->getUserInput();
    $this->configuration['grants'] = $formInput[$shareState];

    $this->execute();
  }

  /**
   * Refreshes the configuration form after an AJAX request.
   *
   * @param array $form
   *   The configuration form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   Returns the share form portion of the form.
   */
  public function refreshConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    // Return the center part of the form, not the entire thing. There is
    // no need to update the action buttons.
    $shareForm = 'foldershare_share_form';
    return $form[$shareForm];
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    // Always a selection that is a root item.
    $selectionIds = $this->getSelectionIds();
    $item = FolderShare::load(reset($selectionIds));

    // Get grants from the configuration and update sharing.
    // This automatically saves the item.
    $grants = json_decode($this->configuration['grants'], TRUE);
    $item->share($grants);

    // Flush the render cache because sharing may have changed what
    // content is viewable throughout the folder tree under this root.
    Cache::invalidateTags(['rendered']);

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $this->getMessenger()->addStatus(
        $this->t("The shared access configuration has been updated."));
    }
  }

}
