/**
 * @file
 * Implements the FolderShare folder selection dialog user interface.
 *
 * A folder selection dialog is used by commands like "move" and "copy" to
 * select a destination folder. The dialog is a light version of a normal
 * file/folder list where all files in the list are disabled and only folders
 * may be selected.
 *
 * This script supports selecting folder rows in a dialog's file/folder list.
 * Rows can only be selected individually. Double-clicking a row opens the
 * row's folder in the dialog. Selecting a choice from an ancestor menu
 * opens a parent folder in the dialog.
 *
 * The dialog communicates with the server in three ways:
 * - Selecting an ancestor menu choice, clicking on a row link, or
 *   double-clicking on a row sets the hidden "parentid" field, which
 *   triggers an AJAX round-trip to the server that returns a new
 *   folder list.
 *
 * - Selecting a folder sets the hidden "currentselection" field, but does
 *   not trigger a form submit.
 *
 * - Selecting the form's submit button triggers an AJAX round-trip to the
 *   server that may close the dialog or update it in some way.
 *
 * This script requires HTML elements added by a table view that uses a name
 * field formatter that attaches attributes to name field anchors. This script
 * uses those attributes to guide prevalidation of menu items as appropriate
 * for selected rows.
 *
 * This script also requires an HTML form that provides:
 * - A field to hold the current selection.
 * - A field to hold the new parent.
 * - A form submit button.
 *
 * @ingroup foldershare
 * @see \Drupal\foldershare\Plugin\Field\FieldFormatter\FolderShareFolderOnlyName
 * @see \Drupal\foldershare\Form\CommandFormWrapper
 */
(($, Drupal) => {
  Drupal.foldershare.FolderSelectionDialog = {
    /*--------------------------------------------------------------------
     *
     * Initialize.
     *
     * The "environment" array created and updated by these functions
     * contains jQuery objects and assorted attributes gathered from the
     * page. Once gathered, these are passed among behavior functions so
     * that they can operate upon them without having to re-search for
     * them on the page.
     *
     *--------------------------------------------------------------------*/

    /**
     * Attaches the module's folder selection dialog UI behaviors.
     *
     * The UI includes
     * - Table row selection.
     *
     * All UI elements and related elements are found, validated,
     * and behaviors attached.
     *
     * @param {Document} pageContext
     *   The page context for this call. Initially, this is the full document.
     *   Later, this is only portions of the document added via AJAX.
     * @param {object} settings
     *   The top-level Drupal settings object.
     *
     * @return {boolean}
     *   Always returns true.
     */
    attach(pageContext, settings) {
      const thisScript = Drupal.foldershare.FolderSelectionDialog;

      //
      // Test and exit
      // -------------
      // This method is called very frequently. It is called at least once
      // when the document is ready, and then again every time AJAX adds
      // anything to the page for any reason. This includes additions that
      // have nothing to do with this module.
      //
      // It is therefore important that this method decide quickly if the
      // context is not relevant for it, then return.
      const featureClass = "foldershare-folder-selection";
      const featureSelector = `.${featureClass}`;
      let $featureRoots;

      // The page context can be any of:
      // - The full document.
      // - The feature root we want.
      // - An element above the feature root we want.
      // - An element below the feature root we want.
      if (typeof pageContext.tagName === "undefined") {
        // The full document. Search down.
        $featureRoots = $(featureSelector, pageContext);
      } else if ($(pageContext).hasClass(featureClass) === true) {
        // Feature root. No search needed.
        $featureRoots = $(pageContext);
      } else {
        // Above or below. Search up first, then down.
        $featureRoots = $(pageContext).parents(featureSelector);
        if ($featureRoots.length === 0) {
          $featureRoots = $(featureSelector, pageContext);
        }
      }

      if ($featureRoots.length === 0) {
        // Fail. The document does not contain the feature root.
        return true;
      }

      //
      // Process top elements
      // --------------------
      // Process each top element. There should be only one.
      $featureRoots.each((index, element) => {
        // Create a new environment object.
        const env = {
          settings: settings,
          $featureRoot: $(element)
        };

        // Find form elements, and the table of files and folders.
        if (thisScript.gather(env) === false) {
          // Fail. UI elements could not be found.
          return true;
        }

        // Build the UI and attach its behaviors.
        if (thisScript.build(env) === false) {
          // Fail. Something didn't work
          return true;
        }

      });

      return true;
    },

    /**
     * Gathers UI elements.
     *
     * Pages that add the UI place it within top element <div>
     * for the UI. Nested within is a <form> that contains the UI's
     * elements. The principal elements are:
     * - An input field for the parent ID to trigger an AJAX reload.
     * - An input field for the current folder selection.
     * - An <input> to submit the command form.
     *
     * This function searches for the UI's elements and saves them
     * into the environment:
     * - env.gather.$table = the <table> containing the file/folder list.
     * - env.gather.$destinationIdInput = the destination ID <input>.
     * - env.gather.$refreshButton = the destination view refresh button.
     * - env.gather.$selectionIdInput = the selection ID <input>.
     * - env.gather.nameColumn = the table column name for the name & attrib.
     * - env.gather.$ancestorMenu = the ancestor menu.
     *
     * @param {object} env
     *   The environment object containing saved object references for
     *   elements to operate upon. The object is updated on success.
     *
     * @return {boolean}
     *   Returns TRUE on success and FALSE otherwise.
     */
    gather(env) {
      //
      // Find dialog parent
      // ------------------
      // Search up for the parent dialog. At the time we attach behaviors,
      // the dialog has not yet been parented to a <div> marked as a dialog.
      // It is a direct child of <body> with content contained in a
      // <div> with class "ui-front". When displayed, a few parent <div>s
      // are created and the action buttons copied to a dialog button area.
      const $dialog = env.$featureRoot.closest(".ui-front").eq(0);
      if ($dialog.length === 0) {
        // Class not found.
        return false;
      }

      //
      // Find inputs
      // -----------
      // The form contains several <input> items used to hold information
      // from the UI operation:
      // - A selection ID <input>.
      // - A destination ID <input>.
      // - A destination refresh <button> or <input>.
      const $selectionIdInput = $(
        'input[name="selectionid"]', env.$featureRoot)
      .eq(0);
      if ($selectionIdInput.length === 0) {
        Drupal.foldershare.utility.printMalformedError(
          "The UI selection ID field is missing."
        );
        return false;
      }

      const $destinationIdInput = $(
        'input[name="destinationid"]', env.$featureRoot)
      .eq(0);
      if ($destinationIdInput.length === 0) {
        Drupal.foldershare.utility.printMalformedError(
          "The UI destination ID field is missing."
        );
        return false;
      }

      let $refreshButton = $('input[name="refresh"]', env.$featureRoot).eq(0);
      if ($refreshButton.length === 0) {
        $refreshButton = $('button[name="refresh"]', env.$featureRoot).eq(0);
        if ($refreshButton.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI refresh button is missing."
          );
          return false;
        }
      }

      //
      // Find table
      // ----------
      // Search for a views <table>.
      //
      // For themes that extend the Drupal core themes (e.g. Bartik),
      // views tables are marked with the class "views-table". But for
      // themes that don't extend core themes (e.g. Bootstrap-based or
      // W3.CSS-based), no such class is included.
      //
      // There should not be any other table in the area, so we have to
      // fall back to looking for a table without a specific class.
      //
      // Bootstrap fix.
      // W3CSS fix.
      const $table = $("table", env.$featureRoot).eq(0);
      if ($table.length === 0) {
        Drupal.foldershare.utility.printMalformedError(
          "The required views <table> could not be found."
        );
        return false;
      }

      //
      // Find ancestor menu
      // ------------------
      // Search for the ancestor menu on the toolbar. It may not be present.
      let $ancestorMenu = $(
        ".foldershare-ancestormenu-menu",
        env.$featureRoot)
      .eq(0);
      if ($ancestorMenu.length === 0) {
        $ancestorMenu = null;
      }

      //
      // Update environment
      // ------------------
      // Save main UI objects.
      env.gather = {
        $dialog: $dialog,
        $table: $table,
        $tbody: $table.find("tbody"),
        $thead: $table.find("thead"),
        nameColumn: "views-field-name",
        $selectionIdInput: $selectionIdInput,
        $destinationIdInput: $destinationIdInput,
        $refreshButton: $refreshButton,
        $ancestorMenu: $ancestorMenu
      };

      return true;
    },

    /*--------------------------------------------------------------------
     *
     * Build the UI.
     *
     *--------------------------------------------------------------------*/

    /**
     * Builds the UI.
     *
     * The main UI has several features:
     * - Selectable rows in the view table.
     *
     * @param {object} env
     *   The environment object.
     *
     * @return {boolean}
     *   Returns TRUE on success and FALSE otherwise.
     */
    build(env) {
      const thisScript = Drupal.foldershare.FolderSelectionDialog;
      const pageDisabled = env.settings.foldershare.page.disabled;

      //
      // Add table behaviors
      // -------------------
      // Add table and table row behaviors, such as for row selection.
      if (pageDisabled !== true) {
        thisScript.tableAttachBehaviors(env);
      }

      //
      // Refresh view on ancestor menu anchor click.
      // -------------------------------------------
      // For each ancestor menu item, add a click behavior that overrides
      // the anchor default behavior and refreshes the view to show the
      // clicked item's folder content.
      if (env.gather.$ancestorMenu !== null) {
        const $menu = env.gather.$ancestorMenu.menu();
        $menu.off("menuselect.foldershare");
        $menu.on("menuselect.foldershare", (ev, ui) => {
          $menu.menu().hide();
          const id = $(ui.item).attr("data-foldershare-id");
          if (typeof id !== "undefined") {
            env.gather.$destinationIdInput.val(id);
            env.gather.$refreshButton.click();
          }

          ev.preventDefault();
          return false;
        });
      }

      return true;
    },

    /*--------------------------------------------------------------------
     *
     * Table behaviors - overview.
     *
     * These functions manage interaction behaviors on the dialog's
     * file & folder table.
     *
     * Selection.
     * ----------
     * Selection marks a row as a potential folder destination.
     *
     * Selection is indicated by giving a row the "selected" class. CSS
     * uses the "selected" class to highlight a selected row.
     *
     * - On left mouse click or touch, the row is selected/unselected.
     *
     * Open.
     * -----
     * Opening a row refreshes the list to show the contents of the opened
     * folder.
     *
     * - On mouse double-click, open the row's item.
     *
     *--------------------------------------------------------------------*/

    /**
     * Attaches behaviors to the table.
     *
     * This method attaches row behaviors to support row selection
     * using mouse and touch events.
     *
     * @param {object} env
     *   The environment object.
     */
    tableAttachBehaviors(env) {
      const thisScript = Drupal.foldershare.FolderSelectionDialog;

      //
      // Refresh view on row double-click.
      // ---------------------------------
      // For each body row, add a double-click behavior that refreshes the
      // view to show the double-clicked row's folder content.
      $("tr", env.gather.$tbody).off("dblclick.foldershare");
      $("tr", env.gather.$tbody).on("dblclick.foldershare", function(e) {
        return thisScript.tableDoubleClick.call(this, e, env);
      });

      //
      // Refresh view on file/folder name anchor click.
      // ----------------------------------------------
      // For each body row, add a click behavior that overrides the anchor
      // default behavior and refreshes the view to show the clicked row's
      // folder content.
      const sel = `tr td.${env.gather.nameColumn} a`;
      $(sel, env.gather.$tbody).off("click.foldershare");
      $(sel, env.gather.$tbody).on("click.foldershare", function(e) {
        return thisScript.anchorClick.call(this, e, env);
      });

      //
      // Select on row click or touch.
      // -----------------------------
      // For each body row, add behaviors that respond to mouse clicks and
      // touch screen touches.
      $("tr", env.gather.$tbody)
        .once("row-click")
        .on("click.foldershare", function(e) {
          return thisScript.tableSelect.call(this, e, env);
        });

      $("tr", env.gather.$tbody)
        .once("row-touch")
        .on("touchend.foldershare", function(e) {
          return thisScript.tableSelect.call(this, e, env);
        });

      //
      // Unselect all on page background.
      // --------------------------------
      // On a click on a blank part of the dialog, clear the selection.
      env.gather.$dialog.on("click.foldershare_table", () => {
        thisScript.tableUnselectAll.call(this, env);
        return true;
      });
    },

    /*--------------------------------------------------------------------
     *
     * Table behaviors - select.
     *
     *--------------------------------------------------------------------*/

    /**
     * Handles a double-click on a table row.
     *
     * A double-click sets the dialog form's destination ID field and triggers
     * a refresh of the view.
     *
     * @param {object} ev
     *   The row event to handle.
     * @param {object} env
     *   The environment object.
     *
     * @return {boolean}
     *   Always returns false.
     */
    tableDoubleClick(ev, env) {
      const $tr = $(this);

      // Out of range row indexes cannot be selected.
      if (this.rowIndex <= 0) {
        ev.preventDefault();
        return;
      }

      // Rows without an anchor cannot be selected.
      const $tdLinkName = $(`td.${env.gather.nameColumn} a`, $tr);
      if ($tdLinkName.length === 0) {
        ev.preventDefault();
        return false;
      }

      // Disabled rows cannot be selected.
      const disabled = $tdLinkName.attr("data-foldershare-disabled");
      if (typeof disabled !== "undefined" && disabled === true) {
        ev.preventDefault();
        return false;
      }

      // Rows without an entity ID cannot be selected.
      const entityId = $tdLinkName.attr("data-foldershare-id");
      if (typeof entityId === "undefined") {
        ev.preventDefault();
        return false;
      }

      // If the row is not selected, clear the prior selection and select it.
      if ($tr.hasClass("selected") === false) {
        // Clear the prior selection, if any.
        const priorSelectedId = env.gather.$selectionIdInput.val();
        if (priorSelectedId !== -1) {
          env.gather.$selectionIdInput.val(-1);
          const $priorTd = $(
            `[data-foldershare-id="${priorSelectedId}"]`,
            env.gather.$tbody
          );
          if ($priorTd.length > 0) {
            $priorTd.closest("tr").toggleClass("selected", false);
          }
        }

        // Select it.
        $tr.toggleClass("selected", true);
        env.gather.$selectionIdInput.val(entityId);
      }

      // A click can sometimes cause a text selection if the mouse
      // moved a little between mouse down and up. Such a text
      // selection is meaningless here, so disable it.
      window.getSelection().removeAllRanges();

      // Push the ID into the destination ID field. This triggers an AJAX
      // request to the server that replaces the current dialog body.
      env.gather.$destinationIdInput.val(entityId);
      env.gather.$refreshButton.click();
      ev.preventDefault();
      return false;
    },

    /**
     * Handles a click on a file/folder name anchor on a table row.
     *
     * A click sets the dialog form's destination ID field and triggers
     * a refresh of the view to open that folder.
     *
     * @param {object} ev
     *   The row event to handle.
     * @param {object} env
     *   The environment object.
     *
     * @return {boolean}
     *   Always returns false.
     */
    anchorClick(ev, env) {
      const $a = $(this);

      // Disabled rows cannot be opened.
      const disabled = $a.attr("data-foldershare-disabled");
      if (typeof disabled !== "undefined" && disabled === true) {
        ev.preventDefault();
        return false;
      }

      // Rows without an entity ID cannot be opened.
      const entityId = $a.attr("data-foldershare-id");
      if (typeof entityId === "undefined") {
        ev.preventDefault();
        return false;
      }

      // If the row is not selected, clear the prior selection and select it.
      const $tr = $a.closest("tr");
      if ($tr.length > 0 && $tr.hasClass("selected") === false) {
        // Clear the prior selection, if any.
        const priorSelectedId = env.gather.$selectionIdInput.val();
        if (priorSelectedId !== -1) {
          env.gather.$selectionIdInput.val(-1);
          const $priorTd = $(
            `[data-foldershare-id="${priorSelectedId}"]`,
            env.gather.$tbody
          );
          if ($priorTd.length > 0) {
            $priorTd.closest("tr").toggleClass("selected", false);
          }
        }

        // Select it.
        $tr.toggleClass("selected", true);
        env.gather.$selectionIdInput.val(entityId);
      }

      // Push the ID into the destination ID field. This triggers an AJAX
      // request to the server that replaces the current dialog body.
      env.gather.$destinationIdInput.val(entityId);
      env.gather.$refreshButton.click();
      ev.preventDefault();
      return false;
    },

    /**
     * Handles a touch or mouse selection event on a table row.
     *
     * At most a single row may be selected.
     * - Clicking on a row when there is no selection, selects the row.
     * - Clicking on a row when there is already a selection, unselects
     *   the previous selection and selects the new row.
     * - Clicking a second time on a selected row does nothing.
     * - Clicking outside of the table unselects everything.
     * - Clicking on a disabled row clears the selection but does not
     *   select the disabled row.
     *
     * @param {object} ev
     *   The row event to handle.
     * @param {object} env
     *   The environment object.
     *
     * @return {boolean}
     *   Always returns false.
     */
    tableSelect(ev, env) {
      const $tr = $(this);

      // If the row is already selected, do nothing.
      if ($tr.hasClass("selected") === true) {
        ev.preventDefault();
        return false;
      }

      // Clear the prior selection, if any.
      const priorSelectedId = env.gather.$selectionIdInput.val();
      if (priorSelectedId !== -1) {
        env.gather.$selectionIdInput.val(-1);
        const $priorTd = $(
          `[data-foldershare-id="${priorSelectedId}"]`,
          env.gather.$tbody
        );
        if ($priorTd.length > 0) {
          $priorTd.closest("tr").toggleClass("selected", false);
        }
      }

      // Out of range row indexes cannot be selected.
      if (this.rowIndex <= 0) {
        ev.preventDefault();
        return false;
      }

      // Row indexes without an anchor cannot be selected.
      const $tdLinkName = $(`td.${env.gather.nameColumn} a`, $tr);
      if ($tdLinkName.length === 0) {
        ev.preventDefault();
        return false;
      }

      // Disabled rows cannot be selected.
      const disabled = $tdLinkName.attr("data-foldershare-disabled");
      if (typeof disabled !== "undefined" && disabled === true) {
        ev.preventDefault();
        return false;
      }

      // Rows without entity IDs cannot be selected.
      const entityId = $tdLinkName.attr("data-foldershare-id");
      if (typeof entityId === "undefined") {
        // Fail. ID is missing. Ignore the row.
        ev.preventDefault();
        return false;
      }

      // Select the row.
      $tr.toggleClass("selected", true);
      env.gather.$selectionIdInput.val(entityId);
      ev.preventDefault();

      // A click can sometimes cause a text selection if the mouse
      // moved a little between mouse down and up. Such a text
      // selection is meaningless here, so disable it.
      window.getSelection().removeAllRanges();
      return false;
    },

    /**
     * Unselects everything.
     *
     * @param {object} env
     *   the environment object.
     */
    tableUnselectAll(env) {
      env.gather.$selectionIdInput.val(-1);
      $("tr.selected", env.gather.$tbody).removeClass("selected");
    }
  };

  /*--------------------------------------------------------------------
   *
   * On Drupal ready behaviors.
   *
   * Set up behaviors to execute when the page is fully loaded, or whenever
   * AJAX sends a new page fragment.
   *
   *--------------------------------------------------------------------*/

  Drupal.behaviors.foldershare_FolderSelectionDialog = {
    attach(pageContext, settings) {
      Drupal.foldershare.FolderSelectionDialog.attach(pageContext, settings);
    }
  };
})(jQuery, Drupal);
