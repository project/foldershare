<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Entity\Exception\RuntimeExceptionWithMarkup;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Settings;

/**
 * Defines a command plugin to archive (compress) files and folders.
 *
 * The command creates a new ZIP archive containing all of the selected
 * files and folders and adds the archive to current folder.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to Archive.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_archive",
 *  label           = @Translation("Compress"),
 *  menuNameDefault = @Translation("Compress"),
 *  menuName        = @Translation("Compress"),
 *  description     = @Translation("Compress selected files and folders into a new ZIP archive. (This command is in development and not recommended for production sites.)"),
 *  category        = "archive",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "file",
 *      "image",
 *      "folder",
 *    },
 *    "access"  = "view",
 *  },
 * )
 */
class Compress extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {

    // Consolidate the selection into a single entity list.
    $children     = [];
    $selectionIds = $this->getSelectionIds();
    $firstItem    = NULL;

    foreach ($selectionIds as $id) {
      $item = FolderShare::load($id);
      if ($item === NULL) {
        // The item does not exist.
        continue;
      }

      $children[] = $item;
      if ($firstItem === NULL) {
        $firstItem = $item;
      }
    }

    $nItems = count($children);
    $archive = NULL;

    // Create an archive.
    try {
      $parent = $this->getParent();
      if ($parent === NULL) {
        $archive = FolderShare::archiveToRoot($children);
      }
      else {
        $archive = $parent->archiveToFolder($children);
        unset($parent);
      }
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $this->getMessenger()->addError($e->getMarkup(), TRUE);
      return;
    }
    catch (\Exception $e) {
      $this->getMessenger()->addError($e->getMessage());
      return;
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      if ($nItems === 1) {
        // One item. Refer to it by name.
        $this->getMessenger()->addStatus(
          $this->t(
            "The @kind '@name' has been compressed and saved into the new '@archive' file.",
            [
              '@kind'    => FolderShare::translateKind($firstItem->getKind()),
              '@name'    => $firstItem->getName(),
              '@archive' => $archive->getName(),
            ]));
      }
      elseif (count($children) === 1) {
        // One kind of items. Refer to them by kind.
        $this->getMessenger()->addStatus(
          $this->t(
            "@number @kinds have been compressed and saved into the new '@archive' file.",
            [
              '@number'  => $nItems,
              '@kinds'   => FolderShare::translateKinds($firstItem->getKind()),
              '@archive' => $archive->getName(),
            ]));
      }
      else {
        // Multiple kinds of items.
        $this->getMessenger()->addStatus(
          $this->t(
            "@number @kinds have been compressed and saved into the new '@archive' file.",
            [
              '@number'  => $nItems,
              '@kinds'   => FolderShare::translateKinds('items'),
              '@archive' => $archive->getName(),
            ]));
      }
    }
  }

}
