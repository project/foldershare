<?php

namespace Drupal\foldershare;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\user\UserInterface;

/**
 * Manages a hierarchy of folders, subfolders, and files.
 *
 * Implementations of this class support operations on folders and files
 * within folders. Operations include create, delete, move, copy, rename,
 * and changes to specific fields, such as names, descriptions, dates,
 * and owners.
 *
 * Object instances represent top-level root folders, subfolders, and specific
 * entries in the folders, such as files.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\foldershare\Entity\FolderShare
 */
interface FolderShareInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /*---------------------------------------------------------------------
   *
   * Constants - Entity type id.
   *
   *---------------------------------------------------------------------*/
  /**
   * The entity type id for the FolderShare entity.
   *
   * This is 'foldershare' and it must match the entity type declaration
   * in this class's comment block.
   *
   * @var string
   */
  const ENTITY_TYPE_ID = 'foldershare';

  /*---------------------------------------------------------------------
   *
   * Constants - Database tables.
   *
   *---------------------------------------------------------------------*/
  /**
   * The base table for 'foldershare' entities.
   *
   * This is 'foldershare' and it must match the base table declaration in
   * this class's comment block.
   *
   * @var string
   */
  const BASE_TABLE = 'foldershare';

  /*---------------------------------------------------------------------
   *
   * Constants - Lock constants.
   *
   *---------------------------------------------------------------------*/
  /**
   * The base name of the process lock for individual content operations.
   *
   * The content lock appends the ID of the entity being locked.
   *
   * @var string
   */
  const CONTENT_LOCK_NAME = 'foldershare_content_lock_';

  /**
   * The base name of the process lock for large-scale operations.
   *
   * The operation lock appends the ID of the root entity for the folder tree
   * being locked.
   *
   * @var string
   */
  const OPERATION_LOCK_NAME = 'foldershare_operation_lock_';

  /*---------------------------------------------------------------------
   *
   * Constants - Misc.
   *
   *---------------------------------------------------------------------*/
  /**
   * The number of operations to complete before checking usage limits.
   *
   * Memory and execution time limits need to be checked periodically
   * during long-running operations, such as copy, delete, and move.
   * Checking them after every item copied, deleted, or moved may be
   * expensive. Instead, checks are performed ever USAGE_CHECK_INTERVAL
   * loads/saves/copies/deletes.
   *
   * @var int
   *
   * @todo This may not be necessary. Benchmarking has found that checking
   * memory use and execution time have negligable cost.
   */
  const USAGE_CHECK_INTERVAL = 100;

  /**
   * The number of times to retry a database transaction on an exception.
   *
   * Drupal's low-level database drivers issue transactions to the database
   * to save or delete content. Occassionally an EntityStorageException is
   * is thrown due to the database being locked. While diagnosis hints that
   * this is a bug somewhere in Drupal core, we nevertheless need to address
   * it pending its eventual resolution. A reliable fix is to retry the
   * transaction immediately. This constant indicates the number of times
   * to retry a transaction. Usually a single retry is sufficient, but using
   * a somewhat higher number provides more safety without any additional
   * cost.
   *
   * Setting this value to zero disables retries.
   *
   * @var int
   */
  const DATABASE_RETRY = 20;

  /**
   * The number of microseconds to delay between database transaction retries.
   *
   * @var int
   */
  const DATABASE_RETRY_DELAY_MICROSEC = 1000;

  /*---------------------------------------------------------------------
   *
   * Constants - Special entity IDs.
   *
   *---------------------------------------------------------------------*/
  /**
   * Indicates that any user ID matches, when used as a method argument.
   *
   * @var int
   */
  const ANY_USER_ID = (-1000);

  /**
   * Indicates that the current user ID matches, when used as a method argument.
   *
   * @var int
   */
  const CURRENT_USER_ID = (-1001);

  /**
   * Indicates that any item ID matches, when used as a method argument.
   *
   * @var int
   */
  const ANY_ITEM_ID = (-2000);

  /**
   * Indicates the user's own root list (a.k.a. personal root list).
   *
   * @var int
   */
  const USER_ROOT_LIST = (-100);

  /**
   * Indicates the user's shared-with-them root list.
   *
   * @var int
   */
  const SHARED_ROOT_LIST = (-103);

  /**
   * Indicates the public root list.
   *
   * @var int
   */
  const PUBLIC_ROOT_LIST = (-101);

  /**
   * Indicates the any-user ("all") root list for admins.
   *
   * @var int
   */
  const ALL_ROOT_LIST = (-102);

  /*---------------------------------------------------------------------
   *
   * Constants - Kinds.
   *
   *---------------------------------------------------------------------*/
  /**
   * The kind for a folder.
   *
   * A folder has a name and list of children. A folder's size is the sum
   * of the sizes of all children.
   *
   * @var string
   * @see ::createFolder()
   * @see ::createRootFolder()
   */
  const FOLDER_KIND = 'folder';

  /**
   * The kind for a file.
   *
   * A file has a name and an underlying managed File entity that points to
   * a local file. A file's size is the size of the underlying file. The
   * File entity is referenced through a 'file' field. A file item has no
   * children.
   *
   * @var string
   * @see ::addFileFromInputStream()
   * @see ::addFileFromLocalFile()
   * @see ::addFileToRootFromInputStream()
   * @see ::addFileToRootFromLocalFile()
   * @see ::addFilesFromFormUpload()
   * @see ::addInputFile()
   * @see ::addInputFileToRoot()
   * @see ::addUploadFilesToRoot()
   * @see ::adduploadFiles()
   */
  const FILE_KIND = 'file';

  /**
   * The kind for an image.
   *
   * An image has a name and an underlying managed File entity that points to
   * a local image file. An image's size is the size of the underlying file.
   * The File entity is referenced through an 'image' field. An image item
   * has no children.
   *
   * @var string
   * @see ::addFileFromInputStream()
   * @see ::addFileFromLocalFile()
   * @see ::addFileToRootFromInputStream()
   * @see ::addFileToRootFromLocalFile()
   * @see ::addFilesFromFormUpload()
   * @see ::addInputFile()
   * @see ::addInputFileToRoot()
   * @see ::addUploadFilesToRoot()
   * @see ::adduploadFiles()
   */
  const IMAGE_KIND = 'image';

  /**
   * The kind for a media holder.
   *
   * A media item has a name and an underlying Media entity that points to
   * any type of media, including local files and remote resources. The
   * Media entity is referenced through an entity reference 'media' field.
   * A media item has no children.
   *
   * @var string
   */
  const MEDIA_KIND = 'media';

  /**
   * The kind for an object.
   *
   * An object item has a name, but no children and no underlying files.
   * It is a place holder and its size is always zero.
   *
   * @var string
   */
  const OBJECT_KIND = 'object';

  /*---------------------------------------------------------------------
   *
   * Constants - MIME types
   *
   *---------------------------------------------------------------------*/
  /**
   * The custom MIME type for folders.
   *
   * There is no official Internet standard for a MIME type for folders,
   * so we are forced to create one. This is primarily used for selecting
   * an icon in the user interface, and this module's styling provides
   * that icon.
   *
   * @var string
   */
  const FOLDER_MIME = 'folder/directory';

  /**
   * The generic MIME type for media.
   *
   * Media are generic wrappers for local or remote content. There is no
   * single MIME type appropriate. A generic MIME type is used as a
   * fallback.
   *
   * @var string
   */
  const MEDIA_MIME = 'application/octet-stream';

  /**
   * The generic OBJECT type.
   *
   * Objects are generic items with no content.
   *
   * @var string
   */
  const OBJECT_MIME = 'object/generic';

  /**
   * The generic MIME type.
   *
   * When there is no file or media, this generic MIME type is used.
   *
   * @var string
   */
  const GENERIC_MIME = 'application/octet-stream';

  /*---------------------------------------------------------------------
   *
   * Constants - Path schemes.
   *
   *---------------------------------------------------------------------*/
  /**
   * The personal scheme for the user's own files, or those shared with them.
   *
   * The scheme is used in virtual file and folder paths of the form:
   * - SCHEME://UID/PATH...
   *
   * @var string
   */
  const PERSONAL_SCHEME = "personal";

  /**
   * The public scheme for the site's publically accessible files.
   *
   * The scheme is used in virtual file and folder paths of the form:
   * - SCHEME://UID/PATH...
   *
   * The public scheme refers to content owned by another user and shared
   * with the anonymous user, which makes it publically accessible.
   *
   * @var string
   */
  const PUBLIC_SCHEME = "public";

  /*---------------------------------------------------------------------
   *
   * Constants - Parameters.
   *
   *---------------------------------------------------------------------*/
  /**
   * The maximum number of UTF-8 characters in a file or folder name.
   *
   * The File module imposes an internal limit of 255 characters for
   * file names. FolderShare uses this same limit for consistency.
   *
   * This limit is in *characters*, not bytes. The File and FolderShare
   * modules support UTF-8 names which include multi-byte characters.
   * So, a 255 character name may be use more than 255 bytes.
   *
   * All name handling must be multi-byte character safe. This often
   * means the use of the 'mb_' functions in PHP, such as 'mb_strlen()'.
   *
   * @var int
   */
  const MAX_NAME_LENGTH = 255;

  /*---------------------------------------------------------------------
   *
   * Kind field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item's kind.
   *
   * An item's kind characterizes the functionality the item supports:
   * - self::FOLDER_KIND: A group of child files and folders.
   * - self::FILE_KIND: An arbitrary data file.
   * - self::IMAGE_KIND: An image file.
   * - self::MEDIA_KIND: A media entity, such as a URL to an external video.
   * - self::OBJECT_KIND: An object with no files or children.
   *
   * <B>Example usage</B>
   * Loop through all children of this item and group them by kind:
   * @code
   *   $groups = [
   *     'folder' => [],
   *     'file'   => [],
   *     'image'  => [],
   *     'media'  => [],
   *   ];
   *   foreach ($item->findChildren() as $child) {
   *     $groups[$child->getKind()][] = $child;
   *   }
   * @endcode
   *
   * @return string
   *   Returns the kind of item.
   *
   * @see ::isFile()
   * @see ::isFolder()
   * @see ::isImage()
   * @see ::isMedia()
   * @see ::getMimeType()
   */
  public function getKind();

  /**
   * Returns TRUE if this item is a file, and FALSE otherwise.
   *
   * @return bool
   *   Returns TRUE if this item is a file.
   */
  public function isFile();

  /**
   * Returns TRUE if this item is a file or image, and FALSE otherwise.
   *
   * @return bool
   *   Returns TRUE if this item is a file or image.
   */
  public function isFileOrImage();

  /**
   * Returns TRUE if this item is a folder, and FALSE otherwise.
   *
   * @return bool
   *   Returns TRUE if this item is a folder.
   */
  public function isFolder();

  /**
   * Returns TRUE if this item is an image, and FALSE otherwise.
   *
   * @return bool
   *   Returns TRUE if this item is an image.
   */
  public function isImage();

  /**
   * Returns TRUE if this item is a media, and FALSE otherwise.
   *
   * @return bool
   *   Returns TRUE if this item is a media.
   */
  public function isMedia();

  /**
   * Returns TRUE if this item is an object, and FALSE otherwise.
   *
   * @return bool
   *   Returns TRUE if this item is an object.
   */
  public function isObject();

  /*---------------------------------------------------------------------
   *
   * MIME type field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item's MIME type.
   *
   * If this item is a file, image, or media item, the returned MIME type is
   * expected to be that of the underlying file. If the MIME type of the
   * file cannot be determined, media items usually return the generic
   * MEDIA_MIME and files return GENERIC_MIME.
   *
   * If this item is a folder, the FOLDER_MIME string is usually returned.
   *
   * If this item is an object, the OBJECT_MIME string is usually returned.
   *
   * @return string
   *   Returns the MIME type.
   *
   * @see \Drupal\file\FileInterface::getMimeType()
   * @see ::isMimeTypeImage()
   * @see ::FOLDER_MIME
   * @see ::GENERIC_MIME
   * @see ::MEDIA_MIME
   * @see ::OBJECT_MIME
   */
  public function getMimeType();

  /**
   * Sets the item's MIME type.
   *
   * When the given MIME type is not empty, the item's MIME type is set. If
   * the item wraps an underlying file, the file's MIME type is set as well
   * and saved.
   *
   * If the given MIME type is empty, the item's MIME type is set to a default.
   * For folders, the default MIME type is FOLDER_MIME. For media, the default
   * is MEDIA_MIME. For objects, the default is OBJECT_MIME. For files and
   * images, the default is that of the underlying file, which is often based
   * upon the result of Drupal's MIME type guesser service and the filename's
   * extension. And for all other content, the default MIME type is
   * GENERIC_MIME.
   *
   * The MIME type is not validated or restricted. The caller can set the
   * MIME type to anything, but they should insure that it has a legal form
   * and is meaningful. This is often done by using Drupal's MIME type guesser
   * service and the name's extension.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * entity and default MIME type.
   *
   * @param string $mime
   *   The new MIME type. The value is not validated but is expected to
   *   be a properly formed MIME type. If the string is empty, the MIME
   *   type is set to the default.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::FOLDER_MIME
   * @see ::GENERIC_MIME
   * @see ::MEDIA_MIME
   * @see ::OBJECT_MIME
   * @see ::getMimeType()
   * @see ::isMimeTypeImage()
   * @see ::setMimeTypeToDefault()
   * @see \Drupal\file\FileInterface::getMimeType()
   * @see \Drupal\file\FileInterface::setMimeType()
   */
  public function setMimeType(string $mime);

  /**
   * Sets the item's MIME type to its default.
   *
   * For folders, the MIME type default is FOLDER_MIME.
   *
   * For files and images, the MIME type default is that of the underlying
   * File entity.
   *
   * For media, the MIME type default is MEDIA_MIME.
   *
   * For objects, the MIME type default is OBJECT_MIME.
   *
   * For all other item kinds, the default is GENERIC_MIME.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the entity and
   * default MIME type. Hooks may return alternate MIME types.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::FOLDER_MIME
   * @see ::GENERIC_MIME
   * @see ::MEDIA_MIME
   * @see ::OBJECT_MIME
   * @see ::getMimeType()
   * @see ::isMimeTypeImage()
   * @see ::setMimeType()
   * @see \Drupal\file\FileInterface::getMimeType()
   * @see \Drupal\file\FileInterface::setMimeType()
   */
  public function setMimeTypeToDefault();

  /*---------------------------------------------------------------------
   *
   * File, Image, and Media fields.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item's file ID, if any.
   *
   * For file items, this method returns the File entity ID for the
   * underlying stored file. For all other item kinds, a FALSE is returned.
   *
   * While image items also have a File entity ID, file and image items
   * store their File entity IDs separately. Use getImageId() to get an
   * image item's File entity ID.
   *
   * <B>Example usage</B>
   * Get the File entity ID for file or image items:
   * @code
   *   $fileId = FALSE;
   *   if ($item->isFile() === TRUE) {
   *     $fileId = $item->getFileId();
   *   }
   *   elseif ($item->isImage() === TRUE) {
   *     $fileId = $item->getImageId();
   *   }
   * @endcode
   *
   * @return bool|int
   *   Returns the entity ID of the underlying File entity for file items,
   *   or FALSE if this item is not for a file.
   *
   * @see ::getFile()
   * @see ::getKind()
   * @see ::isFile()
   */
  public function getFileId();

  /**
   * Returns the item's File entity, if any.
   *
   * For file items, this method returns the File entity for the
   * underlying stored file. For all other item kinds, a NULL is returned.
   *
   * While image items also have a File entity, file and image items
   * store their File entities separately. Use getImage() to get an
   * image item's File entity.
   *
   * <B>Example usage</B>
   * Get the File entity for file or image items:
   * @code
   *   $file = NULL;
   *   if ($item->isFile() === TRUE) {
   *     $file = $item->getFile();
   *   }
   *   elseif ($item->isImage() === TRUE) {
   *     $file = $item->getImage();
   *   }
   * @endcode
   *
   * @return \Drupal\file\FileInterface
   *   Returns the File entity for the underlying file for file items,
   *   or a NULL if this item is not for a file.
   *
   * @see ::getFileId()
   * @see ::getKind()
   * @see ::isFile()
   */
  public function getFile();

  /**
   * Returns the item's image ID, if any.
   *
   * For image items, this method returns the File entity ID for the
   * underlying stored file. For all other item kinds, a FALSE is returned.
   *
   * While file items also have a File entity ID, file and image items
   * store their File entity IDs separately. Use getFileId() to get a
   * file item's File entity ID.
   *
   * <B>Example usage</B>
   * Get the File entity ID for file or image items:
   * @code
   *   $fileId = FALSE;
   *   if ($item->isFile() === TRUE) {
   *     $fileId = $item->getFileId();
   *   }
   *   elseif ($item->isImage() === TRUE) {
   *     $fileId = $item->getImageId();
   *   }
   * @endcode
   *
   * @return bool|int
   *   Returns the entity ID of the underlying File entity for image items,
   *   or FALSE if this item is not for an image file.
   *
   * @see ::getImage()
   * @see ::getKind()
   * @see ::isImage()
   */
  public function getImageId();

  /**
   * Returns the item's image File entity, if any.
   *
   * For image items, this method returns the File entity for the
   * underlying stored file. For all other item kinds, a NULL is returned.
   *
   * While file items also have a File entity, file and image items
   * store their File entities separately. Use getFile() to get a
   * file item's File entity.
   *
   * <B>Example usage</B>
   * Get the File entity for file or image items:
   * @code
   *   $file = NULL;
   *   if ($item->isFile() === TRUE) {
   *     $file = $item->getFile();
   *   }
   *   elseif ($item->isImage() === TRUE) {
   *     $file = $item->getImage();
   *   }
   * @endcode
   *
   * @return \Drupal\file\FileInterface
   *   Returns the File entity for the underlying image file for image items,
   *   or a NULL if this item is not for an image.
   *
   * @see ::getImageId()
   * @see ::getKind()
   * @see ::isImage()
   */
  public function getImage();

  /**
   * Returns the item's media ID, if any.
   *
   * For media items, this method returns the Media entity ID for the
   * underlying media object. For all other item kinds, a FALSE is returned.
   *
   * @return int
   *   Returns the entity ID of the underlying Media entity for media items,
   *   or FALSE if this item is not for media.
   *
   * @see ::getMedia()
   * @see ::getKind()
   * @see ::isMedia()
   */
  public function getMediaId();

  /**
   * Returns the item's Media entity, if any.
   *
   * For media items, this method returns the Media entity for the
   * media object. For all other item kinds, a NULL is returned.
   *
   * @return \Drupal\media\MediaInterface
   *   Returns the Media entity for the underlying media object for media
   *   items, or a NULL if this item is not for media.
   *
   * @see ::getMediaId()
   * @see ::getKind()
   * @see ::isMedia()
   */
  public function getMedia();

  /*---------------------------------------------------------------------
   *
   * Name field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item's name.
   *
   * This function is equivalent to Entity::label(), but faster since it
   * does not require entity type introspection to find the field containing
   * the name.
   *
   * @return string
   *   Returns the name of the item.
   *
   * @see \Drupal\Core\Entity\label()
   * @see ::rename()
   */
  public function getName();

  /**
   * Returns TRUE if a proposed name is unique among this item's children.
   *
   * The $name argument specifies a proposed name for an existing or new
   * child of this item. The name is not validated and is presumed to be
   * of legal length and structure.
   *
   * The optional $inUseId indicates the ID of an existing child of this
   * item that is already using the name. If the value is not given, negative,
   * or FolderShareInterface::ANY_ITEM_ID, then it is presumed that no
   * current child has the proposed name.
   *
   * This function looks through the names of its children and returns TRUE
   * if the proposed name is not in use by any child, except the indicated
   * child, if any. If the name is in use by a child that is not $inUseId,
   * then FALSE is returned.
   *
   * @param string $name
   *   A proposed name.
   * @param int $inUseId
   *   (optional, default = FolderShareInterface::ANY_ITEM_ID) The ID of an
   *   existing FolderShare item child that is already using the proposed name.
   *
   * @return bool
   *   Returns TRUE if the name is unique among the item's children, and
   *   FALSE otherwise.
   *
   * @see ::findChildrenNames()
   * @see ::getName()
   * @see ::isNameLegal()
   * @see ::isRootNameUnique()
   * @see ::createUniqueName()
   */
  public function isNameUnique(string $name, int $inUseId = self::ANY_ITEM_ID);

  /*---------------------------------------------------------------------
   *
   * File name extensions.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item's filename extension.
   *
   * If the item is a folder, an empty string is returned.
   *
   * The extension is always converted to lower case.
   *
   * @return string
   *   The file name extension (the portion of the name after the last ".").
   */
  public function getFilenameExtension();

  /*---------------------------------------------------------------------
   *
   * Size field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item's storage size, in bytes.
   *
   * For file, image, and media items, this function returns the size of
   * the underlying entity. For File and Image items, this is the size of
   * the locally stored file.
   *
   * For folders, this returns the sum of the sizes for all descendants.
   * The field is initialized to zero when a folder is created, and updated
   * each time a descendant is added, deleted, or moved. For some operations,
   * the size is temporarily cleared while the sum of descendant sizes is
   * recalculated. During this time, this function will return FALSE.
   *
   * @return bool|int
   *   Returns the size in bytes, or FALSE if the size is currently unknown.
   *
   * @see ::getKind()
   * @see \Drupal\file\FileInterface::getSize()
   */
  public function getSize();

  /*---------------------------------------------------------------------
   *
   * Created date/time field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the creation timestamp.
   *
   * @return int
   *   Returns the creation time stamp for this item.
   *
   * @see ::setCreatedTime()
   */
  public function getCreatedTime();

  /**
   * Sets the creation timestamp.
   *
   * The timestamp is not validated and is presumed to be valid.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param int $timestamp
   *   The creation time stamp for this item. The value is not validated
   *   and is assumed to be a valid timestamp.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::getCreatedTime()
   */
  public function setCreatedTime($timestamp);

  /*---------------------------------------------------------------------
   *
   * Owner field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns TRUE if the item is owned by the indicated user.
   *
   * @param int $uid
   *   The user ID to check.
   *
   * @return bool
   *   Returns TRUE if the item is owned by the indicated user, and FALSE
   *   otherwise.
   *
   * @see ::getOwnerId()
   */
  public function isOwnedBy(int $uid);

  /**
   * Sets the owner of this item.
   *
   * If this item is a file, the underlying file's ownership is also changed.
   *
   * If this item is a folder, the ownership of the folder's children
   * is not affected.
   *
   * This item is automatically saved after changes are made.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param \Drupal\user\UserInterface $account
   *   The user account of the new owner of the folder. If the value is NULL,
   *   the current user's account is used.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see \Drupal\user\EntityOwnerInterface::getOwner()
   * @see \Drupal\user\EntityOwnerInterface::getOwnerId()
   */
  public function setOwner(UserInterface $account = NULL);

  /**
   * Sets the owner ID of this item.
   *
   * If this item is a file, the underlying file's ownership is also changed.
   *
   * If this item is a folder, the ownership of the folder's children
   * is not affected.
   *
   * The user ID is not validated and is presumed to be a valid ID for a
   * User entity.
   *
   * This item is automatically saved after changes are made.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity. See changeOwnerId().
   *
   * @param int $ownerUid
   *   The user ID of the new owner of the folder. The ID is not validated
   *   and is presumed to be for a valid User entity.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::changeOwnerId()
   * @see \Drupal\user\EntityOwnerInterface::getOwner()
   * @see \Drupal\user\EntityOwnerInterface::getOwnerId()
   */
  public function setOwnerId($ownerUid);

  /*---------------------------------------------------------------------
   *
   * Grant fields.
   *
   *---------------------------------------------------------------------*/
  /**
   * Adds a user to grant them view or author access to an item.
   *
   * If this item is not a root item, no action is taken.
   *
   * Granting a user access adds the user's ID to the item's list
   * of users that have the specified access. Recognized access grants are:
   *
   * - 'view': can view folder fields and content.
   * - 'author': can create, update, and delete fields and content. Any
   *   user granted 'author' access is automatically granted 'view' access.
   *
   * The owner of the item always has view and author access granted.
   * Adding the owner, or any user already granted access, has no affect.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity. Alternately, use the share() method, which uses process
   * locks, calls hooks, and logs operations.
   *
   * @param int $uid
   *   The user ID of a user currently granted access. If the user ID is
   *   negative, matches the owner ID, or does not match a user ID in the
   *   item's current grants list, this call has no effect.
   * @param string $access
   *   The access grant to be removed. This is one of 'author' or 'view'.
   *   All other values and mixes of case are ignored.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::clearAccessGrants()
   * @see ::deleteAccessGrant()
   * @see ::setAccessGrants()
   * @see ::share()
   */
  public function addAccessGrant(int $uid, string $access);

  /**
   * Deletes a user's access to this item.
   *
   * If this item is not a root item, no action is taken.
   *
   * The owner of the item always has view and author access granted.
   * Attempting to delete the owner's access has no effect.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity. Alternately, use the share() method, which uses process
   * locks, calls hooks, and logs operations.
   *
   * @param int $uid
   *   The user ID of a user currently granted access. If the user ID is
   *   negative, matches the owner ID, or does not match a user ID in the
   *   item's current grants list, this call has no effect.
   * @param string $access
   *   The access grant to be removed. This is one of 'author' or 'view'.
   *   All other values and mixes of case are ignored.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::clearAccessGrants()
   * @see ::addAccessGrant()
   * @see ::setAccessGrants()
   * @see ::share()
   */
  public function deleteAccessGrant(int $uid, string $access);

  /**
   * Returns user IDs for users granted author access to this root item.
   *
   * The returned unordered array has one entry for each user ID currently
   * granted author (create, update, delete) access to this root item.
   * The owner of the root item is always included in this list.
   *
   * If this is not a root item, an empty array is returned.
   *
   * @return int[]
   *   Returns an unordered array of user IDs for users with author access to
   *   this root item. If this is not a root item, an empty array is returned.
   *
   * @see ::getAccessGrantViewUserIds()
   * @see ::getAccessGrants()
   * @see ::isAccessGranted()
   */
  public function getAccessGrantAuthorUserIds();

  /**
   * Returns user IDs for users granted view access to this root item.
   *
   * The returned unordered array has one entry for each user ID currently
   * granted view access to this root item. The owner of the root item is
   * always included in this list.
   *
   * If this is not a root item, an empty array is returned.
   *
   * @return int[]
   *   Returns an unordered array of user IDs for users with view access to
   *   this root item. If this is not a root item, an empty array is returned.
   *
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrants()
   * @see ::isAccessGranted()
   */
  public function getAccessGrantViewUserIds();

  /**
   * Returns all user IDs and access to this root item.
   *
   * The returned unordered associative array has user IDs as keys. For each
   * user, the array value is an array that contains either one or two string
   * values:
   * - ['view'] = user only has view access.
   * - ['author'] = user only has author access (which is odd).
   * - ['author', 'view'] = user has view and author access.
   *
   * The owner of the root folder is always included with both view and
   * author access.
   *
   * If this is not a root item, an empty array is returned.
   *
   * The returned array is in the same format as that used by
   * share().
   *
   * @return array
   *   Returns an unordered associative array with user IDs as keys and
   *   arrays as values. Array values contain strings indicating 'view' or
   *   'author' access. If this is not a root item, an empty array is
   *   returned.
   *
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   * @see ::isAccessGranted()
   * @see ::share()
   */
  public function getAccessGrants();

  /**
   * Returns TRUE if access is explicitly granted to a user on this root item.
   *
   * The $uid argument indicates the user to look up to check if they have been
   * explicitly granted access to the root item and its descendants.
   *
   * The $access argument selects whether 'view' or 'author' access is
   * checked. For any other value, FALSE is returned.
   *
   * If this is not a root item, FALSE is returned. Use getRootItem() to
   * get the root item ancestor of this item, and then query it for access.
   *
   * If a root item grants access to the anonymous user (uid = 0), then
   * this method returns TRUE for all user IDs for 'author' and 'view'.
   *
   * A system hidden or disabled item may be queried for its access grants.
   * However the module's access control will deny access anyway if the user
   * is not an administrator.
   *
   * @param int $uid
   *   The user ID of a user granted access.
   * @param string $access
   *   The access to test. One of 'author' or 'view'.
   *
   * @return bool
   *   Returns TRUE if the access is granted, and FALSE otherwise. If this
   *   is not a root item, FALSE is returned.
   *
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   * @see ::getAccessGrants()
   * @see ::share()
   */
  public function isAccessGranted(int $uid, string $access);

  /**
   * Returns TRUE if no-one but the owner has access to this root item.
   *
   * If the root item's owner is the only user ID listed in the item's
   * access grants, then the root item and its descendants are private
   * and this method returns TRUE. Otherwise it returns FALSE.
   *
   * If this is not a root item, FALSE is returned.
   *
   * A system hidden or disabled item may be queried for its access grants.
   * However the module's access control will deny access anyway if the user
   * is not an administrator.
   *
   * @return bool
   *   Returns TRUE if the owner is the only user granted access, and
   *   FALSE otherwise.
   *
   * @see ::isAccessGranted()
   * @see ::isAccessPublic()
   * @see ::isAccessShared()
   */
  public function isAccessPrivate();

  /**
   * Returns TRUE if access is granted to anonymous on this root item.
   *
   * If the root item's 'view' or 'author' grants include the anonymous
   * user (UID = 0), then the root item and its descendants are public and
   * this method returns TRUE. Otherwise it returns FALSE.
   *
   * If this is not a root item, FALSE is returned.
   *
   * A system hidden or disabled item may be queried for its access grants.
   * However the module's access control will deny access anyway if the user
   * is not an administrator.
   *
   * @return bool
   *   Returns TRUE if access is granted to the anonymous user, and
   *   FALSE otherwise.
   *
   * @see ::isAccessGranted()
   * @see ::isAccessPrivate()
   * @see ::isAccessShared()
   */
  public function isAccessPublic();

  /**
   * Returns TRUE if access is granted to a non-owner of this root item.
   *
   * If the root item grants 'view' or 'author' access to anyone other
   * than the item's owner, the item is being shared and this method returns
   * TRUE. Otherwise it returns FALSE.
   *
   * If this is not a root item, FALSE is returned.
   *
   * A system hidden or disabled item may be queried for its access grants.
   * However the module's access control will deny access anyway if the user
   * is not an administrator.
   *
   * @return bool
   *   Returns TRUE if access is granted to anyone beyond the owner, and
   *   FALSE otherwise.
   *
   * @see ::isAccessGranted()
   * @see ::isAccessPrivate()
   * @see ::isAccessPublic()
   */
  public function isAccessShared();

  /**
   * Returns TRUE if the item is shared by the indicated user.
   *
   * An item is shared by a user if:
   * - It is owned by that user.
   * - It has view or author access grants for anybody else.
   *
   * @param int $uid
   *   The user ID of the user to check.
   *
   * @return bool
   *   Returns TRUE if the item is shared by the indicated user, and
   *   FALSE otherwise.
   *
   * @see ::isAccessGranted()
   * @see ::isOwnedBy()
   * @see ::isSharedWith()
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   */
  public function isSharedBy(int $uid);

  /**
   * Returns TRUE if the item is shared with the indicated user.
   *
   * An item is shared with a user if:
   * - It is NOT owned by that user.
   * - It has the indicated access grants for that user.
   *
   * @param int $uid
   *   The user ID of the user to check.
   * @param string $access
   *   The access to test. One of 'author' or 'view'.
   *
   * @return bool
   *   Returns TRUE if the item is shared by the indicated user, and
   *   FALSE otherwise.
   *
   * @see ::isAccessGranted()
   * @see ::isOwnedBy()
   * @see ::isSharedBy()
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   */
  public function isSharedWith(int $uid, string $access);

  /**
   * Sets all user IDs and access grants for the item.
   *
   * If the item is not a root item, this call has no effect.
   *
   * The given associative array must have user IDs as keys. For each user,
   * the array value must be an array of strings with one or two values:
   * - ['view'] = user only has view access.
   * - ['author'] = user only has author access (which is odd).
   * - ['author', 'view'] = user has view and author access.
   *
   * The given array is used to completely reset all access grants for
   * the item. Any prior grants are removed.
   *
   * The owner of the item is automaticaly included with both view and
   * author access, whether or not they are included in the given array.
   *
   * The given array is in the same format as that returned by
   * getAccessGrants().
   *
   * System hidden and disabled entities may be changed, however the module's
   * access control will deny access for any user that is not an administrator.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity. Alternately, use the share() method, which uses process
   * locks, calls hooks, and logs operations.
   *
   * @param array $grants
   *   An unordered associative array with user IDs as keys and
   *   arrays as values. Array values contain strings indicating 'view' or
   *   'author' access.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   * @see ::isAccessGranted()
   * @see ::share()
   */
  public function setAccessGrants(array $grants);

  /*---------------------------------------------------------------------
   *
   * Sharing status.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a simplified statement about the entity's sharing status.
   *
   * Returned values include:
   * - "personal" = the item is not shared and it is owned by the
   *   current user.
   *
   * - "private" = the item is not shared and it is not owned by the
   *   current user.
   *
   * - "shared by you" = the item is shared and it is owned by the
   *   current user.
   *
   * - "shared with you" = the item is shared with the current user and it
   *   is not owned by the current user.
   *
   * - "public" = the item is owned by Anonymous or is shared with Anonymous.
   *
   * System hidden and disabled items are always "private".
   *
   * @return string
   *   Returns one of the above sharing status values.
   */
  public function getSharingStatus();

  /*---------------------------------------------------------------------
   *
   * SystemDisabled field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns TRUE if the item is disabled by the system.
   *
   * Items are temporarily disabled during long-running operations, such as
   * copies and moves. Disabled items are normally visible, but non-functional
   * for users.
   *
   * @return bool
   *   Returns TRUE if the item is disabled, and FALSE otherwise.
   */
  public function isSystemDisabled();

  /*---------------------------------------------------------------------
   *
   * SystemHidden field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns TRUE if the item is hidden by the system.
   *
   * Items are hidden during long-running destructive operations, such as
   * deletion. Hidden items are normally not visible to users.
   *
   * @return bool
   *   Returns TRUE if the item is hidden, and FALSE otherwise.
   */
  public function isSystemHidden();

  /*---------------------------------------------------------------------
   *
   * Description field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the item description.
   *
   * @return string
   *   Returns the description string for the item, or an empty string if
   *   there is no description.
   *
   * @see ::getDescriptionFormat()
   * @see ::setDescription()
   * @see ::setDescriptionFormat()
   */
  public function getDescription();

  /**
   * Returns the item description text format.
   *
   * @return string
   *   Returns the text format used by the description for the item, or an
   *   empty string if there is no format selected.
   *
   * @see ::getDescription()
   * @see ::setDescription()
   * @see ::setDescriptionFormat()
   */
  public function getDescriptionFormat();

  /**
   * Sets the item description.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity. See changeOwnerId().
   *
   * @param string $text
   *   The description string for the item, or an empty string if there is no
   *   description. The description is not validated or filtered.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::getDescription()
   * @see ::getDescriptionFormat()
   * @see ::setDescriptionFormat()
   */
  public function setDescription(string $text);

  /**
   * Sets the item description text format.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity. See changeOwnerId().
   *
   * @param string $format
   *   The test format name for the item's description, or an empty string
   *   if there is no format. The format is not validated or filtered.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::getDescription()
   * @see ::getDescriptionFormat()
   * @see ::setDescription()
   */
  public function setDescriptionFormat(string $format);

  /*---------------------------------------------------------------------
   *
   * Child items.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a list of children IDs of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Example usage</B>
   * Loop through all children of this item:
   * @code
   *   foreach ($item->findChildrenIds() as $id) { ... }
   * @endcode
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return int[]
   *   Returns an unordered array of integer entity IDs for children
   *   of this item, or an empty array if there are no children.
   *
   * @see ::findFileChildrenIds()
   * @see ::findFolderChildrenIds()
   * @see ::findChildrenNames()
   */
  public function findChildrenIds(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns a list of children of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Memory use</B>
   * Warning: There can be a large number of children. Loading them all
   * into memory at the same time could exceed PHP's memory limit.
   *
   * <B>Run time</B>
   * Warning: There can be a large number of children. Loading them all
   * at the same time could exceed PHP's execution time limit.
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return array
   *   Returns a list of entities for children of this item, or an empty
   *   array if there are no children.
   *
   * @see ::findFileChildren()
   * @see ::findFolderChildren()
   */
  public function findChildren(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns a list of names for children of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Example usage</B>
   * Loop through all children names of this item:
   * @code
   *   foreach ($item->findChildrenNames() as $name => $id) { ... }
   * @endcode
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return array
   *   Returns an unordered associative array where keys are item names and
   *   values are entity IDs.
   *
   * @see ::findChildren()
   * @see ::findChildrenIds()
   * @see ::findFileChildren()
   * @see ::findFileChildrenIds()
   * @see ::findFolderChildren()
   * @see ::findFolderChildrenIds()
   */
  public function findChildrenNames(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns the total size, in bytes, of all children.
   *
   * System hidden children are ignored. System disabled items are included.
   *
   * @return int
   *   Returns the size, in bytes, of the sum of the sizes of all children.
   *
   * @see ::findChildrenIds()
   * @see ::findFileChildren()
   * @see ::findChildrenNames()
   * @see ::getFileId()
   */
  public function findChildrenNumberOfBytes();

  /**
   * Returns a list of file, image, and media IDs for children of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Memory use</B>
   * Warning: There can be a large number of file children. Loading them all
   * into memory at the same time could exceed PHP's memory limit.
   *
   * <B>Run time</B>
   * Warning: There can be a large number of file children. Loading them all
   * at the same time could exceed PHP's execution time limit.
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return int[]
   *   Returns an unordered array of integer entity IDs for file, image,
   *   or media children of this item, or an empty array if there are no
   *   matching children.
   *
   * @see ::findChildrenIds()
   * @see ::findFileChildren()
   * @see ::findChildrenNames()
   * @see ::getFileId()
   */
  public function findFileChildrenIds(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns a list of entities for files, images, or media in this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Example usage</B>
   * Loop through all file, image, or media children of this item:
   * @code
   *   foreach ($item->findFileChildren() as $child) { ... }
   * @endcode
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return array
   *   Returns an unordered array of entities for file, image,
   *   or media children of this item, or an empty array if there are no
   *   matching children.
   *
   * @see ::findChildren()
   * @see ::findFileChildrenIds()
   * @see ::findChildrenNames()
   * @see ::getFile()
   * @see ::getImage()
   * @see ::getMedia()
   */
  public function findFileChildren(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns a list of folder IDs for children of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Example usage</B>
   * Loop through all folder children of this item:
   * @code
   *   foreach ($item->findFolderChildrenIds() as $id) { ... }
   * @endcode
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return array
   *   Returns an unordered array of integer entity IDs for folder
   *   children of this item, or an empty array if there are no folder
   *   children.
   *
   * @see ::findChildrenIds()
   * @see ::findFolderChildren()
   * @see ::findChildrenNames()
   */
  public function findFolderChildrenIds(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns a list of entities for folder children of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Memory use</B>
   * Warning: There can be a large number of folder children. Loading them all
   * into memory at the same time could exceed PHP's memory limit.
   *
   * <B>Run time</B>
   * Warning: There can be a large number of folder children. Loading them all
   * at the same time could exceed PHP's execution time limit.
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return array
   *   Returns an unordered array of entities for folder children of
   *   this item, or an empty array if there are no folder children.
   *
   * @see ::findFolderChildrenIds()
   * @see ::findChildrenNames()
   * @see ::findChildren()
   */
  public function findFolderChildren(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns the number of children.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return int
   *   Returns the number of children. If the item is not a folder, a zero
   *   is returned.
   *
   * @see ::findFileChildrenIds()
   */
  public function findNumberOfChildren(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /*---------------------------------------------------------------------
   *
   * Parent field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the parent folder, or NULL if there is no parent.
   *
   * Since only folders can have children, only folders can be parents,
   * and the returned entity is always a folder.
   *
   * A system hidden or disabled item are returned.
   *
   * <B>Example usage</B>
   * Build a list of ancestors by successively getting the parent, then
   * the parent's parent, and so forth:
   * @code
   *   $ancestors = [];
   *   $parent = $item->getParentFolder();
   *   while ($parent !== NULL) {
   *     $ancestors[] = $parent;
   *     $parent = $parent->getParentFolder();
   *   }
   * @endcode
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the parent entity, or NULL if this item is a root item and
   *   therefore has no parent.
   *
   * @see ::getParentFolderId()
   * @see ::getRootItem()
   * @see ::getRootItemId()
   * @see ::isFolder()
   * @see ::isRootItem()
   */
  public function getParentFolder();

  /**
   * Returns the parent folder ID, or a flag if there is no parent.
   *
   * If the item has no parent, it is a root list item and
   * FolderShareInterface::USER_ROOT_LIST is returned.
   *
   * Since only folders can have children, only folders can be parents,
   * and the returned entity ID is always for a folder.
   *
   * A system hidden or disabled item are returned.
   *
   * @return int
   *   Returns the parent ID, or FolderShareInterface::USER_ROOT_LIST if this
   *   item is a root item and therefore has no parent.
   *
   * @see ::getParentFolder()
   * @see ::getRootItem()
   * @see ::getRootItemId()
   * @see ::isFolder()
   * @see ::isRootItem()
   */
  public function getParentFolderId();

  /*---------------------------------------------------------------------
   *
   * Ancestors.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a list of ancestor folder IDs.
   *
   * The returned list is ordered with this item's root first, then each
   * ancestor leading down to this item. This item's ID is not included.
   * If this item is a root, then the returned array is empty.
   *
   * System hidden and disabled items are included in the list.
   *
   * @return int[]
   *   Returns an ordered list of entity IDs as array values, starting
   *   with this item's root.
   *
   * @see ::findAncestorFolders()
   * @see ::findAncestorFolderNames()
   * @see ::findDescendantIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isAncestorOfFolderId()
   * @see ::isDescendantOfFolderId()
   */
  public function findAncestorFolderIds();

  /**
   * Returns a list of ancestor folder names.
   *
   * The returned list is ordered with this item's root first, then each
   * ancestor leading down to this item. This item's name is not included.
   * If this item is a root, then the returned array is empty.
   *
   * System hidden and disabled items are included in the list.
   *
   * <B>Example usage</B>
   * Build a path of ancestor names for this item:
   * @code
   *   $path = '/' . implode('/', $item->findAncestorFolderNames());
   * @endcode
   *
   * @return string[]
   *   Returns an ordered list of entity IDs as array values, starting
   *   with this item's root.
   *
   * @see ::findAncestorFolders()
   * @see ::findAncestorFolderIds()
   */
  public function findAncestorFolderNames();

  /**
   * Returns a list of ancestor folders.
   *
   * The returned list is ordered with this item's root first, then each
   * ancestor leading down to this item. This item is not included.
   * If this item is a root, then the returned array is empty.
   *
   * System hidden and disabled items are included in the list.
   *
   * <B>Memory use</B>
   * Warning: There can be a large number of ancestors. Loading them all
   * into memory at the same time could exceed PHP's memory limit.
   *
   * <B>Run time</B>
   * Warning: There can be a large number of ancestors. Loading them all
   * at the same time could exceed PHP's execution time limit.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns an ordered list of entities as array values, starting
   *   with this item's root.
   *
   * @see ::findAncestorFolderIds()
   * @see ::findAncestorFolderNames()
   * @see ::findDescendantIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isAncestorOfFolderId()
   * @see ::isDescendantOfFolderId()
   */
  public function findAncestorFolders();

  /**
   * Returns TRUE if this item is an ancestor of the given item.
   *
   * System hidden and disabled items are considered in this test.
   *
   * @param int $folderId
   *   The ID of the item to check to see if this item is one of its ancestors.
   *
   * @return bool
   *   Returns TRUE if this item is an ancestor of the given item,
   *   and FALSE otherwise. If the given entity ID is invalid, FALSE
   *   is returned.
   *
   * @see ::findAncestorFolderIds()
   * @see ::findDescendantIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isDescendantOfFolderId()
   */
  public function isAncestorOfFolderId(int $folderId);

  /*---------------------------------------------------------------------
   *
   * Descendants.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a list of descendant item IDs.
   *
   * The returned list includes the entity IDs of children, their children,
   * and so on for all descendants of this item.
   *
   * System hidden or disabled items may or may not be returned, depending
   * upon the parameters.
   *
   * Items marked disabled are in use. They remain visible and are enabled
   * again when they are no longer in use. Typically, disabled items should
   * be included in the returned results, so $includeDisabled defaults to
   * TRUE.
   *
   * Items marked hidden are being deleted. They are not visible and will
   * never be enabled again. Typically, hidden items should not be included
   * in the returned results, so $includeHidden defaults to FALSE.
   *
   * <B>Example usage</B>
   * Find all descendants of this item:
   * @code
   *   $allIds = $this->findDescendantIds();
   * @endcode
   *
   * @param bool $includeDisabled
   *   (optional, default = TRUE) When TRUE, disabled children are included.
   *   When FALSE, they are not.
   * @param bool $includeHidden
   *   (optional, default = FALSE) When TRUE, hidden children are included.
   *   When FALSE, they are not.
   *
   * @return int[]
   *   Returns an unordered list of descendant entity IDs.
   *
   * @see ::findAncestorFolderIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isAncestorOfFolderId()
   * @see ::isDescendantOfFolderId()
   */
  public function findDescendantIds(
    bool $includeDisabled = TRUE,
    bool $includeHidden = FALSE);

  /**
   * Returns a list of descendant item IDs based upon their owner ID.
   *
   * The returned list includes the entity IDs of children, their children,
   * and so on for all descendants of this item.
   *
   * The $ownerUid and $match arguments work together to constrain this list.
   * By default, all descendants for any owner are returned. If $ownerUid is
   * set and $match is TRUE, only those descendants with $ownerUid as the
   * owner are returned. If $ownerUid is set and $match is FALSE, only those
   * descendants that are NOT owned by $ownerUid are returned.
   *
   * System hidden and disabled items are included in the list.
   *
   * <B>Example usage</B>
   * Find all descendants owned by the current user, and all descendants
   * not owned by the current user:
   * @code
   *   $allIdsOwnedBy = $this->FindDescendantIdsByOwner(\Drupal::currentUser()->id());
   *   $allIdsNotOwnedBy = $this->FindDescendantIdsByOwner(\Drupal::currentUser()->id(), FALSE);
   * @endcode
   *
   * @param int $ownerUid
   *   (optional, default = FolderShareInterface::ANY_USER_ID) The user ID
   *   to use when looking up descendants. If the ID is not given or
   *   FolderShareInterface::ANY_USER_ID, then all descendants are returned,
   *   regardless of their owner.
   * @param bool $match
   *   (optional, default = TRUE = include matching owner IDs) When TRUE,
   *   all entities with an owner ID that matches the given $ownerUid are
   *   returned. When FALSE all entities that DO NOT match the given
   *   $ownerUid are returned.
   *
   * @return int[]
   *   Returns an unordered list of descendant entity IDs.
   *
   * @see ::findAncestorFolderIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isAncestorOfFolderId()
   * @see ::isDescendantOfFolderId()
   */
  public function findDescendantIdsByOwnerId(
    int $ownerUid,
    bool $match = TRUE);

  /**
   * Returns a list of descendant item IDs based upon their root ID.
   *
   * The returned list includes the entity IDs of children, their children,
   * and so on for all descendants of this item.
   *
   * The $rootId and $match arguments work together to constrain this list.
   * By default, all descendants with any root ID are returned. If $rootId is
   * set and $match is TRUE, only those descendants with $rootId as the
   * root are returned. If $rootId is set and $match is FALSE, only those
   * descendants that do NOT have $rootId as their root are returned.
   *
   * System hidden and disabled items are included in the list.
   *
   * @param int $rootId
   *   (optional, default = FolderShareInterface::ANY_ITEM_ID) The entity ID
   *   to use when looking up descendants. If the ID is not given or
   *   FolderShareInterface::ANY_ITEM_ID, then all descendants are returned,
   *   regardless of their root ID.
   * @param bool $match
   *   (optional, default = TRUE = include matching root IDs) When TRUE,
   *   all entities with a root ID that matches the given $rootId are
   *   returned. When FALSE all entities that DO NOT match the given
   *   $rootId are returned.
   *
   * @return int[]
   *   Returns an unordered list of descendant entity IDs.
   *
   * @see ::findAncestorFolderIds()
   * @see ::findDescendantIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isAncestorOfFolderId()
   * @see ::isDescendantOfFolderId()
   */
  public function findDescendantIdsByRootId(
    int $rootId,
    bool $match = TRUE);

  /**
   * Returns TRUE if this item is a descendant of the indicated entity.
   *
   * System hidden and disabled items are considered in this test.
   *
   * @param int $proposedAncestorId
   *   The entity ID of a proposed ancestor of this item.
   *
   * @return bool
   *   TRUE if this item is a descendant of the proposed ancestor, and
   *   FALSE otherwise. If the proposed ancestor's ID is invalid,
   *   FALSE is returned.
   *
   * @see ::findAncestorFolderIds()
   * @see ::findDescendantIds()
   * @see ::findDescendantIdsByOwner()
   * @see ::isAncestorOfFolderId()
   */
  public function isDescendantOfFolderId(int $proposedAncestorId);

  /*---------------------------------------------------------------------
   *
   * Root folder field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns this item's root item.
   *
   * If this item is a root item, it returns itself.
   *
   * Applications may check if this item is a root item by any of the
   * following, in fastest to slowest order:
   *
   * @code
   *   $isRoot = ($this->getParentFolderId() == FALSE);
   *   $isRoot = $this->isRootItem();
   *   $isRoot = ($this->id() == $this->getRootItemId());
   *   $isRoot = ($this == $this->getRootItem());
   * @endcode
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the root item, or this item if the item is a root item.
   *
   * @see ::getParentFolder()
   * @see ::getParentFolderId()
   * @see ::getRootItemId()
   * @see ::isRootItem()
   */
  public function getRootItem();

  /**
   * Returns this item's root ID.
   *
   * If this item is a root item, it returns its own ID.
   *
   * Applications may check if this item is a root item by any
   * of the following, in fastest to slowest order:
   *
   * @code
   *   $isRoot = ($this->getParentFolderId() == FolderShareInterface::USER_ROOT_LIST);
   *   $isRoot = $this->isRootItem();
   *   $isRoot = ($this->id() == $this->getRootItemId());
   *   $isRoot = ($this == $this->getRootItem());
   * @endcode
   *
   * @return int
   *   Returns the root ID, or this item's ID if this item is a root item.
   *
   * @see ::getParentFolder()
   * @see ::getParentFolderId()
   * @see ::getRootItem()
   * @see ::isRootItem()
   */
  public function getRootItemId();

  /**
   * Returns TRUE if this item is a root item, and FALSE otherwise.
   *
   * Applications may check if this item is a root item by any
   * of the following, in fastest to slowest order:
   *
   * @code
   *   $isRoot = ($this->getParentFolderId() == FolderShareInterface::USER_ROOT_LIST);
   *   $isRoot = $this->isRootItem();
   *   $isRoot = ($this->id() == $this->getRootItemId());
   *   $isRoot = ($this == $this->getRootItem());
   * @endcode
   *
   * @return bool
   *   Returns TRUE if this item is a root item.
   *
   * @see ::getParentFolder()
   * @see ::getParentFolderId()
   * @see ::getRootItem()
   * @see ::getRootItemId()
   * @see ::isParentFolder()
   */
  public function isRootItem();

  /*---------------------------------------------------------------------
   *
   * Paths.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds a string path to the item.
   *
   * The returned path is a slash (/) followed by a series of slash-separated
   * ancestor names, ending with the item's own name.
   *
   * @return string
   *   Returns a string path to the item.
   */
  public function getPath();

  /*---------------------------------------------------------------------
   *
   * Folder operations.
   *
   *---------------------------------------------------------------------*/
  /**
   * Changes the owner of this item, and optionally all descendants.
   *
   * The owner user ID of this item is changed to the indicated user.
   * If $changeDescendants is TRUE, the owner user ID of all of this item's
   * descendants is changed as well.
   *
   * The user ID is not validated. It is presumed to be a valid entity ID
   * for a User entity. It should not be negative.
   *
   * If this is a root item, then the item's name is checked for a collision
   * with another root item with the same name in the new owner's root list.
   * If there is a collision, an exception is thrown and no change is made.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Background change ownership</B>
   * When $changeDescendants is TRUE, this method schedules background tasks
   * to traverse the folder tree and update descendants. This will delay
   * completion of the change to a time in the future that depends upon
   * the size of the folder tree being changed and server load.
   *
   * <B>Process locks</B>
   * This method locks the item's root folder tree for exclusive use during
   * the change. This will prevent any other edit operation from being
   * performed on the same folder tree until the change completes. When
   * background tasks are used, unlocking the root folder tree does not
   * occur until the last descendant is changed.
   *
   * <B>Post-operation hooks</B>
   * This method calls the "hook_foldershare_post_operation_change_owner" hook.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is changed.
   *
   * @param int $uid
   *   The owner user ID for the new owner of the folder tree. The ID
   *   is not validated and is presumed to be that of a User entity.
   * @param bool $changeDescendants
   *   (optional, default = FALSE) When FALSE, only this item's ownership
   *   is changed. When TRUE, the ownership of all of this item's descendants
   *   is updated as well.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if this is a root item and it's name is
   *   already in use in the root list of the new user.
   *
   * @see ::setOwnerId()
   * @see \Drupal\user\EntityOwnerInterface::getOwner()
   * @see \Drupal\user\EntityOwnerInterface::getOwnerId()
   */
  public function changeOwnerId(int $uid, bool $changeDescendants = FALSE);

  /**
   * Creates a new folder with the given name as a child of this folder.
   *
   * The current item must be a folder. An exception is thrown if the item
   * is a file, image, media, or other kind.
   *
   * If the name is empty, it is set to a default.
   *
   * The name is checked for uniqueness within the parent folder.
   * If needed, a sequence number is appended before the extension(s) to
   * make it unique (e.g. 'My folder 12').
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * FolderShare folder entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_new_folder" hook is called after the
   * folder is created.
   *
   * <B>Process locks</B>
   * This method locks the root folder tree for exclusive use during
   * creation of the folder. This will prevent any other edit operation from
   * being performed on the same folder tree until the creation completes.
   *
   * <B>Activity log</B>
   * This method posts a log message after the folder is created.
   *
   * @param string $name
   *   (optional, default = '') The name for the new folder. If the name is
   *   empty, it is set to a default name.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, the entity will be automatically
   *   renamed, if needed, to insure that it is unique within the folder.
   *   When FALSE, non-unique names cause an exception to be thrown.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new folder.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the new folder. The folder has been saved.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the name is already in use.
   *
   * @see ::isFolder()
   */
  public function createFolder(
    string $name = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1));

  /**
   * Creates a new object with the given name as a child of this folder.
   *
   * The current item must be a folder. An exception is thrown if the item
   * is a file, image, media, or other kind.
   *
   * If the name is empty, it is set to a default.
   *
   * The name is checked for uniqueness within the parent folder.
   * If needed, a sequence number is appended before the extension(s) to
   * make it unique (e.g. 'My folder 12').
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * FolderShare object entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_new_object" hook is called after the
   * object is created.
   *
   * <B>Process locks</B>
   * This method locks the root folder tree for exclusive use during
   * creation of the object. This will prevent any other edit operation from
   * being performed on the same folder tree until the creation completes.
   *
   * <B>Activity log</B>
   * This method posts a log message after the object is created.
   *
   * @param string $name
   *   (optional, default = '') The name for the new object. If the name is
   *   empty, it is set to a default name.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, the entity will be automatically
   *   renamed, if needed, to insure that it is unique within the folder.
   *   When FALSE, non-unique names cause an exception to be thrown.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new object.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the new object. The object has been saved.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the name is already in use.
   *
   * @see ::isFolder()
   * @see ::isObject()
   */
  public function createObject(
    string $name = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1));

  /**
   * Copies this item into the user's root list.
   *
   * A copy of this item is created and added to the current user's root list.
   * The copy is owned by the current user and given default access grants
   * that give the user, and only the user, access.
   *
   * If this item is a folder, copying recurses through all descendants to
   * replicate the folder tree. Copied descendants retain the same names
   * and field values as the originals, and organized in the same tree
   * structure.
   *
   * File, image, and media items are copied along with copies of their
   * underlying File and Media entities.
   *
   * Copying marks each new folder as disabled until all children have been
   * copied into the folder. UIs may show disabled items specially and prevent
   * operations until the item is enabled.
   *
   * The copy will fail if a lock cannot be obtained on the original and
   * the root list. The copy will fail and be incomplete if any of the
   * children cannot be locked as they are copied. Everything that can be
   * copied will be.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Background copy</B>
   * File copies occur immediately, but folder copies schedule background
   * tasks to traverse the folder tree and update descendants. This will
   * delay completion of the copy to a time in the future that depends upon
   * the size of the folder tree being copied and server load.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare entity and a default MIME type.
   *
   * If this is a file or image, the
   * "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the root folder to get a list of filename extensions allowed for
   * copied files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * For compatibility with the File module, the "file_copy" hook is called
   * for each copied file, passing it the destination and source File entities.
   *
   * The "hook_foldershare_post_operation_copy" hook is called after each
   * item is copied.
   *
   * <B>Process locks</B>
   * This method locks the root folder tree of the item being copied, and
   * the user's root list, while copying items.
   *
   * <B>Activity log</B>
   * This method posts a log message each time an item is copied.
   *
   * @param string $newName
   *   (optional, default = '' = no name change) When empty, the copy will
   *   have the same name as the original. When a new name is not empty,
   *   the given name will be used as the name for the copy.
   * @param bool $allowRename
   *   (optional, default = FALSE) When FALSE, the copy uses the same name
   *   as the original, or the given new name. If there is already an item
   *   with the same name in the user's root list, an exception is thrown.
   *   When TRUE, the copy starts with the same name as the original, or the
   *   given new name, and then the name is modified to create a unique name
   *   in the user's root list.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the new root item.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   If $newName is not empty, an exception is thrown if the name is not
   *   legal or if this is a file or image and the name's filename extension
   *   is not allowed for the site. If $allowRename is FALSE, an exception
   *   is thrown if the name is already in use in the user's root list.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   For files, throws an exception if a serious system error occurs while
   *   duplicating the underlying local file. System errors may indicate a
   *   file system has become unreadable/unwritable, is full, or is offline.
   *
   * @see ::copyToFolder()
   * @see ::createFolder()
   */
  public function copyToRoot(string $newName = '', bool $allowRename = FALSE);

  /**
   * Copies this item into a selected folder.
   *
   * A copy of this item is created and added to the given folder.
   * The copy is owned by the current user.
   *
   * If this item is a folder, copying recurses through all descendants to
   * replicate the folder tree. Copied descendants retain the same names
   * and field values as the originals, and organized in the same tree
   * structure.
   *
   * File, image, and media items are copied along with copies of their
   * underlying File and Media entities.
   *
   * Copying marks each new folder as disabled until all children have been
   * copied into the folder. UIs may show disabled items specially and prevent
   * operations until the item is enabled.
   *
   * The copy will fail if a lock cannot be obtained on the original and
   * the new parent. The copy will fail and be incomplete if any of the
   * children cannot be locked as they are copied. Everything that can be
   * copied will be.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Background copy</B>
   * File copies occur immediately, but folder copies schedule background
   * tasks to traverse the folder tree and update descendants. This will
   * delay completion of the copy to a time in the future that depends upon
   * the size of the folder tree being copied and server load.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * copied files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * For compatibility with the File module, the "file_copy" hook is called
   * for each copied file, passing it the destination and source File entities.
   *
   * The "hook_foldershare_post_operation_copy" hook is called after each
   * item is copied.
   *
   * <B>Process locks</B>
   * This method locks the root folder tree of the item being copied, and
   * the root folder tree of the destination, while copying items.
   *
   * <B>Activity log</B>
   * This method posts a log message each time an item is copied.
   *
   * @param \Drupal\foldershare\FolderShareInterface $destination
   *   (optional, default = NULL = copy to the root list) The folder
   *   into which to place the copy. When NULL, the copy is added to the
   *   user's root list (see copyToRoot()).
   * @param string $newName
   *   (optional, default = '' = no name change) When empty, the copy will
   *   have the same name as the original. When a new name is not empty,
   *   the given name will be used as the name for the copy.
   * @param bool $allowRename
   *   (optional, default = FALSE) When FALSE, the copy uses the same name
   *   as the original, or the given new name. If there is already an item
   *   with the same name in the destination, an exception is thrown.
   *   When TRUE, the copy starts with the same name as the original, or the
   *   given new name, and then the name is modified to create a unique name
   *   within the destination.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the new item.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   If $newName is not empty, an exception is thrown if the name is not
   *   legal or if this is a file or image and the name's filename extension
   *   is not allowed for the site. If $allowRename is FALSE, an exception
   *   is thrown if the name is in use already in the destination.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   For files, throws an exception if a serious system error occurs while
   *   duplicating the underlying local file. System errors may indicate a
   *   file system has become unreadable/unwritable, is full, or is offline.
   *
   * @see ::copyToRoot()
   * @see ::createFolder()
   */
  public function copyToFolder(
    FolderShareInterface $destination = NULL,
    string $newName = '',
    bool $allowRename = FALSE);

  /**
   * Deletes this item and all of its descendants.
   *
   * Implements \Drupal\Core\Entity\EntityInterface::delete.
   * Overrides \Drupal\Core\Entity\Entity::delete.
   *
   * The item root's folder tree is locked for the duration of the operation.
   * This will prevent any other edit operation from being performed on the
   * same root folder tree until the deletion completes.
   *
   * If this item is a file, image, or media item, the underlying File or
   * Media entity is deleted along with the item. After deletion, the root
   * folder tree is unlocked.
   *
   * If this item is a folder, the folder is marked as hidden so that it is
   * no longer listed in file/folder listings. A background task is then
   * scheduled to continue deletion by recursively traversing through the
   * folder's descendants. After the folder and descendants have been
   * deleted, the root folder tree is unlocked. Because deletion executes as
   * a background task, completion of the delete will occur after this
   * method returns and at a time in the future that depends upon the size
   * of the folder tree being deleted and server load.
   *
   * System hidden and disabled items can be deleted.
   *
   * <B>Background delete</B>
   * File deletion occurs immediately, but folder deletion schedules background
   * tasks to traverse the folder tree and delete descendants. This will
   * delay completion of the delete to a time in the future that depends upon
   * the size of the folder tree being deleted and server load.
   *
   * <B>Process locks</B>
   * This method locks the item root's folder tree for exclusive use during
   * the delete. This will prevent any other edit operation from being
   * performed on the same root folder tree until the deletion completes.
   * When deletion requires scheduled background tasks, unlocking the root
   * folder tree does not occur until the last descendant is deleted.
   *
   * <B>Hooks</B>
   * This method calls the "hook_foldershare_post_operation_delete" hook as
   * items are deleted. When deletion requires scheduled background tasks,
   * deletion hooks are called as those tasks are serviced.
   *
   * <B>Activity log</B>
   * This method posts a log message each time an item is deleted. When
   * deletion requires scheduled background tasks, log messages are posted
   * as those tasks are serviced.
   *
   * @param bool $useTask
   *   (optional, default = TRUE) When TRUE, the delete is executing in a
   *   direct response to a user request that is still in progress, and it
   *   should therefore spawn a background task and return quickly.
   *   When FALSE, the delete should not spawn a background task and instead
   *   do the work immediately.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   *
   * @see ::isSystemHidden()
   */
  public function delete(bool $useTask = TRUE);

  /**
   * Duplicates this item into the same parent folder or the user's root list.
   *
   * If this item is in a folder, the item is duplicated into the same folder
   * and given a unique name (e.g. "myitem" becomes "mytime 1").
   *
   * If this item is in a root list (whether the user's or another root list),
   * it is duplicated into the user's root list and given a unique name.
   *
   * The copied files and folders are owned by the current user.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Background duplicate</B>
   * File copies occur immediately, but folder copies schedule background
   * tasks to traverse the folder tree and update descendants. This will
   * delay completion of the copy to a time in the future that depends upon
   * the size of the folder tree being copied and server load.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * copied files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * For compatibility with the File module, the "file_copy" hook is called
   * for each copied file, passing it the destination and source File entities.
   *
   * The "hook_foldershare_post_operation_copy" hook is called after each
   * item is copied.
   *
   * <B>Process locks</B>
   * This method locks the item's root folder tree for exclusive use during
   * the duplication. This will prevent any other edit operation from
   * being performed on the same folder tree until the move completes.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is copied.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if a unique name for the duplicate could not be
   *   created.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   For files, throws an exception if a serious system error occurs while
   *   duplicating the underlying local file. System errors may indicate a
   *   file system has become unreadable/unwritable, is full, or is offline.
   *
   * @see ::copyToFolder()
   * @see ::copyToRoot()
   */
  public function duplicate();

  /**
   * Moves this item into the user's root list.
   *
   * The item's current root folder tree, and the item's own folder tree,
   * are both locked at the start of the operation. This will prevent any
   * other edit operation from being performed on either folder tree. After
   * the item is moved to become a root item, the original root folder tree
   * is unlocked. The lock on the item's own folder tree remains until all
   * descendants have been updated and the move completes.
   *
   * This item's parent and root IDs are updated to move it into the root list.
   * The item is then given default access grants that give the user, and
   * only the user, access.
   *
   * If this item is a folder, a background task is scheduled to complete the
   * move by recursively traversing through the folder's descendants to set
   * each one's root ID. After all descendants have been updated, the root
   * folder tree is unlocked. Because the move executes as a background task,
   * completion of the move will occur after this method returns and at a
   * time in the future that depends upon the size of the folder tree being
   * moved and server load.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Background move</B>
   * File moves occur immediately, but folder moves schedule background
   * tasks to traverse the folder tree and update descendants. This will
   * delay completion of the move to a time in the future that depends upon
   * the size of the folder tree being moved and server load.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * FolderShare entity being moved and a default MIME type. The hook is not
   * called for an item's descendants since they have not moved and remain
   * descendants of the original item.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the root list to get a list of filename extensions allowed for
   * moved files. The default is the module's site-wide list of allowed
   * extensions. The hook is not called for an item's descendants since they
   * have not moved and remain descendants of the original item.
   *
   * For compatibility with the File module, the "file_move" hook is called
   * upon completion of each file move, passing it the source File entity.
   *
   * The "hook_foldershare_post_operation_move" hook is called after the
   * move is done. The hook is not called for an item's descendants
   * since they have not moved and remain descendants of the original item.
   *
   * <B>Process locks</B>
   * This method locks the item's original root folder tree, and the item's
   * own folder tree for exclusive use during the move. This will prevent
   * any other edit operation from being performed on the same folder trees
   * until the move completes. The original root folder tree lock is released
   * as soon as the item has moved to the root list. When moves require
   * scheduled background tasks, unlocking the item's own root folder tree
   * does not occur until the last descendant is updated.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is moved. Log messages
   * are not posted as the item's descendants are updated since they have
   * not moved and remain descendants of the original item.
   *
   * @param string $newName
   *   (optional, default = '' = don't rename) The proposed new name for
   *   entity once it is moved into the root folder list. If the name is
   *   empty, the folder is not renamed.
   * @param bool $allowRename
   *   (optional, default = FALSE) When FALSE, the moved item keeps its
   *   original name, or is renamed to the given new name. If there is
   *   already an item with the same name in the user's root list, an
   *   exception is thrown. When TRUE, the move starts with the original
   *   name, or the given new name, and then the name is modified to create
   *   a unique name within the user's root list.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   If $newName is not empty, an exception is thrown if the name is not
   *   legal or if this is a file or image and the name's filename extension
   *   is not allowed for the site. If $allowRename is FALSE, an exception
   *   is thrown if the name is in use already in the user's root list.
   *
   * @see ::moveToFolder()
   */
  public function moveToRoot(
    string $newName = '',
    bool $allowRename = FALSE);

  /**
   * Moves this item to a new destination folder.
   *
   * The item's current root folder tree and the destination's root folder
   * tree are both locked at the start of the operation. This will prevent any
   * other edit operation from being performed on either folder tree. After
   * the item is moved, the original root folder tree is unlocked. The lock
   * on the destination's root folder tree remains until all descendants have
   * been updated and the move completes.
   *
   * This item's parent and root IDs are updated to move it into the
   * destination. The item's access grants are cleared.
   *
   * If this item is a folder, a background task is scheduled to complete the
   * move by recursively traversing through the folder's descendants to set
   * each one's root ID. After all descendants have been updated, the
   * destination root folder tree is unlocked. Because the move executes as
   * a background task, completion of the move will occur after this method
   * returns and at a time in the future that depends upon the size of the
   * folder tree being moved and server load.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * FolderShare entity being moved and a default MIME type. The hook is not
   * called for an item's descendants since they have not moved and remain
   * descendants of the original item.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * moved files. The default is the module's site-wide list of allowed
   * extensions. The hook is not called for an item's descendants since they
   * have not moved and remain descendants of the original item.
   *
   * For compatibility with the File module, the "file_move" hook is called
   * upon completion of each file move, passing it the source File entity.
   *
   * The "hook_foldershare_post_operation_move" hook is called after the
   * move is done. The hook is not called for an item's descendants
   * since they have not moved and remain descendants of the original item.
   *
   * <B>Background move</B>
   * File moves occur immediately, but folder moves schedule background
   * tasks to traverse the folder tree and update descendants. This will
   * delay completion of the move to a time in the future that depends upon
   * the size of the folder tree being moved and server load.
   *
   * <B>Process locks</B>
   * This method locks the item's original root folder tree and the
   * destination's root folder tree for exclusive use during the move. This
   * will prevent any other edit operation from being performed on the same
   * folder trees until the move completes. The original root folder tree
   * lock is released as soon as the item has moved to the destination.
   * When moves require scheduled background tasks, unlocking the destination
   * root folder tree does not occur until the last descendant is updated.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is moved. Log messages
   * are not posted as the item's descendants are updated since they have
   * not moved and remain descendants of the original item.
   *
   * @param \Drupal\foldershare\FolderShareInterface $destination
   *   (optional, default = NULL = move to root) The destination folder
   *   into which to move the item.
   * @param string $newName
   *   (optional, default = '' = don't rename) The proposed new name for
   *   entity once it is moved into the destination folder. If the name is
   *   empty, the item is not renamed.
   * @param bool $allowRename
   *   (optional, default = FALSE) When FALSE, the copy uses the same name
   *   as the original, or the given new name. If there is already an item
   *   with the same name in the destination, an exception is thrown.
   *   When TRUE, the copy starts with the same name as the original, or the
   *   given new name, and then the name is modified to create a unique name
   *   within the destination.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   If $newName is not empty, an exception is thrown if the name is not
   *   legal or if this is a file or image and the name's filename extension
   *   is not allowed for the site. If $allowRename is FALSE, an exception
   *   is thrown if the name is in use already in the destination.
   *
   * @see ::moveToRoot()
   */
  public function moveToFolder(
    FolderShareInterface $destination = NULL,
    string $newName = '',
    bool $allowRename = FALSE);

  /**
   * Renames this item to use the given name.
   *
   * The item's name is changed. If the item is a file, the underling File or
   * Media entity name is also changed. File names are subject to file name
   * extension restrictions, if the site has any.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * FolderShare entity being renamed and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * renamed files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * The "hook_foldershare_post_operation_rename" hook is called after the
   * rename is done.
   *
   * <B>Process locks</B>
   * This method locks the item's root folder tree for the duration of the
   * rename. This will prevent any other edit operation from being performed
   * on the same folder tree until the rename is done.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is renamed.
   *
   * @param string $newName
   *   The new name for this item.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   If the item could not be renamed because the name is invalid or
   *   already in use.
   *
   * @see ::setName()
   */
  public function rename(string $newName);

  /**
   * Shares this root item by setting user IDs and access grants.
   *
   * If this is not a root item, this call has no effect.
   *
   * The given associative array is in the same format as that returned by
   * getAccessGrants(), and is used to completely reset all access grants
   * for this root item. Any prior grants are removed.
   *
   * The array must have user IDs as keys. For each user,
   * the array value must be an array of strings with one or two values:
   * - ['view'] = user only has view access.
   * - ['author'] = user only has author access (which is odd).
   * - ['author', 'view'] = user has view and author access.
   *
   * The owner of the root item is automaticaly included with both view and
   * author access, whether or not they are included in the given array.
   *
   * System hidden and disabled entities may be changed, however the module's
   * access control will deny access for any user that is not an administrator.
   *
   * <B>Hooks</B>
   * This "hook_foldershare_post_operation_share" hook is called after
   * sharing is changed.
   *
   * <B>Process locks</B>
   * This method locks the item's root folder tree for the duration of the
   * update. This will prevent any other edit operation from being performed
   * on the same folder tree until the update is done.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is updated.
   *
   * @param array $grants
   *   An unordered associative array with user IDs as keys and
   *   arrays as values. Array values contain strings indicating 'view' or
   *   'author' access.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   *
   * @see ::getAccessGrants()
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   * @see ::isAccessGranted()
   * @see ::unshare()
   */
  public function share(array $grants);

  /**
   * Unshares this root item for the indicated user and access.
   *
   * If this is not a root item, this call has no effect.
   *
   * Access grants are adjusted to remove the indicated user for shared access.
   * The $access argument may be 'view' or 'author', or left empty to unshare
   * for both.
   *
   * <B>Hooks</B>
   * This "hook_foldershare_post_operation_share" hook is called after
   * sharing is changed.
   *
   * <B>Process locks</B>
   * This method locks the item's root folder tree for the duration of the
   * update. This will prevent any other edit operation from being performed
   * on the same folder tree until the update is done.
   *
   * <B>Activity log</B>
   * This method posts a log message after the item is updated.
   *
   * @param int $uid
   *   The user ID for the user to remove from the item's access grants.
   * @param string $access
   *   (optional, default = '' = view and author) The access grant to remove.
   *   This is either 'view' or 'author', or an empty string to remove both.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   *
   * @see ::getAccessGrants()
   * @see ::getAccessGrantAuthorUserIds()
   * @see ::getAccessGrantViewUserIds()
   * @see ::isAccessGranted()
   * @see ::share()
   */
  public function unshare(int $uid, string $access);

  /*---------------------------------------------------------------------
   *
   * File operations.
   *
   *---------------------------------------------------------------------*/
  /**
   * Creates and adds files to a folder from form uploaded files.
   *
   * When a file is uploaded via an HTTP form post from a browser, PHP
   * automatically saves the data into "upload" files saved in a
   * PHP-managed temporary directory. This method sweeps those uploaded
   * files, pulls out the ones associated with the named form field,
   * and adds them to this folder with their original names.
   *
   * If there are no uploaded files, this method returns immediately.
   *
   * If $allowRename is TRUE, Files will be automatically renamed, if needed,
   * to insure they have unique names within the folder.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate each file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * uploaded files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * This method locks this item's root folder tree for the duration of
   * the addition.
   *
   * <B>Activity log</B>
   * This method posts a log message after the files have been added.
   *
   * @param string $formFieldName
   *   The name of the form field with associated uploaded files pending.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if a file's name collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return array
   *   Returns an array with one entry per uploaded file. Each entry is an
   *   associative array with one or more of the following keys:
   *   - "filename": The client's original name for the file.
   *   - "error": An error message if the file could not be processed.
   *   - "foldershare": The FolderShare entity for the uploaded file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. No files
   *   will have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the field name is empty or this entity is
   *   not a folder.
   *
   * @see ::addFileFromLocalFile()
   * @see ::addFileFromInputStream()
   */
  public function addFilesFromFormUpload(
    string $formFieldName,
    bool $allowRename = TRUE,
    int $ownerUid = (-1));

  /**
   * Adds a PHP input stream file into this folder.
   *
   * When a file is uploaded via an HTTP post handled by a web services
   * "REST" resource, the file's data is available via the PHP input
   * stream. This method reads that stream, creates a file, and adds
   * that file to this folder with the given name.
   *
   * System hidden and disabled items are also affected.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * uploaded files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * This item's root folder tree is locked for exclusive editing access
   * for the duration of the addition.
   *
   * <B>Activity log</B>
   * This method posts a log message after the files have been added.
   *
   * @param string $filename
   *   The name for the new file.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if $filename collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the newly added FolderShare entity wrapping the file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the file name is empty or the entity is
   *   not a folder.
   *
   * @see ::addFileFromFormUpload()
   * @see ::addFileFromLocalFile()
   */
  public function addFileFromInputStream(
    string $filename,
    bool $allowRename = TRUE,
    int $ownerUid = (-1));

  /**
   * Adds a local file to this folder.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * added files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive use for the duration of
   * this operation.
   *
   * <B>Activity log</B>
   * This method posts a log message after the file has been added.
   *
   * @param string $uri
   *   The path to the local file.
   * @param string $filename
   *   (optional, default = '' = use URI filename) When not empty, the user
   *   visible name of the new FolderShare and File entity. Otherwise use
   *   the file name from the URI.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if $filename collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   * @param bool $moveFile
   *   (optional, default = TRUE) When TRUE, the indicated file is moved into
   *   the module's files directory. When FALSE, the indicated file is copied
   *   instead.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the newly added FolderShare entity wrapping the file. Returns
   *   NULL and takes no action if the URI is empty.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the file URI is empty or the entity is
   *   not a folder.
   *
   * @see ::addFileFromFormUpload()
   * @see ::addFileFromInputStream()
   */
  public function addFileFromLocalFile(
    string $uri,
    string $filename = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1),
    bool $moveFile = TRUE);

  /*---------------------------------------------------------------------
   *
   * Archive operations.
   *
   *---------------------------------------------------------------------*/
  /**
   * Archives the given items to a new archive added to this folder.
   *
   * A new ZIP archive is created in this folder and all of the given,
   * items, and their children, recursively, are added to the archive.
   *
   * All items must be children of this item.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_mime_type_alter" hook is called with the unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for the current folder to get a list of filename extensions allowed for
   * files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * archive file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The root folder trees above the items added to the archive are locked
   * for exclusive use during the operation.
   *
   * <B>Activity log</B>
   * This method posts a log message after the archive is created.
   *
   * @param \Drupal\foldershare\FolderShareInterface[] $items
   *   An array of children of this folder that are to be included in a
   *   new archive added as a new child of this folder.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the FolderShare entity for the new archive.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Thrown if an access lock on this folder could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Thrown if this item is not a folder or root folder, or if any of
   *   the children in the array are not children of this folder.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Thrown if the archive file could not be created, such as if the
   *   temporary directory does not exist or is not writable, if a temporary
   *   file for the archive could not be created, or if any of the file
   *   children of this item could not be read and saved into the archive.
   */
  public function archiveToFolder(array $items);

  /**
   * Extracts all contents of this archive and adds them to the parent folder.
   *
   * This item must be a ZIP file. All of the contents of the ZIP file are
   * extracted and added as new files and folders into this file's parent
   * folder.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate each file
   * extracted from the archive, before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare entity and a default MIME type.
   *
   * The "hook_foldershare_allowed_filename_extensions_alter" hook is called
   * for each folder to get a list of filename extensions allowed for
   * added files. The default is the module's site-wide list of allowed
   * extensions.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after each
   * file has been added to the folder tree.
   *
   * The "hook_foldershare_post_operation_new_folder" hook is called after each
   * folder has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The root folder tree above the ZIP file is locked for exclusive use.
   *
   * <B>Activity log</B>
   * This method posts a log message after each file is uncompressed.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Thrown if an access lock on this folder could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Thrown if the archive is not a valid archive or it has become
   *   corrpted.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Thrown if the archive file could not be accessed or there was a
   *   problem creating any of the new files and folders from the archive.
   */
  public function unarchiveFromZip();

}
