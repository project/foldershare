<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a command plugin to delete files or folders, from a root list.
 *
 * This is one of several versions of the "Delete" command. This version is
 * only available on rootlists, not on folders. On a rootlist, it can delete
 * any root file or folder as long as it is owned by the current user.
 *
 * The command deletes all selected entities, as long as the user owns them.
 * Deletion recurses and deletes all folder content as well.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to delete.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_delete_on_rootlist",
 *  label           = @Translation("Delete"),
 *  menuNameDefault = @Translation("Delete..."),
 *  menuName        = @Translation("Delete..."),
 *  description     = @Translation("Delete selected top-level files and folders."),
 *  category        = "delete",
 *  weight          = 0,
 *  userConstraints = {
 *    "noadminpermission",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *    },
 *    "access"  = "update",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "ownership" = {
 *      "ownedbyuser",
 *    },
 *    "access"  = "delete",
 *  },
 * )
 */
class DeleteOnRootList extends DeleteBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

}
