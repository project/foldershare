<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\file\Entity\File;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\ManageHooks;

/**
 * Get/set FolderShare entity mime field.
 *
 * This trait includes get methods for FolderShare entity mime field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetMimeTrait {

  /*---------------------------------------------------------------------
   *
   * Mime field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getMimeType() {
    return $this->get('mime')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setMimeType(string $mime) {
    if (empty($mime) === TRUE) {
      return $this->setMimeTypeToDefault();
    }

    // Call hooks to alter the MIME type.
    $mime = ManageHooks::callHookMimeTypeAlter($this, $mime);

    // Set the underlying file, if any.
    if ($this->isFileOrImage() === TRUE) {
      // Files have a MIME type based on the underlying file.
      // Get the file.
      if ($this->isFile() === TRUE) {
        $fileId = $this->getFileId();
      }
      else {
        $fileId = $this->getImageId();
      }

      if ($fileId !== (-1) && ($file = File::load($fileId)) !== NULL) {
        // If the MIME type has changed, update the file.
        $oldMime = $file->getMimeType();
        if ($mime !== $oldMime) {
          $file->setMimeType($mime);
          self::saveFileWithRetry($file);
        }
      }
    }

    // Set the item.
    $this->set('mime', $mime);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setMimeTypeToDefault() {
    $mime = FolderShareInterface::GENERIC_MIME;
    $file = NULL;

    if ($this->isFolder() === TRUE) {
      // Folders have a fixed default MIME type.
      $mime = FolderShareInterface::FOLDER_MIME;
    }
    elseif ($this->isMedia() === TRUE) {
      // MIME entities have a fixed default MIME type.
      $mime = FolderShareInterface::MEDIA_MIME;
    }
    elseif ($this->isObject() === TRUE) {
      // MIME entities have a fixed default MIME type.
      $mime = FolderShareInterface::OBJECT_MIME;
    }
    elseif ($this->isFileOrImage() === TRUE) {
      // Files have a MIME type based on the underlying file.
      //
      // Get the file.
      if ($this->isFile() === TRUE) {
        $fileId = $this->getFileId();
      }
      else {
        $fileId = $this->getImageId();
      }

      if ($fileId !== (-1) && ($file = File::load($fileId)) !== NULL) {
        // Use the MIME type guesser service to guess the file's MIME type.
        $mime = \Drupal::service('file.mime_type.guesser')
          ->guessMimeType($file->getFilename());
      }
    }

    return $this->setMimeType($mime);
  }

  /*---------------------------------------------------------------------
   *
   * Mime utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns TRUE if a MIME type refers to an image type.
   *
   * MIME types are a concatenation of a top-level type name, a "/",
   * and a subtype name with an optional prefix, suffix, and parameters.
   * The top-level type name is one of several well-known names:
   * - application.
   * - audio.
   * - example.
   * - font.
   * - image.
   * - message.
   * - model.
   * - multipart.
   * - text.
   * - video.
   *
   * This function returns TRUE if the top-level type name is 'image'.
   *
   * @param string $mimeType
   *   The MIME type to check.
   *
   * @return bool
   *   Returns TRUE if the MIME type is for an image, and FALSE otherwise.
   */
  public static function isMimeTypeImage(string $mimeType) {
    list($topLevel,) = explode('/', $mimeType, 2);
    return ($topLevel === 'image');
  }

}
