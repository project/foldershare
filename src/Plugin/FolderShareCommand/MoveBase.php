<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Defines a command plugin to move files and folders.
 *
 * The command moves all selected files and folders to a chosen
 * destination folder or the root list.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to duplicate.
 * - 'destinationId': the destination folder, if any.
 *
 * @ingroup foldershare
 */
abstract class MoveBase extends CopyMoveBase {

  /*--------------------------------------------------------------------
   *
   * Configuration.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateDestinationConstraints() {
    if ($this->destinationValidated === TRUE) {
      return;
    }

    if ($this->selectionValidated === FALSE) {
      $this->validateSelectionConstraints();
    }

    // Handle special cases for destination.
    $destinationId = $this->getDestinationId();
    if ($destinationId === FolderShareInterface::ALL_ROOT_LIST) {
      // User-facing exception message.
      $message1 = $this->t(
        'Items cannot be moved to the administrator\'s "@title" list.',
        [
          '@title' => $this->rootItemsAllTitle,
        ]);
      $message2 = $this->t('Please select a subfolder instead.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message1, $message2));
    }

    if ($destinationId === FolderShareInterface::PUBLIC_ROOT_LIST) {
      // User-facing exception message.
      $message1 = $this->t(
        'Items cannot be moved to the "@title" list.',
        [
          '@title' => $this->rootItemsPublicTitle,
        ]);
      $message2 = $this->t('Please select a subfolder instead.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message1, $message2));
    }

    parent::validateDestinationConstraints();
  }

  /**
   * {@inheritdoc}
   */
  public function validateParameters() {
    if ($this->parametersValidated === TRUE) {
      // Already validated.
      return;
    }

    //
    // Validate destination.
    // ---------------------
    // There must be a destination ID. It must be a valid ID. It must
    // not be one of the selected items. And it must not be a descendant
    // of the selected items.
    //
    // A positive destination ID is for a folder to receive the selected
    // items.
    //
    // A negative destination ID is for the user's root list.
    $destinationId = $this->getDestinationId();

    if ($destinationId < 0) {
      // Destination is the user's root list. Nothing further to validate.
      $this->parametersValidated = TRUE;
      return;
    }

    // Destination is a specific folder.
    $destination = FolderShare::load($destinationId);

    if ($destination === NULL) {
      // Destination ID is not valid. This should have been caught
      // well before this validation stage.
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . " was called with an invalid entity ID '$destinationId'.");
    }

    // Verify that the destination is not in the selection. That would
    // be a copy to self, which is not valid.
    $selectionIds = $this->getSelectionIds();
    if (in_array($destinationId, $selectionIds) === TRUE) {
      // User-facing exception message.
      $message = $this->t('Items cannot be moved into themselves.');
      throw new ValidationException(
        FormatUtilities::createFormattedMessage($message));
    }

    // Verify that the destination is not a descendant of the selection.
    // That would be a recursive tree copy into itself.
    foreach ($selectionIds as $id) {
      $item = FolderShare::load($id);
      if ($item === NULL) {
        // The item does not exist.
        continue;
      }

      if ($destination->isDescendantOfFolderId($item->id()) === TRUE) {
        // User-facing exception message.
        $message = $this->t('Items cannot be moved into their own subfolders.');
        throw new ValidationException(
          FormatUtilities::createFormattedMessage($message));
      }

      unset($item);
    }

    unset($destination);

    $this->parametersValidated = TRUE;

    // Garbage collect.
    gc_collect_cycles();
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    $shared = $this->doesSelectionContainSharedWithOthers();
    if ($shared === FALSE) {
      return [];
    }

    $ids         = $this->getSelectionIds();
    $idsAndRoots = FolderShare::findRootIdsForIds($ids);
    $rootIds     = array_unique(array_values($idsAndRoots));
    $hasRoots    = (empty(array_intersect($ids, $rootIds)) === FALSE);
    $kindsAndIds = FolderShare::findKindsForIds($ids);

    if (count($ids) === 1) {
      $kinds = array_keys($kindsAndIds);

      if ($hasRoots === FALSE) {
        return $this->t(
          'Since this @kind is shared, moving it will affect other users.',
          [
            '@kind' => FolderShare::translateKind($kinds[0]),
          ]);
      }

      return $this->t(
        'Since this @kind is shared, moving it will end shared access and affect other users.',
        [
          '@kind' => FolderShare::translateKind($kinds[0]),
        ]);
    }

    if ($hasRoots === FALSE) {
      return $this->t(
        'Since some of these @kinds are shared, moving them will affect other users.',
        [
          '@kinds' => FolderShare::translateKinds('items'),
        ]);
    }

    return $this->t(
      'Since some of these @kinds are shared, moving them will end shared access and affect other users.',
      [
        '@kinds' => FolderShare::translateKinds('items'),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    $ids = $this->getSelectionIds();

    if (count($ids) === 1) {
      return $this->t(
        'Move "@name"',
        [
          '@name' => FolderShare::findNameForId($ids[0]),
        ]);
    }

    $kinds = FolderShare::translateKindsForIds($ids);
    return $this->t(
      "Move @kinds",
      [
        '@kinds' => $kinds['translation'],
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Move');
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $ids = $this->getSelectionIds();
    $destination = $this->getDestination();

    if ($destination === NULL) {
      FolderShare::moveToRootMultiple($ids);
    }
    else {
      FolderShare::moveToFolderMultiple($ids, $destination);
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $count = count($ids);
      if ($count <= 1) {
        $message = $this->t("The item has been moved.");
      }
      else {
        $message = $this->t(
          "@count items have been moved.",
          [
            '@count' => $count,
          ]);
      }
      $this->getMessenger()->addStatus($message);
    }
  }

}
