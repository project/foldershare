<?php

namespace Drupal\foldershare\Entity\Exception;

use Drupal\Component\Render\MarkupInterface;
use Throwable;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Defines an exception indicating that the requested file's download failed.
 *
 * @ingroup foldershare
 */
class FileDownloadFailedException extends HttpException
{
    /*--------------------------------------------------------------------
     *
     * Constructors.
     *
     *--------------------------------------------------------------------*/
    /**
     * Constructs an exception.
     *
     * @param string|MarkupInterface $message
     *   (optional, default = NULL) The message string or an instance of
     *   \Drupal\Component\Render\MarkupInterface. If NULL, a default
     *   message is used.
     * @param int $code
     *   (optional, default = 0) An error code.
     * @param Throwable $previous
     *   (optional, default = NULL) A previous exception that this extends.
     */
    public function __construct(
        $message = NULL,
        int $code = 0,
        Throwable $previous = NULL)
    {

        if (empty($message) === TRUE) {
            $message = $this->t('File download failed');
        }

        parent::__construct(500, $message, $previous, array(), $code);
    }
}
