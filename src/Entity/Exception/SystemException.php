<?php

namespace Drupal\foldershare\Entity\Exception;

/**
 * Defines an exception indicating a system error.
 *
 * System errors are probably catastrophic and are best handled by
 * stopping whatever is going on.
 *
 * @ingroup foldershare
 */
class SystemException extends RuntimeExceptionWithMarkup {

  /*--------------------------------------------------------------------
   *
   * Constructors.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs an exception.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface $message
   *   (optional, default = NULL) The message string or an instance of
   *   \Drupal\Component\Render\MarkupInterface. If NULL, a default
   *   message is used.
   * @param int $code
   *   (optional, default = 0) An error code.
   * @param \Throwable $previous
   *   (optional, default = NULL) A previous exception that this extends.
   */
  public function __construct(
    $message = NULL,
    int $code = 0,
    \Throwable $previous = NULL) {

    if (empty($message) === TRUE) {
      $message = $this->t('System error');
    }

    parent::__construct($message, $code, $previous);
  }

}
