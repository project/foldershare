<?php

namespace Drupal\foldershare\Entity\FolderShareScheduledTaskTraits;

use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\Utilities\CacheUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;

/**
 * Execute FolderShareScheduledTask entities.
 *
 * This trait includes execute methods that execute individual tasks,
 * ready tasks, or all tasks.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShareScheduledTask entity class. It is a
 * mechanism to group functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait ExecuteTrait {

  /*---------------------------------------------------------------------
   *
   * Execute enable/disable.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns TRUE if task execution is enabled, and FALSE otherwise.
   *
   * At the start of every process, this is TRUE and ready tasks will execute
   * each time executeTasks() is called.
   *
   * Task execution can be disabled FOR THE CURRENT PROCESS ONLY by calling
   * setTaskExecutionEnabled() with a FALSE argument.
   *
   * @return bool
   *   Returns TRUE if enabled.
   *
   * @see ::setTaskExecutionEnabled()
   * @see ::executeTasks()
   */
  public static function isTaskExecutionEnabled() {
    return self::$enabled;
  }

  /**
   * Enables or disables task execution.
   *
   * At the start of every process, this is TRUE and ready tasks will execute
   * each time executeTasks() is called.
   *
   * When set to FALSE, executeTasks() will return immediately, doing nothing.
   * This may be used to temporarily disable task execution FOR THE CURRENT
   * PROCESS ONLY. This has no effect on other processes or future processes.
   *
   * A common use of execution disabling is by drush, which needs to execute
   * commands without necessarily running pending tasks.
   *
   * @param bool $enable
   *   TRUE to enable, FALSE to disable.
   *
   * @see ::isTaskExecutionEnabled()
   * @see ::executeTasks()
   */
  public static function setTaskExecutionEnabled(bool $enable) {
    self::$enabled = $enable;
  }

  /*---------------------------------------------------------------------
   *
   * Execute tasks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Executes the task.
   *
   * The task does not have to be in the queue (though it is assumed to be)
   * and it does not have to have a scheduled time that marks it ready.
   *
   * The task is deleted prior to calling the task's callback.
   *
   * @see ::executeTasks()
   * @see ::finishTasks()
   */
  private function execute() {
    // Copy out the task's values.
    $callback      = $this->getCallback();
    $json          = $this->getParameters();
    $requester     = $this->getRequester();
    $comments      = $this->getComments();
    $started       = $this->getStartedTime();
    $executionTime = $this->getAccumulatedExecutionTime();
    $flags         = $this->getFlags();

    // Delete the task to reduce the chance that another process will
    // try to service the same task at the same time. This can still
    // happen and tasks must be written to consider this.
    $this->delete();

    // Decode JSON-encoded task parameters.
    if (empty($json) === TRUE) {
      $parameters = [];
    }
    else {
      $parameters = json_decode($json, TRUE, 512, JSON_OBJECT_AS_ARRAY);
      if ($parameters === NULL) {
        // The parameters could not be decoded! This should not happen
        // since they were encoded using json_encode() during task creation.
        // There is nothing we can do with the task.
        ManageLog::error(
          "Programmer error: Missing or malformed parameters for '@callback' task.",
          [
            '@callback' => $callback,
          ]);
        return;
      }
    }

    // Dispatch to the callback. The class and method were previously
    // validated when the task was created, so this call should not fail
    // with a PHP error. It still may fail due to a callback error.
    try {
      @call_user_func(
        $callback,
        $requester,
        $parameters,
        $started,
        $comments,
        $executionTime,
        $flags);
    }
    catch (\Exception $e) {
      // Unexpected exception.
      ManageLog::exception($e);
    }
  }

  /**
   * Executes ready tasks.
   *
   * All tasks with scheduled times equal to or earlier than the given
   * time stamp will be considered ready to run and executed in oldest to
   * newest order. Tasks are deleted just before they are executed. Tasks
   * may add more tasks.
   *
   * The method returns immediately, without executing tasks, if:
   * - Task execution has been disabled for the current process.
   * - The PHP ellapsed execution time is above the soft limit.
   * - There are no tasks queued.
   * - There are no tasks ready to run.
   *
   * Task execution in the current process can be disabled with
   * setTaskExecutionEnabled(). This only affects the current process, and
   * not other processes or future processes.
   *
   * @param int $timestamp
   *   The time used to find all ready tasks. Any task scheduled to run at
   *   this time, or earlier, is considered ready and will be executed.
   *   This is typically set to the current time, or the time at which an
   *   HTTP request was made.
   *
   * @see ::findNumberOfReadyTasks()
   * @see ::findReadyTaskIds()
   * @see ::isTaskExecutionEnabled()
   * @see ::setTaskExecutionEnabled()
   * @see \Drupal\foldershare\Utilities\LimitUtilities::aboveExecutionTimeLimit()
   * @see \Drupal\foldershare\EventSubscriber\FolderShareScheduledTaskHandler
   */
  public static function executeTasks(int $timestamp) {
    if (self::isTaskExecutionEnabled() === FALSE) {
      return;
    }

    // If execution time is already above the soft execution limit,
    // return immediately.
    if (LimitUtilities::aboveExecutionTimeLimit() === TRUE) {
      return;
    }

    //
    // Quick reject.
    // -------------
    // This function is often called after every page is sent to a user. It is
    // essential that it quickly decide if there is anything to do.
    try {
      if (self::findNumberOfReadyTasks($timestamp) === 0) {
        // Nothing to do.
        return;
      }
    }
    catch (\Exception $e) {
      // Query failed?
      return;
    }

    //
    // Get ready tasks.
    // ----------------
    // A task is ready if the current time is greater than or equal to
    // its scheduled time.
    try {
      $taskIds = self::findReadyTaskIds($timestamp);
    }
    catch (\Exception $e) {
      // Query failed?
      return;
    }

    //
    // Execute tasks.
    // --------------
    // Loop through the tasks and execute them.
    //
    // If a task fails to load, it has already been deleted. It may have
    // been serviced by another execution of this same method running in
    // another process after delivering a page for another user.
    foreach ($taskIds as $taskId) {
      $task = self::load($taskId);
      if ($task === NULL) {
        // The task has already been deleted.
        continue;
      }

      $task->execute();
      unset($task);

      // If execution time is already above the soft execution limit,
      // stop processing tasks.
      if (LimitUtilities::aboveExecutionTimeLimit() === TRUE) {
        break;
      }

      // After execution, some tasks may have re-scheduled themselves. Since
      // we have disabled the execution time limit, rescheduling may be due to
      // a memory limit. Flush everything we can and keep going.
      CacheUtilities::flushAllMemoryCaches();
      CacheUtilities::flushAllEntityCaches(FolderShare::ENTITY_TYPE_ID);
      gc_collect_cycles();
    }
  }

  /**
   * Executes all tasks to completion, except re-occurring tasks.
   *
   * All non-re-occurring tasks are executed to completion. Tasks are deleted
   * just before they are executed. Tasks may add more tasks.
   *
   * The method returns immediately, without executing tasks, if:
   * - Task execution has been disabled for the current process.
   * - There are no tasks queued.
   *
   * Task execution in the current process can be disabled with
   * setTaskExecutionEnabled(). This only affects the current process, and
   * not other processes or future processes.
   *
   * <B>Resource use</B>
   * Depending upon the number of tasks and their complexity, this method
   * may take a long time to finish all tasks and it may consume a lot of
   * memory.  For this reason, this method sets PHP execution time and
   * memory use limits to be unlimited.
   *
   * @return bool
   *   Returns TRUE on success, and FALSE if the task queue could not be fully
   *   drained. This may occur if there are repeating tasks in the queue
   *   that were not properly marked as repeating, or if there are tasks
   *   that are repeatedly re-queueing themselves because they are running
   *   into a problem that prevents them from completing.
   *
   * @see ::executeTasks()
   * @see ::findTaskIds()
   * @see ::isTaskExecutionEnabled()
   * @see ::setTaskExecutionEnabled()
   * @see \Drupal\foldershare\EventSubscriber\FolderShareScheduledTaskHandler
   */
  public static function finishTasks() {
    if (self::isTaskExecutionEnabled() === FALSE) {
      return TRUE;
    }

    LimitUtilities::setUnlimited();

    // Tasks are SUPPOSED to be marked whether they repeat or not. But it
    // is possible that a task is not properly marked. If a repeating task
    // is found that isn't marked as repeating, the loop below will run it
    // over and over and over forever. The loop will never exit.
    //
    // Tasks are SUPPOSED to finish if they aren't marked repeating. They
    // can re-queue themselves to execute multiple phases of a task, but
    // eventually they should finish. But if a non-repeating task keeps
    // blocking or crashing and cannot finish its work, it will keep
    // re-queueing and the loop below will run it over and over and over
    // forever. The loop will never exit.
    //
    // To provide a fail-safe, we define a maximum loop count. This is
    // intentionally high so that looping can process a very deep legitimate
    // task queue or long tasks that re-queue a lot.
    $maximumLoopCount = 500;

    // Loop over all tasks repeatedly until the only tasks left are those
    // that repeat. This drains the queue of non-repeating tasks, such as
    // those finishing large deletes, copies, moves, etc.
    $foundNonRepeating = TRUE;
    $i = 0;
    for (; $i < $maximumLoopCount && $foundNonRepeating === TRUE; ++$i) {
      // Get a list of task IDs. This list includes ALL tasks, whether or
      // not they have reached their scheduled time to execute.
      $taskIds = self::findTaskIds();
      if (empty($taskIds) === TRUE) {
        // No more tasks.
        break;
      }

      $foundNonRepeating = FALSE;

      // Execute all of the tasks that don't repeat.
      foreach ($taskIds as $taskId) {
        $task = self::load($taskId);
        if ($task === NULL) {
          // The task has already been deleted.
          continue;
        }

        if ($task->isRepeating() === FALSE) {
          $task->execute();
          $foundNonRepeating = TRUE;
        }
        unset($task);
      }

      // After execution, some tasks may have re-scheduled themselves. Since
      // we have disabled the execution time limit, rescheduling may be due to
      // a memory limit. Flush everything we can and keep going.
      CacheUtilities::flushAllMemoryCaches();
      CacheUtilities::flushAllEntityCaches(FolderShare::ENTITY_TYPE_ID);
      gc_collect_cycles();
    }

    if ($i >= $maximumLoopCount) {
      // The task queue hit the maximum loop count. The queue may contain
      // tasks that were not properly marked as repeating, or there may be
      // tasks that are constantly re-queueing because soemthing is going
      // wrong.
      return FALSE;
    }

    // The task queue was drained normally of all non-repeating tasks.
    return TRUE;
  }

}
