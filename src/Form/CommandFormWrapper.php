<?php

namespace Drupal\foldershare\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Url;

use Drupal\foldershare\Entity\Exception\RuntimeExceptionWithMarkup;
use Drupal\foldershare\Plugin\FolderShareCommandManager;
use Drupal\foldershare\Plugin\FolderShareCommand\FolderShareCommandInterface;
use Drupal\foldershare\Utilities\AjaxUtilities;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Creates a form that wraps a command plugin to prompt for its parameters.
 *
 * This form is invoked to prompt the user for additional plugin
 * command-specific parameters, beyond primary parameters for the parent
 * folder (if any), destination folder (if any), and selection (if any).
 * Each plugin has its own (optional) form fragment that prompts for its
 * parameters. This form page incorporates that fragment and adds a
 * page title and submit button. It further orchestrates creation of
 * the plugin and invokation of its functions.
 *
 * The form page accepts a URL with a single required 'encoded' parameter
 * that includes a base64-encoded JSON-encoded associative array that
 * has the plugin ID, configuration (parent, destination, selection),
 * and URL to get back to the invoking page.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
class CommandFormWrapper extends FormBase {

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The plugin manager for folder shared commands.
   *
   * @var \Drupal\foldershare\FolderShareCommand\FolderShareCommandManager
   */
  protected $commandPluginManager;

  /**
   * The messenger, set at construction time.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected $messenger;

  /*--------------------------------------------------------------------
   *
   * Fields - command.
   *
   *--------------------------------------------------------------------*/
  /**
   * An instance of the command plugin.
   *
   * The command instance is created using the plugin ID from the
   * URL parameters.
   *
   * @var \Drupal\foldershare\FolderShareCommand\FolderShareCommandInterface
   */
  protected $command;

  /**
   * The URL to redirect to (or back to) after form submission.
   *
   * The URL comes from the command parameters and indicates the original
   * page to return to after the command executes.
   *
   * @var string
   */
  protected $url;

  /**
   * The parent form ID that triggered the command.
   *
   * The parent form ID comes from the command parameters and indicates the
   * parent form ID that triggered the command. This is used to direct an
   * AJAX refresh back to that form when the command finishes.
   *
   * @var string
   */
  protected $parentFormId;

  /**
   * Whether to enable AJAX.
   *
   * The flag comes from the command parameters given to buildForm().
   * By default, this is FALSE, but it can be set TRUE if the caller
   * sets the flag in the parameters. This would be the case if the
   * caller has embedded the form within an AJAX dialog and therefore
   * requires that this form continue to use AJAX as well.
   *
   * @var bool
   */
  protected $enableAjax;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new form.
   *
   * @param \Drupal\foldershare\FolderShareCommand\FolderShareCommandManager $commandPluginManager
   *   The command plugin manager.
   * @param \Drupal\Core\Messenger\MessengerInterface $messenger
   *   The messenger.
   */
  public function __construct(
    FolderShareCommandManager $commandPluginManager,
    MessengerInterface $messenger) {

    // Save the plugin manager, which we'll need to create a plugin
    // instance based upon a parameter on the form's URL.
    $this->commandPluginManager = $commandPluginManager;
    $this->messenger = $messenger;

    $this->enableAjax = TRUE;
    $this->command = NULL;
    $this->url = '';
    $this->parentFormId = '';
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('foldershare.plugin.manager.foldersharecommand'),
      $container->get('messenger')
    );
  }

  /*--------------------------------------------------------------------
   *
   * Form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return mb_strtolower(str_replace('\\', '_', get_class($this)));
  }

  /*--------------------------------------------------------------------
   *
   * Form build.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildForm(
    array $form,
    FormStateInterface $formState = NULL,
    string $encoded = NULL) {

    //
    // Decode parameters.
    // ------------------
    // Decode the incoming URL's parameters, which include:
    // - The command plugin's ID.
    // - The configuration for that command, including a selection, parent,
    //   destination, etc.
    // - The URL of the page to return to.
    // - Whethe AJAX is enabled.
    $parameters = $this->decodeParameters($encoded);
    if (empty($parameters) === TRUE) {
      // No parameters? This should not be possible because the route
      // requires them.
      return $form;
    }

    $this->url          = $parameters['url'];
    $this->parentFormId = $parameters['parentFormId'];
    $this->enableAjax   = $parameters['enableAjax'];

    $forPage = ($this->enableAjax === FALSE);

    if ($this->command === NULL) {
      //
      // Instantiate plugin.
      // -------------------
      // On the first form build, instatiate the plugin. Thereafter the
      // plugin is serialized / unserialized as the form goes to the user,
      // comes back, returns to the user again, etc. Once the plugin is
      // instantiated, we don't need the plugin manager any more.
      //
      // Give the new plugin the incoming configuration. The configuration
      // may change as the plugin's forms do further prompting.
      $this->command = $this->commandPluginManager->createInstance(
        $parameters['pluginId'],
        $parameters['configuration']);

      $this->commandPluginManager = NULL;

      //
      // Pre-validate.
      // -------------
      // The invoker of this form should already have checked that the
      // parent and selection are valid for this command. But validate
      // again now to insure that internal cached values from validation
      // are set before we continue. If validation fails, there is no
      // point in building much of a form.
      try {
        $this->command->validateParentConstraints();
        $this->command->validateSelectionConstraints();
      }
      catch (RuntimeExceptionWithMarkup $e) {
        $this->getMessenger()->addError($e->getMarkup());
        return NULL;
      }
      catch (\Exception $e) {
        $this->getMessenger()->addError($e->getMessage());
        return NULL;
      }
    }

    //
    // Set up form.
    // ------------
    // The command provides its own form fragment to prompt for the
    // values it needs to complete its configuration.
    $commandFormWrapper = 'foldershare_command_form_wrapper';
    $form['#prefix'] = '<div id="' . $commandFormWrapper . '">';
    $form['#suffix'] = '</div>';

    // Attach libraries.
    $form['#attached']['library'][] = 'foldershare/foldershare.general';
    $form['#attributes']['class'][] = $parameters['pluginId'];
    $form['#attributes']['class'][] = 'foldershare-dialog';

    // Mark form as tree-structured and non-collapsable.
    $form['#tree'] = TRUE;

    // Disable caching the form. Dialogs are always specific to the
    // current situation.
    $form['#cache'] = [
      'max-age' => 0,
    ];

    //
    // Set the page title, if any.
    // ---------------------------
    // Get the command's page title. This may vary based on whether the
    // form is in a stand-alone page or in a dialog. The title may be empty.
    $form['#title'] = $this->command->getTitle($forPage);

    //
    // Set the description, if any.
    // ----------------------------
    // Get the command's description. This may vary based on whether the
    // form is in a stand-alone page or in a dialog.
    $description = $this->command->getDescription($forPage);
    if (empty($description) === FALSE) {
      if (is_array($description) === FALSE) {
        $primaryDescription = $description;
        $secondaryDescription = '';
      }
      elseif (count($description) === 1) {
        $primaryDescription = $description[0];
        $secondaryDescription = '';
      }
      else {
        $primaryDescription = $description[0];
        $secondaryDescription = $description[1];
      }

      if (empty($primaryDescription) === FALSE ||
          empty($secondaryDescription) === FALSE) {
        $form['foldershare-intro-description'] = [
          '#type'       => 'container',
          '#weight'     => -100,
          '#attributes' => [
            'class' => [
              'foldershare-intro-description',
            ],
          ],
        ];

        if (empty($primaryDescription) === FALSE) {
          $form['foldershare-intro-description']['foldershare-primary-description'] = [
            '#type'   => 'html_tag',
            '#tag'    => 'p',
            '#value'  => $primaryDescription,
            '#attributes' => [
              'class' => [
                'foldershare-primary-description',
              ],
            ],
          ];
        }

        if (empty($secondaryDescription) === FALSE) {
          $form['foldershare-intro-description']['foldershare-secondary-description'] = [
            '#type'   => 'html_tag',
            '#name'   => 'secondary-description',
            '#tag'    => 'p',
            '#value'  => $secondaryDescription,
            '#attributes' => [
              'class' => [
                'foldershare-secondary-description',
              ],
            ],
          ];
        }
      }
    }

    //
    // Add actions.
    // ------------
    // Add the submit button.
    $form['actions'] = [
      '#type'          => 'actions',
      '#weight'        => 1000,
      'submit'         => [
        '#type'        => 'submit',
        '#name'        => 'submit',
        '#value'       => $this->command->getSubmitButtonName(),
        '#button_type' => 'primary',
        '#attributes' => [
          'class' => [
            'dialog-submit',
          ],
        ],
      ],
    ];

    // When AJAX is enabled, add an AJAX callback to the submit button
    // and a cancel button.
    if ($this->enableAjax === TRUE) {
      $form['actions']['cancel'] = [
        '#type'  => 'button',
        '#name'  => 'cancel',
        '#value' => $this->t('Cancel'),
        '#attributes' => [
          'class' => [
              // Marking the button with "dialog-cancel" marks the button as
              // a cancel button that simply closes the dialog, without sending
              // anything to the server.
            'dialog-cancel',
          ],
        ],
      ];

      $form['actions']['submit']['#ajax'] = [
        'callback'   => [$this, 'submitFormAjax'],
        'event'      => 'click',
        'wrapper'    => $commandFormWrapper,
        'disable-refocus' => TRUE,
        'url'        => Url::fromRoute(
          'entity.foldersharecommand.plugin',
          [
            'encoded' => base64_encode(json_encode($parameters)),
          ]),
        'options'    => [
          'query'    => [
            'ajax_form' => 1,
          ],
        ],
        'progress'   => [
          'type'     => 'none',
        ],
      ];
    }

    //
    // Add command form.
    // -----------------
    // Add the command's form.
    $form = $this->command->buildConfigurationForm($form, $formState);

    return $form;
  }

  /*--------------------------------------------------------------------
   *
   * Form validate.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    // Validate the user's input for the form by passing them to the
    // command to validate.
    if ($this->command === NULL) {
      // Fail. No command?
      return;
    }

    //
    // Validate additions
    // ------------------
    // Let the command's form validate the user's input.
    $this->command->validateConfigurationForm($form, $formState);
    if ($formState->hasAnyErrors() === TRUE) {
      // The command's validation failed. There is no point in continuing.
      $formState->setRebuild(TRUE);
      return;
    }

    if ($formState->isRebuilding() === TRUE) {
      // The command directs that the form should be rebuilt and form use
      // should continue.
      return;
    }

    //
    // Validate rest
    // -------------
    // The command's configuration form should have validated whatever
    // additional parameters it has added. And earlier we validated the
    // parent and selection. But just to be sure, validate everything
    // one last time. Since validation skips work if it has already
    // been done, this will be fast if everything is validated fine already.
    try {
      $this->command->validateConfiguration();
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $this->getMessenger()->addError($e->getMarkup());
    }
    catch (\Exception $e) {
      // Validation failed. This should not be possible if all prior
      // validation checks occurred as they were supposed to.
      $this->getMessenger()->addError($e->getMessage());
    }
  }

  /*--------------------------------------------------------------------
   *
   * Form submit.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    // Command forms now use AJAX for submission so that they can return
    // errors within the dialog, or present an error or confirmation
    // dialog afterwards.
    //
    // This submit function is called when the dialog's submit button is
    // pressed. It's job is to:
    //
    // - Ignore anything but submit button presses (there should not be any).
    //
    // - Clear away any old errors since we only want to deal with new errors
    //   generated by the submit.
    //
    // - Submit the command's configuration, which typically executes the
    //   command!
    //
    // - Catch command execution errors and send them to the messenger.
    //
    // - If needed, redirect back to a starting page.
    //
    // BUT, with AJAX that last step changes. Instead of redirecting, this
    // submit function falls through to the AJAX submit function and THAT
    // function either redirects or presents a dialog with error messages.
    //
    // Validate submit.
    // ----------------
    // Reject unrecognized form responses, letting only submit go through.
    if ($this->command === NULL ||
        $this->command->isValidated() === FALSE ||
        $formState->getTriggeringElement()['#name'] !== 'submit') {
      // On cancel, return to the starting page. On anything else (there
      // shouldn't be anything), update the page as well.
      //
      // This chould never occur. The dialogs are marked so that cancel
      // automatically takes down the dialog.
      if ($this->enableAjax === TRUE) {
        // The AJAX submit function will handle this.
        return;
      }
      if ($this->url !== NULL) {
        $formState->setRedirectUrl(Url::fromUri($this->url));
      }
      return;
    }

    //
    // Clear old messages.
    // -------------------
    // Clear any pending messenger messages. While there should not be any,
    // if there are they will infiltrate any error dialog shown below. Since
    // we only want those dialogs to show errors with the command about to
    // be executed, clear pending messenger messages first.
    $this->getMessenger()->deleteAll();

    //
    // Submit command.
    // ---------------
    // Call the command's submit function, which executes the command.
    // Catch exceptions and send them to the messenger.
    try {
      $this->command->submitConfigurationForm($form, $formState);
    }
    catch (AccessDeniedHttpException $e) {
      $this->getMessenger()->addError(FormatUtilities::createFormattedMessage(
        $this->t('You do not have sufficient permissions.'),
        $this->t('The operation could not be completed.')));
    }
    catch (NotFoundHttpException $e) {
      $this->getMessenger()->addError(FormatUtilities::createFormattedMessage(
        $this->t('One or more items could not be found.'),
        $this->t('The operation could not be completed.')));
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $this->getMessenger()->addError($e->getMarkup());
    }
    catch (\Exception $e) {
      $this->getMessenger()->addError($e->getMessage());
    }

    //
    // Redirect or AJAX.
    // -----------------
    // Without AJAX, redirect to a returning URL. With AJAX, do nothing and
    // fall through to the AJAX submit function.
    if ($this->enableAjax === FALSE) {
      if ($this->url !== NULL) {
        $formState->setRedirectUrl(Url::fromUri($this->url));
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitFormAjax(array &$form, FormStateInterface $formState) {
    // Command forms now use AJAX as an ADDITIONAL step in form submission.
    // The regular submit function is called (see submitForm() above). And
    // then this AJAX-specific function is called.
    //
    // Since this function is called after the regular submit, this function
    // just does AJAX-specific cleanup. That clean up is:
    //
    // - Take down the dialog.
    //
    // - Get any error messages from execution of the command and show them
    //   in an error dialog.
    //
    // - Redirect to a final URL, if any.
    if ($formState->isRebuilding() === TRUE) {
      return $form;
    }

    //
    // Close dialog.
    // -------------
    // Respond to a cancel or unexpected trigger.
    $response = AjaxUtilities::createCloseModalDialogResponse();

    switch ($formState->getTriggeringElement()['#name']) {
      case 'cancel':
        // On cancel, close the dialog and return to the starting page.
        //
        // This should never occur. The dialogs are marked so that cancel
        // automatically takes down the dialog.
        return $response;

      case 'submit':
        break;

      default:
        // On any unrecognized trigger, the best we can do is force a
        // refresh of the page after taking down the dialog.
        // Redirect to a new page.
        return AjaxUtilities::createRedirectResponse($this->url, $response);
    }

    //
    // Show errors in a dialog.
    // ------------------------
    // If there are any messenger messages, show then in an error dialog.
    $msgs = $this->getMessenger()->all();
    if (isset($msgs[MessengerInterface::TYPE_ERROR]) === TRUE ||
        isset($msgs[MessengerInterface::TYPE_WARNING]) === TRUE) {
      $this->command = NULL;
      return AjaxUtilities::createMessengerErrorsDialogResponse($response);
    }
    elseif ($formState->hasAnyErrors() === TRUE) {
      $this->command = NULL;
      return AjaxUtilities::createFormErrorsDialogResponse($formState, $response);
    }

    //
    // Redirect.
    // ---------
    // After command execution, redirect or refresh as the command requires.
    $executeBehavior = $this->command->getExecuteBehavior();
    if (isset($msgs[MessengerInterface::TYPE_STATUS]) === TRUE) {
      // The command posted a status message. If it asked for a view refresh
      // after execution, change it to a page refresh so that the page's
      // status area gets refreshed and shows the status message.
      if ($executeBehavior === FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH) {
        $executeBehavior = FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH;
      }
    }

    switch ($executeBehavior) {
      default:
      case FolderShareCommandInterface::POST_EXECUTE_PAGE_REDIRECT:
      case FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH:
        // Refresh the current page.
        $this->command = NULL;
        return AjaxUtilities::createRedirectResponse($this->url, $response);

      case FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH:
        // Refresh the view associated with the menu the command came from.
        $selector = '#drupal-foldershare-form-uifolderbrowsermenu';
        $method   = 'trigger';
        $args     = [
          'FolderShareRefreshView',
        ];

        $this->command = NULL;
        $response->addCommand(new InvokeCommand($selector, $method, $args));
        return $response;
    }
  }

  /*--------------------------------------------------------------------
   *
   * Utilities.
   *
   *--------------------------------------------------------------------*/
  /**
   * Returns the messenger.
   *
   * @return \Drupal\Core\Messenger\MessengerInterface
   *   Returns the messenger.
   */
  private function getMessenger() {
    return $this->messenger;
  }

  /**
   * Decodes plugin information passed to the form via its URL.
   *
   * @param string $encoded
   *   The base64 encoded string included as a parameter on the form URL.
   */
  protected function decodeParameters(string $encoded) {
    //
    // The incoming parameters are an encoded associative array that provides
    // the plugin ID, configuration, and redirect URL. Encoding has expressed
    // the array as a base64 string so that it could be included as a
    // URL parameter.  Decoding reverses the base64 encode, and a JSON decode
    // to return an object, which we cast to an array as needed.
    //
    // Decode parameters.
    $parameters = (array) json_decode(base64_decode($encoded), TRUE);

    if (empty($parameters) === TRUE) {
      // Fail. No parameters?
      //
      // This should not be possible because the route requires that there
      // be parameters. At a minimum, we need the command plugin ID.
      $this->getMessenger()->addError($this->t(
        "Communications problem.\nThe command could not be completed because it is missing one or more required parameters."));
      return [];
    }

    // Get plugin ID.
    if (empty($parameters['pluginId']) === TRUE) {
      // Fail. No plugin ID?
      //
      // The parameters are malformed and missing the essential plugin ID.
      $this->getMessenger()->addError($this->t(
        "Communications problem.\nThe command could not be completed because it is missing one or more required parameters."));
      return [];
    }

    // Get initial configuration.
    if (isset($parameters['configuration']) === FALSE) {
      // Fail. No configuration?
      //
      // The parameters are malformed and missing the essential configuration.
      $this->getMessenger()->addError($this->t(
        "Communications problem.\nThe command could not be completed because it is missing one or more required parameters."));
      return [];
    }

    // Insure the configuration is an array.
    $parameters['configuration'] = (array) $parameters['configuration'];

    // Provide a default Ajax value.
    if (isset($parameters['enableAjax']) === FALSE) {
      $parameters['enableAjax'] = FALSE;
    }

    return $parameters;
  }

}
