<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\Core\Database\Database;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Utilities\CacheUtilities;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Get/set FolderShare entity system disabled field.
 *
 * This trait includes get and set methods for FolderShare entity
 * system disabled field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetSystemDisabledTrait {

  /*---------------------------------------------------------------------
   *
   * SystemDisabled field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function isSystemDisabled() {
    $value = $this->get('systemdisabled')->value;

    // An empty field is also FALSE.
    if (empty($value) === TRUE || $value === FALSE) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Sets the system disabled flag.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param bool $state
   *   The new flag state.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::isSystemDisabled()
   */
  public function setSystemDisabled(bool $state) {
    $this->systemdisabled->setValue($state);
    return $this;
  }

  /**
   * Clears the system disabled flag on all hidden items.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * This method is intended for use in system debugging and file system
   * fixes in order to quickly reset all disabled items. Since disabled
   * items are usually in the process of being changed (copied, moved,
   * ownership changed, etc.), reset all disabled items can cause confusion.
   *
   * The entity cache is cleared and the render cache invalidated to insure
   * that changed items are visible to the user.
   *
   * @return int
   *   Returns the number of items changed.
   *
   * @see ::setSystemDisabled()
   * @see \Drupal\foldershare\Utilities\CacheUtilities::flushAllEntityCaches()
   * @see \Drupal\foldershare\Utilities\CacheUtilities::invalidateRenderCache()
   */
  public static function clearAllSystemDisabled() {
    $connection = Database::getConnection();

    // Clear the disabled flag on all disabled items.
    $query = $connection->update(FolderShareInterface::BASE_TABLE);
    $query->condition('systemdisabled', TRUE, '=');
    $query->fields([
      'systemdisabled' => (int) FALSE,
    ]);
    $n = $query->execute();
    if ($n === 0) {
      return 0;
    }

    CacheUtilities::flushAllEntityCaches(FolderShareInterface::ENTITY_TYPE_ID);
    CacheUtilities::invalidateRenderCache();
    return $n;
  }

  /*---------------------------------------------------------------------
   *
   * Standard error messages.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a standard disabled message.
   *
   * This method provides a generic message that may be used by
   * operations that need to report that an item is disabled.
   *
   * @param mixed $operationName
   *   The past tense name of the operation being performed, such as
   *   'deleted', 'moved', or 'copied'. The value may be a string or
   *   translated markup (perferred).
   * @param string $itemName
   *   (optional, default = NULL = multiple items) The name of a single item
   *   involved in an operation that cannot be done. If NULL, a multi-item
   *   message is returned instead.
   *
   * @return \Drupal\Core\Render\Markup
   *   Returns a markup object containing a formatted standard
   *   exception message.
   */
  public static function getStandardDisabledMessage(
    $operationName,
    string $itemName = NULL) {

    if (empty($operationName) === TRUE) {
      $operationName = t('updated');
    }

    if (empty($itemName) === TRUE) {
      return FormatUtilities::createFormattedMessage(
        t('This item is in use by another user or by the system and cannot be @operation at this time.',
          [
            '@operation' => $operationName,
          ]),
          t('Please try again in a moment.'));
    }

    return FormatUtilities::createFormattedMessage(
      t(
        'The item "@name" is in use by another user or by the system and cannot be @operation at this time.',
        [
          '@name'      => $itemName,
          '@operation' => $operationName,
        ]),
      t('Please try again in a moment.'));
  }

}
