<?php

namespace Drupal\foldershare\Entity\Exception;

/**
 * Defines an exception indicating that an time limit has been reached.
 *
 * This is an intentional exception that may be thrown when a chosen
 * process execution time limit has been exceeded. This differs from PHP's
 * own handling of execution time limits, which interrupts the process and does
 * not throw an exception.
 *
 * @ingroup foldershare
 */
class ExecutionTimeLimitException extends RuntimeExceptionWithMarkup {

  /*--------------------------------------------------------------------
   *
   * Constructors.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs an exception.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface $message
   *   (optional, default = NULL) The message string or an instance of
   *   \Drupal\Component\Render\MarkupInterface. If NULL, a default
   *   message is used.
   * @param int $code
   *   (optional, default = 0) An error code.
   * @param \Throwable $previous
   *   (optional, default = NULL) A previous exception that this extends.
   */
  public function __construct(
    $message = NULL,
    int $code = 0,
    \Throwable $previous = NULL) {

    if (empty($message) === TRUE) {
      $message = $this->t('Execution time limit exceeded');
    }

    parent::__construct($message, $code, $previous);
  }

}
