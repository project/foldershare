<?php

namespace Drupal\foldershare\Utilities;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\CloseModalDialogCommand;
use Drupal\Core\Ajax\RedirectCommand;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;

use Drupal\foldershare\Ajax\OpenErrorDialogCommand;

/**
 * Defines utility functions for working with AJAX.
 *
 * The functions in this class help to create common AJAX responses.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
final class AjaxUtilities {

  /*---------------------------------------------------------------------
   *
   * Utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Strips AJAX query parameters from a URL and returns the URL string.
   *
   * If no URL is given, the URL for the current page is used.
   *
   * @param \Drupal\Core\Url $url
   *   (optional, default = NULL = current page) The URL to redirect to.
   *
   * @return string
   *   Returns the URL.
   */
  public static function stripAjaxFromUrl(Url $url = NULL) {
    if ($url === NULL) {
      // Get the current page's URI.
      $requestStack = \Drupal::service('request_stack');
      $request = $requestStack->getCurrentRequest();
      $uri = $request->getUri();
    }
    else {
      $uri = $url->toString();
    }

    // Parse the URI into pieces.
    $u = parse_url($uri);

    // If there are no query parameters (unlikely since AJAX is in use),
    // return the URL.
    if (empty($u['query']) === TRUE) {
      return $uri;
    }

    // Strip the query part (if any) of AJAX query parameters.
    $queryArguments = [];
    parse_str($u['query'], $queryArguments);

    // Ignore AJAX query arguments.
    unset($queryArguments['_wrapper_format']);
    unset($queryArguments['ajax_form']);

    if (empty($queryArguments) === TRUE) {
      unset($u['query']);
    }
    else {
      $u['query'] = http_build_query($queryArguments);
    }

    // Reassemble the URL.
    $scheme   = (empty($u['scheme']) === FALSE) ? $u['scheme'] . '://' : '';
    $host     = (empty($u['host']) === FALSE) ? $u['host'] : '';
    $port     = (empty($u['port']) === FALSE) ? ':' . $u['port'] : '';
    $user     = (empty($u['user']) === FALSE) ? $u['user'] : '';
    $pass     = (empty($u['pass']) === FALSE) ? ':' . $u['pass'] : '';
    $pass     = ($user || $pass) ? "$pass@" : '';
    $path     = (empty($u['path']) === FALSE) ? $u['path'] : '';
    $query    = (empty($u['query']) === FALSE) ? '?' . $u['query'] : '';
    $fragment = (empty($u['fragment']) === FALSE) ? '#' . $u['fragment'] : '';
    return "$scheme$user$pass$host$port$path$query$fragment";
  }

  /*---------------------------------------------------------------------
   *
   * Standard AJAX responses.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns an AJAX response that redirects to a new page.
   *
   * @param string|\Drupal\Core\Url $url
   *   (optional, default = NULL = current page) The URL to redirect to.
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   (optional, default = NULL = create a new response) The response to
   *   add an AJAX command to.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   *
   * @see ::createRefreshResponse()
   */
  public static function createRedirectResponse(
    $url = NULL,
    AjaxResponse $response = NULL) {

    if (empty($url) === TRUE) {
      $url = Url::fromRoute('<current>')->toString();
    }
    elseif ($url instanceof Url) {
      $url = $url->toString();
    }
    elseif (is_string($url) === FALSE) {
      $url = (string) $url;
    }

    $command = new RedirectCommand($url);

    if ($response === NULL) {
      $response = new AjaxResponse();
    }

    $response->addCommand($command);
    return $response;
  }

  /**
   * Returns an AJAX response that refreshes the current page.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   (optional, default = NULL = create a new response) The response to
   *   add an AJAX command to.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   *
   * @see ::createRedirectResponse()
   */
  public static function createRefreshResponse(AjaxResponse $response = NULL) {
    return self::createRedirectResponse(NULL, $response);
  }

  /**
   * Returns an AJAX response that shows form errors in a dialog.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The input values for the form's elements.
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   (optional, default = NULL = create a new response) The response to
   *   add an AJAX command to.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   *
   * @see ::createMessengerErrorsDialogResponse()
   */
  public static function createFormErrorsDialogResponse(
    FormStateInterface $formState,
    AjaxResponse $response = NULL) {

    $dialog = new OpenErrorDialogCommand(t('Problem'));
    $dialog->setFromFormErrors($formState);

    if ($response === NULL) {
      $response = new AjaxResponse();
    }
    $response->addCommand($dialog);
    return $response;
  }

  /**
   * Returns an AJAX response that shows messenger errors in a dialog.
   *
   * @param \Drupal\Core\Ajax\AjaxResponse $response
   *   (optional, default = NULL = create a new response) The response to
   *   add an AJAX command to.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   *
   * @see ::createFormErrorsDialogResponse()
   */
  public static function createMessengerErrorsDialogResponse(
    AjaxResponse $response = NULL) {

    $dialog = new OpenErrorDialogCommand(t('Problem'));
    $dialog->setFromMessengerMessages(\Drupal::messenger());

    if ($response === NULL) {
      $response = new AjaxResponse();
    }

    $response->addCommand($dialog);
    return $response;
  }

  /**
   * Returns an AJAX response that shows messenger errors in a dialog.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   *
   * @see ::createFormErrorsDialogResponse()
   */
  public static function createCloseModalDialogResponse(
    AjaxResponse $response = NULL) {

    $dialog = new CloseModalDialogCommand();

    if ($response === NULL) {
      $response = new AjaxResponse();
    }

    $response->addCommand($dialog);
    return $response;
  }

}
