<?php

namespace Drupal\foldershare\Entity\FolderShareScheduledTaskTraits;

/**
 * Get/set FolderShareScheduledTask entity fields.
 *
 * This trait includes get and set methods that access task field values.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShareScheduledTask entity class. It is a
 * mechanism to group functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetTrait {

  /*---------------------------------------------------------------------
   *
   * Fields access.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the task's approximate accumulated execution time in seconds.
   *
   * A task may keep track of its accumulated execution time through a
   * chain of tasks, starting with the initial run of the task, followed
   * by a series of continuation runs.
   *
   * The execution time is approximate. If an operation schedules a
   * safety net task, runs for awhile, and is interrupted before it can
   * swap the safety net task with a continuation task, then the accumulated
   * execution time of the interrupted task will not have had a chance to
   * be saved into a continuation task.
   *
   * @return int
   *   Returns the approximate accumulated execution time in seconds.
   */
  public function getAccumulatedExecutionTime() {
    return $this->get('executiontime')->value;
  }

  /**
   * Returns the task's callback in the form "class::method".
   *
   * @return string
   *   Returns the name of the task callback.
   */
  public function getCallback() {
    return $this->get('operation')->value;
  }

  /**
   * Returns the task's optional comments.
   *
   * Comments are optional and may be used by a task to annotate why the
   * task exists or how it is progressing.
   *
   * @return string
   *   Returns the comments for the task.
   */
  public function getComments() {
    return $this->get('comments')->value;
  }

  /**
   * Returns the task's creation timestamp.
   *
   * @return int
   *   Returns the creation timestamp for this task.
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * Returns the bitmask of the task's flags.
   *
   * @return int
   *   Returns the flags bitmask for this task.
   */
  public function getFlags() {
    // The 'flags' field was added in release 8.x-1.3. There can be a narrow
    // time gap between when the module code is installed and the module's
    // database table is updated. During this gap, the 'flags' field does
    // not exist, but code may still query it.
    if ($this->hasField('flags') === FALSE) {
      return 0;
    }

    return $this->get('flags')->value;
  }

  /**
   * Returns the task's parameters.
   *
   * @return array
   *   Returns an array of task parameters.
   */
  public function getParameters() {
    return $this->get('parameters')->value;
  }

  /**
   * Returns the user ID of the user that initiated the task.
   *
   * @return int
   *   Returns the requester's user ID.
   */
  public function getRequester() {
    return (int) $this->get('uid')->target_id;
  }

  /**
   * Returns the task's scheduled run timestamp.
   *
   * @return int
   *   Returns the scheduled run timestamp for this task.
   */
  public function getScheduledTime() {
    return $this->get('scheduled')->value;
  }

  /**
   * Returns the task's operation start timestamp.
   *
   * The start time is the time when an operation began, such as
   * the request time for a copy, move, or delete. This operation led to
   * the creation of the task object, which has created and scheduled times.
   * If that task reschedules itself into a continuing series of tasks,
   * all of them should share the same operation started timestamp.
   *
   * @return int
   *   Returns the original start timestamp for this task.
   */
  public function getStartedTime() {
    return $this->get('started')->value;
  }

  /**
   * Returns tRUE if the task's flags indicate it is repeating.
   *
   * @return bool
   *   Returns TRUE if the task repeats.
   */
  public function isRepeating() {
    return (($this->getFlags() & self::REPEATING_FLAG) !== 0);
  }

}
