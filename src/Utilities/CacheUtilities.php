<?php

namespace Drupal\foldershare\Utilities;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines utility functions for working with caches.
 *
 * The functions in this class help flush specific caches completely, or
 * to remove specific cached entities.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
final class CacheUtilities {

  /*---------------------------------------------------------------------
   *
   * Functions - memory caches.
   *
   *---------------------------------------------------------------------*/
  /**
   * Flushes all static memory caches.
   *
   * This method flushes all static variable values and all entity memory
   * caches managed by Drupal core and available through the core
   * "entity.memory_cache" service.
   *
   * This cannot flush cached values held in static variables in classes.
   * Such values are not recommended and should use drupal_static() instead.
   *
   * The PHP garbage collector is called after flushing the cache. This will
   * reduce the process's current memory use.
   *
   * Flushing memory caches can have a performance impact on the current
   * process if any of the flushed content is needed again and must be
   * reloaded from the database.
   *
   * @see ::flushAllEntityCaches()
   * @see drupal_static()
   * @see drupal_static_reset()
   */
  public static function flushAllMemoryCaches() {

    // Flush all static variables.
    drupal_static_reset();

    // Flush the entire entity memory cache, clearing cached entries for
    // all entity types.
    $cache = \Drupal::service('entity.memory_cache');
    if ($cache !== NULL) {
      $cache->deleteAll();
    }

    // Garbage collect.
    gc_collect_cycles();
  }

  /*---------------------------------------------------------------------
   *
   * Functions - entity caches.
   *
   *---------------------------------------------------------------------*/
  /**
   * Flushes all entries in the entity type's caches.
   *
   * This method gets the entity type's storage manager and flushes its
   * entity caches. Depending upon the storage manager implementation, this
   * may flush a persistent cache, a memory cache, or both.
   *
   * This method also invalidates the entity type's list cache tags. This
   * marks any list of entities as invalid and in need of rebuilding.
   *
   * This does not invalidate individual entities of the type. This means
   * that eny cached items that depend upon the entity (such as render
   * cache entries) will NOT be flushed.
   *
   * The PHP garbage collector is called afterwards. This will reduce the
   * process's current memory use.
   *
   * Flushing all entity caches can have a performance impact on the current
   * process and future processes if any of that content is needed again and
   * must be reloaded from the database.
   *
   * @param string $entityTypeId
   *   The entity type who's caches are flushed.
   *
   * @see ::flushEntityFromCache()
   * @see ::invalidateEntityCaches()
   *
   * @internal
   * The cache API currently has no way to flush or invalidate all entries
   * in all caches for all entities of a given entity type. If this is fixed,
   * this method should be updated to flush everything to do with this
   * entity type.
   */
  public static function flushAllEntityCaches(string $entityTypeId) {

    try {
      // Flush the entity type's storage manager caches. Depending upon the
      // storage manager, this will flush persistent and memory caches.
      $storage = \Drupal::entityTypeManager()
        ->getStorage($entityTypeId);
      $storage->resetCache(NULL);

      // Clear the entity type's list cache tags for all caches.
      $entityType = \Drupal::entityTypeManager()
        ->getDefinition($entityTypeId, FALSE);
      if ($entityType !== NULL) {
        Cache::invalidateTags($entityType->getListCacheTags());
      }
    }
    catch (\Exception $e) {
      // Ignore.
    }

    // Unfortunately, there is no API call to invalidate ALL cached
    // entries for an entity type. You have to have the loaded entity
    // in order to get its cache tags in order to invalidate its
    // cache entry. That is very expensive and not at all practical.
    //
    // Garbage collect.
    gc_collect_cycles();
  }

  /**
   * Flushes an entity from the appropriate entity type caches.
   *
   * This method gets the entity type's storage manager and flushes the
   * given entity from its caches. Depending upon the entity type, this
   * includes static, persistent, and render caches.
   *
   * The PHP garbage collector is called afterwards. This will reduce the
   * process's current memory use.
   *
   * Flushing memory caches can have a performance impact on the current
   * process if the flushed content is needed again and must be reloaded
   * from the database.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to flush from the entity caches.
   *
   * @see ::flushAllEntityCaches()
   * @see ::invalidateEntityCaches()
   */
  public static function flushEntityFromCache(EntityInterface $entity) {

    if ($entity === NULL) {
      return;
    }

    $storage = \Drupal::entityTypeManager()
      ->getStorage($entity->getEntityTypeId());
    $storage->resetCache([$entity->id()]);

    // Garbage collect.
    gc_collect_cycles();
  }

  /**
   * Invalidates an entity from the appropriate entity type caches.
   *
   * When an item has changed and cached values are out of date, this
   * method invalidates it from known caches. If a cache is in memory,
   * this will reduce the current process's memory use, but the primary
   * impact is to invalidate the item from database caches that are queried
   * by other users.
   *
   * When an entity is changed and saved, cache invalidation occurs
   * automatically. But if the entity's database tables have been changed
   * directly, then this change is not detected automatically and cache
   * invalidation must be done intentionally.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to flush from the entity caches.
   *
   * @see ::flushAllEntityCaches()
   * @see ::flushEntityFromCache()
   */
  public static function invalidateEntityCaches(EntityInterface $entity) {

    if ($entity === NULL) {
      return;
    }

    // Clear the entity's entry in the entity cache.
    $storage = \Drupal::entityTypeManager()
      ->getStorage($entity->getEntityTypeId());
    $storage->resetCache([$entity->id()]);

    // Clear the entity's entry in all caches.
    Cache::invalidateTags(Cache::mergeTags(
      $entity->getEntityType()->getListCacheTags(),
      $entity->getCacheTagsToInvalidate()));

    // Garbage collect.
    gc_collect_cycles();
  }

  /*---------------------------------------------------------------------
   *
   * Functions - render cache.
   *
   *---------------------------------------------------------------------*/
  /**
   * Invalidates the entire render cache.
   *
   * Rendered entities, breadcrumbs, blocks, and other page elements are
   * stored within a large shared render cache. This method invalidates the
   * entire cache, which will force all future processes to rebuild these
   * items. Until they are rebuilt and added back into the cache, there
   * will be a performance impact.
   *
   * @see ::flushAllEntityCaches()
   * @see ::flushAllMemoryCaches()
   */
  public static function invalidateRenderCache() {
    \Drupal::service('cache.render')->invalidateAll();
  }

}
