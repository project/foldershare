<?php

namespace Drupal\foldershare\Plugin\search_api_autocomplete\search;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\search_api\Plugin\PluginFormTrait;
use Drupal\search_api\Plugin\views\query\SearchApiQuery;
use Drupal\search_api_autocomplete\SearchApiAutocompleteException;
use Drupal\search_api_autocomplete\Search\SearchPluginBase;
use Drupal\views\Views;

use Drupal\foldershare\Settings;

/**
 * Provides Search API Autocomplete support for the folder browser search box.
 *
 * When the Search API and Search API Autocomplete modules are installed,
 * the folder browser's search box is enabled, and the box is configured
 * to use the Search API, this plugin provides autocomplete support with
 * the Search API Autocomplete module.
 *
 * Note that the UI Search Box form directly references the plugin ID of
 * this plugin in order to load it and attach it to the form.
 *
 * @see \Drupal\foldershare\Form\UISearchBox
 * @see \Drupal\foldershare\ManageSearch
 *
 * @SearchApiAutocompleteSearch(
 *   id = "UISearchBoxAutocomplete",
 *   label = @Translation("FolderShare search box"),
 *   description = @Translation("Searches provided by the FolderShare folder browser search box"),
 *   provider = "views",
 * )
 */
class UISearchBoxAutocomplete extends SearchPluginBase implements PluginFormInterface {

  use PluginFormTrait;

  /*--------------------------------------------------------------------
   *
   * Fields.
   *
   *-------------------------------------------------------------------*/
  /**
   * A cached copy of the most recently requested view info.
   *
   * @var array
   */
  protected $cachedViewInfo = [];

  /*--------------------------------------------------------------------
   *
   * Utilities.
   *
   *-------------------------------------------------------------------*/
  /**
   * Finds the view executable and display for a path.
   *
   * Searches through enabled views for the view and display with the given
   * URL path. An empty array is returned if no view is found. Otherwise an
   * associative array with several values is returned:
   * - 'view' = the view object.
   * - 'viewExec' = the executable view object.
   * - 'displayId' = the display ID for the display with the given path.
   *
   * @param string $path
   *   The path to the view and display.
   *
   * @return array
   *   Returns an associative array with view information, or an empty array
   *   if no matching view was found.
   *
   * @see ::calculateDependencies()
   * @see ::createQuery()
   * @see ::getIndexId()
   */
  protected function findViewForPath(string $path) {
    // If there is a cached array and it is for this path, return it.
    if (empty($this->cachedViewInfo) === FALSE &&
        $this->cachedViewInfo['path'] === $path) {
      return $this->cachedViewInfo;
    }

    // Search through all enabled views.
    foreach (Views::getEnabledViews() as $view) {
      $viewExec = $view->getExecutable();

      // Insure the view has initialized its list of display handlers.
      $viewExec->initDisplay();
      if (empty($viewExec->displayHandlers) === TRUE) {
        // No display handlers? Something is wrong with the view. Skip it.
        continue;
      }

      // Get all of the display IDs for those handlers.
      $displayIds = $viewExec->displayHandlers->getInstanceIds();

      // Search through all of the view's displays to find one with the
      // given path.
      foreach ($displayIds as $displayId) {
        // Get the display and path. Prefix '/' since the view paths are
        // not stored with a leading '/', but the given path does have '/'.
        $display     = $viewExec->displayHandlers->get($displayId);
        $displayPath = '/' . $display->getPath();

        if ($displayPath === $path) {
          // A display with the right path has been found. Return it.
          $this->cachedViewInfo = [
            'path'      => $path,
            'view'      => $view,
            'viewExec'  => $viewExec,
            'displayId' => $displayId,
          ];
          return $this->cachedViewInfo;
        }
      }
    }

    // No suitable view and display were found.
    $this->cachedViewInfo = [];
    return $this->cachedViewInfo;
  }

  /**
   * {@inheritdoc}
   *
   * @see ::findViewForPath()
   */
  public function getIndexId() {
    // Get the search path.
    $searchPath = Settings::getSearchAPISearchPath();
    if (empty($searchPath) === TRUE) {
      return '';
    }

    // Look up the view for the search path.
    $viewInfo = $this->findViewForPath($searchPath);

    // Get the index for that view.
    $index = SearchApiQuery::getIndexFromTable(
      $viewInfo['view']->get('base_table'));
    if ($index === NULL) {
      return '';
    }

    // And return its ID.
    return $index->id();
  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *-------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    // The default configuration form has:
    // - An enable checkbox.
    // - Choices for how suggestions are handled.
    // - Settings for the maximum number of suggestions and minimum word size.
    // - Autosubmit support when pressing the 'Enter' key.
    //
    // These are all sufficient for the search box. Add nothing.
    return $form;
  }

  /*--------------------------------------------------------------------
   *
   * Query.
   *
   *-------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   *
   * This plugin exists if the Search API Autocomplete module is installed.
   * But the plugin is only used if the folder browser search box form
   * attaches the plugin to the search field. And that is only done if:
   * - The Search API module is installed.
   * - The Search API Autocomplete module is installed.
   * - The search path and search filter ID are configured.
   *
   * @see \Drupal\foldershare\ManageSearch
   * @see \Drupal\foldershare\Settings
   * @see \Drupal\foldershare\Form\UISearchBox
   * @see ::findViewForPath()
   */
  public function createQuery($keys, array $data = []) {
    // This function is modeled after the Views plugin in the
    // Search API Autocomplete module.
    $searchPath = Settings::getSearchAPISearchPath();
    if (empty($searchPath) === TRUE) {
      throw new SearchApiAutocompleteException(
        'The FolderShare search box is not configured to use the Search API. The search path is empty.');
    }

    $style = Settings::getSearchAPISearchURLStyle();
    if ($style !== 'query') {
      throw new SearchApiAutocompleteException(
        "The FolderShare search box is not configured to use a View's query URL style with the Search API.");
    }

    $filterId = Settings::getSearchAPISearchFilterId();
    if (empty($filterId) === TRUE) {
      throw new SearchApiAutocompleteException(
        'The FolderShare search box is not configured to use the Search API. The search query name (exposed filter ID) is empty.');
    }

    $viewInfo = $this->findViewForPath($searchPath);
    if (empty($viewInfo) === TRUE) {
      throw new SearchApiAutocompleteException(
        "The FolderShare search box is not configured to use the Search API. The search path '$searchPath' does not correspond to a view.");
    }

    //
    // Set up view.
    // ------------
    // Get the view executable and display ID and configure the view.
    // Then set up the view filter.
    $view = $viewInfo['viewExec'];
    $data += [
      'display'   => $viewInfo['displayId'],
      'filter'    => $filterId,
      'arguments' => [],
    ];

    $view->setDisplay($data['display']);
    $view->setArguments($data['arguments']);

    // Set the keys via the exposed input, to get the correct handling for the
    // filter in question.
    $singleFieldFilter = !empty($data['field']);
    if (!empty($data['filter'])) {
      $input = $keys;
      // The Views filter for individual fulltext fields uses a nested "value"
      // field for the real input, due to Views internals.
      if ($singleFieldFilter) {
        $input = ['value' => $keys];
      }
      $view->setExposedInput([
        $data['filter'] => $input,
      ]);
    }

    $view->preExecute();

    // Since we only have a single value in the exposed input, any exposed
    // filters set to "Required" might cause problems – especially "Search:
    // Fulltext search", which aborts the query when validation fails (instead
    // of relying on the Form API "#required" validation). The normal filters
    // which use the Form API actually don't seem to cause problems, but it's
    // still better to be on the safe side here and just disabled "Required" for
    // all filters. (It also makes the code simpler.)
    foreach ($view->display_handler->getHandlers('filter') as $filter) {
      $filter->options['expose']['required'] = FALSE;
    }

    $view->build();

    //
    // Return the query.
    // -----------------
    // The view *must* be using a Search API query object.
    $queryPlugin = $view->getQuery();
    if (($queryPlugin instanceof SearchApiQuery) === FALSE) {
      // View is not using the Search API.
      $viewsId    = $view->id();
      $viewsLabel = $view->storage->label() ?: $viewsId;

      throw new SearchApiAutocompleteException(
        "Could not create search query for view '$viewsLabel': view is not based on Search API.");
    }

    $query = $queryPlugin->getSearchApiQuery();
    if (!$query) {
      $viewsId = $view->id();
      $viewsLabel = $view->storage->label() ?: $viewsId;
      throw new SearchApiAutocompleteException(
        "Could not create search query for view '$viewsLabel'.");
    }

    if ($singleFieldFilter) {
      $query->setFulltextFields([$data['field']]);
    }

    return $query;
  }

  /**
   * {@inheritdoc}
   *
   * @see ::findViewForPath()
   */
  public function calculateDependencies() {
    $this->dependencies = parent::calculateDependencies();

    $searchPath = Settings::getSearchAPISearchPath();
    if (empty($searchPath) === FALSE) {
      $viewInfo = $this->findViewForPath($searchPath);
      if (empty($viewInfo) === FALSE) {
        $view = $viewInfo['view'];
        $key  = $view->getConfigDependencyKey();
        $name = $view->getConfigDependencyName();

        $this->addDependency($key, $name);
      }
    }

    return $this->dependencies;
  }

}
