<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\file\FileInterface;
use Drupal\file\Entity\File;

use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\ManageFileSystem;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Utilities\FormatUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Entity\Exception\LockException;
use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\Exception\SystemException;

/**
 * Add File entity as FolderShare entity.
 *
 * This trait includes methods to wrap File entities as FolderShare
 * entities with kind 'file' or 'image'. The File object's ID is saved
 * to the 'file' or 'image' entity reference field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait OperationAddFileTrait {

  /*---------------------------------------------------------------------
   *
   * Add file from a local file.
   *
   *---------------------------------------------------------------------*/
  /**
   * Adds a local file to the user's root list.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive use for the duration of
   * this operation.
   *
   * <B>Activity log</B>
   * This method posts a log message after the file has been added.
   *
   * @param string $uri
   *   The path to the local file.
   * @param string $filename
   *   (optional, default = '' = use URI filename) When not empty, the user
   *   visible name of the new FolderShare and File entity. Otherwise use
   *   the file name from the URI.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if $filename collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   * @param bool $moveFile
   *   (optional, default = TRUE) When TRUE, the indicated file is moved into
   *   the module's files directory. When FALSE, the indicated file is copied
   *   instead.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the newly added FolderShare entity wrapping the file. Returns
   *   NULL and takes no action if the URI is empty.
   *
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the file URI is empty.
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   *
   * @see ::addFileFromLocalFile()
   * @see ::addFileToRootFromFormUpload()
   * @see ::addFileToRootFromInputStream()
   */
  public static function addFileToRootFromLocalFile(
    string $uri,
    string $filename = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1),
    bool $moveFile = TRUE) {

    return self::addFileFromLocalFileInternal(
      NULL,
      $uri,
      $filename,
      $allowRename,
      $ownerUid,
      $moveFile);
  }

  /**
   * {@inheritdoc}
   */
  public function addFileFromLocalFile(
    string $uri,
    string $filename = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1),
    bool $moveFile = TRUE) {

    return self::addFileFromLocalFileInternal(
      $this,
      $uri,
      $filename,
      $allowRename,
      $ownerUid,
      $moveFile);
  }

  /**
   * Implements adding a local file to a folder or root list.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive use for the duration of
   * this operation.
   *
   * <B>Activity log</B>
   * This method posts a log message after the file has been added.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   (optional, default = NULL) The parent entity, or a NULL if there is
   *   no parent and the file should be added to the owner's root list.
   * @param string $uri
   *   The path to the local file.
   * @param string $filename
   *   (optional, default = '' = use URI filename) When not empty, the user
   *   visible name of the new FolderShare and File entity. Otherwise use
   *   the file name from the URI.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if $filename collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   * @param bool $moveFile
   *   (optional, default = TRUE) When TRUE, the indicated file is moved into
   *   the module's files directory. When FALSE, the indicated file is copied
   *   instead.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the newly added FolderShare entity wrapping the file. Returns
   *   NULL and takes no action if the URI is empty.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the file URI is empty or a parent entity is
   *   not a folder.
   *
   * @see ::addFileFromFormUpload()
   * @see ::addFileFromInputStream()
   * @see ::addFileToRootFromLocalFile()
   */
  private static function addFileFromLocalFileInternal(
    FolderShareInterface $parent = NULL,
    string $uri = '',
    string $filename = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1),
    bool $moveFile = TRUE) {

    //
    // Validate.
    // ---------
    // Bad values are rejected.
    if (empty($uri) === TRUE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an empty source file URI.');
    }

    if (empty($filename) === TRUE) {
      $filename = pathinfo($uri, PATHINFO_BASENAME);
    }

    if (empty($filename) === TRUE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an empty source filename.');
    }

    if ($parent !== NULL && $parent->isFolder() === FALSE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called on an item that is not a folder.');
    }

    //
    // Create and add.
    // ---------------
    // Create a File entity that wraps the given local file (or a copy of it),
    // then add that File entity to the owner's root list or parent folder.
    $file = NULL;

    try {
      $file = self::createFileEntityFromLocalFile(
        $uri,
        $filename,
        $ownerUid,
        $moveFile);

      if ($parent === NULL) {
        return self::addFileToRoot($file, $allowRename, $ownerUid);
      }

      return $parent->addFile($file, $allowRename, $ownerUid);
    }
    catch (\Exception $e) {
      if ($file !== NULL) {
        // Delete the file. Unfortunately, if we were told to move the
        // file, then deleting the File entity here will delete the moved
        // file as well.
        self::deleteFileWithRetry($file);
      }
      throw $e;
    }
  }

  /*---------------------------------------------------------------------
   *
   * Add file from an input stream.
   *
   *---------------------------------------------------------------------*/
  /**
   * Adds a PHP input stream file into the root list.
   *
   * When a file is uploaded via an HTTP post handled by a web services
   * "REST" resource, the file's data is available via the PHP input
   * stream. This method reads that stream, creates a file, and adds
   * that file to this folder with the given name.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive use for the duration of
   * this operation.
   *
   * <B>Activity log</B>
   * This method posts a log message after the file has been added.
   *
   * @param string $filename
   *   The name for the new file.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if $filename collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the newly added FolderShare entity wrapping the file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the file name is empty.
   *
   * @see ::addFileToRootFromFormUpload()
   * @see ::addFileToRootFromLocalFile()
   */
  public static function addFileToRootFromInputStream(
    string $filename,
    bool $allowRename = TRUE,
    int $ownerUid = (-1)) {

    return self::addFileFromInputStreamInternal(
      NULL,
      $filename,
      $allowRename,
      $ownerUid);
  }

  /**
   * {@inheritdoc}
   */
  public function addFileFromInputStream(
    string $filename,
    bool $allowRename = TRUE,
    int $ownerUid = (-1)) {

    return self::addFileFromInputStreamInternal(
      $this,
      $filename,
      $allowRename,
      $ownerUid);
  }

  /**
   * Implements adding a file created from the PHP input stream.
   *
   * When a file is uploaded via an HTTP post handled by a web services
   * "REST" resource, the file's data is available via the PHP input
   * stream. This method reads that stream, creates a file, names it,
   * and adds that file to the owner's root list or a folder.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive use for the duration of
   * this operation.
   *
   * <B>Activity log</B>
   * This method posts a log message after the file has been added.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   (optional, default = NULL) The parent entity, or a NULL if there is
   *   no parent and the file should be added to the owner's root list.
   * @param string $filename
   *   (optional, default = '') The name for the new file.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if $filename collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the newly added FolderShare entity wrapping the file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired.
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the file name is empty or a parent entity is
   *   not a folder.
   *
   * @see ::addFileToRootFromFormUpload()
   * @see ::addFileToRootFromLocalFile()
   */
  private static function addFileFromInputStreamInternal(
    FolderShareInterface $parent = NULL,
    string $filename = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1)) {

    //
    // Validate.
    // ---------
    // Bad values are rejected.
    if (empty($filename) === TRUE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an empty destination filename.');
    }

    if ($parent !== NULL && $parent->isFolder() === FALSE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called on an item that is not a folder.');
    }

    //
    // Create and add.
    // ---------------
    // Copy the PHP input stream into a local file, then create a File entity
    // that wraps the local file. Then add the File entity to the owner's
    // root list or parent folder.
    $tempUri = '';
    $file = NULL;

    try {
      // Throws an exception if the input stream cannot be read or a local
      // file cannot be created and written.
      $tempUri = self::createLocalFileFromInputStream();

      // Throws an exception on file system problems.
      $file = self::createFileEntityFromLocalFile(
        $tempUri,
        $filename,
        $ownerUid,
        TRUE);

      // Throws an exception if a lock could not be acquired or if hooks
      // find that the file is invalid (e.g. there is a file format error).
      if ($parent === NULL) {
        return self::addFileToRoot($file, $allowRename, $ownerUid);
      }

      return $parent->addFile($file, $allowRename, $ownerUid);
    }
    catch (\Exception $e) {
      // On any exception, delete the File entity (if any) and delete
      // the underlying local file (if any). Then forward the exception.
      if ($file !== NULL) {
        self::deleteFileWithRetry($file);
      }

      if (empty($tempUri) === FALSE) {
        FileUtilities::unlink($tempUri);
      }

      throw $e;
    }
  }

  /*---------------------------------------------------------------------
   *
   * Add files from a form upload's local files.
   *
   *---------------------------------------------------------------------*/
  /**
   * Creates and adds files to the user's root list from form uploaded files.
   *
   * When a file is uploaded via an HTTP form post from a browser, PHP
   * automatically saves the data into "upload" files saved in a
   * PHP-managed temporary directory. This method sweeps those uploaded
   * files, pulls out the ones associated with the named form field,
   * and adds them to the user's root list with their original names.
   *
   * If there are no uploaded files, this method returns immediately.
   *
   * If $allowRename is TRUE, Files will be automatically renamed, if needed,
   * to insure they have unique names within the folder.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * This method locks the user's root list for the duration of the addition.
   *
   * <B>Activity log</B>
   * This method posts a log message after the files have been added.
   *
   * @param string $formFieldName
   *   The name of the form field with associated uploaded files pending.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if a file's name collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return array
   *   Returns an array with one entry per uploaded file. Each entry is an
   *   associative array with one or more of the following keys:
   *   - "filename": The client's original name for the file.
   *   - "error": An error message if the file could not be processed.
   *   - "foldershare": The FolderShare entity for the uploaded file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. No files
   *   will have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the field name is empty.
   *
   * @see ::addFile()
   * @see ::addFiles()
   */
  public static function addFilesToRootFromFormUpload(
    string $formFieldName,
    bool $allowRename = TRUE,
    int $ownerUid = (-1)) {

    return self::addFilesFromFormUploadInternal(
      NULL,
      $formFieldName,
      $allowRename,
      $ownerUid);
  }

  /**
   * {@inheritdoc}
   */
  public function addFilesFromFormUpload(
    string $formFieldName,
    bool $allowRename = TRUE,
    int $ownerUid = (-1)) {

    return self::addFilesFromFormUploadInternal(
      $this,
      $formFieldName,
      $allowRename,
      $ownerUid);
  }

    public static function addFileIdsToRootFromFormUpload(
        array $fileIds,
        bool $allowRename = TRUE,
        int $ownerUid = (-1)) {

        return self::addFileIdsFromFormUploadInternal(
            NULL,
            $fileIds,
            $allowRename,
            $ownerUid);
    }

    /**
     * {@inheritdoc}
     */
    public function addFileIdsFromFormUpload(
        array $fileIds,
        bool $allowRename = TRUE,
        int $ownerUid = (-1)) {

        return self::addFileIdsFromFormUploadInternal(
            $this,
            $fileIds,
            $allowRename,
            $ownerUid);
    }

  /**
   * Creates and adds files from form uploaded files.
   *
   * When a file is uploaded via an HTTP form post from a browser, PHP
   * automatically saves the data into "upload" files saved in a
   * PHP-managed temporary directory. This method sweeps those uploaded
   * files, pulls out the ones associated with the named form field,
   * and adds them to the indicated parent folder or the user's root list
   * with their original names.
   *
   * If there are no uploaded files, this method returns immediately.
   *
   * If $allowRename is TRUE, Files will be automatically renamed, if needed,
   * to insure they have unique names within the folder.
   *
   * <B>Process locks</B>
   * If there is no parent, the user's root list is locked for the duration
   * of the addition. If there is a parent, the parent's root folder tree is
   * locked for the duration of the addition.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Activity log</B>
   * This method posts a log message after the files have been added.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   The parent folder into which to place the uploaded files. If NULL,
   *   uploaded files are added to the root list.
   * @param string $formFieldName
   *   The name of the form field with associated uploaded files pending.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE, if a file's name collides with the
   *   name of an existing entity, the name is modified by adding a number on
   *   the end so that it doesn't collide. When FALSE, a file name collision
   *   throws an exception.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return array
   *   Returns an array with one entry per uploaded file. Each entry is an
   *   associative array with one or more of the following keys:
   *   - "filename": The client's original name for the file.
   *   - "error": An error message if the file could not be processed.
   *   - "foldershare": The FolderShare entity for the uploaded file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. No files
   *   will have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the field name is empty or a parent entity is
   *   not a folder.
   *
   * @see ::addFilesFromFormUpload()
   * @see ::addFilesToRootFromFormUpload()
   * @see ::addFilesInternal()
   * @see ManageHooks::callHookValidateFile()
   * @see ManageHooks::callHookAllowedFilenameExtensionsAlter()
   * @see ManageHooks::callHookFileUploadSizeAlter()
   */
  private static function addFilesFromFormUploadInternal(
    FolderShareInterface $parent = NULL,
    string $formFieldName = '',
    bool $allowRename = TRUE,
    int $ownerUid = (-1)) {

    //
    // Drupal's file_save_upload() function is widely used for handling
    // uploaded files. It does several activities at once:
    //
    // - 1. Collect all uploaded files.
    // - 2. Validate that the uploads completed.
    // - 3. Run plug-in validators.
    // - 4. Optionally check for file name extensions.
    // - 5. Add ".txt" on executable files.
    // - 6. Rename inner extensions (e.g. ".tar.gz" -> "._tar.gz").
    // - 7. Validate file names are not too long.
    // - 8. Validate the destination exists.
    // - 9. Optionally rename files to avoid destination collisions.
    // - 10. chmod the file to be accessible.
    // - 11. Create a File object.
    // - 12. Set the File object's URI, filename, and MIME type.
    // - 13. Optionally replace a prior File object with a new file.
    // - 14. Save the File object.
    // - 15. Cache the File objects.
    //
    // There are several problems, though:
    //
    // - The plug-in validators in (3) are not needed by FolderShare.
    //
    // - The extension checking in (4) can only be turned off for
    //   the first file checked, due to a bug in the current Drupal code.
    //
    // - The extension changes in (5) and (6) are mandatory, but
    //   not needed by FolderShare because an .htaccess file for the file
    //   directory disables all dangerous interpretations of extensions.
    //
    // - The file name length check in (7) uses PHP functions that
    //   are not multi-byte character safe, and it limits names to
    //   240 characters, independent of the actual field length.
    //
    // - The file name handling loses the original file name that
    //   we need to maintain and show to users.
    //
    // - The file movement can't leave the file in our desired
    //   destination directory because that directory's name is a
    //   function of the entity ID, which isn't known until after
    //   file_save_upload() has created the file and moved it to
    //   what it thinks is the final destination.
    //
    // - Any errors generated are logged and reported directly to
    //   to the user. No exceptions are thrown. The only error
    //   indicator returned to the caller is that the array of
    //   returned File objects can include a FALSE for a file that
    //   failed. But there is no indication about which file it was
    //   that failed, or why.
    //
    // THIS function repeats some of the steps in file_save_upload(),
    // but skips ones we don't need. It also keeps track of errors and
    // returns error messages instead of blurting them out to the user.
    //
    // Validate starting point.
    // ------------------------
    // Make sure we have everything we need to get started:
    //
    // - If no field name has been provided, there is no way to proceed.
    //   It's a programmer error.
    //
    // - If there is a parent entity but it isn't a folder, files cannot be
    //   processed as children. It's a programmer error.
    //
    // - If there were no files uploaded, there's nothing to do. Return.
    if (empty($formFieldName) === TRUE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an empty field name.');
    }

    if ($parent !== NULL && $parent->isFolder() === FALSE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called on an item that is not a folder.');
    }

    if ($ownerUid < 0) {
      $ownerUid = self::getCurrentUserId()[0];
    }

    // Get a list of all uploaded files for the current request. The files
    // are organized by file field name since a single form could have
    // multiple file fields, and thus multiple groups of uploads.
    $allFiles = \Drupal::request()->files->get('files', []);

    // If there is nothing for the requested form field, return.
    if (isset($allFiles[$formFieldName]) === FALSE) {
      return [];
    }

    // If the file list for the requested form field is empty, return.
    $filesToProcess = $allFiles[$formFieldName];
    unset($allFiles);
    if (empty($filesToProcess) === TRUE) {
      return [];
    }

    // If there is just one item, turn it into an array of items
    // to simplify further code.
    if (is_array($filesToProcess) === FALSE) {
      $filesToProcess = [$filesToProcess];
    }

    //
    // Validate that each file was uploaded.
    // -------------------------------------
    // Loop through the available uploaded files and check for errors.
    // Initialize the $results array and an array of the good uploaded files.
    //
    // The $results array built below has one entry per file, in the
    // same order as the files. That entry is an associative array with
    // these keys:
    // - "filename": the file information object for the file.
    // - "error": the error message, if any.
    // - "foldershare": the FolderShare entity created for the file.
    $files = [];
    $results = [];

    foreach ($filesToProcess as $index => $fileInfo) {
      $results[$index] = [
        'filename'    => NULL,
        'error'       => '',
        'foldershare' => NULL,
      ];

      if ($fileInfo === NULL) {
        // Odd. A file is listed in the uploads, but it isn't really there.
        $results[$index]['error'] = (string) t(
          "Website internal error. The @index-th uploaded file could not be found. Please try again.",
          [
            '@index' => $index,
          ]);
        continue;
      }

      $filename = $fileInfo->getClientOriginalName();
      $results[$index]['filename'] = $filename;

      // Check for errors. On any error, create an error message
      // and add it to the results array. If the error is very
      // severe, also log it.
      switch ($fileInfo->getError()) {
        case UPLOAD_ERR_INI_SIZE:
          // Exceeds max PHP size.
        case UPLOAD_ERR_FORM_SIZE:
          // Exceeds max form size.
          $results[$index]['error'] = (string) t(
            "Maximum file size limit exceeded.\nThe file '@file' could not be added to the folder because it exceeds the website's maximum allowed file size of @maxsize.",
            [
              '@file'    => $filename,
              '@maxsize' => FormatUtilities::formatBytes(LimitUtilities::getPhpFileUploadSizeLimit()),
            ]);
          break;

        case UPLOAD_ERR_PARTIAL:
          // Uploaded only partially uploaded.
          $results[$index]['error'] = (string) t(
            "Interrupted file upload.\nThe file '@file' could not be added to the folder because the upload was interrupted and only part of the file was received.",
            [
              '@file' => $filename,
            ]);
          break;

        case UPLOAD_ERR_NO_FILE:
          // Upload wasn't started.
          $results[$index]['error'] = (string) t(
            "Maximum upload number limit exceeded.\nThe file '@file' could not be added to the folder because its inclusion exceeds the website's maximum allowed number of file uploads at one time.",
            [
              '@file' => $filename,
            ]);
          break;

        case UPLOAD_ERR_NO_TMP_DIR:
          // No temp directory configured.
          $results[$index]['error'] = (string) t(
            "Website configuration problem.\nThe file '@file' could not be added to the folder because the website encountered a site configuration error about a missing temporary directory. Please report this to the site administrator.",
            [
              '@file' => $filename,
            ]);
          ManageLog::critical(
            "Local file system: File upload failed because the server's PHP temporary directory is missing!");
          break;

        case UPLOAD_ERR_CANT_WRITE:
          // Temp directory not writable.
          $results[$index]['error'] = (string) t(
            "Website configuration problem.\nThe file '@file' could not be added to the folder because the website encountered a site configuration error. The site's temporary directory is missing write permissions. Please report this to the site administrator.",
            [
              '@file' => $filename,
            ]);
          ManageLog::critical(
            "Local file system: File upload failed because the server's PHP temporary directory is not writable!");
          break;

        case UPLOAD_ERR_EXTENSION:
          // PHP extension failed for some reason.
          $results[$index]['error'] = (string) t(
            "Website configuration problem.\nThe file '@file' could not be added to the folder because the website encountered a site configuration error. Please report this to the site administrator.",
            [
              '@file' => $filename,
            ]);
          ManageLog::critical(
            "Misconfigured web site: File upload failed because a PHP extension failed for an unknown reason.\nThe server's installation of PHP appears to be misconfigured.");
          break;

        case UPLOAD_ERR_OK:
          // Success!
          if (is_uploaded_file($fileInfo->getRealPath()) === FALSE) {
            // But the file doesn't actually exist!
            $results[$index]['error'] = (string) t(
              "Website internal error.\nThe file '@file' could not be added to the folder because the data was lost during the upload.",
              [
                '@file' => $filename,
              ]);
            ManageLog::error(
              "Local file system: File upload failed because the local uploaded file went missing after the upload completed.");
          }
          else {
            $files[$index] = $fileInfo;
          }
          break;

        default:
          // Unknown error.
          $results[$index]['error'] = (string) t(
            "Website internal error.\nThe file '@file' could not be added to the folder because of an unknown problem.",
            [
              '@file' => $filename,
            ]);
          ManageLog::warning(
            "Local file system: File upload failed with an unrecognized error '@code'.",
            [
              '@code' => $fileInfo->getError(),
            ]);
          break;
      }
    }

    unset($filesToProcess);

    if (empty($files) === TRUE) {
      return $results;
    }

    //
    // Process files.
    // --------------
    // At this point we have a list of uploaded files that all exist
    // on the server.  Create File objects.
    //
    // Get the file system service.
    $fileSystem = \Drupal::service('file_system');

    // Get the MIME type service.
    $mimeGuesser = \Drupal::service('file.mime_type.guesser');

    // Loop through the files and create initial File objects.  Move
    // each file into the Drupal temporary directory.
    $fileObjects = [];
    foreach ($files as $index => $fileInfo) {
      // Get file information from the upload.
      $filename   = $fileInfo->getClientOriginalName();
      $filesize   = $fileInfo->getSize();
      $uploadPath = $fileInfo->getRealPath();

      // Create a MIME type guess. This may be overridden later during
      // addition of the file to the folder tree.
      $filemime = $mimeGuesser->guessMimeType($filename);

      // Create a URI for a temporary location of the file within the
      // Drupal temp directory. This usually differs from the PHP temp
      // directory where the uploaded file exists now.
      $tempUri = $fileSystem->getDestinationFilename(
        'temporary://' . $filename,
        FileSystemInterface::EXISTS_RENAME);

      // Move file to Drupal temp directory.
      //
      // The file needs to be moved out of PHP's temporary directory
      // into Drupal's temporary directory.
      //
      // PHP's move_uploaded_file() can do this, but it doesn't
      // handle Drupal streams. So use Drupal's file system for this.
      //
      // Let the URI get changed to avoid collisions. This does not
      // affect the user-visible file name.
      if ($fileSystem->moveUploadedFile($uploadPath, $tempUri) === FALSE) {
        // Failure likely means a site problem, such as a bad file system,
        // full disk, etc.  Try to keep going with the rest of the files.
        //
        // DEPRECATED. Remove this when Drupal 8.7 is no longer supported.
        $versionParts = explode('.', \Drupal::VERSION);
        $drupalVersion = floatval($versionParts[0] . '.' . $versionParts[1]);
        if ($drupalVersion < 8.8) {
          // Pre-Drupal 8.8, the FileSystem's getTempDirectory method
          // did not exist. Call the deprecated file_directory_temp() function.
          $drupalTemp = call_user_func('file_directory_temp()');
        }
        // END DEPRECATED.
        else {
          $drupalTemp = $fileSystem->getTempDirectory();
        }

        $results[$index] = (string) t(
          "Website configuration error.\nThe file '@file' could not be added to the folder because the website encountered a site configuration error about a missing Drupal temporary directory. Please report this to the site administrator.",
          [
            '@file' => $filename,
          ]);
        ManageLog::critical(
          "Local file system: File upload failed because local Drupal temporary directory '@dir' is missing!",
          [
            '@dir' => $drupalTemp,
          ]);
        continue;
      }

      // Set permissions.  Make the file accessible to the web server, etc.
      FileUtilities::chmod($tempUri);

      // Create a File object. Make it owned by the current user. Give
      // it the temp URI, file name, MIME type, etc. A status of 0 means
      // the file is temporary still.
      $file = File::create([
        'uid'      => $ownerUid,
        'uri'      => $tempUri,
        'filename' => $filename,
        'filemime' => $filemime,
        'filesize' => $filesize,
        'status'   => 0,
        'source'   => $formFieldName,
      ]);

      // Save!  Saving the File object assigns it a unique entity ID.
      self::saveFileWithRetry($file);
      $fileObjects[$index] = $file;
    }

    unset($files);

    // If there are no good files left, return the errors.
    if (empty($fileObjects) === TRUE) {
      return $results;
    }

    //
    // Move into local directory.
    // --------------------------
    // This module manages files within a directory tree built from
    // the entity ID.  This entity ID is not known until after the
    // File object is done.  So we now need another pass through the
    // File objects to use their entity IDs and move the files to their
    // final destinations.
    //
    // Along the way we also mark the file as permanent.
    $movedObjects = [];

    foreach ($fileObjects as $index => $file) {
      // Create the final destination URI. This is the URI that uses
      // the entity ID and places the file within the FolderShare directory
      // tree.
      $finalUri = ManageFileSystem::getFileUri($file);

      // Move it there. The directory will be created automatically if needed.
      try {
        // Set the file's URI and make it permanent.
        self::moveFileEntity($file, $finalUri, TRUE);
        $movedObjects[$index] = $file;
      }
      catch (\Exception $e) {
        // The move failed. This is likely a file system problem.
        $results[$index]['error'] = (string) t(
          'The file "@file" could not be added to the folder because the website encountered a system failure.',
          ['@file' => $filename]);
        self::deleteFileWithRetry($file);
      }
    }

    // And reduce the good files list to the ones that got moved.
    $fileObjects = $movedObjects;
    unset($movedObjects);

    // If there are no good files left, return the errors.
    if (empty($fileObjects) === TRUE) {
      return $results;
    }

    //
    // Add to folder.
    // --------------
    // At this point, $fileObjects contains a list of fully-created
    // File objects for files that have already been moved into their
    // correct locations. Add them to the folder!
    try {
      // Watch for bad names or name collisions and rename
      // if needed. Don't bother checking if the files are already in
      // the folder since we know they aren't. Do lock.
      $added = self::addFilesInternal(
        $parent,
        $fileObjects,
        $ownerUid,
        TRUE,
        $allowRename,
        FALSE);

      foreach ($added as $index => $a) {
        if (is_string($a) === TRUE) {
          self::deleteFileWithRetry($fileObjects[$index]);
          $results[$index]['error'] = $a;
        }
        else {
          $results[$index]['foldershare'] = $a;
        }
      }
    }
    catch (\Exception $e) {
      // An unrecoverable error occurred, such as a lock that could not
      // be acquired. None of the files have been added. Abort.
      foreach ($fileObjects as $index => $file) {
        self::deleteFileWithRetry($file);
      }

      throw $e;
    }

    return $results;
  }

    private static function addFileIdsFromFormUploadInternal(
        FolderShareInterface $parent = NULL,
        array $fileIds = NULL,
        bool $allowRename = TRUE,
        int $ownerUid = (-1)) {

        //
        // Drupal's file_save_upload() function is widely used for handling
        // uploaded files. It does several activities at once:
        //
        // - 1. Collect all uploaded files.
        // - 2. Validate that the uploads completed.
        // - 3. Run plug-in validators.
        // - 4. Optionally check for file name extensions.
        // - 5. Add ".txt" on executable files.
        // - 6. Rename inner extensions (e.g. ".tar.gz" -> "._tar.gz").
        // - 7. Validate file names are not too long.
        // - 8. Validate the destination exists.
        // - 9. Optionally rename files to avoid destination collisions.
        // - 10. chmod the file to be accessible.
        // - 11. Create a File object.
        // - 12. Set the File object's URI, filename, and MIME type.
        // - 13. Optionally replace a prior File object with a new file.
        // - 14. Save the File object.
        // - 15. Cache the File objects.
        //
        // There are several problems, though:
        //
        // - The plug-in validators in (3) are not needed by FolderShare.
        //
        // - The extension checking in (4) can only be turned off for
        //   the first file checked, due to a bug in the current Drupal code.
        //
        // - The extension changes in (5) and (6) are mandatory, but
        //   not needed by FolderShare because an .htaccess file for the file
        //   directory disables all dangerous interpretations of extensions.
        //
        // - The file name length check in (7) uses PHP functions that
        //   are not multi-byte character safe, and it limits names to
        //   240 characters, independent of the actual field length.
        //
        // - The file name handling loses the original file name that
        //   we need to maintain and show to users.
        //
        // - The file movement can't leave the file in our desired
        //   destination directory because that directory's name is a
        //   function of the entity ID, which isn't known until after
        //   file_save_upload() has created the file and moved it to
        //   what it thinks is the final destination.
        //
        // - Any errors generated are logged and reported directly to
        //   to the user. No exceptions are thrown. The only error
        //   indicator returned to the caller is that the array of
        //   returned File objects can include a FALSE for a file that
        //   failed. But there is no indication about which file it was
        //   that failed, or why.
        //
        // THIS function repeats some of the steps in file_save_upload(),
        // but skips ones we don't need. It also keeps track of errors and
        // returns error messages instead of blurting them out to the user.
        //
        // Validate starting point.
        // ------------------------
        // Make sure we have everything we need to get started:
        //
        // - If no field name has been provided, there is no way to proceed.
        //   It's a programmer error.
        //
        // - If there is a parent entity but it isn't a folder, files cannot be
        //   processed as children. It's a programmer error.
        //
        // - If there were no files uploaded, there's nothing to do. Return.
        if ($fileIds === NULL || !$fileIds || empty($fileIds)) {
            // Developer-facing exception message.
            throw new ValidationException(
                __METHOD__ . ' was called with an empty list of file ids.');
        }

        if ($parent !== NULL && $parent->isFolder() === FALSE) {
            // Developer-facing exception message.
            throw new ValidationException(
                __METHOD__ . ' was called on an item that is not a folder.');
        }

        if ($ownerUid < 0) {
            $ownerUid = self::getCurrentUserId()[0];
        }

        //
        // Validate that each file was uploaded.
        // -------------------------------------
        // Loop through the available uploaded files and check for errors.
        // Initialize the $results array and an array of the good uploaded files.
        //
        // The $results array built below has one entry per file, in the
        // same order as the files. That entry is an associative array with
        // these keys:
        // - "filename": the file information object for the file.
        // - "error": the error message, if any.
        // - "foldershare": the FolderShare entity created for the file.
        $fileObjects = [];
        $results = [];

        $index = 0;
        foreach ($fileIds as $fileId) {
            $results[$index] = [
                'filename'    => NULL,
                'error'       => '',
                'foldershare' => NULL,
            ];

            $fileObject = File::load($fileId);
            $fileObjects[$index] = $fileObject;

            $filename = $fileObject->getFilename();
            $results[$index]['filename'] = $filename;
            $index++;
        }

        if (empty($fileObjects) === TRUE) {
            return $results;
        }

        // Get the file system service.
        $fileSystem = \Drupal::service('file_system');

        //
        // Move into local directory.
        // --------------------------
        // This module manages files within a directory tree built from
        // the entity ID.  This entity ID is not known until after the
        // File object is done.  So we now need another pass through the
        // File objects to use their entity IDs and move the files to their
        // final destinations.
        //
        // Along the way we also mark the file as permanent.
        $movedObjects = [];

        foreach ($fileObjects as $index => $file) {
            // Create the final destination URI. This is the URI that uses
            // the entity ID and places the file within the FolderShare directory
            // tree.
            $finalUri = ManageFileSystem::getFileUri($file);

            // Move it there. The directory will be created automatically if needed.
            try {
                // Set the file's URI and make it permanent.
                self::moveFileEntity($file, $finalUri, TRUE);
                $movedObjects[$index] = $file;
            }
            catch (\Exception $e) {
                // The move failed. This is likely a file system problem.
                $results[$index]['error'] = (string) t(
                    'The file "@file" could not be added to the folder because the website encountered a system failure.',
                    ['@file' => $filename]);
                self::deleteFileWithRetry($file);
            }
        }

        // And reduce the good files list to the ones that got moved.
        $fileObjects = $movedObjects;
        unset($movedObjects);

        // If there are no good files left, return the errors.
        if (empty($fileObjects) === TRUE) {
            return $results;
        }

        //
        // Add to folder.
        // --------------
        // At this point, $fileObjects contains a list of fully-created
        // File objects for files that have already been moved into their
        // correct locations. Add them to the folder!
        try {
            // Watch for bad names or name collisions and rename
            // if needed. Don't bother checking if the files are already in
            // the folder since we know they aren't. Do lock.
            $added = self::addFilesInternal(
                $parent,
                $fileObjects,
                $ownerUid,
                TRUE,
                $allowRename,
                FALSE);

            foreach ($added as $index => $a) {
                if (is_string($a) === TRUE) {
                    self::deleteFileWithRetry($fileObjects[$index]);
                    $results[$index]['error'] = $a;
                }
                else {
                    $results[$index]['foldershare'] = $a;
                }
            }
        }
        catch (\Exception $e) {
            // An unrecoverable error occurred, such as a lock that could not
            // be acquired. None of the files have been added. Abort.
            foreach ($fileObjects as $index => $file) {
                self::deleteFileWithRetry($file);
            }

            throw $e;
        }

        return $results;
    }

    /*---------------------------------------------------------------------
     *
     * Add file(s).
     *
     *---------------------------------------------------------------------*/
  /**
   * Adds a file to the root list.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * If $allowRename is FALSE, an exception is thrown if the file's
   * name is not unique within the root list. But if $allowRename is
   * TRUE and the name is not unique, the file's name is adjusted
   * to include a sequence number immediately before the first "."
   * in the name, or at the end of the name if there is no "."
   * (e.g. "myfile.png" becomes "myfile 1.png").
   *
   * An exception is thrown if the file is already in a folder.
   *
   * <B>This method assumes the File entity refers to a valid file, that
   * the file is properly named using the File entity ID, and that the
   * file is in the module's files directory.</B>
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive editing access by this
   * function for the duration of the modification.
   *
   * <B>Activity log</B>
   * This method posts a log message after each file is added.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file to be added to the root list.
   * @param bool $allowRename
   *   (optional, default = FALSE) When TRUE, the file's name should be
   *   automatically renamed to insure it is unique within the folder.
   *   When FALSE, non-unique file names cause an exception to be thrown.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the new FolderShare entity wrapping the given file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. The file
   *   will not have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the given file is NULL, it does not have a
   *   legal name, it uses a filename extension that is not allowed, or
   *   the name collides with another item already in the root list and
   *   the file could not be renamed because $allowRename is FALSE.
   *
   * @see ::addFiles()
   * @see ::addFilesToRoot()
   * @see ::addFileToRootFromFormUpload()
   * @see ::addFileToRootFromInputStream()
   * @see ::addFileToRootFromLocalFile()
   * @see ::addFilesInternal()
   * @see ManageHooks::callHookValidateFile()
   * @see ManageHooks::callHookAllowedFilenameExtensionsAlter()
   * @see ManageHooks::callHookFileUploadSizeAlter()
   */
  private static function addFileToRoot(
    FileInterface $file,
    bool $allowRename = FALSE,
    int $ownerUid = (-1)) {

    if ($file === NULL) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with a NULL File to add.');
    }

    $added = self::addFilesToRoot([$file], $allowRename, $ownerUid);
    if (empty($added) === TRUE) {
      // Should not be possible.
      return NULL;
    }

    // If a string was returned, throw an exception.
    if (is_string($added[0]) === TRUE) {
      // User-facing exception message.
      throw new ValidationException($added[0]);
    }

    return $added[0];
  }

  /**
   * Adds files to the root list.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * If $allowRename is FALSE, an exception is thrown if the file's
   * name is not unique within the root list. But if $allowRename is
   * TRUE and the name is not unique, the file's name is adjusted
   * to include a sequence number immediately before the first "."
   * in the name, or at the end of the name if there is no "."
   * (e.g. "myfile.png" becomes "myfile 1.png").
   *
   * An exception is thrown if any file is already in a folder.
   *
   * <B>This method assumes the File entity refers to a valid file, that
   * the file is properly named using the File entity ID, and that the
   * file is in the module's files directory.</B>
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * The user's root list is locked for exclusive editing access by this
   * function for the duration of the modification.
   *
   * <B>Activity log</B>
   * This method posts a log message after each file is added.
   *
   * @param \Drupal\file\FileInterface[] $files
   *   An array of files to be added to the root list.  NULL files
   *   are silently skipped.
   * @param bool $allowRename
   *   (optional, default = FALSE) When TRUE, a file's name should be
   *   automatically renamed to insure it is unique within the folder.
   *   When FALSE, non-unique file names cause an exception to be thrown.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return mixed[]
   *   Returns an array with one entry for each file in the $files array.
   *   Each entry is either a FolderShareInterface object for the new entity
   *   wrapping the file, or it is a text string containing an error message
   *   explaining why the file could not be added.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. No files
   *   will have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the parent is not NULL and not a folder. No
   *   files will be added.
   *
   * @see ::addFiles()
   * @see ::addFileToRoot()
   * @see ::addFileToRootFromFormUpload()
   * @see ::addFileToRootFromInputStream()
   * @see ::addFileToRootFromLocalFile()
   * @see ::addFilesInternal()
   * @see ManageHooks::callHookValidateFile()
   * @see ManageHooks::callHookAllowedFilenameExtensionsAlter()
   * @see ManageHooks::callHookFileUploadSizeAlter()
   */
  private static function addFilesToRoot(
    array $files,
    bool $allowRename = FALSE,
    int $ownerUid = (-1)) {

    return self::addFilesInternal(
      NULL,
      $files,
      $ownerUid,
      TRUE,
      $allowRename,
      TRUE);
  }

  /**
   * Adds a file to this folder.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * If $allowRename is FALSE, an exception is thrown if the file's
   * name is not unique within the folder. But if $allowRename is
   * TRUE and the name is not unique, the file's name is adjusted
   * to include a sequence number immediately before the first "."
   * in the name, or at the end of the name if there is no "."
   * (e.g. "myfile.png" becomes "myfile 1.png").
   *
   * An exception is thrown if the file is already in a folder.
   *
   * <B>This method assumes the File entity refers to a valid file, that
   * the file is properly named using the File entity ID, and that the
   * file is in the module's files directory.</B>
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * This method locks this item's root folder tree for exclusive use for
   * the duration of the addition. This will prevent any other edit operation
   * from being performed on the same folder tree until the addition
   * completes.
   *
   * <B>Activity log</B>
   * This method posts a log message after the files have been added.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file to be added to this folder.
   * @param bool $allowRename
   *   (optional, default = FALSE) When TRUE, the file's name should be
   *   automatically renamed to insure it is unique within the folder.
   *   When FALSE, non-unique file names cause an exception to be thrown.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns the new FolderShare entity wrapping the given file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. The file
   *   will not have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the given file is NULL, it does not have a
   *   legal name, it uses a filename extension that is not allowed, or
   *   the name collides with another item already in the root list and
   *   the file could not be renamed because $allowRename is FALSE.
   *
   * @see ::addFileToRoot()
   * @see ::addFiles()
   * @see ::addFileFromFormUpload()
   * @see ::addFileFromInputStream()
   * @see ::addFileFromLocalFile()
   * @see ::addFilesInternal()
   * @see ManageHooks::callHookValidateFile()
   * @see ManageHooks::callHookAllowedFilenameExtensionsAlter()
   * @see ManageHooks::callHookFileUploadSizeAlter()
   */
  private function addFile(
    FileInterface $file,
    bool $allowRename = FALSE,
    int $ownerUid = (-1)) {

    if ($file === NULL) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with a NULL File to add.');
    }

    $added = $this->addFiles([$file], $allowRename, $ownerUid);
    if (empty($added) === TRUE) {
      // Should not be possible.
      return NULL;
    }

    // If a string was returned, throw an exception.
    if (is_string($added[0]) === TRUE) {
      // User-facing exception message.
      throw new ValidationException($added[0]);
    }

    return $added[0];
  }

  /**
   * Adds files to this folder.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * If $allowRename is FALSE, an exception is thrown if the file's
   * name is not unique within the folder. But if $allowRename is
   * TRUE and the name is not unique, the file's name is adjusted
   * to include a sequence number immediately before the first "."
   * in the name, or at the end of the name if there is no "."
   * (e.g. "myfile.png" becomes "myfile 1.png").
   *
   * All files are checked and renamed before any files are added
   * to the folder. If any file is invalid, an exception is thrown
   * and no files are added.
   *
   * An exception is thrown if any file is already in a folder.
   *
   * <B>This method assumes the File entity refers to a valid file, that
   * the file is properly named using the File entity ID, and that the
   * file is in the module's files directory.</B>
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * This method locks this item's root folder tree for exclusive use for
   * the duration of the addition. This will prevent any other edit operation
   * from being performed on the same folder tree until the addition
   * completes.
   *
   * <B>Activity log</B>
   * This method posts a log message after the files have been added.
   *
   * @param \Drupal\file\FileInterface[] $files
   *   An array of files to be added to this folder.  NULL files
   *   are silently skipped.
   * @param bool $allowRename
   *   (optional, default = FALSE) When TRUE, each file's name should be
   *   automatically renamed to insure it is unique within the folder.
   *   When FALSE, non-unique file names cause an exception to be thrown.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   *
   * @return mixed[]
   *   Returns an array with one entry for each file in the $files array.
   *   Each entry is either a FolderShareInterface object for the new entity
   *   wrapping the file, or it is a text string containing an error message
   *   explaining why the file could not be added.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. No files
   *   will have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the parent is not NULL and not a folder. No
   *   files will be added.
   *
   * @see ::addFile()
   * @see ::addFilesToRoot()
   * @see ::addFileFromFormUpload()
   * @see ::addFileFromInputStream()
   * @see ::addFileFromLocalFile()
   * @see ManageHooks::callHookValidateFile()
   * @see ManageHooks::callHookAllowedFilenameExtensionsAlter()
   * @see ManageHooks::callHookFileUploadSizeAlter()
   */
  private function addFiles(
    array $files,
    bool $allowRename = FALSE,
    int $ownerUid = (-1)) {

    return self::addFilesInternal(
      $this,
      $files,
      $ownerUid,
      TRUE,
      $allowRename,
      TRUE);
  }

  /**
   * Implements adding files to a root list or folder.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * If $checkForInFolder is TRUE, all files are checked first to see if they
   * are already in the folder tree. If so, they are silently skipped.
   *
   * If $checkNames is TRUE, the names used by the files are checked to
   * see that they are legal, use allowed filename extensions, and do not
   * collide with other items in the same root list or folder. On a collision,
   * if $allowRename is TRUE, the file's name is silently renamed by adding
   * a unique sequence number immediately before the first "." in the name.
   *
   * Files with errors are skipped, while all other files are added. The
   * returned array contains the created FolderShare entities for added
   * files, or an error message for files with errors.
   *
   * <B>Hooks</B>
   * The "hook_foldershare_validate_file" hook is called to validate the file
   * before it is added to the folder tree.
   *
   * The "hook_foldershare_mime_type_alter" hook is called with each unsaved
   * FolderShare file entity and a default MIME type.
   *
   * The "hook_foldershare_post_operation_add_files" hook is called after the
   * file has been added to the folder tree.
   *
   * <B>Process locks</B>
   * This method locks the user's root list for additions at the root level,
   * or locks the destination parent's root folder tree for additions to a
   * folder. Locks are maintained for the duration of the addition.
   *
   * <B>Activity log</B>
   * This method posts a log message after each file is added.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   The parent entity, or NULL for the root list, to which to add
   *   the files.
   * @param \Drupal\file\FileInterface[] $files
   *   An array of File enities to be added to this folder.  An empty
   *   array and NULL file objects are silently skipped.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new files.
   * @param bool $checkNames
   *   (optional, default = TRUE) When TRUE, each file's name is checked
   *   to be sure that it is valid and not already in use. When FALSE,
   *   name checking is skipped (including renaming) and the caller must
   *   assure that names are good.
   * @param bool $allowRename
   *   (optional, default = TRUE) When TRUE (and when $checkNames is TRUE),
   *   each file's name will be automatically renamed, if needed, to insure
   *   that it is unique within the folder. When FALSE (and when $checkNames
   *   is TRUE), non-unique file names cause an exception to be thrown.
   * @param bool $checkForInFolder
   *   (optional, default = TRUE) When TRUE, an exception is thrown if the
   *   file is already in a folder. When FALSE, this expensive check is
   *   skipped.
   *
   * @return mixed[]
   *   Returns an array with one entry for each file in the $files array.
   *   Each entry is either a FolderShareInterface object for the new entity
   *   wrapping the file, or it is a text string containing an error message
   *   explaining why the file could not be added.
   *
   * @throws \Drupal\foldershare\Entity\Exception\LockException
   *   Throws an exception if an access lock could not be acquired. No files
   *   will have been added.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the parent is not NULL and not a folder. No
   *   files will be added.
   *
   * @see ::addFile()
   * @see ::addFiles()
   * @see ::addFileToRoot()
   * @see ::addFilesToRoot()
   * @see ManageHooks::callHookValidateFile()
   * @see ManageHooks::callHookAllowedFilenameExtensionsAlter()
   * @see ManageHooks::callHookFileUploadSizeAlter()
   */
  private static function addFilesInternal(
    FolderShareInterface $parent = NULL,
    array $files = [],
    int $ownerUid = (-1),
    bool $checkNames = TRUE,
    bool $allowRename = FALSE,
    bool $checkForInFolder = TRUE) {

    // Validate starting point.
    // ------------------------
    // Make sure we have everything we need to get started:
    //
    // - If no files have been provided, there's nothing to add. Return.
    //
    // - If there is a parent entity but it isn't a folder, files cannot be
    //   processed as children. It's a programmer error.
    //
    // - For any File that is already in a folder, save it's FolderShare
    //   entity to the results and silently skip it.
    //
    if (empty($files) === TRUE) {
      return [];
    }

    if ($parent !== NULL && $parent->isFolder() === FALSE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with a parent item that is not a folder.');
    }

    if ($ownerUid < 0) {
      $ownerUid = self::getCurrentUserId()[0];
    }

    // Mark NULL files. There shouldn't be any.
    $results = [];
    foreach ($files as $index => $file) {
      if ($file === NULL) {
        $results[$index] = (string) t(
          '@method was called with a NULL file array entry at index @index.',
          [
            '@method' => __METHOD__,
            '@index'  => $index,
          ]);
      }
    }

    // Remove files from the $files list if they have already been
    // added to the folder tree. This could only occur in error, such as
    // with a redundant call to this method.
    if ($checkForInFolder === TRUE) {
      foreach ($files as $index => $file) {
        if ($file === NULL) {
          continue;
        }

        // Look up the file. This returns FALSE if the file is not found,
        // and the FolderShare entity ID wrapping it if it is found.
        $wrapperId = self::findFileWrapperId($file);

        if ($wrapperId !== FALSE) {
          // File entity already in use. Note the wrapper entity and skip it.
          $results[$index] = self::load($wrapperId);
          $files[$index] = NULL;
        }
      }
    }

    //
    // Check file validity.
    // --------------------
    // Let module hooks check if each file has an acceptable size, and is
    // properly formatted and valid for the current use.
    $fileUploadLimit = ManageHooks::callHookFileUploadSizeLimitAlter(
      $parent,
      $ownerUid,
      0);

    foreach ($files as $index => $file) {
      if ($file === NULL) {
        continue;
      }

      $uri = $file->getFileUri();
      $size = FileUtilities::filesize($uri);
      if ($size > $fileUploadLimit) {
        $results[$index] = (string) t(
          'The file "%filename" is too large to store here and has been skipped. Please limit files to @bytes bytes.',
          [
            '%filename' => $file->getFilename(),
            '@bytes'    => number_format($fileUploadLimit),
          ]);
        $files[$index] = NULL;
        continue;
      }

      try {
        ManageHooks::callHookValidateFile($parent, $ownerUid, $file);
      }
      catch (\Exception $e) {
        // The file did not validate. Save the message and skip the file.
        $results[$index] = $e->getMessage();
        $files[$index] = NULL;
      }
    }

    // START CRITICAL REGION.
    $originalUserAbort = @ignore_user_abort(TRUE);

    //
    // Lock root list or root folder.
    // ------------------------------
    // When adding files to the user's root list, lock that root list.
    // This lock is needed while we check for name collisions there.
    //
    // When adding files to a folder, lock that folder. This too is
    // needed while we check for name collisions theer.
    if ($parent === NULL) {
      // LOCK USER'S ROOT LIST.
      if (self::acquireUserRootListLock() === FALSE) {
        // END CRITICAL REGION.
        @ignore_user_abort($originalUserAbort);
        // User-facing exception message.
        $operator = t('added');
        throw new LockException(
          self::getStandardLockExceptionMessage($operator, NULL));
      }
    }
    else {
      // LOCK PARENT ROOT'S FOLDER TREE.
      $rootId = $parent->getRootItemId();
      if (self::acquireRootOperationLock($rootId) === FALSE) {
        // END CRITICAL REGION.
        @ignore_user_abort($originalUserAbort);
        // User-facing exception message.
        $operator = t('added');
        throw new LockException(
          self::getStandardLockExceptionMessage($operator, NULL));
      }
    }

    //
    // Check file names.
    // -----------------
    // Check that all files have legal names and allowed filename extensions,
    // and that they do not collide (or can be renamed to not collide) with
    // anything already in the destination.
    if ($checkNames === TRUE) {
      // Prepare to check for allowed filename extensions by getting the
      // site's allowed list, altered by module hooks.
      $extensions = ManageHooks::callHookAllowedFilenameExtensionsAlter(
        $parent,
        $ownerUid,
        []);

      // Prepare to check for name collisions by getting a list of names
      // already in use in the destination root list or folder.
      if ($parent === NULL) {
        $childNamesWithIds = self::findAllRootItemNames($ownerUid);
      }
      else {
        $childNamesWithIds = $parent->findChildrenNames();
      }

      // Loop through the files.
      foreach ($files as $index => $file) {
        if ($file === NULL) {
          continue;
        }

        $name = $file->getFilename();

        // Validate that the file's name and extension are usable.
        try {
          self::validateNameAndFilenameExtension($name, $extensions);
        }
        catch (\Exception $e) {
          $results[$index] = $e->getMessage();
          $files[$index] = NULL;
          continue;
        }

        // Validate that the name is not already in use.
        if ($allowRename === FALSE) {
          if (isset($childNamesWithIds[$name]) === TRUE) {
            // Not unique and not allowed to rename. Fail.
            $results[$index] = (string) self::getStandardNameInUseExceptionMessage($name);
            $files[$index] = NULL;
            continue;
          }

          // Add to the child name list so that further checks
          // will catch collisions.
          $childNamesWithIds[$name] = 1;
        }
        else {
          $uniqueName = self::createUniqueName($childNamesWithIds, $name);
          if ($uniqueName === FALSE) {
            // Not unique and rename failed.
            $results[$index] = (string) self::getStandardCannotCreateUniqueNameExceptionMessage('new file');
            $files[$index] = NULL;
            continue;
          }

          // The file was OK as-is, or it was renamed. Update the
          // File entity if needed.
          if ($uniqueName !== $name) {
            $file->setFilename($uniqueName);
            $file->setFileUri(ManageFileSystem::getFileUri($file));
            self::saveFileWithRetry($file);
          }

          // Add to the child name list so that further checks
          // will catch collisions.
          $childNamesWithIds[$uniqueName] = 1;
        }
      }
    }

    //
    // Add files.
    // ----------
    // Create FolderShare entities for the files. For each one,
    // set the parent ID to be the parent folder, and the root ID to
    // be the parent folder's root.
    $parentid    = ($parent === NULL) ? NULL : $parent->id();
    $rootid      = ($parent === NULL) ? NULL : $parent->getRootItemId();
    $newEntities = [];

    foreach ($files as $index => $file) {
      if ($file === NULL) {
        continue;
      }

      // Create new FolderShare entity in the parent folder or the root list.
      // Initially, call the file a data file, not an image. This is adjusted
      // below, if needed.
      // - Automatic id.
      // - Automatic uuid.
      // - Automatic creation date.
      // - Automatic changed date.
      // - Automatic langcode.
      // - Empty description.
      // - Empty author grants (set below).
      // - Empty view grants (set below).
      // - Empty MIME type (set below).
      $wrapper = self::create([
        'name'     => $file->getFilename(),
        'uid'      => $ownerUid,
        'kind'     => FolderShareInterface::FILE_KIND,
        'mime'     => $file->getMimeType(),
        'file'     => $file->id(),
        'size'     => $file->getSize(),
        'parentid' => $parentid,
        'rootid'   => $rootid,
      ]);

      // Add default grants to a root item.
      $wrapper->addDefaultAccessGrants();

      // Set the default MIME type. This will invoke the MIME type guesser
      // and hooks to decide on the default MIME type. This overrides any
      // MIME type the file already has and updates the File entity to
      // match the new FolderShare entity.
      $wrapper->setMimeTypeToDefault();

      // If the MIME type indicates the file is an image, move the File entity
      // ID to the 'image' field and switch the kind to IMAGE_KIND.
      if (self::isMimeTypeImage($wrapper->getMimeType()) === TRUE) {
        $wrapper->swapFileToImage();
      }

      $wrapper->save();

      $newEntities[] = $results[$index] = $wrapper;
    }

    // Update the sizes of the parent folder and its ancestors.
    if ($parent !== NULL) {
      $parent->updateSizeAndAncestors();
    }

    //
    // Unlock.
    // -------
    // Unlock the user's root list or the parent root folder tree, depending
    // on which one was locked earlier.
    if ($parent === NULL) {
      // UNLOCK USER'S ROOT LIST.
      self::releaseUserRootListLock();
    }
    else {
      // UNLOCK PARENT ROOT'S FOLDER TREE.
      self::releaseRootOperationLock($parent->getRootItemId());
    }

    //
    // Hook & log.
    // -----------
    // Announce the addition.
    if (empty($newEntities) === FALSE) {
      if ($parent === NULL) {
        ManageHooks::callHookPostOperation(
          'add_files',
          [
            NULL,
            $newEntities,
            $ownerUid,
          ]);
        foreach ($newEntities as $e) {
          ManageLog::activity(
            "Added file '@name' (# @id) to top level.",
            [
              '@id'    => $e->id(),
              '@name'  => $e->getName(),
              'entity' => $e,
              'uid'    => $ownerUid,
            ]);
        }
      }
      else {
        ManageHooks::callHookPostOperation(
          'add_files',
          [
            $parent,
            $newEntities,
            $ownerUid,
          ]);
        foreach ($newEntities as $e) {
          ManageLog::activity(
            "Added file '@name' (# @id) to '@parentName' (# @parentId).",
            [
              '@id'         => $e->id(),
              '@name'       => $e->getName(),
              '@parentId'   => $parent->id(),
              '@parentName' => $parent->getName(),
              'entity'      => $e,
              'uid'         => $ownerUid,
            ]);
        }
      }
    }

    // END CRITICAL REGION.
    @ignore_user_abort($originalUserAbort);

    return $results;
  }

  /*---------------------------------------------------------------------
   *
   * File utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Creates a new local file from data read from the PHP input stream.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * PHP's input stream is opened and read to acquire incoming data
   * to route into a new temporary file. The URI of the new file is returned.
   *
   * @return string
   *   The URI of the new temporary file.
   *
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if one of the following occurs:
   *   - The input stream cannot be read.
   *   - A temporary file cannot be created.
   *   - A temporary file cannot be written.
   *
   * @see ::addFileToRootFromInputStream()
   * @see ::addFileFromInputStream()
   */
  private static function createLocalFileFromInputStream() {
    //
    // Open stream.
    // ------------
    // Use the "php" stream to access incoming data appended to the
    // current HTTP request. The stream is opened for reading in
    // binary (the binary part is required for Windows).
    $stream = fopen('php://input', 'rb');
    if ($stream === FALSE) {
      // Developer-facing exception message.
      throw new SystemException(
        "System error. An input stream cannot be opened or read.");
    }

    //
    // Create temp file.
    // -----------------
    // Use the Drupal "temproary" stream to create a temporary file and
    // open it for writing in binary.
    $tempUri = FileUtilities::tempnam('temporary://', 'file');
    $temp = fopen($tempUri, 'wb');
    if ($temp === FALSE) {
      fclose($stream);
      // Developer-facing exception message.
      throw new SystemException(
        "System error. A temporary file at '$tempUri' could not be created.\nThere may be a problem with directories or permissions. Please report this to the site administrator.");
    }

    //
    // Copy stream to file.
    // --------------------
    // Loop through the input stream until EOF, copying data into the
    // temporary file.
    while (feof($stream) === FALSE) {
      $data = fread($stream, 8192);

      if ($data === FALSE) {
        // We aren't at EOF, but the read failed. Something has gone wrong.
        fclose($stream);
        fclose($temp);
        // Developer-facing exception message.
        throw new SystemException(
          "System error. An input stream cannot be opened or read.");
      }

      if (fwrite($temp, $data) === FALSE) {
        fclose($stream);
        fclose($temp);
        // Developer-facing exception message.
        throw new SystemException(
          "System error. A file at '$temp' could not be written.\nThere may be a problem with permissions. Please report this to the site administrator.");
      }
    }

    // Close the stream and file.
    fclose($stream);
    fclose($temp);

    return $tempUri;
  }

  /**
   * Creates a File entity for an existing local file.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * A new File entity is created using the existing file, and named using
   * the given file name. In the process, the file is moved into the proper
   * FolderShare directory tree and the stored file renamed using the
   * module's numeric naming scheme.
   *
   * The filename, MIME type, and file size are set, the File entity is
   * marked as a permanent file, and the file's permissions are set for
   * access by the web server.
   *
   * <B>Process locks</B>
   * This function does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param string $uri
   *   The URI to a stored file on the local file system.
   * @param string $filename
   *   The publically visible file name for the new File entity. This should
   *   be a legal file name, without a preceding path or scheme. The MIME
   *   type of the new File entity is based on this name.
   * @param int $ownerUid
   *   (optional, default = (-1) = current user) The user ID of the owner
   *   of the new file.
   * @param bool $moveFile
   *   (optional, default = TRUE) When TRUE, the indicated file is moved into
   *   the module's files directory. When FALSE, the indicated file is copied
   *   instead.
   *
   * @return \Drupal\file\FileInterface
   *   Returns a new File entity with the given file name, and a properly
   *   set MIME type and size. The entity is owned by the current user.
   *   The File's URI points to a FolderShare directory tree file moved
   *   from the given local path. A NULL is returned if the local path is
   *   empty.
   *
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the URI or filename are empty strings.
   *
   * @throws \Drupal\foldershare\Entity\Exception\SystemException
   *   Throws an exception if an error occurs when trying to create, copy,
   *   or move a file, or create a directory.
   */
  public static function createFileEntityFromLocalFile(
    string $uri,
    string $filename,
    int $ownerUid = (-1),
    bool $moveFile = TRUE) {

    if (empty($uri) === TRUE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an empty source file URI.');
    }

    if (empty($filename) === TRUE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an empty destination filename.');
    }

    if ($ownerUid < 0) {
      $ownerUid = self::getCurrentUserId()[0];
    }

    //
    // Copy the local file, if needed.
    // -------------------------------
    // If the local file needs to be copied first, copy it into the Drupal
    // temp directory.
    if ($moveFile === FALSE) {
      $fileSystem = \Drupal::service('file_system');
      $tempUri = FileUtilities::tempnam('temporary://', 'tmp');
      try {
        $tempUri = $fileSystem->copy(
          $uri,
          $tempUri,
          FileSystemInterface::EXISTS_RENAME);
      }
      catch (FileException $e) {
        ManageLog::critical(
          "Local file system: A file at '@uri' could not be copied to '@tempUri'.\nThere may be a problem with the server's directories or permissions.",
          [
            '@uri'     => $uri,
            '@tempUri' => $tempUri,
          ]);
        // Developer-facing exception message.
        throw new SystemException(
          "System error. A file at '$uri' could not be moved to '$tempUri'.\nThere may be a problem with directories or permissions. Please report this to the site administrator.");
      }

      $uri = $tempUri;
    }

    //
    // Create initial File entity.
    // ---------------------------
    // Create a File object that wraps the file in its current location.
    // This is not the final File object, which must be adjusted by moving
    // the file from its current location to FolderShare's directory tree.
    //
    // The status of 0 marks the file as temporary. If left this way, the
    // File module will automatically delete the file in the future.
    $mimeType = \Drupal::service('file.mime_type.guesser')->guessMimeType($filename);
    $fileSize = FileUtilities::filesize($uri);

    $file = File::create([
      'uid'      => $ownerUid,
      'uri'      => $uri,
      'filename' => $filename,
      'filemime' => $mimeType,
      'filesize' => $fileSize,
      'status'   => 0,
    ]);

    self::saveFileWithRetry($file);

    //
    // Move file and update File entity.
    // ---------------------------------
    // Creating the File object assigns it a unique ID. We need this ID
    // to create a new long-term local file name and location when the
    // file is in its proper location in the FolderShare directory tree.
    //
    // Create the new proper file URI with a numeric ID-based name and path.
    $storedUri = ManageFileSystem::getFileUri($file);

    // Move the stored file from its current location to the FolderShare
    // directory tree and rename it using the File entity's ID.
    try {
      // Move the file to the new URI and make it permanent.
      // The URI is changed, but NOT the filename, MIME type, etc.
      self::moveFileEntity($file, $storedUri, TRUE);
    }
    catch (\Exception $e) {
      // The file move has failed.
      self::deleteFileWithRetry($file);
      ManageLog::critical(
        "Local file system: A file at '@uri' could not be moved to '@storedUri'.\nThere may be a problem with the server's directories or permissions.",
        [
          '@uri'       => $uri,
          '@storedUri' => $storedUri,
        ]);
      // Developer-facing exception message.
      throw new SystemException(
        "System error. A file at '$uri' could not be moved to '$storedUri'.\nThere may be a problem with directories or permissions. Please report this to the site administrator.");
    }

    return $file;
  }

  /**
   * Duplicates a File entity.
   *
   * The file wrapped by the source File entity is duplicated and wrapped by
   * a new File entity, which is then returned.
   *
   * Exceptions are very unlikely and should only occur when something
   * catastrophic happens to the underlying file system, such as if it
   * runs out of space, if someone deletes key directories, or if the
   * file system goes offline.
   *
   * <B>Hooks</B>
   * For compatibility with the File module, the "file_copy" hook is called
   * upon completion, passing it the destination and source File entities.
   *
   * <B>Process locks</B>
   * This function does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param \Drupal\file\FileInterface $source
   *   The file to copy.
   * @param string $destinationName
   *   (optional, default = '') The new filename for the file copy. If empty,
   *   the existing filename is used.
   * @param int $destinationOwnerUid
   *   (optional, default = (-1)) The new owner ID for the file copy.
   *   If negative, the existing owner ID is used.
   *
   * @return \Drupal\file\FileInterface
   *   The new file copy.
   *
   * @throws \Drupal\Core\File\Exception\FileException
   *   Throws an exception if the file cannot be copied.
   *
   * @see ::copyAndAddFilesInternal()
   */
  public static function duplicateFileEntity(
    FileInterface $source,
    string $destinationName = '',
    int $destinationOwnerUid = (-1)) {

    if (empty($destinationName) === TRUE) {
      $destinationName = $source->getFilename();
    }

    if ($destinationOwnerUid < 0) {
      $destinationOwnerUid = $source->getOwnerId();
    }

    $fileSystem = \Drupal::service('file_system');

    //
    // Copy the wrapped file to a temp file.
    //
    // If the source or destination are bad, an exception is thrown.
    $copiedUri = $fileSystem->copy(
      $source->getFileUri(),
      ManageFileSystem::createLocalTempFile(),
      FileSystemInterface::EXISTS_RENAME);

    //
    // Create a new File entity. Update the URI and filename, but keep
    // all other fields (e.g. MIME type, size).
    $destination = $source->createDuplicate();
    $destination->setFileUri($copiedUri);
    $destination->setFilename($destinationName);
    $destination->setOwnerId($destinationOwnerUid);
    self::saveFileWithRetry($destination);

    //
    // Saving has assigned a unique File entity ID. Use that ID to create
    // a final URI for the copy, then move the file to it.
    try {
      $destinationUri = ManageFileSystem::getFileUri($destination);
      self::moveFileEntity($destination, $destinationUri, TRUE);
    }
    catch (\Exception $e) {
      // The file move has failed.
      self::deleteFileWithRetry($destination);
      throw $e;
    }

    // Hooks should be passive and just note the change. But it is possible
    // for a hook to be destructive and change the file name, URI, or
    // anything. This would make a mess but there's not much we can do.
    \Drupal::moduleHandler()->invokeAll(
      'file_copy',
      [
        $destination,
        $source,
      ]);

    return $destination;
  }

  /**
   * Moves the file within a File entity.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The file referenced by a File entity is moved from its current URI
   * to a destination URI. If there is an existing file at the destination,
   * an exception is thrown.
   *
   * The File entity is updated by the move, changing its URI and saving it.
   *
   * <B>Hooks</B>
   * For compatibility with the File module, the "file_move" hook is called
   * upon completion, passing it the source File entity.
   *
   * <B>Process locks</B>
   * This function does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param \Drupal\file\FileInterface $source
   *   The File entity referring to a file to be moved.
   * @param string $destinationUri
   *   The URI for the moved file.
   * @param bool $savePermanent
   *   (optional, default = TRUE) When TRUE, the File entity is marked as
   *   permanent. When FALSE, the File entity is not changed from whatever
   *   state it currently has.
   *
   * @return \Drupal\file\FileInterface
   *   The updated File entity is returned.
   *
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the destination URI uses an invalid scheme.
   * @throws \Drupal\Core\File\Exception\FileException
   *   Throws an exception if the file cannot be moved.
   *
   * @internal
   * The Drupal core File module provides a file_move() method intended to
   * move the file referenced by a File entity. However, the method has a
   * few problems that are fixed by this module's method:
   *
   * - file_move() reports destination URI problems by logging them and
   *   returning FALSE. It should be throwing an exception instead so that
   *   calling code can decide if the problem is worth logging.
   *
   * - file_move() also reports destination URI problems to the messenger,
   *   which causes an alarming message to be shown to the user. Again, it
   *   should throw an exception instead and never blurt things out directly
   *   to the user.
   *
   * - file_move() supports replace and rename behaviors when the destination
   *   URI refers to an existing file. For FolderShare, these should never be
   *   used becuase the file directory is carefully managed. An exception
   *   should be thrown on a name collision.
   *
   * - file_move() clones the original File entity, changes the clone, saves
   *   it, then deletes the original. This causes extra database activity
   *   we can avoid by changing the original File entity directly.
   *
   * - file_move() can change the stored file name after moving the file.
   *   Since FolderShare uses the stored file name as the user-visible name,
   *   separate from the stored name, a move of the stored file should not
   *   change the file name.
   *
   * - file_move() calls save() and delete() on File entities. We need to
   *   replace these with retry-loop versions to accommodate rare database
   *   lock errors.
   */
  private static function moveFileEntity(
    FileInterface $source,
    string $destinationUri,
    bool $savePermanent = TRUE) {

    $fileSystem = \Drupal::service('file_system');

    //
    // Validate the destination URI.
    //
    $versionParts = explode('.', \Drupal::VERSION);
    $drupalVersion = floatval($versionParts[0] . '.' . $versionParts[1]);
    if ($drupalVersion < 8.8) {
      $scheme = $fileSystem->uriScheme($destinationUri);
      $schemeValid = $fileSystem->validScheme($scheme);
    }
    else {
      $mgr = \Drupal::service('stream_wrapper_manager');
      $schemeValid = $mgr->isValidUri($destinationUri);
    }

    if ($schemeValid === FALSE) {
      // Developer-facing exception message.
      throw new ValidationException(
        __METHOD__ . ' was called with an invalid destination URI.');
    }

    //
    // Clone the source File entity if we'll need it for hook calls.
    $moduleHandler = \Drupal::moduleHandler();
    $sourceClone = NULL;
    if ($moduleHandler->hasImplementations('file_move')) {
      // There is at least one hook to notify on a file move. Since the
      // hook needs to know the original File URI, create a clone to pass
      // to the hook. Do not save the clone.
      $sourceClone = clone $source;
    }

    //
    // Move the file referenced by the File entity.
    //
    // If the source or destination are bad, an exception is thrown.
    $sourceUri = $source->getFileUri();
    $movedUri = $fileSystem->move(
      $sourceUri,
      $destinationUri,
      FileSystemInterface::EXISTS_ERROR);

    //
    // Update the File entity.
    try {
      $source->setFileUri($movedUri);
      if ($savePermanent === TRUE) {
        $source->setPermanent();
      }
      self::saveFileWithRetry($source);
    }
    catch (\Exception $e) {
      // The save failed. This can occur very rarely due to database lock
      // problems that retries attempt to get around. If retries fail, the
      // save has failed. Try to move the file back to its original location.
      try {
        $movedUri = $fileSystem->move(
          $movedUri,
          $sourceUri,
          FileSystemInterface::EXISTS_ERROR);
      }
      catch (\Exception $e) {
        // Restoring the file has failed. This is very unusual, but it leaves
        // us with no way to proceed. The File entity is now invalid and it
        // cannot be updated to be valid.
        //
        // The best we can do is fall through and throw the original database
        // exception.
      }

      throw $e;
    }

    // Notify hooks.
    if ($sourceClone !== NULL) {
      // Hooks should be passive and just note the change. But it is possible
      // for a hook to be destructive and change the file name, URI, or
      // anything. This would make a mess but there's not much we can do.
      $moduleHandler->invokeAll(
        'file_move',
        [
          $source,
          $sourceClone,
        ]);
    }

    return $source;
  }

}
