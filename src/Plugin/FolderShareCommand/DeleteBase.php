<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Provides the base class for command plugins that delete files or folders.
 *
 * The command deletes all selected entities. Deletion recurses and
 * deletes all folder content as well.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to delete.
 *
 * @ingroup foldershare
 */
abstract class DeleteBase extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    return TRUE;
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription(bool $forPage) {
    $ids = $this->getSelectionIds();
    $kindsAndIds = FolderShare::findKindsForIds($ids);
    $shared = $this->doesSelectionContainSharedWithOthers();

    if (count($ids) === 1) {
      $kinds = array_keys($kindsAndIds);
      if ($shared === TRUE) {
        return $this->t(
          "This @kind will be permanently deleted. Since it is shared, deleting it will affect other users. You can't undo this action.",
          [
            '@kind' => FolderShare::translateKind($kinds[0]),
          ]);
      }

      return $this->t(
        "This @kind will be permanently deleted. You can't undo this action.",
        [
          '@kind' => FolderShare::translateKind($kinds[0]),
        ]);
    }

    if ($shared === TRUE) {
      return $this->t(
        "These @kinds will be permanently deleted. Since some of these are shared, deleting them will affect other users. You can't undo this action.",
        [
          '@kinds' => FolderShare::translateKinds('items'),
        ]);
    }

    return $this->t(
      "These @kinds will be permanently deleted. You can't undo this action.",
      [
        '@kinds' => FolderShare::translateKinds('items'),
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(bool $forPage) {
    $ids = $this->getSelectionIds();

    if (count($ids) === 1) {
      return $this->t(
        'Delete "@name"?',
        [
          '@name' => FolderShare::findNameForId($ids[0]),
        ]);
    }

    $kinds = FolderShare::translateKindsForIds($ids);
    return $this->t(
      "Delete @kinds?",
      [
        '@kinds' => $kinds['translation'],
      ]);
  }

  /**
   * {@inheritdoc}
   */
  public function getSubmitButtonName() {
    return $this->t('Delete');
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    // The command wrapper provides form basics:
    // - Attached libraries.
    // - Page title (if not an AJAX dialog).
    // - Description (from ::getDescription()).
    // - Submit buttion (labeled with ::getSubmitButtonName()).
    // - Cancel button (if AJAX dialog).
    $form['#attributes']['class'][] = 'confirmation';
    $form['#attributes']['class'][] = 'foldershare-delete';
    $form['#theme'] = 'confirm_form';

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState) {
    // Nothing to do.
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    $this->execute();
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {
    $ids = $this->getSelectionIds();

    FolderShare::deleteMultiple($ids);

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $count = count($ids);
      if ($count <= 1) {
        $message = $this->t("The item has been deleted.");
      }
      else {
        $message = $this->t(
          "@count items have been deleted.",
          [
            '@count' => $count,
          ]);
      }
      $this->getMessenger()->addStatus($message);
    }
  }

}
