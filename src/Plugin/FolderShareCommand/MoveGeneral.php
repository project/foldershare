<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Defines a command plugin to move files and folders, not on a rootlist.
 *
 * The command moves all selected files and folders to a chosen
 * destination folder or the root list.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 * - 'selectionIds': selected entities to duplicate.
 * - 'destinationId': the destination folder, if any.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_move",
 *  label           = @Translation("Move"),
 *  menuNameDefault = @Translation("Move..."),
 *  menuName        = @Translation("Move..."),
 *  description     = @Translation("Move selected files and folders to a new location."),
 *  category        = "copy & move",
 *  weight          = 0,
 *  specialHandling = {
 *    "move",
 *  },
 *  userConstraints = {
 *    "any",
 *  },
 *  parentConstraints = {
 *    "kinds"   = {
 *      "folder",
 *    },
 *    "access"  = "update",
 *  },
 *  destinationConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "update",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "one",
 *      "many",
 *    },
 *    "kinds"   = {
 *      "any",
 *    },
 *    "access"  = "update",
 *  },
 * )
 */
class MoveGeneral extends MoveBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

}
