/**
 * @file
 * Implements the FolderShare share dialog user interface.
 *
 * The dialog for the "Share" command presents a table of users and the
 * sharing permissions they have been granted. The dialog also contains
 * an auto-complete text field for entering the name of a new user to add
 * to the list. An "Add" button triggers the addition.
 *
 * This script supports the dialog by mapping a carriage return on the add
 * user text field into a trigger of the "Add" button.
 *
 * @ingroup foldershare
 * @see \Drupal\foldershare\Plugin\FolderShareCommand\Share
 * @see \Drupal\foldershare\Form\CommandFormWrapper
 */
(($, Drupal) => {
  Drupal.foldershare.ChangeOwnerDialog = {
    /**
     * Attaches the module's UI behaviors.
     *
     * @param {Document} pageContext
     *   The page context for this call. Initially, this is the full document.
     *   Later, this is only portions of the document added via AJAX.
     *
     * @return {boolean}
     *   Always returns true.
     */
    attach(pageContext) {
      //
      // Test and exit
      // -------------
      // Quickly exit if the page context does not include the add-user form.
      const changeOwnerForm = ".foldershare-change-owner";
      let $changeOwnerForm;

      if (typeof pageContext.tagName === "undefined") {
        // The full document. Search down.
        $changeOwnerForm = $(changeOwnerForm, pageContext);
      } else {
        // Above or below. Search up first, then down.
        $changeOwnerForm = $(pageContext).parents(changeOwnerForm);
        if ($changeOwnerForm.length === 0) {
          $changeOwnerForm = $(changeOwnerForm, pageContext);
        }
      }

      if ($changeOwnerForm.length === 0) {
        // Fail. The document does not contain the add-user form.
        return true;
      }

      //
      // Process top elements
      // --------------------
      // Process each change owner form. There should be only one.
      $changeOwnerForm.each((index, element) => {
        // Get the text field.
        const $changeOwnerField = $(
          ".foldershare-change-owner-new-owner",
          $(element)
        ).eq(0);
        if ($changeOwnerField.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI change owner field is missing."
          );
          return false;
        }

        // The form always has a submit button.
        const $submitButton = $(".dialog-submit", $(element)).eq(0);
        if ($submitButton.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI change owner submit button is missing."
          );
          return false;
        }

        // Attach a behavior to trigger the button on a carriage return.
        $changeOwnerField.off("keypress.foldershare");
        $changeOwnerField.on("keypress.foldershare", ev => {
          if (ev.keyCode === 13) {
            ev.preventDefault();
            $submitButton.prop("disabled", false);
            $submitButton.click();
            return false;
          }
        });

        // Attach a behavior to enable/disable the submit button based
        // on whether the text field is empty.
        //
        // Complicating things... Drupal wraps the form with a dialog that
        // can add duplicate action buttons to a standard location in the
        // dialog. This is done with Javascript, so we can't search for
        // the additional buttons until it is time to enable/disable them.
        $changeOwnerField.off("keyup.foldershare");
        $changeOwnerField.on("keyup.foldershare", () => {
          // Determine the new state.
          const name = $changeOwnerField.val();
          let disabled = false;
          if (name === null || name.length === 0) {
            disabled = true;
          }

          // Set the disabled property on the main form submit button.
          $submitButton.prop("disabled", disabled);

          // Look for the duplicate submit button and set its
          // disabled property too.
          const $parentDialog = $(element).closest(".ui-dialog");
          if ($parentDialog.length !== 0) {
            const $parentSubmitButton = $(
              ".ui-dialog-buttonset .dialog-submit",
              $parentDialog
            ).eq(0);
            if ($parentSubmitButton.length !== 0) {
              $parentSubmitButton.prop("disabled", disabled);
            }
          }
        });

        $changeOwnerField.focus();

        // Enable/disable the submit button initially based on whether the text
        // field is empty.
        const name = $changeOwnerField.val();
        let disabled = false;
        if (name === null || name.length === 0) {
          disabled = true;
        }
        $submitButton.prop("disabled", disabled);
      });

      return true;
    }
  };

  /*--------------------------------------------------------------------
   *
   * On Drupal ready behaviors.
   *
   * Set up behaviors to execute when the page is fully loaded, or whenever
   * AJAX sends a new page fragment.
   *
   *--------------------------------------------------------------------*/

  Drupal.behaviors.foldershare_ChangeOwnerDialog = {
    attach(pageContext) {
      Drupal.foldershare.ChangeOwnerDialog.attach(pageContext);
    }
  };
})(jQuery, Drupal);
