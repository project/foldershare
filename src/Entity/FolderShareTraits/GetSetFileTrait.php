<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\file\Entity\File;
use Drupal\media\Entity\Media;

/**
 * Get/set FolderShare entity file, image, and media fields.
 *
 * This trait includes get methods for FolderShare entity file, image,
 * and media fields. Each field is an entity reference that stores
 * a target entity ID. At most one of these three fields may be set.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetFileTrait {

  /*---------------------------------------------------------------------
   *
   * File field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Clears the item's file ID.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::setFileId()
   */
  private function clearFileId() {
    $this->file->setValue(['target_id' => NULL]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getFile() {
    $id = $this->getFileId();
    if ($id === FALSE) {
      return NULL;
    }

    return File::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getFileId() {
    if ($this->isFile() === TRUE) {
      $value = $this->get('file')->target_id;
      if ($value === NULL) {
        return FALSE;
      }

      return (int) $value;
    }

    return FALSE;
  }

  /**
   * Sets the item's file ID.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The entity ID is not validated and is presumed to be a File entity ID.
   * This item is presumed to be a file kind (i.e. isFile() returns TRUE).
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param int $id
   *   The new File entity ID. The value is not validated but is expected
   *   to be a valid File entity ID. If the value is negative, the file ID
   *   is cleared.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::clearFileId()
   */
  private function setFileId(int $id) {
    if ($id < 0) {
      $this->file->setValue(['target_id' => NULL]);
    }
    else {
      $this->file->setValue(['target_id' => $id]);
    }

    return $this;
  }

  /*---------------------------------------------------------------------
   *
   * Image field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Clears the item's image ID.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::setImageId()
   */
  private function clearImageId() {
    $this->image->setValue(['target_id' => NULL]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getImage() {
    $id = $this->getImageId();
    if ($id === FALSE) {
      return NULL;
    }

    // The image module does not define an Image entity type.
    // Instead it uses the File entity type, but references it
    // via an image field type. So, to load an image, we need
    // to use the File module.
    return File::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getImageId() {
    if ($this->isImage() === TRUE) {
      $value = $this->get('image')->target_id;
      if ($value === NULL) {
        return FALSE;
      }

      return (int) $value;
    }

    return FALSE;
  }

  /**
   * Sets the item's image ID.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The entity ID is not validated and is presumed to be a File entity ID.
   * This item is presumed to be an image kind (i.e. isImage() returns TRUE).
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param int $id
   *   The new File entity ID. The value is not validated but is expected
   *   to be a valid File entity ID. If the value is negative, the image ID
   *   is cleared.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::clearImageId()
   */
  private function setImageId(int $id) {
    if ($id < 0) {
      $this->image->setValue(['target_id' => NULL]);
    }
    else {
      $this->image->setValue(['target_id' => $id]);
    }

    return $this;
  }

  /*---------------------------------------------------------------------
   *
   * Media field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Clears the item's media ID.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::setMediaId()
   */
  private function clearMediaId() {
    $this->media->setValue(['target_id' => NULL]);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getMedia() {
    $id = $this->getMediaId();
    if ($id === FALSE) {
      return NULL;
    }

    return Media::load($id);
  }

  /**
   * {@inheritdoc}
   */
  public function getMediaId() {
    if ($this->isMedia() === TRUE) {
      $value = $this->get('media')->target_id;
      if ($value === NULL) {
        return FALSE;
      }

      return (int) $value;
    }

    return FALSE;
  }

  /**
   * Sets the item's media ID.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The entity ID is not validated and is presumed to be a Media entity ID.
   * This item is presumed to be a media kind (i.e. isMedia() returns TRUE).
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param int $id
   *   The new Media entity ID. The value is not validated but is expected
   *   to be a valid Media entity ID. If the vlaue is negative, the media ID
   *   is cleared.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::clearMediaId()
   */
  private function setMediaId(int $id) {
    if ($id < 0) {
      $this->media->setValue(['target_id' => NULL]);
    }
    else {
      $this->media->setValue(['target_id' => $id]);
    }

    return $this;
  }

}
