<?php

namespace Drupal\foldershare\Entity\Builder;

use Drupal\Component\Utility\Html;
use Drupal\Core\Entity\EntityViewBuilder;

use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Defines a builder for views of a FolderShare entity.
 *
 * This class helps to build an entity view. It supports special behavior
 * based upon the view mode:
 *
 * - 'full' and other view modes: the build creates a human-readable view
 *   of the entity, including all configured fields and pseudo-fields.
 *
 * - 'search_index': the build adds the language code and optionally adds
 *   file content.
 *
 * - 'search_result': the build adds the language code.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\foldershare\Entity\FolderShare
 * @see \Drupal\foldershare\Entity\Controller\FolderShareViewController
 */
final class FolderShareViewBuilder extends EntityViewBuilder {

  /*---------------------------------------------------------------------
   *
   * Build.
   *
   * These functions build the indicated components of an entity view.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildComponents(
    array &$build,
    array $entities,
    array $displays,
    $viewMode) {

    if (empty($entities) === TRUE) {
      return;
    }

    // Handle view modes specially.
    switch ($viewMode) {
      case 'search_index':
        $this->buildComponentsForSearchIndex($build, $entities, $displays);
        return;

      case 'search_result':
        $this->buildComponentsForSearchResults($build, $entities, $displays);
        return;

      default:
        $this->buildComponentsForDefault($build, $entities, $displays, $viewMode);
        return;
    }
  }

  /**
   * Builds the component fields and properties of entities for most view modes.
   *
   * The given renderable build array is updated to contain viewable entity
   * content intended for most displays of the entity.
   *
   * @param array $build
   *   The renderable array representing the entity content.
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The FolderShare entities whose content is being built.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface[] $displays
   *   The array of entity view displays holding the display options
   *   configured for the entity components.
   * @param string $viewMode
   *   The view mode in which the entity is being viewed.
   */
  protected function buildComponentsForDefault(
    array &$build,
    array $entities,
    array $displays,
    string $viewMode) {
    //
    // Build defaults
    // --------------
    // Let the parent class build a view of the displayable fields. This
    // pulls in the entity's description (if any) and extension fields
    // (if any).
    parent::buildComponents($build, $entities, $displays, $viewMode);

    //
    // Suffix content with details.
    // ----------------------------
    // Add the langcode field.
    foreach ($entities as $id => $entity) {
      $display = $displays[$entity->bundle()];
      if ($display->getComponent('langcode') !== NULL) {
        $build[$id]['langcode'] = [
          '#type'   => 'item',
          '#title'  => $this->t('Language'),
          '#markup' => Html::escape($entity->language()->getName()),
          '#prefix' => '<div id="field-language-display">',
          '#suffix' => '</div>',
        ];
      }
    }
  }

  /**
   * Builds the component fields and properties of entities for a search index.
   *
   * The given renderable build array is updated to contain viewable entity
   * content as well as additional entity-related content intended for use
   * in the search index entries for the given entities.
   *
   * @param array $build
   *   The renderable array representing the entity content.
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The FolderShare entities whose content is being built.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface[] $displays
   *   The array of entity view displays holding the display options
   *   configured for the entity components.
   */
  protected function buildComponentsForSearchIndex(
    array &$build,
    array $entities,
    array $displays) {

    //
    // Build view
    // ----------
    // Let the parent class build a view.
    parent::buildComponents($build, $entities, $displays, 'search_index');

    //
    // Suffix content with details.
    // ----------------------------
    // Add the langcode field.
    //
    // If there are attached files, and search indexing of file content
    // is enabled, and the file has an allowed extension, add that content.
    $addFileContent = Settings::getSearchIndexFileContentEnable();
    $fileExtensions = explode(' ', Settings::getAllowedSearchIndexFilenameExtensions());
    $maxFileBytes = Settings::getSearchIndexMaximumFileSize();

    foreach ($entities as $id => $entity) {
      // Language code.
      $display = $displays[$entity->bundle()];
      if ($display->getComponent('langcode') !== NULL) {
        $build[$id]['langcode'] = [
          '#type'   => 'item',
          '#title'  => $this->t('Language'),
          '#markup' => Html::escape($entity->language()->getName()),
          '#prefix' => '<div id="field-language-display">',
          '#suffix' => '</div>',
        ];
      }

      // Invoke hooks (if any) to add their own search text to the item.
      $hookOutput = $this->moduleHandler->invokeAll(
        FolderShare::ENTITY_TYPE_ID . '_update_index',
        [$entity]);

      if (empty($hookOutput) === FALSE) {
        $build[$id]['hook'] = [
          '#type'  => 'html_tag',
          '#tag'   => 'div',
          '#value' => Html::escape(implode(' ', $hookOutput)),
        ];
      }
      unset($hookOutput);

      // File content.
      if ($addFileContent === TRUE) {
        $files = [
          $entity->getFile(),
          $entity->getImage(),
        ];

        foreach ($files as $index => $file) {
          if ($file === NULL) {
            continue;
          }
          $uri = $file->getFileUri();
          $ext = pathinfo($uri, PATHINFO_EXTENSION);

          if (empty($fileExtensions) === FALSE &&
              in_array(mb_strtolower($ext), $fileExtensions) === FALSE) {
            // File does not have an allowed extension.
            continue;
          }

          // Read file text, up to the maximum bytes allowed, if any.
          $nBytes = @filesize($uri);
          if ($nBytes !== FALSE && $nBytes > 0) {
            if ($nBytes > $maxFileBytes && $maxFileBytes > 0) {
              $nBytes = $maxFileBytes;
            }

            $fp = @fopen($uri, 'rb');
            if ($fp !== FALSE) {
              $newText = fread($fp, $nBytes);
              fclose($fp);

              if ($newText !== FALSE) {
                $build[$id]['file_' . $index . '_content'] = [
                  '#type'  => 'html_tag',
                  '#tag'   => 'div',
                  '#value' => Html::escape($newText),
                ];
              }
              unset($newText);
            }
          }

          unset($uri);
          unset($ext);
        }

        unset($files);
      }
    }
  }

  /**
   * Builds the component fields and properties of entities for search results.
   *
   * The given renderable build array is updated to contain viewable entity
   * content intended for use in the search results entries for the given
   * entities.
   *
   * @param array $build
   *   The renderable array representing the entity content.
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The FolderShare entities whose content is being built.
   * @param \Drupal\Core\Entity\Display\EntityViewDisplayInterface[] $displays
   *   The array of entity view displays holding the display options
   *   configured for the entity components.
   */
  protected function buildComponentsForSearchResults(
    array &$build,
    array $entities,
    array $displays) {
    //
    // Build view
    // ----------
    // Let the parent class build a view.
    parent::buildComponents($build, $entities, $displays, 'search_result');

    //
    // Suffix content with details.
    // ----------------------------
    // Add the langcode field.
    foreach ($entities as $id => $entity) {
      $display = $displays[$entity->bundle()];
      if ($display->getComponent('langcode') !== NULL) {
        $build[$id]['langcode'] = [
          '#type'   => 'item',
          '#title'  => $this->t('Language'),
          '#markup' => Html::escape($entity->language()->getName()),
          '#prefix' => '<div id="field-language-display">',
          '#suffix' => '</div>',
        ];
      }
    }
  }

}
