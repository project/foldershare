<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\foldershare\Settings;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Defines a command plugin to create a new folder.
 *
 * The command creates a new folder in the current parent folder, if any.
 * If there is no parent folder, the command creates a new root folder.
 * The new folder is empty and has a default name.
 *
 * Configuration parameters:
 * - 'parentId': the parent folder, if any.
 *
 * @ingroup foldershare
 *
 * @FolderShareCommand(
 *  id              = "foldersharecommand_new_folder",
 *  label           = @Translation("New Folder"),
 *  menuNameDefault = @Translation("New Folder"),
 *  menuName        = @Translation("New Folder"),
 *  description     = @Translation("Create a new folder."),
 *  category        = "open",
 *  weight          = 0,
 *  parentConstraints = {
 *    "kinds"   = {
 *      "rootlist",
 *      "folder",
 *    },
 *    "access"  = "create",
 *  },
 *  selectionConstraints = {
 *    "types"   = {
 *      "none",
 *    },
 *  },
 * )
 */
class NewFolder extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Creates an instance of the plugin.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The plugin configuration.
   * @param string $pluginId
   *   The plugin ID.
   * @param mixed $pluginDefinition
   *   The plugin implementation definition.
   */
  public static function create(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {
    return new static(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);
  }

  /*--------------------------------------------------------------------
   *
   * Execute.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function execute() {

    $parent = $this->getParent();
    try {
      if ($parent === NULL) {
        $newFolder = FolderShare::createRootFolder('');
      }
      else {
        $newFolder = $parent->createFolder('');
      }
    }
    catch (\Exception $e) {
      throw $e;
    }

    if (Settings::getCommandNormalCompletionReportEnable() === TRUE) {
      $this->getMessenger()->addStatus(
        $this->t(
          "A @kind named '@name' has been created.",
          [
            '@kind' => FolderShare::translateKind($newFolder->getKind()),
            '@name' => $newFolder->getName(),
          ]));
    }
  }

}
