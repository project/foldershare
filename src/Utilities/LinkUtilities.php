<?php

namespace Drupal\foldershare\Utilities;

use Drupal\Core\Url;
use Drupal\Core\Link;
use Drupal\Component\Utility\Html;

use Symfony\Component\HttpFoundation\Request;

/**
 * Defines static utility functions for creating links.
 *
 * The functions in this class help create HTML links to:
 * - Drupal.org documentation pages.
 * - Help pages at the site.
 * - Routed pages at the site.
 * - External URLs.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
final class LinkUtilities {

  /*--------------------------------------------------------------------
   *
   * Constants.
   *
   *--------------------------------------------------------------------*/
  /**
   * The base URL to Drupal.org module documentation pages.
   *
   * Append the module's machine name to create a URL to the module's
   * documentation. For instance, Drupal core's "system" module documentation
   * is at: DRUPAL_CORE_MODULE_DOC . 'system'.
   *
   * @var string
   */
  const DRUPAL_CORE_MODULE_DOC = 'https://www.drupal.org/docs/8/core/modules/';

  /**
   * The base URL to Drupal.org module project pages.
   *
   * Append the module's machine name to create a URL to the module's
   * project page.
   *
   * @var string
   */
  const DRUPAL_PROJECT = 'https://www.drupal.org/project/';

  /**
   * The route to the help page.
   *
   * The value must have the form "help.page.MODULE".  To get to the
   * help page for a module, the "name" parameter must be set to
   * the module's name.
   *
   * @var string
   */
  const ROUTE_HELP = 'help.page';

  /*--------------------------------------------------------------------
   *
   * Functions.
   *
   *--------------------------------------------------------------------*/
  /**
   * Returns an HTML anchor to module documentation page at Drupal.org.
   *
   * If the link cannot be created, plain text is returned that contains
   * the module name.
   *
   * @param string $moduleName
   *   The name of the module at Drupal.org.
   * @param string $title
   *   (optional, default = '') The title for the link to the page.
   *   If empty, the module name is used.
   *
   * @return string
   *   The HTML markup for a link to the module's documentation at Drupal.org.
   *
   * @see ::createRouteLink()
   * @see ::createHelpLink()
   * @see ::createUrlLink()
   */
  public static function createDocLink(
    string $moduleName,
    string $title = '') {

    // Get the title. Fall back to the module name if no title was given.
    if (empty($title) === TRUE) {
      $title = Html::escape($moduleName);
    }
    else {
      $title = Html::escape($title);
    }

    // Create the link using Drupal.org's URL.
    $lnk = Link::fromTextAndUrl(
      $title,
      Url::fromUri(self::DRUPAL_CORE_MODULE_DOC . $moduleName));
    return ($lnk === NULL) ? $title : $lnk->toString();
  }

  /**
   * Returns an HTML anchor to a routed help page at the site.
   *
   * If no title is given, the route's title is used.
   *
   * If the Drupal core "Help" module is not installed, plain text is
   * returned that contains the title (if given) or the module name
   * (if no title given).
   *
   * @param string $moduleName
   *   The name of a module.
   * @param string $title
   *   (optional, default = '') The title for the link to the route.
   *   If empty, the route's title is used.
   *
   * @return string
   *   The HTML markup for a link to the module's page.
   *
   * @see ::createDocLink()
   * @see ::createRouteLink()
   * @see ::createUrlLink()
   * @see ::getRouteTitle()
   */
  public static function createHelpLink(
    string $moduleName,
    string $title = '') {

    // If the help module is not installed, return the title.
    if (\Drupal::service('module_handler')->moduleExists('help') === FALSE) {
      if (empty($title) === TRUE) {
        return Html::escape($moduleName);
      }

      return Html::escape($title);
    }

    // Get the route's title, if no title was provided.
    $routeName = self::ROUTE_HELP;
    if (empty($title) === TRUE) {
      $routeTitle = self::getRouteTitle($routeName);
      if (empty($routeTitle) === FALSE) {
        $title = $routeTitle;
      }
    }

    // Create the link.
    $options = [];
    if (empty($moduleName) === FALSE) {
      $options['name'] = $moduleName;
    }

    $lnk = Link::createFromRoute($title, $routeName, $options);
    return ($lnk === NULL) ? $title : $lnk->toString();
  }

  /**
   * Returns an HTML anchor to a project page at Drupal.org.
   *
   * If the link cannot be created, plain text is returned that contains
   * the project name.
   *
   * @param string $projectName
   *   The name of the project at Drupal.org.
   * @param string $title
   *   (optional, default = '') The title for the link to the page.
   *   If empty, the project name is used.
   *
   * @return string
   *   The HTML markup for a link to the module's documentation at Drupal.org.
   *
   * @see ::createRouteLink()
   * @see ::createHelpLink()
   * @see ::createUrlLink()
   */
  public static function createProjectLink(
    string $projectName,
    string $title = '') {

    // Get the title. Fall back to the module name if no title was given.
    if (empty($title) === TRUE) {
      $title = Html::escape($projectName);
    }
    else {
      $title = Html::escape($title);
    }

    // Create the link using Drupal.org's URL.
    $lnk = Link::fromTextAndUrl(
      $title,
      Url::fromUri(self::DRUPAL_PROJECT . $projectName));
    return ($lnk === NULL) ? $title : $lnk->toString();
  }

  /**
   * Returns an HTML anchor to a routed page at the site.
   *
   * If no title is given, the route's title is used.
   *
   * If the route cannot be found, plain text is returned that contains
   * the title (if given) or the route name (if no title given).
   *
   * @param string $routeName
   *   The name of a route for an enabled module.
   * @param string $target
   *   (optional, default = '') The optional name of a page fragment target
   *   (i.e. a "#name", sans '#').
   * @param string $title
   *   (optional, default = '') The optional title for the link to the route.
   *   If empty, the route's title is used.
   * @param array $arguments
   *   (optional, default = []) The optional array of route arguments.
   *
   * @return string
   *   The HTML markup for a link to the module's page.
   *
   * @see ::createDocLink()
   * @see ::createHelpLink()
   * @see ::createUrlLink()
   * @see ::getRouteTitle()
   */
  public static function createRouteLink(
    string $routeName,
    string $target = '',
    string $title = '',
    array $arguments = []) {

    // Get the route's title, if no title was provided.
    if (empty($title) === TRUE) {
      $routeTitle = self::getRouteTitle($routeName);
      if (empty($routeTitle) === FALSE) {
        $title = $routeTitle;
      }
    }

    // Create the link.
    $options = [];
    if (empty($target) === FALSE) {
      $options['fragment'] = $target;
    }

    $lnk = Link::createFromRoute($title, $routeName, $arguments, $options);
    return ($lnk === NULL) ? $title : $lnk->toString();
  }

  /**
   * Returns an HTML anchor to an external URL.
   *
   * If no title is given, the path is used.
   *
   * @param string $path
   *   The text URL path.
   * @param string $title
   *   (optional, default = '') The title for the link to the page.
   *   If empty, the path is used.
   *
   * @return string
   *   The HTML markup for a link to the module's documentation.
   *
   * @see ::createDocLink()
   * @see ::createHelpLink()
   * @see ::createRouteLink()
   */
  public static function createUrlLink(
    string $path,
    string $title = '') {

    // Get the title. Fall back to the path if no title was given.
    if (empty($title) === TRUE) {
      $title = Html::escape($path);
    }
    else {
      $title = Html::escape($title);
    }

    // Create the link.
    $lnk = Link::fromTextAndUrl($title, Url::fromUri($path));
    return ($lnk === NULL) ? $title : $lnk->toString();
  }

  /**
   * Returns the title for a route at the site.
   *
   * @param string $routeName
   *   The name of a route for an enabled module.
   *
   * @return string
   *   The string title for the route. Returns an empty string if the
   *   title could not be found. This can occur when routes lead to views.
   *
   * @see ::createHelpLink()
   * @see ::createRouteLink()
   */
  public static function getRouteTitle(string $routeName) {
    // The route's title can be set in a module's routing.yml file with
    // one or more of:
    // - _title
    // - _title_arguments
    // - _title_context
    // - _title_callback
    //
    // Drupal's "title_resolver" service figures out which of these were
    // used and, if necessary, invokes the proper callback.
    $routeProvider = \Drupal::service('router.route_provider');
    $titleResolver = \Drupal::service('title_resolver');

    $route = $routeProvider->getRouteByName($routeName);
    try {
      return $titleResolver->getTitle(new Request(), $route);
    }
    catch (\Exception $e) {
      return '';
    }
  }

}
