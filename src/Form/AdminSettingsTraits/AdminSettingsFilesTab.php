<?php

namespace Drupal\foldershare\Form\AdminSettingsTraits;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StreamWrapper\PrivateStream;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;

use Drupal\foldershare\ManageFilenameExtensions;
use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Utilities\LinkUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Manages the "Files" tab for the module's settings form.
 *
 * <B>Warning:</B> This is an internal trait that is strictly used by
 * the AdminSettings form class. It is a mechanism to group functionality
 * to improve code management.
 *
 * @ingroup foldershare
 */
trait AdminSettingsFilesTab {

  /*---------------------------------------------------------------------
   *
   * Build.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds the files tab.
   *
   * @param array $form
   *   An associative array containing the structure of the form. The form
   *   is modified to include additional render elements for the tab.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current state of the form.
   * @param string $tabGroup
   *   The name of the tab group.
   */
  private function buildFilesTab(
    array &$form,
    FormStateInterface $formState,
    string $tabGroup) {

    //
    // Find installed modules.
    // -----------------------
    // Tab sections vary their presentation based upon what modules are
    // installed at the site.
    $helpInstalled = $this->moduleHandler->moduleExists('help');

    //
    // Set up tab names.
    // -----------------
    // Create the machine name for the tab and the tab's title.
    $tabMachineName = 'files';
    $tabName        = 'foldershare_' . $tabMachineName . '_tab';
    $tabTitle       = $this->t('Files');
    $tabPaneTitle   = $this->t('Manage files');

    //
    // Set up class names.
    // -------------------
    // Use a set of standard class names for tab sections.
    $tabSubtitleClass        = 'foldershare-settings-subtitle';
    $sectionWrapperClass     = 'foldershare-settings-section-wrapper';
    $sectionClass            = 'foldershare-settings-section';
    $sectionDescriptionClass = 'foldershare-settings-section-description';
    $itemDefaultClass        = 'foldershare-settings-item-default';
    $itemNoteClass           = 'foldershare-settings-item-note';
    $warningClass            = 'foldershare-warning';

    $restoreExtensionsButton = 'foldershare_extensions_restore';

    //
    // Create the tab
    // --------------
    // Start the tab with a title, subtitle, and description.
    $form[$tabName] = [
      '#type'                    => 'details',
      '#open'                    => FALSE,
      '#group'                   => $tabGroup,
      '#title'                   => $tabTitle,

      '#description'             => [
        'subtitle'               => [
          '#type'                => 'html_tag',
          '#tag'                 => 'h2',
          '#value'               => $tabPaneTitle,
          '#attributes'          => [
            'class'              => [$tabSubtitleClass],
          ],
        ],

        'description'            => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            'Control where files are saved, the file name extensions supported, and the maximum file upload size.'),
          '#attributes'          => [
            'class'              => [
              'foldershare-settings-description',
            ],
          ],
        ],
      ],
      '#attributes'              => [
        'class'                  => [
          'foldershare-settings-tab',
          'foldershare-files-tab',
        ],
      ],
    ];

    //
    // File system.
    // ------------
    // If the site currently has stored files, then the storage scheme
    // and subdirectory choice cannot be changed.
    //
    // If the site doesn't have stored files, but the private file system
    // has not been configured, then the storage scheme cannot be changed.
    $inUseWarning          = '';
    $noPrivateWarning      = '';
    $storageSchemeDisabled = FALSE;

    if (FolderShare::hasFiles() === TRUE) {
      // Create a link to the Drupal core system page to delete all content
      // for the entity type.
      $inUseWarning = [
        '#type'                  => 'html_tag',
        '#tag'                   => 'p',
        '#value'                 => $this->t(
          'The above settings are disabled because there are currently files managed by this module. You must @deleteAllLink files before changing the settings above.',
          [
            '@deleteAllLink'     => LinkUtilities::createRouteLink(
              'system.prepare_modules_entity_uninstall',
              '',
              $this->t('Delete all'),
              [
                'entity_type_id' => FolderShare::ENTITY_TYPE_ID,
              ]),
          ]),
        '#attributes'            => [
          'class'                => [
            $warningClass,
            $itemNoteClass,
          ],
        ],
      ];
      $storageSchemeDisabled = TRUE;
    }

      $file_scheme_options = [];
      $unsupported_file_scheme_options = [];

      foreach (array_keys(FileUtilities::getAvailableStreamWrappers(StreamWrapperInterface::ALL)) as $scheme) {
          $streamWrapper = FileUtilities::getStreamWrapper($scheme);
          if (FileUtilities::isStreamWrapperTypeSupported($streamWrapper->getType())) {
              $file_scheme_options[$scheme] = $streamWrapper->getName();
          } else {
              $unsupported_file_scheme_options[$scheme] = $streamWrapper->getName();
          }
      }

      if (empty(PrivateStream::basePath()) === TRUE && !$file_scheme_options) {
      if ($helpInstalled === TRUE) {
        $noPrivateWarning = [
          '#type'                  => 'html_tag',
          '#tag'                   => 'p',
          '#value'                 => $this->t(
            'This setting is limited to the <em>Public</em> directory because a <em>Private</em> directory has not been set up yet for the site (see @fileHelpLink and @fileDocLink, @fileSettingsLink settings, and the "file_private_path" setting in the site\'s "settings.php" file).',
            [
              '@fileSettingsLink'  => LinkUtilities::createRouteLink(
                'system.file_system_settings',
                '',
                $this->t('File system')),
              '@fileHelpLink'      => LinkUtilities::createHelpLink(
                'file',
                $this->t('File help')),
              '@fileDocLink'       => LinkUtilities::createDocLink(
                'file',
                $this->t('File documentation')),
            ]),
          '#attributes'            => [
            'class'                => [
              $warningClass,
              $itemNoteClass,
            ],
          ],
        ];
      }
      else {
        $noPrivateWarning = [
          '#type'                  => 'html_tag',
          '#tag'                   => 'p',
          '#value'                 => $this->t(
            'This setting is limited to the <em>Public</em> directory because a <em>Private</em> directory has not been set up yet for the site (see @fileDocLink, @fileSettingsLink settings, and the "file_private_path" setting in the site\'s "settings.php" file).',
            [
              '@fileSettingsLink'  => LinkUtilities::createRouteLink(
                'system.file_system_settings',
                '',
                $this->t('File system')),
              '@fileDocLink'       => LinkUtilities::createDocLink(
                'file',
                $this->t('File documentation')),
            ]),
          '#attributes'            => [
            'class'                => [
              $warningClass,
              $itemNoteClass,
            ],
          ],
        ];
      }
      $storageSchemeDisabled = TRUE;
    }


    $form[$tabName]['directories'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('File storage'),
      '#open'                    => FALSE,
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],

      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],

        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            "Files are stored using the stream wrapper configured below. Access controls are enforced the same for all options."),
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        'directory-choice'       => [
          '#type'                => 'select',
          '#options'             => $file_scheme_options,
          '#default_value'       => Settings::getFileScheme(),
          '#disabled'            => $storageSchemeDisabled,
          '#name'                => 'directory-choice',
        ],

        // The default setting.
        'directory-choice-description' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            'Default: @default',
            [
              '@default'         => $file_scheme_options[Settings::getFileSchemeDefault()],
            ]),
          '#attributes'          => [
            'class'              => [
              'description',
              $itemDefaultClass,
              $itemNoteClass,
            ],
          ],
        ],
      ],
    ];

    // Add warning if there are things disabled.
    if (empty($noPrivateWarning) === FALSE) {
      $form[$tabName]['directories']['section']['warning'] = $noPrivateWarning;
    }
    elseif (empty($inUseWarning) === FALSE) {
      $form[$tabName]['directories']['section']['warning'] = $inUseWarning;
    }

    if ($unsupported_file_scheme_options) {
        $unsupported_file_scheme_options_text = implode(", ", array_values($unsupported_file_scheme_options));
        $form[$tabName]['directories']['section']['unsupported_file_scheme_warning'] = [
            '#type'                  => 'html_tag',
            '#tag'                   => 'p',
            '#value'                 => $this->t(
                "<b>Warning:</b><br/>The following stream wrappers were found but are not of a supported type: $unsupported_file_scheme_options_text<br/>Only stream wrappers that can handle reading & writing visible files can be used."
            ),
            '#attributes'            => [
                'class'                => [
                    $warningClass,
                    $itemNoteClass,
                ],
            ],
        ];
    }

    $raw_file_scheme = Settings::getRawFileScheme();
    if ($raw_file_scheme !== Settings::getFileScheme()) {
        // the originally configured file scheme is no longer available
        $form[$tabName]['directories']['section']['file_scheme_warning'] = [
            '#type'                  => 'html_tag',
            '#tag'                   => 'p',
            '#value'                 => $this->t(
                "<b>Warning:</b><br/>The originally configured <em>$raw_file_scheme</em> stream wrapper scheme is no longer available. As a result, accessing any file that was using this file scheme will now throw an error.<br/>To fix this, re-install a stream wrapper for the <em>$raw_file_scheme</em> scheme."
                ),
            '#attributes'            => [
                'class'                => [
                    $warningClass,
                    $itemNoteClass,
                ],
            ],
        ];
    }

    //
    // File upload size
    // ----------------
    // Select whether file uploads should be restricted based on their size,
    // and what maximum size is allowed.
    //
    // Select whether file uploads should be restricted based on their
    // file name extensions, and what extensions are allowed.
    $fileUploadSizeLimit             = Settings::getFileUploadSizeLimit();
    $fileUploadSizeLimitDefault      = Settings::getFileUploadSizeLimitDefault();
    $fileRestrictExtensions          = Settings::getFileRestrictFilenameExtensions();
    $fileRestrictExtensionsDefault   = Settings::getFileRestrictFilenameExtensionsDefault();
    $phpFileUploadSizeLimit          = LimitUtilities::getPhpFileUploadSizeLimit();
    $formattedPhpFileUploadSizeLimit = number_format($phpFileUploadSizeLimit);

    // Create default value messages.
    if ($fileUploadSizeLimitDefault === 0) {
      $defaultUploadSizeMessage = $this->t(
        "Default: Allow any size up to the PHP maximum of @phplimit bytes.",
        [
          '@phplimit' => $formattedPhpFileUploadSizeLimit,
        ]);
    }
    else {
      $defaultUploadSizeMessage = $this->t(
        'Default: @bytes bytes',
        ['@bytes' => $fileUploadSizeLimitDefault]);
    }

    if ($fileRestrictExtensionsDefault === TRUE) {
      $defaultExtensionsMessage = $this->t(
        'Default: Restrict files to a list of allowed extensions');
    }
    else {
      $defaultExtensionsMessage = $this->t('Default: Allow all extensions');
    }

    // Create hook override messages.
    $hookSizeModules = ManageHooks::getHookFileUploadSizeLimitAlter();
    if (empty($hookSizeModules) === TRUE) {
      $hookSizeModulesMessage = '';
    }
    elseif (count($hookSizeModules) === 1) {
      $hookSizeModulesMessage = $this->t(
        "The %name module has installed an override that can further limit upload sizes for specific folders or users.",
        [
          '%name' => $this->moduleHandler->getName($hookSizeModules[0]),
        ]);
    }
    else {
      $names = [];
      foreach ($hookSizeModules as $moduleName) {
        $names[] = $this->moduleHandler->getName($moduleName);
      }
      $hookSizeModulesMessage = $this->t(
        "@num modules have installed overrides that can further limit upload sizes for specific folders or users: %list.",
        [
          '@num'  => count($names),
          '%list' => implode(', ', $names),
        ]);
    }

    $hookExtensionsModules = ManageHooks::getHookAllowedFilenameExtensionsAlter();
    if (empty($hookExtensionsModules) === TRUE) {
      $hookExtensionsModulesMessage = '';
    }
    elseif (count($hookExtensionsModules) === 1) {
      $hookExtensionsModulesMessage = $this->t(
        "The %name module has installed an override that can limit extensions for specific folders or users.",
        [
          '%name' => $this->moduleHandler->getName($hookExtensionsModules[0]),
        ]);
    }
    else {
      $names = [];
      foreach ($hookExtensionsModules as $moduleName) {
        $names[] = $this->moduleHandler->getName($moduleName);
      }
      $hookExtensionsModulesMessage = $this->t(
        "@num modules have installed overrides that can limit extensions for specific folders or users: %list.",
        [
          '@num'  => count($names),
          '%list' => implode(', ', $names),
        ]);
    }

    $form[$tabName]['fileuploadsize'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('File upload size restrictions'),
      '#open'                    => FALSE,
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],
      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],

        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            "Optionally restrict file uploads to a maximum size, up to the maximum set by the <code>upload_max_filesize</code> PHP directive in the server's <em>php.ini</em> configuration file."),
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        // A checkbox to enable/disable limiting file sizes.
        'any-file-upload-size'    => [
          '#type'                => 'checkbox',
          '#title'               => $this->t('Allow any file upload size'),
          '#default_value'       => ($fileUploadSizeLimit === 0),
          '#return_value'        => 'enabled',
          '#name'                => 'any-file-upload-size',
        ],

        // The default setting.
        'any-file-upload-size-default' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $defaultUploadSizeMessage,
          '#attributes'          => [
            'class'              => [
              'description',
              $itemDefaultClass,
              $itemNoteClass,
            ],
          ],
        ],

        // Specific limit.
        'set-file-upload-size'   => [
          '#type'                => 'fieldset',
          '#title'               => $this->t('Restrict file upload size'),
          '#states'              => [
            'visible'            => [
              'input[name="any-file-upload-size"]' => [
                'checked'        => FALSE,
              ],
            ],
          ],

          // The upload size limit.
          'file-upload-size'     => [
            '#type'              => 'number',
            '#title'             => $this->t('Maximum file size:'),
            '#default_value'     => $fileUploadSizeLimit,
            '#required'          => FALSE,
            '#min'               => 0,
            '#max'               => $phpFileUploadSizeLimit,
            '#step'              => 1,
            '#size'              => 24,
            '#name'              => 'file-upload-size',
            '#description'       => $this->t(
              "Enter a size up to the PHP maximum. A zero uses the maximum. Changing the size does not affect files that are already on the site."),
          ],
        ],

        // A list of modules that override the extension list.
        'file-upload-size-hooks' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $hookSizeModulesMessage,
          '#attributes'          => [
            'class'              => [
              $itemNoteClass,
            ],
          ],
        ],
      ],
    ];

    if (empty($hookSizeModulesMessage) === TRUE) {
      unset($form[$tabName]['fileuploadsize']['section']['file-upload-size-hooks']);
    }

    $form[$tabName]['fileextensions'] = [
      '#type'                    => 'details',
      '#title'                   => $this->t('File upload filename extension restrictions'),
      '#open'                    => FALSE,
      '#attributes'              => [
        'class'                  => [$sectionWrapperClass],
      ],
      'section'                  => [
        '#type'                  => 'container',
        '#attributes'            => [
          'class'                => [$sectionClass],
        ],

        'section-description'    => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $this->t(
            'Optionally restrict file uploads to files with specific extensions.'),
          '#attributes'          => [
            'class'              => [$sectionDescriptionClass],
          ],
        ],

        // A checkbox to enable/disable allowing all extensions.
        'all-file-extensions'    => [
          '#type'                => 'checkbox',
          '#title'               => $this->t('Allow all extensions'),
          '#default_value'       => ($fileRestrictExtensions === FALSE),
          '#return_value'        => 'enabled',
          '#name'                => 'all-file-extensions',
        ],

        // The default setting.
        'all-file-extensions-default' => [
          '#type'                => 'html_tag',
          '#tag'                 => 'p',
          '#value'               => $defaultExtensionsMessage,
          '#attributes'          => [
            'class'              => [
              'description',
              $itemDefaultClass,
              $itemNoteClass,
            ],
          ],
        ],

        // Specific extensions.
        'set-extensions'         => [
          '#type'                => 'fieldset',
          '#title'               => $this->t('Restrict file uploads extensions'),
          '#states'              => [
            'visible'            => [
              'input[name="all-file-extensions"]' => [
                'checked'        => FALSE,
              ],
            ],
          ],

          // A list of allowed extensions.
          'file-extensions'      => [
            '#type'              => 'textarea',
            '#title'             => $this->t('Allowed extensions:'),
            '#default_value'     => Settings::getAllowedFilenameExtensions(),
            '#required'          => FALSE,
            '#rows'              => 10,
            '#name'              => 'file-extensions',
            '#description'       => $this->t(
              'Separate extensions with spaces. Do not include dots. Changing this list does not affect files that are already on the site.'),
            '#attributes'        => [
              // No browsers support this. Disable autocomplete.
              'autocomplete' => 'off',
              // Most browsers support this. Disables automatic capitalization.
              'autocapitalize' => 'none',
              // Most browsers support this. Disables spell check.
              'spellcheck'   => 'false',
              // Safari only supports this. Disables spell check.
              'autocorrect'  => 'off',
              // Firefox for Android only. Names the 'Enter' key on a
              // virtual keybd.
              'mozactionhint' => $this->t('Change'),
            ],
          ],

          // A button to restore the default configuration.
          $restoreExtensionsButton => [
            '#type'              => 'submit',
            '#value'             => $this->t('Restore'),
            '#name'              => $restoreExtensionsButton,
            '#attributes'        => [
              'class'            => [
                $restoreExtensionsButton,
              ],
            ],
          ],

          // A description for the restore button.
          'extensions-restore-description' => [
            '#type'              => 'html_tag',
            '#tag'               => 'p',
            '#value'             => $this->t('Restore extensions to default values.'),
            '#attributes'        => [
              'class'            => [
                $restoreExtensionsButton . '_description',
              ],
            ],
          ],
        ],

        // A list of modules that override the extension list.
        'file-extensions-hooks' => [
          '#type'              => 'html_tag',
          '#tag'               => 'p',
          '#value'             => $hookExtensionsModulesMessage,
          '#attributes'          => [
            'class'              => [
              $itemNoteClass,
            ],
          ],
        ],
      ],
    ];

    if (empty($hookExtensionsModulesMessage) === TRUE) {
      unset($form[$tabName]['fileextensions']['section']['file-extensions-hooks']);
    }

    // Get the current extensions, if any, on the form.
    $value = $formState->getValue('file-extensions');
    if ($value !== NULL) {
      $form[$tabName]['fileextensions']['section']['file-extensions']['#value'] = $value;
    }
  }

  /*---------------------------------------------------------------------
   *
   * Validate.
   *
   *---------------------------------------------------------------------*/
  /**
   * Validates form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  private function validateFilesTab(
    array &$form,
    FormStateInterface $formState) {

    //
    // Restore filename extensions.
    // ----------------------------
    // Restore the list of allowed file name extensions.
    $trigger = $formState->getTriggeringElement();
    $restoreExtensionsButton = 'foldershare_extensions_restore';

    if ($trigger['#name'] === $restoreExtensionsButton) {
      // Revert extensions configuration.
      $value = Settings::getAllowedFilenameExtensionsDefault();
      Settings::setAllowedFilenameExtensions($value);
      ManageFilenameExtensions::setAllowedFilenameExtensions($value);

      // Reset the form's extensions list.
      $formInput = $formState->getUserInput();
      $formInput['file-extensions'] = $value;
      $formState->setUserInput($formInput);

      // Rebuild the page.
      $formState->setRebuild(TRUE);
      $formState->cleanValues();
      $formState->setTriggeringElement(NULL);

      $this->messenger()->addStatus(
        $this->t('The file extensions list has been restored to its default values.'));
      return;
    }

    //
    // Directories.
    // ------------
    // It must be "public" or "private". If "private", the site's private
    // file system must have been configured.
    $hasPrivate = empty(PrivateStream::basePath()) === FALSE;

    $scheme = $formState->getValue('directory-choice');
    $streamWrapper = FileUtilities::getStreamWrapper($scheme);
    if (!$streamWrapper || !FileUtilities::isStreamWrapperTypeSupported($streamWrapper->getType())) {
      $formState->setErrorByName(
        'directory-choice',
        $this->t('The configured stream wrapper is not supported.'));
    }

    if ($scheme === 'private' && $hasPrivate === FALSE) {
      $formState->setErrorByName(
        'directory-choice',
        $this->t('The <em>Private</em> directory is not available because the site has not been configured to support it yet.'));
    }
  }

  /*---------------------------------------------------------------------
   *
   * Submit.
   *
   *---------------------------------------------------------------------*/
  /**
   * Stores submitted form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  private function submitFilesTab(
    array &$form,
    FormStateInterface $formState) {

    //
    // File directory.
    // ---------------
    // If the site does not have any files, update the directory choice setting.
    if (FolderShare::hasFiles() === FALSE) {
      $value = $formState->getValue('directory-choice');
      Settings::setFileScheme($value);
    }

    //
    // File extensions.
    // ----------------
    // Set the flag allowing all extensions or not, and the extension list.
    // Note that the checkbox is TRUE to *allow*, while the setting has the
    // reverse sense and is TRUE to *restrict*.
    $value = $formState->getValue('all-file-extensions');
    $allowAll = ($value === 'enabled') ? TRUE : FALSE;
    Settings::setFileRestrictFilenameExtensions($allowAll === FALSE);

    // Specific extensions, set only if not allowing all extensions.
    if ($allowAll === FALSE) {
      $extensions = $formState->getValue('file-extensions');
      Settings::setAllowedFilenameExtensions($extensions);
    }

    // Forward extensions to the file field.
    if ($allowAll === TRUE) {
      // Everything allowed. No extensions list.
      ManageFilenameExtensions::setAllowedFilenameExtensions('');
    }
    else {
      ManageFilenameExtensions::setAllowedFilenameExtensions(
        Settings::getAllowedFilenameExtensions());
    }

    //
    // File upload size limit.
    // -----------------------
    // If any size is allowed, revert to a zero to mean use the PHP max.
    // Otherwise update the limit using the user's value. The setting
    // automatically restricts the value to something sane.
    $value = $formState->getValue('any-file-upload-size');
    $allowAll = ($value === 'enabled') ? TRUE : FALSE;
    if ($allowAll === TRUE) {
      $fileUploadSize = 0;
    }
    else {
      $fileUploadSize = intval($formState->getValue('file-upload-size'));
    }

    Settings::setFileUploadSizeLimit($fileUploadSize);
  }

}
