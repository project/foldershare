<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\ManageLog;

/**
 * Get/set FolderShare entity size field.
 *
 * This trait includes get and set methods for FolderShare entity size field,
 * along with methods to traverse the folder tree and update the field
 * the field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetSizeTrait {

  /*---------------------------------------------------------------------
   *
   * Size field.
   *
   *---------------------------------------------------------------------*/
  /**
   * Clears this item's storage size to empty, if it is a folder.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * For file, image, and media items, the size field mirrors an equivalent
   * size stored with the underlying File or Media entity. The mirror copy
   * is made when the FolderShare entity is created and does not change
   * thereafter.
   *
   * For folder items, the size field is the sum of size fields for all
   * descendants. The field is initialized to zero when the folder is
   * created. It is cleared to NULL here to flag that the size needs to be
   * recalculated due to changes in the folder's descendants. Size
   * recalculation can be done at the end of a series of descendant
   * operations (such as move, copy, and delete) or by a work queue task.
   *
   * If this item is not a folder, this method has no effect.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::getSize()
   * @see ::setSize()
   * @see ::updateSizeAndAncestors()
   */
  private function clearSize() {
    if ($this->isFolder() === TRUE) {
      $this->set('size', NULL);
    }

    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getSize() {
    $value = $this->get('size')->getValue();
    if (empty($value) === TRUE) {
      return FALSE;
    }

    return (int) $value[0]['value'];
  }

  /**
   * Sets the item's storage size, in bytes.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The size value is not validated. It is presumed to be a non-negative
   * value for items with a size, and negative if the size field should be
   * cleared. Size fields are normally only cleared for folders in order
   * to indicate that the folder's descendant sizes need to be recalculated.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should use appropriate
   * locks before modifying this entity.
   *
   * @param int $size
   *   The non-negative size in bytes, or negative to clear the field.
   *
   * @see ::clearSize()
   * @see ::getSize()
   * @see ::updateSizeAndAncestors()
   */
  private function setSize(int $size) {
    if ($size < 0) {
      $this->set('size', NULL);
    }
    else {
      $this->set('size', $size);
    }

    return $this;
  }

  /*---------------------------------------------------------------------
   *
   * Size updates.
   *
   *---------------------------------------------------------------------*/
  /**
   * Updates sizes for this item and its ancestors.
   *
   * If this item is a file, image, or media wrapper, its size is updated
   * to the size of the wrapped file.
   *
   * If this item is a non-hidden folder, its size is set to the sum of
   * the sizes of its immediate non-hidden children.
   *
   * Thereafter, each of this item's ancestors is updated to set its size
   * to the sum of the sizes of its immediate non-hidden children.
   *
   * <B>Process locks</B>
   * This method does not lock anything. The caller MUST have locked the
   * root folder tree containing this item.
   *
   * @see ::clearSize()
   * @see ::getSize()
   * @see ::setSize()
   */
  public function updateSizeAndAncestors() {
    try {
      // Update this entity's size.
      $sizeChanged = FALSE;

      switch ($this->getKind()) {
        case FolderShareInterface::FILE_KIND:
          // The entity wraps a file. Update the size from the file.
          $file = $this->getFile();
          if ($file !== NULL) {
            $size = $file->getSize();
            if ($size !== $this->getSize()) {
              $this->setSize($size);
              $this->save();
              $sizeChanged = TRUE;
            }
          }
          break;

        case FolderShareInterface::IMAGE_KIND:
          // The entity wraps an image. Update the size from the image file.
          $file = $this->getImage();
          if ($file !== NULL) {
            $size = $file->getSize();
            if ($size !== $this->getSize()) {
              $this->setSize($size);
              $this->save();
              $sizeChanged = TRUE;
            }
          }
          break;

        case FolderShareInterface::MEDIA_KIND:
          // The entity wraps media. Media have no standard notion of a size.
          // Set the size to zero.
          if ($this->getSize() !== 0) {
            $this->setSize(0);
            $this->save();
            $sizeChanged = TRUE;
          }
          break;

        case FolderShareInterface::OBJECT_KIND:
          // The entity is an object. Objects have no size.
          if ($this->getSize() !== 0) {
            $this->setSize(0);
            $this->save();
            $sizeChanged = TRUE;
          }
          break;

        case FolderShareInterface::FOLDER_KIND:
          if ($this->isSystemHidden() === FALSE) {
            // The entity is a non-hidden folder. Update its size.
            $size = $this->findChildrenNumberOfBytes();
            if ($size !== $this->getSize()) {
              $this->setSize($size);
              $this->save();
              $sizeChanged = TRUE;
            }
          }
          break;
      }

      if ($sizeChanged === TRUE) {
        // Loop over the ancestors from here to the root. Set each one's
        // size to the sum of their immediate children.
        $ancestorIds = array_reverse($this->findAncestorFolderIds());
        foreach ($ancestorIds as $ancestorId) {
          $ancestor = self::load($ancestorId);
          if ($ancestor !== NULL) {
            // Set the size to the sum of all children's sizes.
            $ancestor->setSize($ancestor->findChildrenNumberOfBytes());
            $ancestor->save();

            unset($ancestor);
          }
        }
      }
    }
    catch (\Exception $e) {
      ManageLog::exception($e);
    }
  }

}
