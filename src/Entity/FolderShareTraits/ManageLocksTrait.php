<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Manages content locks for exclusive access to FolderShare entities.
 *
 * This trait includes internal aquire and release methods for locks on
 * FolderShare root folder trees and root lists.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait ManageLocksTrait {

  /*---------------------------------------------------------------------
   *
   * User root list locks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Acquires a lock on a user's root list.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * This lock is used to get exclusive edit access to the user's root list.
   *
   * @param int $uid
   *   (optional, default = FolderShareInterface::CURRENT_USER_ID) The
   *   user ID of the root list to lock. This defaults to the current
   *   user's root list.
   *
   * @return bool
   *   Returns TRUE if a lock on the current user's root list was acquired,
   *   and FALSE otherwise.
   *
   * @see ::releaseUserRootListLock()
   */
  public static function acquireUserRootListLock(
    int $uid = FolderShareInterface::CURRENT_USER_ID) {

    if ($uid < 0) {
      $uid = (int) \Drupal::currentUser()->id();
    }

    return \Drupal::lock()->acquire(
      FolderShareInterface::CONTENT_LOCK_NAME . 'USER_ROOT_LIST_' . $uid,
      Settings::getContentLockDuration());
  }

  /**
   * Releases a lock on the current user's root list.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * @param int $uid
   *   (optional, default = FolderShareInterface::CURRENT_USER_ID) The
   *   user ID of the root list to lock. This defaults to the current
   *   user's root list.
   *
   * @see ::acquireUserRootListLock()
   */
  public static function releaseUserRootListLock(
    int $uid = FolderShareInterface::CURRENT_USER_ID) {

    if ($uid < 0) {
      $uid = (int) \Drupal::currentUser()->id();
    }

    return \Drupal::lock()->release(
      FolderShareInterface::CONTENT_LOCK_NAME . 'USER_ROOT_LIST_' . $uid);
  }

  /*---------------------------------------------------------------------
   *
   * Root operation locks.
   *
   *---------------------------------------------------------------------*/
  /**
   * Acquires a lock on an entire folder tree under a root item.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * This lock is used to get exclusive edit access to the entire folder
   * tree anchored at the indicated root. This is used to lock out other
   * processes during major folder tree operations that may take some time.
   *
   * @param int $rootId
   *   The entity ID of a root item whose folder tree is to be locked.
   *
   * @return bool
   *   Returns TRUE if a lock on the root's folder tree was acquired,
   *   and FALSE otherwise.
   *
   * @see ::acquireUserRootListLock()
   * @see ::releaseRootOperationLock()
   */
  public static function acquireRootOperationLock(int $rootId) {
    return \Drupal::lock()->acquire(
      FolderShareInterface::OPERATION_LOCK_NAME . $rootId,
      Settings::getOperationLockDuration());
  }

  /**
   * Returns TRUE if the root folder tree under a root item may be locked.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The root folder tree lock is tested and TRUE is returned if it appears
   * to currently be available. This is not a guarantee that a subsequent
   * call to acquireRootOperationLock() will succeed. Another process may
   * acquire the lock first.
   *
   * @param int $rootId
   *   The entity ID of a root item whose folder tree is to be tested for
   *   an available lock.
   *
   * @return bool
   *   Returns TRUE if a lock on the root's folder tree may be available,
   *   and FALSE otherwise.
   *
   * @see ::acquireUserRootListLock()
   * @see ::releaseRootOperationLock()
   */
  public static function isRootOperationLockAvailable(int $rootId) {
    return \Drupal::lock()->lockMayBeAvailable(
      FolderShareInterface::OPERATION_LOCK_NAME . $rootId);
  }

  /**
   * Releases a lock on the user's root list.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * @param int $rootId
   *   The entity ID of a root item whose folder tree is to be locked.
   *
   * @see ::acquireRootOperationLock()
   * @see ::releaseUserRootListLock()
   */
  public static function releaseRootOperationLock(int $rootId) {
    return \Drupal::lock()->release(
      FolderShareInterface::OPERATION_LOCK_NAME . $rootId);
  }

  /*---------------------------------------------------------------------
   *
   * Standard error messages.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a standard lock exception message.
   *
   * This method provides a generic lock exception message that may be used by
   * operations that are unable to acquire a needed lock.
   *
   * @param mixed $operationName
   *   The past tense name of the operation being performed, such as
   *   'deleted', 'moved', or 'copied'. The value may be a string or
   *   translated markup (perferred).
   * @param string $itemName
   *   (optional, default = NULL = multiple items) The name of a single item
   *   involved in an operation that cannot be done. If NULL, a multi-item
   *   message is returned instead.
   *
   * @return \Drupal\Core\Render\Markup
   *   Returns a markup object containing a formatted standard lock
   *   exception message.
   */
  public static function getStandardLockExceptionMessage(
    $operationName,
    string $itemName = NULL) {

    if (empty($operationName) === TRUE) {
      $operationName = t('updated');
    }

    if (empty($itemName) === TRUE) {
      return FormatUtilities::createFormattedMessage(
        t('The system is busy. One or more items cannot be @operation at this time.',
          [
            '@operation' => $operationName,
          ]),
          t('Please try again in a moment.'));
    }

    return FormatUtilities::createFormattedMessage(
      t(
        'The system is busy. The item "@name" cannot be @operation at this time.',
        [
          '@name'      => $itemName,
          '@operation' => $operationName,
        ]),
      t('Please try again in a moment.'));
  }

}
