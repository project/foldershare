<?php

namespace Drupal\foldershare\Plugin;

use Drupal\Component\Plugin\CategorizingPluginManagerInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Plugin\CategorizingPluginManagerTrait;
use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\StringTranslation\TranslatableMarkup;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Defines a manager for plugins for commands on a shared folder.
 *
 * Similar to Drupal core Actions, shared folder commands encapsulate
 * logic to perform a single operation, such as creating, editing, or
 * deleting files or folders.
 *
 * This plugin manager keeps a list of all plugins after validating
 * that their definitions are well-formed.
 *
 * @ingroup foldershare
 */
class FolderShareCommandManager extends DefaultPluginManager implements CategorizingPluginManagerInterface {

  use CategorizingPluginManagerTrait;
  use LoggerChannelTrait;

  /*--------------------------------------------------------------------
   *
   * Construct.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new command manager.
   *
   * @param \Traversable $namespaces
   *   An object containing the root paths, keyed by the corresponding
   *   namespace, in which to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cacheBackend
   *   The backend cache to use to store plugin information.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(
    \Traversable $namespaces,
    CacheBackendInterface $cacheBackend,
    ModuleHandlerInterface $moduleHandler) {

    // Create a manager and indicate the name of the directory for plugins,
    // the namespaces to look through, the module handler, the interface
    // plugins should implement, and the annocation class that describes
    // the plugins.
    parent::__construct(
      'Plugin/FolderShareCommand',
      $namespaces,
      $moduleHandler,
      'Drupal\foldershare\Plugin\FolderShareCommand\FolderShareCommandInterface',
      'Drupal\foldershare\Annotation\FolderShareCommand',
      []);

    // Define module hooks that alter FolderShareCommand information to be
    // named "XXX_foldersharecommand_info_alter", where XXX is the module's
    // name.
    $this->alterInfo('foldersharecommand_info');

    // Use the 'config:core.extension' cache tag so that the plugin cache is
    // invalidated on module install and uninstall.
    $cacheTag = 'config:core.extension';
    $this->setCacheBackend(
      $cacheBackend,
      'foldershare_command_plugins',
      [$cacheTag]);
  }

  /*--------------------------------------------------------------------
   *
   * Validate constraint specifications in command annotation.
   *
   *--------------------------------------------------------------------*/
  /**
   * Validates 'access' constraints.
   *
   * The value must be one of:
   * - 'chown'.
   * - 'create'.
   * - 'delete'.
   * - 'share'.
   * - 'update'.
   * - 'view'.
   *
   * The 'access' field and values are converted to lower case.
   *
   * If no 'access' field constraint exists, or it is empty, the given
   * default is used.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   * @param array $constraints
   *   The constraints array with an 'access' field.
   * @param string $default
   *   (optional, default = 'view') The default value to use if 'access'
   *   is omitted.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateAccess(
    array &$pluginDefinition,
    array &$constraints,
    string $default = 'view') {

    $pid = $pluginDefinition['id'];

    // If no access is provided, use the default.
    if (empty($constraints['access']) === TRUE) {
      $constraints['access'] = mb_strtolower($default);
      return TRUE;
    }

    // If the access is not a scalar string, error.
    if (is_string($constraints['access']) === FALSE) {
      $this->logError($pid, "'access' must be a scalar string.");
      return FALSE;
    }

    $access = $constraints['access'];
    $lower = mb_strtolower($access);
    switch ($lower) {
      case 'chown':
      case 'create':
      case 'delete':
      case 'share':
      case 'update':
      case 'view':
        break;

      default:
        $this->logError($pid, "Unrecognied access constraint: \"$access\".");
        return FALSE;
    }

    $constraints['access'] = $lower;
    return TRUE;
  }

  /**
   * Validates the definition's category.
   *
   * The definition's category must be non-empty. It is automatically updated
   * to use lower case.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateCategory(array &$pluginDefinition) {
    $pid = $pluginDefinition['id'];

    // The category cannot be empty.
    if (empty($pluginDefinition['category']) === TRUE) {
      $this->logError($pid, "'category' value must be defined.");
      return FALSE;
    }

    // If the category is not a scalar string, error.
    if (is_string($pluginDefinition['category']) === FALSE) {
      $this->logError($pid, "'category' must be a scalar string.");
      return FALSE;
    }

    // Map category to lower-case.
    $pluginDefinition['category'] =
      mb_strtolower($pluginDefinition['category']);

    return TRUE;
  }

  /**
   * Validates 'filenameextensions' field for selection constraints.
   *
   * The values are a list of extensions. Leading dots are removed and
   * extensions are converted to lower case.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   * @param array $constraints
   *   The constraints array with a 'filenameextensions' field. The field is
   *   added if missing, and adjusted to use lower case if needed.
   * @param array $default
   *   (optional, default = []) The default values to use if
   *   'filenameextensions' is omitted.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateFilenameExtensions(
    array &$pluginDefinition,
    array &$constraints,
    array $default = []) {

    $pid = $pluginDefinition['id'];

    // If no file extension is provided, use the default.
    if (empty($constraints['filenameextensions']) === TRUE) {
      $lowerDefault = [];
      foreach ($default as $d) {
        $lowerDefault[] = mb_strtolower($d);
      }
      $constraints['filenameextensions'] = $lowerDefault;
      return TRUE;
    }

    // If the file extensions is not an array, error.
    if (is_array($constraints['filenameextensions']) === FALSE) {
      $this->logError($pid, "'filenameextensions' must be an array.");
      return FALSE;
    }

    // Insure there are leading dots, and convert to lower case.
    $anyPresent = FALSE;
    foreach ($constraints['filenameextensions'] as $index => $f) {
      $f = mb_strtolower($f);
      switch ($f) {
        case 'any':
          $anyPresent = TRUE;
          break;

        case 'none':
          break;

        default:
          if (mb_strpos($f, '.') !== 0) {
            // No leading dot. Add one.
            $f = '.' . $f;
          }
      }

      $constraints['filenameextensions'][$index] = $f;
    }

    // Simplify if 'any' is present. An empty array means 'any'.
    if ($anyPresent === TRUE) {
      $constraints['filenameextensions'] = [];
    }

    return TRUE;
  }

  /**
   * Validates 'kinds' constraints.
   *
   * The values must be one of:
   * - 'any'.
   * - 'rootlist'.
   * - one of the FolderShare kinds (e.g. 'file', 'image', 'media',
   *   'folder', or 'object').
   *
   * The kinds are converted to lower case.
   *
   * If 'any' is present, the constraints are simplified to just 'any'.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   * @param array $constraints
   *   The constraints array with a 'kinds' field. The field is added if
   *   missing, and adjusted to use lower case if needed.
   * @param string $default
   *   (optional, default = 'any') The default value to use if 'kinds' is
   *   omitted.
   * @param bool $allowRootlist
   *   (optional, default = TRUE) When FALSE, if the 'rootlist' kind is
   *   included, an error message is logged.
   * @param bool $allowNone
   *   (optional, default = FALSE) When FALSE, if the 'none' kind is
   *   included, an error message is logged.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   *
   * @see \Drupal\foldershare\FolderShareInterface::FILE_KIND
   * @see \Drupal\foldershare\FolderShareInterface::FOLDER_KIND
   * @see \Drupal\foldershare\FolderShareInterface::IMAGE_KIND
   * @see \Drupal\foldershare\FolderShareInterface::MEDIA_KIND
   * @see \Drupal\foldershare\FolderShareInterface::OBJECT_KIND
   */
  private function validateKinds(
    array &$pluginDefinition,
    array &$constraints,
    string $default = 'any',
    bool $allowRootlist = TRUE,
    bool $allowNone = FALSE) {

    $pid = $pluginDefinition['id'];

    // If no kind is provided, use the default.
    if (empty($constraints['kinds']) === TRUE) {
      $constraints['kinds'] = [mb_strtolower($default)];
      return TRUE;
    }

    // If the kind is not an array, error.
    if (is_array($constraints['kinds']) === FALSE) {
      $this->logError($pid, "'kinds' must be an array.");
      return FALSE;
    }

    // Check for known kinds, and convert to lower case.
    $anyPresent = FALSE;
    foreach ($constraints['kinds'] as $index => $k) {
      $lower = mb_strtolower($k);

      switch ($lower) {
        case 'any':
          $anyPresent = TRUE;
          break;

        case 'none':
          if ($allowNone === FALSE) {
            $this->logError($pid, "'none' kind is only allowed on destination constraints");
            return FALSE;
          }
          break;

        case 'rootlist':
          if ($allowRootlist === FALSE) {
            $this->logError($pid, "'rootlist' kind is only allowed on parent and destination constraints");
            return FALSE;
          }
          break;

        case FolderShare::FILE_KIND:
        case FolderShare::IMAGE_KIND:
        case FolderShare::MEDIA_KIND:
        case FolderShare::OBJECT_KIND:
        case FolderShare::FOLDER_KIND:
          break;

        default:
          $this->logError($pid, "Unrecognied kind constraint: \"$k\".");
          return FALSE;
      }

      $constraints['kinds'][$index] = $lower;
    }

    // Simplify if 'any' is present.
    if ($anyPresent === TRUE) {
      $constraints['kinds'] = ['any'];
    }

    return TRUE;
  }

  /**
   * Validates the definition's label, title, and menu names.
   *
   * The definition's label must be non-empty.
   *
   * The remaining values may be empty and default to lesser values.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateLabel(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // The label cannot be empty.
    if (empty($pluginDefinition['label']) === TRUE) {
      $this->logError($pid, "'label. value must be defined.");
      return FALSE;
    }

    // If the category is not a scalar string, error.
    if ($pluginDefinition['label'] instanceof TranslatableMarkup === FALSE &&
        is_string($pluginDefinition['label']) === FALSE) {
      $this->logError($pid, "'label' must be a scalar string or string translation.");
      return FALSE;
    }

    // If no menu name is given, use the label.
    if (empty($pluginDefinition['menuNameDefault']) === TRUE) {
      $pluginDefinition['menuNameDefault'] = $pluginDefinition['label'];
    }

    if (empty($pluginDefinition['menuName']) === TRUE) {
      $pluginDefinition['menuName'] = $pluginDefinition['menuNameDefault'];
    }

    if (empty($pluginDefinition['description']) === TRUE) {
      $pluginDefinition['description'] = $pluginDefinition['menuName'];
    }

    return TRUE;
  }

  /**
   * Validates 'mimetypes' constraints.
   *
   * The values must be one of:
   * - 'any'.
   * - MAJOR/MINOR.
   * - MAJOR/*.
   *
   * MAJOR and MINOR are text without embedded spaces or "*". MINOR can be a "*"
   * to indicate all MINOR values. MAJOR cannot be "*".
   *
   * The mimetype words are converted to lower case.
   *
   * If 'any' is present, the constraints are simplified to just 'any'.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   * @param array $constraints
   *   The constraints array with a 'mimetypes' field. The field is added if
   *   missing, and adjusted to use lower case if needed.
   * @param string $default
   *   (optional, default = "any") The default value to use if 'mimetypes'
   *   is omitted.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateMimeTypes(
    array &$pluginDefinition,
    array &$constraints,
    string $default = 'any') {

    $pid = $pluginDefinition['id'];

    // If no mimetypes list is provided, use the default.
    if (empty($constraints['mimetypes']) === TRUE) {
      $constraints['mimetypes'] = [mb_strtolower($default)];
      return TRUE;
    }

    // If the mimetypes is not an array, error.
    if (is_array($constraints['mimetypes']) === FALSE) {
      $this->logError($pid, "'mimetypes' must be an array.");
      return FALSE;
    }

    // Check for known mimetypes, and convert to lower case.
    $anyPresent = FALSE;
    foreach ($constraints['mimetypes'] as $index => $m) {
      $lower = mb_strtolower($m);

      switch ($lower) {
        case 'any':
        case '*/*':
          $anyPresent = TRUE;
          break;

        default:
          $parts = mb_split('/', $lower);
          if (count($parts) !== 2) {
            $this->logError($pid, "'mimetypes' constraint is not in the form MAJOR/MINOR: \"$m\".");
            return FALSE;
          }

          if (mb_strpos($parts[0], '*') !== FALSE) {
            $this->logError($pid, "'mimetypes' constraint cannot have an embedded \"*\": \"$m\".");
            return FALSE;
          }

          if ($parts[1] !== '*' && mb_strpos($parts[1], '*') !== FALSE) {
            $this->logError($pid, "'mimetypes' constraint cannot have an embedded \"*\": \"$m\".");
            return FALSE;
          }
          break;
      }

      $constraints['mimetypes'][$index] = $lower;
    }

    // Simplify if 'any' is present.
    if ($anyPresent === TRUE) {
      $constraints['mimetypes'] = ['any'];
    }

    return TRUE;
  }

  /**
   * Validates 'ownership' constraints.
   *
   * The values must be one of:
   * - 'any'.
   * - 'ownedbyuser'.
   * - 'ownedbyanonymous'.
   * - 'ownedbyanother'.
   * - 'sharedwithusertoview'.
   * - 'sharedwithusertoauthor'.
   * - 'sharedbyuser'.
   *
   * The values are converted to lower case.
   *
   * If 'any' is present, the constraints are simplified to just 'any'.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   * @param array $constraints
   *   The constraints array with a 'ownership' field. The field is added if
   *   missing, and adjusted to use lower case if needed.
   * @param string $default
   *   (optional, default = 'any') The default value to use if 'ownership'
   *   is omitted.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateOwnership(
    array &$pluginDefinition,
    array &$constraints,
    string $default = 'any') {

    $pid = $pluginDefinition['id'];

    // If no ownership is provided, use the default.
    if (empty($constraints['ownership']) === TRUE) {
      $constraints['ownership'] = [mb_strtolower($default)];
      return TRUE;
    }

    // If the ownership is not an array, error.
    if (is_array($constraints['ownership']) === FALSE) {
      $this->logError($pid, "'ownership' must be an array.");
      return FALSE;
    }

    // Check for known ownership, and convert to lower case.
    $anyPresent = FALSE;
    foreach ($constraints['ownership'] as $index => $t) {
      $lower = mb_strtolower($t);

      switch ($lower) {
        case 'any':
          $anyPresent = TRUE;
          break;

        case 'ownedbyuser':
        case 'ownedbyanonymous':
        case 'ownedbyanother':
        case 'sharedbyuser':
        case 'sharedwithusertoview':
        case 'sharedwithusertoauthor':
        case 'sharedwithanonymoustoview':
        case 'sharedwithanonymoustoauthor':
          break;

        default:
          $this->logError($pid, "Unrecognied ownership constraint: \"$t\".");
          return FALSE;
      }

      $constraints['ownership'][$index] = $lower;
    }

    // Simplify if 'any' is present.
    if ($anyPresent === TRUE) {
      $constraints['ownership'] = ['any'];
    }

    return TRUE;
  }

  /**
   * Validates 'types' selection constraints.
   *
   * The values must be one of:
   * - 'none'.
   * - 'parent'.
   * - 'one'.
   * - 'many'.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   * @param array $constraints
   *   The constraints array with a 'types' field. The field is added if
   *   missing, and adjusted to use lower case if needed.
   * @param string $default
   *   (optional, default = 'none') The default value to use if 'types'
   *   is omitted.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateSelectionTypes(
    array &$pluginDefinition,
    array &$constraints,
    string $default = 'none') {

    $pid = $pluginDefinition['id'];

    // If no type is provided, use the default.
    if (empty($constraints['types']) === TRUE) {
      $constraints['types'] = [mb_strtolower($default)];
      return TRUE;
    }

    // If the types is not an array, error.
    if (is_array($constraints['types']) === FALSE) {
      $this->logError($pid, "'types' must be an array.");
      return FALSE;
    }

    // Check for known types, and convert to lower case.
    foreach ($constraints['types'] as $index => $t) {
      $lower = mb_strtolower($t);

      switch ($lower) {
        case 'none':
        case 'parent':
        case 'one':
        case 'many':
          break;

        default:
          $this->logError($pid, "Unrecognied selection type constraint: \"$t\".");
          return FALSE;
      }

      $constraints['types'][$index] = $lower;
    }

    return TRUE;
  }

  /**
   * Validates the definition's special handling.
   *
   * Defined by the 'specialHandling' annotation, this value is an array
   * with known values:
   * - 'copy': the command moves items and is used for row drag-and-drop.
   * - 'move': the command copies items and is used for row drag-and-drop.
   * - 'upload': the command uploads files and is used for file drag-and-drop.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateSpecialHandling(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // If there is no special handling, use defaults.
    if (empty($pluginDefinition['specialHandling']) === TRUE) {
      $pluginDefinition['specialHandling'] = [];
      return TRUE;
    }

    // Special handling must be an array.
    $handling = &$pluginDefinition['specialHandling'];
    if (is_array($handling) === FALSE) {
      $this->logError($pid, "'specialHandling' must be an array.");
      return FALSE;
    }

    foreach ($handling as $index => $h) {
      $lower = mb_strtolower($h);

      switch ($lower) {
        case 'copy':
        case 'move':
        case 'upload':
        case 'external_upload':
          break;

        default:
          $this->logError($pid, "Unrecognied special handling value: \"$h\".");
          return FALSE;
      }

      $handling[$index] = $lower;
    }

    return TRUE;
  }

  /**
   * Validates the definition's user constraints.
   *
   * The constraints supported:
   * - 'any' (default).
   * - 'anonymous'.
   * - 'authenticated'.
   * - 'adminpermission'.
   * - 'noadminpermission'.
   * - 'authorpermission'.
   * - 'sharepermission'.
   * - 'sharepublicpermission'.
   * - 'viewpermission'.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateUserConstraints(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // If there are no constraints, use default.
    if (empty($pluginDefinition['userConstraints']) === TRUE) {
      $pluginDefinition['userConstraints'] = ['any'];
      return TRUE;
    }

    // Constraints must be an array.
    $constraints = &$pluginDefinition['userConstraints'];
    if (is_array($constraints) === FALSE) {
      $this->logError($pid, "'userConstraints' must be an array.");
      return FALSE;
    }

    // Check for known user choices, and convert to lower case.
    $anyPresent = FALSE;
    foreach ($constraints as $index => $u) {
      $lower = mb_strtolower($u);

      switch ($lower) {
        case 'any':
          $anyPresent = TRUE;
          break;

        case 'anonymous':
        case 'authenticated':
        case 'adminpermission':
        case 'noadminpermission':
        case 'authorpermission':
        case 'sharepermission':
        case 'sharepublicpermission':
        case 'viewpermission':
          break;

        default:
          $this->logError($pid, "Unrecognied user constraint: \"$u\".");
          return FALSE;
      }

      $constraints[$index] = $lower;
    }

    // Simplify if 'any' is present.
    if ($anyPresent === TRUE) {
      $pluginDefinition['userConstraints'] = ['any'];
    }

    return TRUE;
  }

  /**
   * Validates the definition's weight.
   *
   * The definition's weight must be an integer and defaults to zero.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateWeight(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // Default an empty weight to zero. Otherwise convert to integer.
    if (empty($pluginDefinition['weight']) === TRUE) {
      $pluginDefinition['weight'] = (int) 0;
      return TRUE;
    }

    // If the weight is not a scalar integer, error.
    if (is_int($pluginDefinition['weight']) === FALSE) {
      $this->logError($pid, "'weight' must be a scalar integer.");
      return FALSE;
    }

    return TRUE;
  }

  /*--------------------------------------------------------------------
   *
   * Validate groups of annotation constraints.
   *
   *--------------------------------------------------------------------*/
  /**
   * Logs an error message about the indicated plugin command.
   *
   * @param string $pid
   *   The plugin ID (i.e. name).
   * @param string $message
   *   The error message.
   */
  private function logError(string $pid, string $message) {
    $this->getLogger(Constants::MODULE)->error(
      "Malformed FolderShareCommand plugin \"$pid\": $message");
  }

  /**
   * Checks that a plugin's definition is complete and valid.
   *
   * A complete definition must have a label, category, parent constraints,
   * selection constraints, destination constraints, and special handling.
   * All values must be valid.
   *
   * If needed, the definition is adjusted in-place to convert to lower case,
   * fill in a default, and use standardize values.
   *
   * If the definition is not valid, error messages are logged and FALSE
   * is returned. Otherwise TRUE is returned for valid definitions.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise.
   */
  private function validateDefinition(array &$pluginDefinition) {
    return $this->validateLabel($pluginDefinition) &&
        $this->validateCategory($pluginDefinition) &&
        $this->validateWeight($pluginDefinition) &&
        $this->validateUserConstraints($pluginDefinition) &&
        $this->validateParentConstraints($pluginDefinition) &&
        $this->validateSelectionConstraints($pluginDefinition) &&
        $this->validateDestinationConstraints($pluginDefinition) &&
        $this->validateSpecialHandling($pluginDefinition);
  }

  /**
   * Validates the definition's parent constraints.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateParentConstraints(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // If there are no constraints, use defaults.
    if (empty($pluginDefinition['parentConstraints']) === TRUE) {
      $pluginDefinition['parentConstraints'] = [
        'access'             => 'view',
        'kinds'              => ['any'],
        'filenameextensions' => [],
        'mimetypes'          => ['any'],
        'ownership'          => ['any'],
      ];
      return TRUE;
    }

    // Constraints must be an array.
    $constraints = &$pluginDefinition['parentConstraints'];
    if (is_array($constraints) === FALSE) {
      $this->logError($pid, "'parentConstraints' must be an array.");
      return FALSE;
    }

    return $this->validateAccess($pluginDefinition, $constraints, 'view') &&
      $this->validateFilenameExtensions($pluginDefinition, $constraints, []) &&
      $this->validateKinds($pluginDefinition, $constraints, 'any', TRUE, FALSE) &&
      $this->validateMimeTypes($pluginDefinition, $constraints, 'any') &&
      $this->validateOwnership($pluginDefinition, $constraints, 'any');
  }

  /**
   * Validates the definition's selection requirements.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateSelectionConstraints(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // If there are no constraints, use defaults.
    if (empty($pluginDefinition['selectionConstraints']) === TRUE) {
      $pluginDefinition['selectionConstraints'] = [
        'access'             => 'view',
        'filenameextensions' => [],
        'kinds'              => ['any'],
        'mimetypes'          => ['any'],
        'ownership'          => ['any'],
        'types'              => ['none'],
      ];
      return TRUE;
    }

    // Constraints must be an array.
    $constraints = &$pluginDefinition['selectionConstraints'];
    if (is_array($constraints) === FALSE) {
      $this->logError($pid, "'selectionConstraints' must be an array.");
      return FALSE;
    }

    return $this->validateAccess($pluginDefinition, $constraints, 'view') &&
      $this->validateFilenameExtensions($pluginDefinition, $constraints, []) &&
      $this->validateKinds($pluginDefinition, $constraints, 'any', FALSE, FALSE) &&
      $this->validateMimeTypes($pluginDefinition, $constraints, 'any') &&
      $this->validateOwnership($pluginDefinition, $constraints, 'any') &&
      $this->validateSelectionTypes($pluginDefinition, $constraints, 'none');
  }

  /**
   * Validates the definition's destination requirements.
   *
   * @param array $pluginDefinition
   *   The definition of the plugin.
   *
   * @return bool
   *   Returns TRUE if the plugin definition is complete and valid,
   *   and FALSE otherwise. When FALSE, a message is logged.
   */
  private function validateDestinationConstraints(array &$pluginDefinition) {

    $pid = $pluginDefinition['id'];

    // If there are no constraints, use defaults.
    if (empty($pluginDefinition['destinationConstraints']) === TRUE) {
      $pluginDefinition['destinationConstraints'] = [
        'access'             => 'update',
        'kinds'              => ['none'],
        'filenameextensions' => [],
        'mimetypes'          => ['any'],
        'ownership'          => ['any'],
      ];
      return TRUE;
    }

    // Constraints must be an array.
    $constraints = &$pluginDefinition['destinationConstraints'];
    if (is_array($constraints) === FALSE) {
      $this->logError($pid, "'destinationConstraints' must be an array.");
      return FALSE;
    }

    return $this->validateAccess($pluginDefinition, $constraints, 'update') &&
      $this->validateFilenameExtensions($pluginDefinition, $constraints, []) &&
      $this->validateKinds($pluginDefinition, $constraints, 'none', TRUE, TRUE) &&
      $this->validateMimeTypes($pluginDefinition, $constraints, 'any') &&
      $this->validateOwnership($pluginDefinition, $constraints, 'any');
  }

  /*--------------------------------------------------------------------
   *
   * Find definitions.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  protected function findDefinitions() {
    // Let the parent class discover definitions, process them, and
    // let other modules alter them. The parent class also filters out
    // definitions for modules that have been uninstalled.
    $definitions = parent::findDefinitions();

    // At this point, the definitions have been created and modules
    // have had a chance to alter them. Now we can check that the
    // definitions are complete and valid. If not, remove them from
    // the definitions list.
    foreach ($definitions as $pluginId => $pluginDefinition) {
      if ($this->validateDefinition($pluginDefinition) === FALSE) {
        // Bad definition. Remove it from the list.
        unset($definitions[$pluginId]);
      }
      else {
        // Validation may have corrected the definition. Save it.
        $definitions[$pluginId] = $pluginDefinition;
      }
    }

    return $definitions;
  }

}
