<?php

namespace Drupal\foldershare\Entity\Exception;

/**
 * Defines an exception indicating a content validation problem.
 *
 * In addition to standard exception parameters (such as the message),
 * a validation exception includes an optional item number that indicates
 * when the exception applies to a specific item in a list of items.
 *
 * @ingroup foldershare
 */
class ValidationException extends RuntimeExceptionWithMarkup {

  /*--------------------------------------------------------------------
   *
   * Fields.
   *
   *--------------------------------------------------------------------*/
  /**
   * The optional item number to which this exception applies.
   *
   * @var int
   */
  private $itemNumber = -1;

  /*--------------------------------------------------------------------
   *
   * Constructors.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs an exception.
   *
   * @param string|\Drupal\Component\Render\MarkupInterface $message
   *   (optional, default = NULL) The message string or an instance of
   *   \Drupal\Component\Render\MarkupInterface. If NULL, a default
   *   message is used.
   * @param int $code
   *   (optional, default = 0) An error code.
   * @param \Throwable $previous
   *   (optional, default = NULL) A previous exception that this extends.
   */
  public function __construct(
    $message = NULL,
    int $code = 0,
    \Throwable $previous = NULL) {

    if (empty($message) === TRUE) {
      $message = $this->t('Validation exception');
    }

    parent::__construct($message, $code, $previous);
  }

  /*--------------------------------------------------------------------
   *
   * Methods.
   *
   *--------------------------------------------------------------------*/
  /**
   * Returns the item number.
   *
   * This may be used to indicate when an exception applies to a single
   * faulty item in a list of items.
   *
   * @return int
   *   The list index of the item causing the exception, or a -1
   *   if an index is not pertinent.
   */
  public function getItemNumber() {
    return $this->itemNumber;
  }

  /**
   * Sets the item number.
   *
   * This may be used to indicate when an exception applies to a single
   * faulty item in a list of items.
   *
   * @param int $num
   *   The list index of the item causing the exception, or a -1
   *   if an index is not pertinent.
   */
  public function setItemNumber(int $num) {
    $this->itemNumber = (($num <= (-1)) ? (-1) : $num);
  }

}
