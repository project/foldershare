<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

/**
 * Get/set FolderShare entity created time field.
 *
 * This trait includes get and set methods for FolderShare entity
 * created time field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetCreatedTimeTrait {

  /*---------------------------------------------------------------------
   *
   * Created field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return (int) $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    // For compatability with other entities with setCreatedTime() methods
    // (e.g. Node), the $timestamp argument is not given an "int" type hint.
    // Nevertheless, it is documented as being an integer and is cast as one.
    $this->set('created', (int) $timestamp);
    return $this;
  }

}
