<?php

namespace Drupal\foldershare\Entity\Controller;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpFoundation\Request;

use Drupal\Component\Utility\Html;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Render\Element\Link as RenderLink;

use Drupal\Core\Controller\TitleResolver;
use Drupal\Core\Entity\Controller\EntityViewController;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Entity\EntityViewDisplay;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Link;
use Drupal\Core\Render\RendererInterface;
use Drupal\Core\Routing\RouteProvider;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\Core\Url;

use Drupal\user\Entity\User;
use Drupal\views\Entity\View;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareAccessControlHandler;
use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Form\UIAncestorMenu;
use Drupal\foldershare\Form\UIFolderBrowserMenu;
use Drupal\foldershare\Form\UISearchBox;
use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\LinkUtilities;

/**
 * Presents a view of a FolderShare entity or root list.
 *
 * This controller builds a view of an entity or root list. For an entity,
 * the view presents the entity's fields and pseudo fields, one of which
 * shows a folder's list of child files and folders. For a root list,
 * the view shows a root list of files and folders. In both cases, the list
 * of files and folders is created by an embedded View from the Views module.
 *
 * There are several entry points to this class:
 *
 * - viewAllFolder() shows all root files and folders for an admin.
 *
 * - viewHomeFolder() shows all root files and folders owned by the
 * current user.
 *
 * - viewSharedFolder() shows all root files and folders shared with
 * the current user.
 *
 * - viewPublicFolder() shows all root files and folders that have
 * been made public.
 *
 * - viewEntity() shows all files and folders children of a specific entity.
 *
 * In addition to the above entry points, this class also defines and
 * implements several pseudo-fields for the FolderShare entity. These
 * pseudo-fields act like fields for purposes of field display, but they
 * are implemented here to include forms, user interfaces, and related
 * features that are more than a traditional (label, value) field display.
 *
 * The most important of these pseudo-fields is for the folder browser.
 * This browser's implementation shares code with the display of root files and
 * folders, which needs a similar user interface.
 *
 * Pseudo-fields are defined by hooks in foldershare.module that redirect to
 * this class.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 *
 * @see \Drupal\foldershare\Entity\FolderShare
 * @see \Drupal\foldershare\Form\UIFolderBrowserMenu
 * @see \Drupal\foldershare\Form\UISearchBox
 * @see \Drupal\foldershare\Entity\Builder\FolderShareViewBuilder
 */
final class FolderShareViewController extends EntityViewController {
  use StringTranslationTrait;

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The route provider, set at construction time.
   *
   * @var \Drupal\Core\Routing\RouteProvider
   */
  private $routeProvider;

  /**
   * The route title resolver, set at construction time.
   *
   * @var \Drupal\Core\Controller\TitleResolver
   */
  private $titleResolver;

  /**
   * The current user account, set at construction time.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  private $currentUser;

  /**
   * The module handler.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  private $moduleHandler;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  private $formBuilder;

  /*---------------------------------------------------------------------
   *
   * Construction.
   *
   *---------------------------------------------------------------------*/
  /**
   * Constructs a FolderShare view controller.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\Render\RendererInterface $renderer
   *   The renderer.
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user account.
   * @param \Drupal\Core\Routing\RouteProvider $routeProvider
   *   The route provider.
   * @param \Drupal\Core\Controller\TitleResolver $titleResolver
   *   The page title resolver.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The entity form builder.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    RendererInterface $renderer,
    AccountInterface $currentUser,
    RouteProvider $routeProvider,
    TitleResolver $titleResolver,
    ModuleHandlerInterface $moduleHandler,
    FormBuilderInterface $formBuilder) {

    parent::__construct($entityTypeManager, $renderer);

    $this->currentUser   = $currentUser;
    $this->routeProvider = $routeProvider;
    $this->titleResolver = $titleResolver;
    $this->moduleHandler = $moduleHandler;
    $this->formBuilder   = $formBuilder;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('entity_type.manager'),
      $container->get('renderer'),
      $container->get('current_user'),
      $container->get('router.route_provider'),
      $container->get('title_resolver'),
      $container->get('module_handler'),
      $container->get('form_builder'));
  }

  /*---------------------------------------------------------------------
   *
   * Pseudo-fields setup.
   *
   * Module hooks in foldershare.module define pseudo-fields by redirecting
   * to this class's methods.
   *
   *---------------------------------------------------------------------*/
  /**
   * Defines available pseudo-fields for the FolderShare entity.
   *
   * Pseudo-fields show information derived from an entity, rather
   * than information stored explicitly in an entity field. We define:
   *
   * - The path of folder ancestors.
   * - The table of an entity's child content.
   * - A sharing status message.
   * - Links to related content.
   *
   * @return array
   *   A nested array of pseudo-fields with keys and values that specify
   *   the characteristics and treatment of pseudo-fields.
   *
   * @see foldershare_entity_extra_field_info()
   */
  public static function getEntityExtraFieldInfo() {
    $display = [
      // The "folder_path" pseudo-field presents a '/' separated path to
      // the current file or folder. Path components are all links to
      // ancestor folders.
      //
      // By default, this is not visible because it is often redundant if a
      // site has a breadcrumb on the page.
      'folder_path'    => [
        'label'        => t('Path'),
        'description'  => t('Names and links for ancestor folders.'),
        'weight'       => 100,
        'visible'      => FALSE,
      ],

      // The "folder_table" pseudo-field presents a table of child files and
      // folders. Items in the table include links to child items.
      //
      // By default, this is visible because a list of folder contents is
      // essential information about the folder.
      //
      // This exists as a pseudo-field so that site administrators can use
      // the Field UI module and its 'Manage display' form to adjust the
      // order in which fields and this table are presented on a page.
      'folder_table'   => [
        'label'        => t('Folder contents table'),
        'description'  => t('Table of child files and folders.'),
        'weight'       => 110,
        'visible'      => TRUE,
      ],

      // The "sharing_status" pseudo-field summarizes whether the current
      // item is shared with the current user, other users, or the public,
      // or if it is instead private.
      'sharing_status' => [
        'label'        => t('Status'),
        'description'  => t('Sharing status'),
        'weight'       => 120,
        'visible'      => FALSE,
      ],

      // The "links" psudo-field holds an extensible list of links available
      // on entity pages, similar to the "links" pseudo-field on Nodes.
      'links'          => [
        'label'        => t('Links'),
        'description'  => t('Links to related content'),
        'weight'       => 130,
        'visible'      => TRUE,
      ],
    ];

    // The returned array is indexed by the entity type, bundle,
    // and display/form configuration.
    $entityType = FolderShare::ENTITY_TYPE_ID;
    $bundleType = $entityType;

    $extras = [];
    $extras[$entityType] = [];
    $extras[$entityType][$bundleType] = [];
    $extras[$entityType][$bundleType]['display'] = $display;

    return $extras;
  }

  /**
   * Adds a renderable description of an entity's pseudo-fields.
   *
   * Pseudo-fields show information derived from an entity, rather
   * than information stored explicitly in an entity field.
   *
   * @param array $page
   *   The initial rendering array modified by this method and returned.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity being presented.
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $display
   *   The field_ui display to use.
   * @param string $viewMode
   *   (optional) The field_ui view mode to use.
   * @param string $langcode
   *   (optional) The language code to use.
   *
   * @return array
   *   The given page array modified to contain the renderable
   *   pseudo-fields for a folder presentation.
   *
   * @see foldershare_foldershare_view()
   */
  public static function getFolderShareView(
    array &$page,
    EntityInterface $entity,
    EntityViewDisplay $display,
    string $viewMode,
    string $langcode = '') {

    $controller = self::create(\Drupal::getContainer());

    //
    // There are several view modes defined through common use in Drupal core:
    //
    // - 'full' = generate a full view.
    // - 'search_index' = generate a keywords-only view.
    // - 'search_result' = generate an abbreviated search result view.
    //
    // Each of the pseudo-fields handle this in their own way.
    if ($display->getComponent('folder_path') !== NULL) {
      $controller->addPathPseudoField(
        $page,
        $entity,
        $display,
        $viewMode,
        $langcode);
    }

    if ($display->getComponent('folder_table') !== NULL) {
      $controller->addViewPseudoField(
        $page,
        $entity,
        $display,
        $viewMode,
        $langcode);
    }

    if ($display->getComponent('sharing_status') !== NULL) {
      $controller->addSharingPseudoField(
        $page,
        $entity,
        $display,
        $viewMode,
        $langcode);
    }

    if ($display->getComponent('links') !== NULL) {
      $controller->addLinksPseudoField(
        $page,
        $entity,
        $display,
        $viewMode,
        $langcode);
    }

    // Add a class to mark the outermost element for the view of the
    // entity. This element contains the pseudo-fields added above,
    // plus all other fields being displayed for the entity. This
    // element also includes, nested within, the toolbar, search box,
    // and embedded view, and all user interface elements.
    //
    // This class is used by the Javascript UI library to
    // find the top-level element containing content for the user interface.
    $page['#attributes']['class'][] = 'foldershare-view';

    return $page;
  }

  /*---------------------------------------------------------------------
   *
   * Page title generation.
   *
   * Routes to view pages for an entity and root lists use a title callback
   * that redirects to these methods.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the title of the indicated entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $foldershare
   *   The entity for which the title is needed.  NOTE: This function is
   *   the target of a route with an entity ID argument. The name of the
   *   function argument here *must be* named after the entity
   *   type: 'foldershare'.
   *
   * @return string
   *   The page title.
   */
  public function getEntityTitle(EntityInterface $foldershare) {
    // The name does not need to be HTML escaped here because the caller
    // handles that.
    return $foldershare->getName();
  }

  /**
   * Returns the title of the "All" root list.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function getAllFolderTitle() {
    return $this->t("All");
  }

  /**
   * Returns the title of the "Home" root list.
   *
   * The title is set to the user's display name.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function getHomeFolderTitle() {
    $displayName = $this->currentUser->getDisplayName();
    if (empty($displayName) === TRUE) {
      $displayName = $this->currentUser->getAccountName();
    }

    $displayName = mb_convert_case($displayName, MB_CASE_TITLE);

    return $this->t(
      "@displayname",
      [
        "@displayname" => $displayName,
      ]);
  }

  /**
   * Returns the title of the "Shared" root list.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function getSharedFolderTitle() {
    return $this->t("Shared");
  }

  /**
   * Returns the title of the "Public" root list.
   *
   * @return \Drupal\Core\StringTranslation\TranslatableMarkup
   *   The page title.
   */
  public function getPublicFolderTitle() {
    return $this->t("Public");
  }

  /*---------------------------------------------------------------------
   *
   * View generation.
   *
   * Routes to view pages for an entity and root lists redirect to these
   * methods.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds and returns a renderable array describing an entity.
   *
   * Each of the fields shown by the selected view mode are added
   * to the renderable array in the proper order.  Pseudo-fields
   * for the contents table and folder path are added as appropriate.
   *
   * @param \Drupal\Core\Entity\EntityInterface $foldershare
   *   The entity being shown.  NOTE: This function is the target of
   *   a route with an entity ID argument. The name of the function
   *   argument here *must be* named after the entity type: 'foldershare'.
   * @param string $viewMode
   *   (optional) The name of the view mode. Defaults to 'full'.
   *
   * @return array
   *   A Drupal renderable array.
   */
  public function viewEntity(EntityInterface $foldershare, $viewMode = 'full') {
    //
    // The parent class sets a few well-known keys:
    // - #entity_type: the entity's type (e.g. "foldershare")
    // - #ENTITY_TYPE: the entity, where ENTITY_TYPE is "foldershare"
    //
    // The parent class invokes the view builder, and both of them
    // set up pre-render callbacks:
    // - #pre_render: callbacks executed at render time
    //
    // The EntityViewController adds a callback to buildTitle() to
    // set the page title.
    //
    // The EntityViewBuilder adds a callback to build() to build
    // the fields for the page.
    //
    // Otherwise, the returned page array has nothing in it.  All
    // of the real work of adding fields is deferred until render
    // time when the builder's build() callback is called.
    if ($foldershare->isSystemHidden() === TRUE) {
      // Hidden items do not exist.
      throw new NotFoundHttpException(
        FolderShare::getStandardHiddenMessage($foldershare->getName()));
    }

    if ($foldershare->isSystemDisabled() === TRUE) {
      // Disabled items cannot be viewed.
      throw new ConflictHttpException(
        FolderShare::getStandardDisabledMessage('viewed', $foldershare->getName()));
    }

    $page = parent::view($foldershare, $viewMode);

    // Add the theme and attach libraries.
    $page['#theme'] = Constants::THEME_FOLDER;

    $page['#attached'] = [
      'library' => [
        'foldershare/foldershare.general',
      ],
    ];

    if ($viewMode !== 'search_index' && $viewMode !== 'search_result') {
      ManageHooks::callHookPostOperation(
        'view',
        [
          $foldershare,
          $this->currentUser->id(),
        ]);
    }

    return $page;
  }

  /**
   * Returns a page listing all root items.
   *
   * The view associated with this page lists all root items owned by
   * anybody, regardless of share grants.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws an exception if the user is not an administrator.
   */
  public function viewAllFolder() {
    // Get the page title.
    $dummyRequest = new Request();

    $route = $this->routeProvider->getRouteByName(
      Constants::ROUTE_ROOT_ITEMS_ALL);
    $title = $this->titleResolver->getTitle($dummyRequest, $route);

    return $this->buildRootList(
      $title,
      Constants::VIEW_LISTS,
      Constants::VIEW_DISPLAY_LIST_ALL,
      FolderShareInterface::ALL_ROOT_LIST);
  }

  /**
   * Returns a page listing root items owned by the user.
   *
   * The view associated with this page lists all root items owned by
   * the current user.
   *
   * If the current user is anonymous, this reverts to listing public root
   * items.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws an exception if the the view display does not allow access.
   */
  public function viewHomeFolder() {
    if ($this->currentUser->isAnonymous() === TRUE) {
      return $this->viewPublicFolder();
    }

    // Get the page title.
    $dummyRequest = new Request();

    $route = $this->routeProvider->getRouteByName(
      Constants::ROUTE_ROOT_ITEMS_PERSONAL);
    $title = $this->titleResolver->getTitle($dummyRequest, $route);

    return $this->buildRootList(
      $title,
      Constants::VIEW_LISTS,
      Constants::VIEW_DISPLAY_LIST_PERSONAL,
      FolderShareInterface::USER_ROOT_LIST);
  }

  /**
   * Returns a page listing root items shared with the user.
   *
   * The view associated with this page lists all root items shared with
   * the current user.
   *
   * If the current user is anonymous, this reverts to listing public root
   * items.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws an exception if the the view display does not allow access.
   */
  public function viewSharedFolder() {
    if ($this->currentUser->isAnonymous() === TRUE) {
      return $this->viewPublicFolder();
    }

    // Get the page title.
    $dummyRequest = new Request();

    $route = $this->routeProvider->getRouteByName(
      Constants::ROUTE_ROOT_ITEMS_SHARED);
    $title = $this->titleResolver->getTitle($dummyRequest, $route);

    return $this->buildRootList(
      $title,
      Constants::VIEW_LISTS,
      Constants::VIEW_DISPLAY_LIST_SHARED,
      FolderShareInterface::SHARED_ROOT_LIST);
  }

  /**
   * Returns a page listing root items owned by/shared with anonymous.
   *
   * The view associated with this page lists all root items
   * owned by or shared with anonymous.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws an exception if the the view display does not allow access.
   */
  public function viewPublicFolder() {

    // Get the page title.
    $dummyRequest = new Request();

    $route = $this->routeProvider->getRouteByName(
      Constants::ROUTE_ROOT_ITEMS_PUBLIC);
    $title = $this->titleResolver->getTitle($dummyRequest, $route);

    return $this->buildRootList(
      $title,
      Constants::VIEW_LISTS,
      Constants::VIEW_DISPLAY_LIST_PUBLIC,
      FolderShareInterface::PUBLIC_ROOT_LIST);
  }

  /**
   * Builds and returns a renderable array describing a view page.
   *
   * Arguments name the view and display to use. If the view or display
   * do not exist, a 'misconfigured website' error message is logged and
   * the user is given a generic error message. If the display does not
   * allow access, an access denied exception is thrown.
   *
   * Otherwise, a page is generated that includes a user interface above
   * an embed of the named view and display.
   *
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   *   The page title.
   * @param string $viewName
   *   The name of the view to embed in the page.
   * @param string $displayName
   *   The name of the view display to embed in the page.
   * @param int $rootListId
   *   The root list ID.
   *
   * @return array
   *   A renderable array.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws an exception if the named display's access controls
   *   do not allow access.
   */
  private function buildRootList(
    TranslatableMarkup $title,
    string $viewName,
    string $displayName,
    int $rootListId) {

    $enableCommandMenu = Settings::getUserInterfaceCommandMenuEnable() &&
      ManageHooks::callHookCommandMenuEnable(NULL, (-1));

    $enableAncestorMenu = Settings::getUserInterfaceAncestorMenuEnable() &&
      ManageHooks::callHookAncestorMenuEnable(NULL, (-1));

    $enableSearchBox = Settings::getUserInterfaceSearchBoxEnable() &&
      ManageHooks::callHookSearchBoxEnable(NULL, (-1));

    $enableSidebar = Settings::getUserInterfaceSidebarEnable() &&
      ManageHooks::callHookSidebarEnable(NULL, (-1));

    $enableSidebarButton = Settings::getUserInterfaceSidebarButtonEnable() &&
      ManageHooks::callHookSidebarButtonEnable(NULL, (-1));

    $page = [];
    $this->addFolderBrowser(
      $page,
      $title,
      $viewName,
      $displayName,
      0,
      $enableCommandMenu,
      $enableAncestorMenu,
      $enableSearchBox,
      $enableSidebar,
      $enableSidebarButton,
      NULL,
      $rootListId);

    return $page;
  }

  /*---------------------------------------------------------------------
   *
   * Utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the view executable for the give view and display.
   *
   * @param string $viewName
   *   The machine name of the view.
   * @param string $displayName
   *   The machine name of the display in the view.
   * @param bool $forDialog
   *   (optional, default = FALSE) When TRUE, the view is checked to see
   *   that it uses the dialog-specific field formatter for the name field.
   *   When FALSE, the non-dialog field formatter is required.
   *
   * @return \Drupal\views\ViewExecutable|null
   *   Returns the view executable.
   *
   * @throws \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
   *   Throws an exception if the named display's access controls
   *   do not allow access.
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception on failure, setting the message to explain the
   *   problem. Typically, the exception message should be logged as critical
   *   by the caller.
   */
  public static function getViewExecutable(
    string $viewName,
    string $displayName,
    bool $forDialog = FALSE) {

    // Load the view.
    $view = View::load($viewName);
    if ($view === NULL) {
      // Site admin-facing exception message. No translation.
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The required '%viewName' view is missing.\nPlease check the Views module configuration and, if needed, restore the view using the module's configuration page.",
        [
          '%viewName'   => $viewName,
        ]));
    }

    // Get the view executable.
    $viewExecutable = $view->getExecutable();
    if ($view === NULL) {
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The required '%displayName' display for the '%viewName' view is missing.\nPlease check the Views module configuration and, if needed, restore the view using the module's configuration page.",
        [
          '%viewName'    => $viewName,
          '%displayName' => $displayName,
        ]));
    }

    // Confirm access.
    if ($viewExecutable->access($displayName) === FALSE) {
      // User does not have access. Access denied.
      // User-facing exception message.
      $message = t("You do not have permission to view this content.");
      throw new AccessDeniedHttpException($message);
    }

    // Set the display.
    if ($viewExecutable->setDisplay($displayName) === FALSE) {
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The required '%displayName' display for the '%viewName' view is missing.\nPlease check the Views module configuration and, if needed, restore the view using the module's configuration page.",
        [
          '%viewName'    => $viewName,
          '%displayName' => $displayName,
        ]));
    }

    // Confirm AJAX is enabled.
    if ($viewExecutable->getDisplay()->ajaxEnabled() === FALSE) {
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The '%displayName' display of the '%viewName' view does not have AJAX enabled.\nThe folder browser user interface will not function without AJAX. Please check the Views module configuration and enable AJAX for all displays or restore the view using the module's configuration page.",
        [
          '%viewName'    => $viewName,
          '%displayName' => $displayName,
        ]));
    }

    // Get display configuration.
    $error = FALSE;
    try {
      $displayConfig = &$view->getDisplay($displayName);
      if ($displayConfig === NULL) {
        $error = TRUE;
      }
    }
    catch (\Exception $e) {
      $error = TRUE;
    }
    try {
      $defaultDisplayConfig = &$view->getDisplay('default');
      if ($defaultDisplayConfig === NULL) {
        $error = TRUE;
      }
    }
    catch (\Exception $e) {
      $error = TRUE;
    }

    if ($error === TRUE) {
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The required '%displayName' display for the '%viewName' view is missing.\nPlease check the Views module configuration and, if needed, restore the view using the module's configuration page.",
        [
          '%viewName'    => $viewName,
          '%displayName' => $displayName,
        ]));
    }

    // Confirm the display configuration has a 'name' field, since that
    // field is used to host descriptive information about an entity that
    // is needed by the front-end.
    $nameField = NULL;
    if (isset($defaultDisplayConfig['display_options']['fields']['name']) === TRUE) {
      $nameField = &$defaultDisplayConfig['display_options']['fields']['name'];
    }
    if (isset($displayConfig['display_options']['fields']['name']) === TRUE) {
      $nameField = &$displayConfig['display_options']['fields']['name'];
    }

    if ($nameField === NULL) {
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The '%fieldName' field is missing in the '%displayName' display of the '%viewName' view.\nThe field MUST be included and use the module's '%formatterName' field formatter. This formatter adds essential data to the name column that is needed by the user interface. Please check the Views module configuration to include the '%fieldName' field, or restore the view using the module's configuration page.",
        [
          '%fieldName'     => 'name',
          '%viewName'      => $viewName,
          '%displayName'   => $displayName,
          '%formatterName' => Constants::INTERNAL_NAME_FORMATTER,
        ]));
    }

    // And make sure the internal field formatter is used for the 'name' field.
    $requiredFormatter = Constants::INTERNAL_NAME_FORMATTER;
    if ($forDialog === TRUE) {
      $requiredFormatter = Constants::INTERNAL_FOLDER_NAME_FORMATTER;
    }

    if ($nameField['type'] !== $requiredFormatter) {
      throw new ValidationException(new FormattableMarkup(
        "Misconfigured website: The '%fieldName' field does not use the required field formatter on the '%displayName' display of the '%viewName' view.\nThe field MUST use the module's '%formatterName' field formatter. This formatter adds essential data to the name column that is needed by the user interface. Please check the Views module configuration to set the '%fieldName' t the correct field formatter or restore the view the module's configuration page.",
        [
          '%fieldName'     => 'name',
          '%viewName'      => $viewName,
          '%displayName'   => $displayName,
          '%formatterName' => $requiredFormatter,
        ]));
    }

    return $viewExecutable;
  }

  /*---------------------------------------------------------------------
   *
   * Pseudo-field implementations.
   *
   *---------------------------------------------------------------------*/
  /**
   * Adds an ancestor path of folder links to the page array.
   *
   * Several well-defined view modes are recognized:
   *
   * - 'full' = generate a full view
   * - 'search_index' = generate a keywords-only view
   * - 'search_result' = generate an abbreviated search result view
   *
   * For the 'search_index' and 'search_result' view modes, this function
   * returns immediately without adding anything to the build. The folder
   * path has no place in a search index or in search results.
   *
   * For the 'full' view mode, and all other view modes, this function
   * returns the folder path.
   *
   * Folder names on a path are always separated by '/', per the convention
   * for web URLs, and for Linux and macOS directory paths.  There
   * is no configuration setting to change this path presentation
   * to use a '\' per Windows convention.
   *
   * If the folder parameter is NULL, a simplified path with a single
   * link to the root list is added to the build.
   *
   * @param array $page
   *   A render array into which we insert our element.
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional) The item for which we generate a path render element.
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $display
   *   The field_ui display to use.
   * @param string $viewMode
   *   (optional) The field_ui view mode to use.
   * @param string $langcode
   *   (optional) The language code to use.
   */
  private function addPathPseudoField(
    array &$page,
    FolderShareInterface $item = NULL,
    EntityViewDisplay $display = NULL,
    string $viewMode = 'full',
    string $langcode = '') {
    //
    // For search view modes, do nothing. The folder path has no place
    // in a search index or in search results.
    if ($viewMode === 'search_index' || $viewMode === 'search_result') {
      return;
    }

    // Get the weight for this component.
    $component = $display->getComponent('folder_path');
    if (isset($component['weight']) === TRUE) {
      $weight = $component['weight'];
    }
    else {
      $weight = 0;
    }

    // Link to one of the root list pages.
    //
    // Several cases:
    // - The root is owned by the current user.
    //   - If the current user is anonymous, use PUBLIC_ROOT_LIST.
    //   - Otherwise use USER_ROOT_LIST.
    //
    // - The root is owned by someone else.
    //   - If the current user is an admin, use ALL_ROOT_LIST.
    //   - If the root is shared with anonymous, use PUBLIC_ROOT_LIST.
    //   - Otherwise use USER_ROOT_LIST.
    $rootItem = $item->getRootItem();
    $rootOwner = $rootItem->getOwner();

    if ((int) $this->currentUser->id() === (int) $rootOwner->id()) {
      if ($this->currentUser->isAnonymous() === TRUE) {
        $routeName = Constants::ROUTE_ROOT_ITEMS_PUBLIC;
      }
      else {
        $routeName = Constants::ROUTE_ROOT_ITEMS_PERSONAL;
      }
    }
    elseif ($this->currentUser->hasPermission(Constants::ADMINISTER_PERMISSION) === TRUE) {
      $routeName = Constants::ROUTE_ROOT_ITEMS_ALL;
    }
    elseif ($this->currentUser->isAnonymous() === TRUE) {
      $routeName = Constants::ROUTE_ROOT_ITEMS_PUBLIC;
    }
    elseif ($rootItem->isAccessPublic() === TRUE) {
      $routeName = Constants::ROUTE_ROOT_ITEMS_PUBLIC;
    }
    else {
      $routeName = Constants::ROUTE_ROOT_ITEMS_PERSONAL;
    }

    $path = LinkUtilities::createRouteLink($routeName);

    // When there is no entity, the path only includes the top-level
    // root list. When there is an entity, and it is a root, then again
    // the path only includes the top-level root list because the
    // page itself provides the root item. Otherwise, fill in with
    // children.
    if ($item !== NULL) {
      // Show a chain of links starting with the root list, then root folder,
      // then ancestors down to the item itself.
      //
      // Get ancestors of this item. The list does not include this item.
      // The first entry in the list is the root.
      $folders = [];
      if ($item->isRootItem() === FALSE) {
        $folders = $item->findAncestorFolders();
      }
      $folders[$item->id()] = $item;

      // Loop through the folders, starting at the root. For each one,
      // create a link to the folder's page.
      $links = [];
      foreach ($folders as $f) {
        if ($item->id() === $f->id() ||
            $f->access('view', $this->currentUser) === FALSE) {
          // The item in the list is the current folder, or the user doesn't
          // have view access.  Just show the item as plain text, not a link.
          //
          // The folder name needs to be escaped manually here.
          $links[] = Html::escape($item->getName());
        }
        else {
          // Create a link to the folder.
          //
          // No need to HTML escape the folder name here. This is done
          // automatically by Link.
          $links[] = Link::createFromRoute(
            $f->getName(),
            Constants::ROUTE_FOLDERSHARE,
            [Constants::ROUTE_FOLDERSHARE_ID => $f->id()])->toString();
        }
      }

      // Create the markup.
      $path = $path . ' / ' . implode(' / ', $links);
    }

    // Add the path to the page.
    //
    // No need to add cache context or tags since the page this is included
    // on is always marked as not cacheable.
    $name = 'foldershare-folder-path';
    $page[$name] = [
      '#name'   => $name,
      '#type'   => 'item',
      '#prefix' => '<div class="' . $name . '">',
      '#markup' => $path,
      '#suffix' => '</div>',
      '#weight' => $weight,
    ];
  }

  /**
   * Adds a links field.
   *
   * @param array $page
   *   A render array into which we insert our element.
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional) The item for which we generate a path render element.
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $display
   *   The field_ui display to use.
   * @param string $viewMode
   *   (optional) The field_ui view mode to use.
   * @param string $langcode
   *   (optional) The language code in which the entity is being viewed.
   *
   * @see ::renderLinks()
   * @see ::trustedCallbacks()
   */
  private function addLinksPseudoField(
    array &$page,
    FolderShareInterface $item = NULL,
    EntityViewDisplay $display = NULL,
    string $viewMode = 'full',
    string $langcode = '') {

    //
    // Validate.
    // ---------
    // For search view modes, do nothing. The links have no usefulness
    // in a search index or in search results.
    if ($viewMode === 'search_index' || $viewMode === 'search_result') {
      return;
    }

    // Get the weight for this component.
    $component = $display->getComponent('links');
    if (isset($component['weight']) === TRUE) {
      $weight = $component['weight'];
    }
    else {
      $weight = 0;
    }

    // Add the links later.
    $page['links'] = [
      '#lazy_builder' => [
        get_called_class() . '::renderLinks',
        [
          $item->id(),
          $viewMode,
          $langcode,
        ],
      ],
      '#weight' => $weight,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    // Add the 'renderLinks' callback below used to build entity links.
    $callbacks = parent::trustedCallbacks();
    $callbacks[] = 'renderLinks';
    return $callbacks;
  }

  /**
   * Renders links as a #lazy_builder callback.
   *
   * @param string $id
   *   The entity id.
   * @param string $viewMode
   *   The field_ui view mode.
   * @param string $langcode
   *   The language code in which the entity is being viewed.
   *
   * @see ::addLinksPseudoField()
   * @see ::trustedCallbacks()
   */
  public static function renderLinks(
    string $id,
    string $viewMode,
    string $langcode) {

    // Set up an empty container for links.
    $links = [
      '#theme'      => 'links__foldershare',
      '#pre_render' => [[RenderLink::class, 'preRenderLinks']],
      '#attributes' => [
        'class'     => [
          'links',
          'inline',
        ],
      ],
    ];

    // Load the entity.
    $item = FolderShare::load($id);

    $context = [
      'view_mode' => $viewMode,
      'langcode'  => $langcode,
    ];

    // Add default links.
    if (\Drupal::moduleHandler()->moduleExists('comment') === TRUE &&
        \Drupal::hasService('comment.link_builder') === TRUE) {
      $builder = \Drupal::service('comment.link_builder');
      $links += $builder->buildCommentedEntityLinks($item, $context);
    }

    // Allow modules to add their own links.
    \Drupal::moduleHandler()->alter(
      'foldershare_links',
      $links,
      $item,
      $context);

    return $links;
  }

  /**
   * Adds a sharing status field.
   *
   * @param array $page
   *   A render array into which we insert our element.
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional) The item for which we generate a path render element.
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $display
   *   The field_ui display to use.
   * @param string $viewMode
   *   (optional) The field_ui view mode to use.
   * @param string $langcode
   *   (optional) The language code to use.
   */
  private function addSharingPseudoField(
    array &$page,
    FolderShareInterface $item = NULL,
    EntityViewDisplay $display = NULL,
    string $viewMode = 'full',
    string $langcode = '') {

    //
    // Validate.
    // ---------
    // For search view modes, do nothing. The sharing status is a fake
    // field that has no usefulness in a search index or in search results.
    if ($viewMode === 'search_index' || $viewMode === 'search_result') {
      return;
    }

    //
    // Prepare.
    // --------
    // Get the weight for this component.
    $component = $display->getComponent('sharing_status');
    if (isset($component['weight']) === TRUE) {
      $weight = $component['weight'];
    }
    else {
      $weight = 0;
    }

    // Get assorted values.
    $root          = $item->getRootItem();
    $owner         = $item->getOwner();
    $ownerId       = (int) $owner->id();
    $rootOwner     = $root->getOwner();
    $rootOwnerId   = (int) $rootOwner->id();
    $currentUserId = (int) $this->currentUser->id();
    $anonymousId   = (int) User::getAnonymousUser()->id();

    $sharingMessage = NULL;

    // Create a sharing message based upon access grants and ownership.
    if ($item->isSystemHidden() === TRUE) {
      // Should not be possible. Access grants should have blocked this.
      // Only an admin should see this.
      $sharingMessage = $this->t('Hidden by the system');
    }
    elseif ($item->isSystemDisabled() === TRUE) {
      // Should not be possible. Access grants should have blocked this.
      // Only an admin should see this.
      $sharingMessage = $this->t('Disabled by the system');
    }
    elseif ($rootOwner->isAnonymous() === TRUE) {
      // Owned by anonymous. Shared with everyone.
      $sharingMessage = $this->t('Public content shared with everyone');
    }
    else {
      // The specifics depend upon the root's access grants.
      $uids = array_merge(
        $root->getAccessGrantViewUserIds(),
        $root->getAccessGrantAuthorUserIds());

      $sharedWithSomeone = FALSE;
      foreach ($uids as $uid) {
        // Ignore grant entries for:
        // - The owner (who always has access).
        // - The site administrator (who normally isn't even listed).
        if ($uid === $rootOwnerId || $uid === 1) {
          continue;
        }

        if ($uid === $anonymousId) {
          // Shared with anonymous (everyone) by the root owner.
          if ($rootOwnerId === $currentUserId) {
            // The root owner is the current user.
            $sharingMessage = $this->t('Public content shared with everyone by you');
          }
          else {
            // The root owner is not the current user.
            $sharingMessage = $this->t(
              'Public content shared with everyone by @displayName',
              [
                '@displayName' => $rootOwner->getDisplayName(),
              ]);
          }

          break;
        }

        if ($uid === (int) $currentUserId) {
          // Shared with the current user by the root owner.
          $sharingMessage = $this->t(
            'Shared with you by @displayName',
            [
              '@displayName' => $rootOwner->getDisplayName(),
            ]);
          break;
        }

        $sharedWithSomeone = TRUE;
      }

      if ($sharingMessage === NULL) {
        // Since no sharing message has been created yet, we know:
        // - The item is not owned by anonymous (and thus public).
        // - The item is not shared with anonymous (and thus public).
        // - The item is not shared with the current user by someone else.
        //
        // Since we are a view controller for the entity, access controls
        // have been checked and the current user has been granted access.
        // This means they are one of:
        // - The owner of the root (and thus always have access).
        // - A user with admin access.
        if ($sharedWithSomeone === FALSE) {
          if ($ownerId === $currentUserId) {
            // The item is owned by the current user.
            $sharingMessage = $this->t('Private content owned by you');
          }
          else {
            // The root is owned by someone else. Since the current user
            // is viewing it and yet they have not been granted access, the
            // current user is an admin.
            $sharingMessage = $this->t(
              'Private content owned by @displayName',
              [
                // Show the item owner's name, not the root owner's name.
                '@displayName' => $owner->getDisplayName(),
              ]);
          }
        }
        else {
          // Shared with someone, but not the current user.
          if ($ownerId === $currentUserId) {
            // The item is owned by the current user.
            $sharingMessage = $this->t('Shared content owned by you');
          }
          else {
            // The root is owned by someone else. Since the current user
            // is viewing it and yet they have not been granted access, the
            // current user is an admin.
            $sharingMessage = $this->t(
              'Shared content owned by @displayName',
              [
                // Show the item owner's name, not the root owner's name.
                '@displayName' => $owner->getDisplayName(),
              ]);
          }
        }
      }
    }

    $name = 'foldershare-sharing-status';
    $page[$name] = [
      '#name'   => $name,
      '#type'   => 'container',
      '#attributes' => [
        'class' => [
          $name,
          'field',
        ],
      ],
      'item'  => [
        '#type'   => 'html_tag',
        '#tag'    => 'div',
        '#attributes' => [
          'class' => ['field__item'],
        ],
        '#value' => $sharingMessage,
      ],
      '#weight' => $weight,
    ];
  }

  /**
   * Adds to the page a table showing a list of the item's children.
   *
   * Several well-defined view modes are recognized:
   *
   * - 'full' = generate a full view
   * - 'search_index' = generate a keywords-only view
   * - 'search_result' = generate an abbreviated search result view
   *
   * For the 'search_index' and 'search_result' view modes, this function
   * returns immediately without adding anything to the build. The contents
   * view has no place in a search index or in search results.
   *
   * For the 'full' view mode, and all other view modes, this function
   * returns the contents view.
   *
   * @param array $page
   *   A render array into which we insert our element.
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional) The item for which we generate a contents render element.
   * @param \Drupal\Core\Entity\Entity\EntityViewDisplay $display
   *   The field_ui display to use.
   * @param string $viewMode
   *   (optional) The field_ui view mode to use.
   * @param string $langcode
   *   (optional) The language code to use.
   */
  private function addViewPseudoField(
    array &$page,
    FolderShareInterface $item,
    EntityViewDisplay $display = NULL,
    string $viewMode = 'full',
    string $langcode = '') {

    //
    // Setup
    // -----
    // For search view modes, do nothing. The contents view has
    // no place in a search index or in search results.
    if ($viewMode === 'search_index' || $viewMode === 'search_result') {
      return;
    }

    // Get the weight for this component.
    $component = $display->getComponent('folder_table');
    $weight = 0;
    if (isset($component['weight']) === TRUE) {
      $weight = $component['weight'];
    }

    $enableCommandMenu = Settings::getUserInterfaceCommandMenuEnable() &&
      ManageHooks::callHookCommandMenuEnable($item, (-1));

    $enableAncestorMenu = Settings::getUserInterfaceAncestorMenuEnable() &&
      ManageHooks::callHookAncestorMenuEnable($item, (-1));

    $enableSearchBox = $item->isFolder() === TRUE &&
      Settings::getUserInterfaceSearchBoxEnable() &&
      ManageHooks::callHookSearchBoxEnable($item, (-1));

    $enableSidebar = Settings::getUserInterfaceSidebarEnable() &&
      ManageHooks::callHookSidebarEnable(NULL, (-1));

    $enableSidebarButton = Settings::getUserInterfaceSidebarButtonEnable() &&
      ManageHooks::callHookSidebarButtonEnable(NULL, (-1));

    $this->addFolderBrowser(
      $page,
      NULL,
      Constants::VIEW_LISTS,
      Constants::VIEW_DISPLAY_LIST_FOLDER,
      $weight,
      $enableCommandMenu,
      $enableAncestorMenu,
      $enableSearchBox,
      $enableSidebar,
      $enableSidebarButton,
      $item);
  }

  /**
   * Adds a folder browser view to the given render array.
   *
   * Boolean arguments control the visibility of toolbar items:
   * - The sidebar show/hide button.
   * - The command menu.
   * - The ancestor menu.
   * - The search box.
   *
   * An additional boolean controls the visibility of the sidebar.
   *
   * @param array $page
   *   A render array into which we insert our element.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup $title
   *   The page title.
   * @param string $viewName
   *   The machine name of the view.
   * @param string $displayName
   *   The machine name of the display.
   * @param int $weight
   *   The render weight of the folder table view.
   * @param bool $enableCommandMenu
   *   Whether to include the command menus.
   * @param bool $enableAncestorMenu
   *   Whether to include the ancestor menu.
   * @param bool $enableSearchBox
   *   Whether to include the search box.
   * @param bool $enableSidebar
   *   Whether to include the sidebar.
   * @param bool $enableSidebarButton
   *   Whether to include the sidebar show/hide button.
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL) The item for which to generate a folder
   *   view. When NULL, the view is expected to be for a root list.
   * @param int $rootListId
   *   (optional, default = USER_ROOT_LIST) The root list for which to
   *   generate a folder view.
   */
  private function addFolderBrowser(
    array &$page,
    TranslatableMarkup $title = NULL,
    string $viewName,
    string $displayName,
    int $weight,
    bool $enableCommandMenu,
    bool $enableAncestorMenu,
    bool $enableSearchBox,
    bool $enableSidebar,
    bool $enableSidebarButton,
    FolderShareInterface $item = NULL,
    int $rootListId = FolderShareInterface::USER_ROOT_LIST) {

    //
    // View setup
    // ----------
    // Find the embedded view and display, confirming that both exist and
    // that the user has access. Log errors if something is wrong.
    $error = FALSE;
    $pageName = 'foldershare-folder-browser';

    if ($title !== NULL) {
      $page['#title'] = $title;
    }

    if ($item === NULL) {
      $page['#theme'] = Constants::THEME_VIEW;
    }

    $page['#attached']['library'][] = 'foldershare/foldershare.general';

    $enableFolderBrowser = TRUE;
    if ($item !== NULL && $item->isFolder() === FALSE) {
      $enableFolderBrowser = FALSE;
    }

    if ($enableFolderBrowser === TRUE) {
      try {
        // The returned view executable is not needed here because we
        // use the view and display names below in a view field.
        self::getViewExecutable($viewName, $displayName);
      }
      catch (ValidationException $e) {
        // All validation exceptions are serious problems.
        ManageLog::critical($e->getMessage());
        $error = TRUE;
      }
      catch (AccessDeniedHttpException $e) {
        throw $e;
      }

      // If the view could not be found, there is nothing to embed and there
      // is no point in adding a UI. Return an error message in place of the
      // view's content.
      if ($error === TRUE) {
        $page[$pageName] = $this->buildErrorMessage($weight);
        return;
      }
    }

    //
    // Prefix
    // ------
    // If the item is NOT a folder, create a prefix and suffix that marks
    // the view accordingly. CSS can then hide irrelevant parts of the UI.
    //
    // If the item is NOT a folder, then there's no point in having a
    // folder tree search box either.
    if ($item !== NULL && $item->isFolder() === FALSE) {
      $viewPrefix = '<div class="foldershare-nonfolder-browser">';
      $viewSuffix = '</div';
    }
    else {
      $viewPrefix = $viewSuffix = '';
    }

    //
    // Build it.
    // ---------
    // The render array is built to have:
    // - No-script message if Javascript is disabled.
    // - Toolbar.
    //   - Command menus.
    //   - Ancestor menu.
    //   - Search box.
    // - Sidebar.
    // - Folder table view.
    //
    $page[$pageName] = [
      '#weight'         => $weight,
      '#prefix'         => $viewPrefix,
      '#suffix'         => $viewSuffix,
      '#attributes'     => [
        'class'         => [
          'foldershare-view',
          'foldershare-view-page',
        ],
      ],

      // Do not cache this page. If anybody adds or removes a folder or
      // changes sharing, the view will change and the page needs to
      // be regenerated.
      '#cache'          => [
        'max-age'       => 0,
      ],

      'noscript'        => $this->buildNoScriptMessage(),

      'toolbar-and-folder-browser' => [
        '#type'         => 'container',
        '#attributes'   => [
          'class'       => [
            'foldershare-toolbar-and-folder-browser',
          ],
        ],

        'toolbar'       => [
          '#type'       => 'container',
          '#attributes' => [
            'class'     => [
              'foldershare-toolbar',
            ],
          ],

          // If enabled, include sidebar show/hide button.
          'sidebarbutton' => ($enableFolderBrowser === TRUE && $enableSidebarButton === TRUE) ?
            $this->buildSidebarButton() : NULL,

          // If enabled, include the main command menu form.
          'folderbrowsermenu' => ($enableCommandMenu === TRUE) ?
            $this->buildCommandMenuForm($item, $rootListId) : NULL,

          // If enabled, include the ancestor menu.
          'ancestormenu' => ($enableAncestorMenu === TRUE) ?
            $this->buildAncestorMenuButton($item) : NULL,

          // If enabled, include the search form.
          'searchbox' => ($enableSearchBox === TRUE) ?
            $this->buildSearchForm($item) : NULL,
        ],

        'sidebar-and-folder-browser' => ($enableFolderBrowser === TRUE) ?
          $this->buildFolderBrowser(
            $viewName,
            $displayName,
            $enableSidebar,
            $item,
            $rootListId) : NULL,
      ],
    ];
  }

  /**
   * Builds and returns a standard build problem error message.
   *
   * A standard message is included that there was a problem embedding the
   * folder browser.
   *
   * @param int $weight
   *   The render weight of the folder table view.
   */
  private function buildErrorMessage(int $weight) {
    return [
      '#attributes' => [
        'class'   => [
          'foldershare-error',
        ],
      ],

      // Do not cache this page. If any of the above conditions change,
      // the page needs to be regenerated.
      '#cache' => [
        'max-age' => 0,
      ],

      '#weight'   => $weight,

      'error'     => [
        '#type'   => 'item',
        '#markup' => $this->t(
          "The website has encountered an administrator configuration problem with this page.\nPlease report this to the website administrator."),
      ],
    ];
  }

  /**
   * Builds and returns a standard no-script error message.
   *
   * A standard no-script message is included that the folder browser
   * cannot work without Javascript.
   */
  private function buildNoScriptMessage() {
    return [
      '#type'         => 'html_tag',
      '#tag'          => 'noscript',
      '#value'        => $this->t('Alert: Your browser does not have Javascript enabled. The folder browser user interface will not work properly without it.'),
      '#attributes'   => [
        'class'       => [
          'foldershare-noscript',
          'color-error',
        ],
      ],
    ];
  }

  /**
   * Builds and returns the sidebar show/hide button.
   *
   * The button is used to show or hide the sidebar. Javascript adds the
   * behavior and cookie management to save/restore the user's preference.
   *
   * @return array
   *   Returns a render array containing the button.
   */
  private function buildSidebarButton() {
    // The "button" form element creates a button with <input>, which
    // requires text for the button's label. Since we want an image,
    // we have to provide explicit <button> HTML and an image tag.
    //
    // To insure the button gets styled like a button, include a variety
    // of classes used by Drupal core, jQuery UI, and popular classes of
    // themes to mark the button as a button.
    //
    // Create the <img> HTML.
    $sidebarButtonUrl   = '/' . \Drupal::service('extension.list.module')->getPath('foldershare') .
      '/images/button-sidebar.svg';
    $sidebarButtonAlt   = $this->t('Show/hide');
    $sidebarButtonImage = "<img alt=\"$sidebarButtonAlt\" src=\"$sidebarButtonUrl\">";
    $sidebarButtonClass = 'foldershare-sidebar-button';

    // Create the render array using an explicit <button>.
    return [
      '#type'            => 'html_tag',
      '#tag'             => 'button',
      '#value'           => $sidebarButtonImage,
      '#name'            => $sidebarButtonClass,
      '#attributes'      => [
        'title'          => $this->t('Show/hide the sidebar'),
        'class'          => [
          $sidebarButtonClass,
          // Core classes.
          'button',
          'button--image-button',
          'js-form-submit',
          'form-submit',
          // Bootstrap classes.
          'btn',
          // W3.CSS classes.
          'w3-button',
          'w3-border',
          'w3-theme-border',
          // jQuery.UI. With some themes, these get lost when set by
          // jQuery using button(). Why?
          'ui-button',
          'ui-corner-all',
          'ui-widget',
        ],
      ],
    ];
  }

  /**
   * Builds and returns the command menu form.
   *
   * The bulk of the menu command form is hidden except for a menu button.
   * Javascript creates and attaches a menu to the button, tracks selections
   * in the folder browser, sets up command arguments in the form, and
   * triggers the form to issue a command to the server. Typical commands
   * delete, copy, move, and rename. Commands are all plugins.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL) The current item.
   * @param int $rootListId
   *   (optional, default = USER_ROOT_LIST) The root list parent for
   *   menu commands. This is only used if there is no parent item.
   *
   * @return array
   *   Returns a render array containing the command menu form.
   */
  private function buildCommandMenuForm(
    FolderShareInterface $item = NULL,
    int $rootListId = FolderShareInterface::USER_ROOT_LIST) {

    if ($item !== NULL) {
      return $this->formBuilder->getForm(
        UIFolderBrowserMenu::class,
        $item->id());
    }
    return $this->formBuilder->getForm(
      UIFolderBrowserMenu::class,
      $rootListId);
  }

  /**
   * Builds and returns the ancestor menu and button.
   *
   * The bulk of the ancestor menu code is hidden except for a menu button.
   * Javascript creates and attaches a menu of ancestor links to the button
   * and monitors those links to trigger a page load for a chosen link.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL) The current item.
   *
   * @return array
   *   Returns a render array containing the button.
   */
  private function buildAncestorMenuButton(FolderShareInterface $item = NULL) {
    if ($item !== NULL) {
      return UIAncestorMenu::build($item->id());
    }
    return UIAncestorMenu::build();
  }

  /**
   * Builds and returns the search form.
   *
   * The search form contains a search field and submit button that are
   * typically shown on the folder browser toolbar. The search uses the
   * current FolderShare item, if any, as context from which to start
   * a search.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL) The current item.
   *
   * @return array
   *   Returns a render array containing the search box form.
   */
  private function buildSearchForm(FolderShareInterface $item = NULL) {
    if ($item !== NULL) {
      return $this->formBuilder->getForm(UISearchBox::class, $item->id());
    }

    return $this->formBuilder->getForm(UISearchBox::class);
  }

  /**
   * Builds and returns the sidebar link block.
   *
   * The sidebar link block contains a pair of blocks:
   * - Favorites links.
   * - Additional links.
   *
   * Favorites links are defined here and contain well-known links:
   * - Home (authenticated only).
   * - Shared (authenticated only).
   * - Public (only if public access allowed).
   * - All (admins only).
   *
   * Additional links are defined by hooks. If no hooks provide links,
   * the block is not included.
   *
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL) The current item.
   *
   * @return array
   *   Returns a render array containing the sidebar link block.
   */
  private function buildSidebarLinkBlock(FolderShareInterface $item = NULL) {
    $isAnonymous = $this->currentUser->isAnonymous();

    // Create the links container.
    $sidebarLinkBlock = [
      '#type' => 'container',
      '#attributes' => [
        'class' => [
          'foldershare-sidebar-link-block',
        ],
      ],

      'sidebar-link-block-title' => [
        '#type'  => 'html_tag',
        '#tag'   => 'h3',
        '#value' => $this->t('Favorites'),
        '#attributes' => [
          'class' => [
            'foldershare-sidebar-link-block-title',
          ],
        ],
      ],

      'sidebar-link-block-favorites-links' => [
        '#type'   => 'html_tag',
        '#tag'    => 'ul',
        '#attributes' => [
          'class' => [
            'foldershare-sidebar-favorites-links',
          ],
        ],
      ],

      'sidebar-link-block-additional-links' => [
        '#type'   => 'html_tag',
        '#tag'    => 'ul',
        '#attributes' => [
          'class' => [
            'foldershare-sidebar-additional-links',
          ],
        ],
      ],
    ];

    // Make a list of favorites links. These are IN PREFERRED ORDER,
    // which is not necessarily alphabetic.
    $linkNames = [
      "home"   => $this->getHomeFolderTitle()->render(),
      "shared" => $this->getSharedFolderTitle()->render(),
      "public" => $this->getPublicFolderTitle()->render(),
      "all"    => $this->getAllFolderTitle()->render(),
    ];

    // Loop through the link names and add a link for each one, if
    // appropriate.
    $sidebarLinkWeight = 0;
    foreach ($linkNames as $linkKey => $linkValue) {
      $permission = FALSE;
      $route = '';

      switch ($linkKey) {
        case 'all':
          $permission = ($isAnonymous === FALSE) &&
            (FolderShareAccessControlHandler::canView(
              FolderShareInterface::ALL_ROOT_LIST) === TRUE);
          $route = 'entity.foldershare.rootitems.all';
          break;

        case 'home':
          $permission = ($isAnonymous === FALSE) &&
            (FolderShareAccessControlHandler::canView(
              FolderShareInterface::USER_ROOT_LIST) === TRUE);
          $route = 'entity.foldershare.rootitems';
          break;

        case 'public':
          $permission = FolderShareAccessControlHandler::canView(
              FolderShareInterface::PUBLIC_ROOT_LIST) === TRUE;
          $route = 'entity.foldershare.rootitems.public';
          break;

        case 'shared':
          $permission = ($isAnonymous === FALSE) &&
            (FolderShareAccessControlHandler::canView(
              FolderShareInterface::SHARED_ROOT_LIST) === TRUE);
          $route = 'entity.foldershare.rootitems.shared';
          break;

        default:
          break;
      }

      if ($permission === TRUE) {
        $sidebarLinkBlock['sidebar-link-block-favorites-links'][$linkKey . '-folder-link'] = [
          '#prefix'     => '<li>',
          '#sufix'      => '</li>',
          '#type'       => 'link',
          '#title'      => $linkValue,
          '#url'        => Url::fromRoute($route),
          '#weight'     => $sidebarLinkWeight++,
          '#attributes' => [
            'class' => [
              'foldershare-sidebar-link',
              'foldershare-sidebar-favorites-link',
              'foldershare-sidebar-' . $linkKey . '-folder-link',
              'file',
              'file--mime-' . $linkKey . '-directory',
              'file--mime-rootfolder-group-directory',
            ],
          ],
        ];
      }
    }

    // Hooks may return a list of additional links. If any, these are
    // added after the favorites.
    $additionalLinks = ManageHooks::callHookSidebarLinks($item);
    if (empty($additionalLinks) === TRUE) {
      unset($sidebarLinkBlock['sidebar-link-block-additional-links']);
    }
    else {
      $sidebarLinkWeight = 0;
      foreach ($additionalLinks as $linkValue) {
        if ($linkValue instanceof Link) {
          $sidebarLinkBlock['sidebar-link-block-additional-links'][] = [
            '#prefix'     => '<li>',
            '#sufix'      => '</li>',
            '#type'       => 'link',
            '#title'      => $linkValue->getText(),
            '#url'        => $linkValue->getUrl(),
            '#weight'     => $sidebarLinkWeight++,
            '#attributes' => [
              'class' => [
                'foldershare-sidebar-link',
                'foldershare-sidebar-additional-link',
              ],
            ],
          ];
        }
      }
    }
    return $sidebarLinkBlock;
  }

  /**
   * Builds and returns the folder browser and sidebar.
   *
   * The folder browser and sidebar are side-by-side content at the same
   * height. The sidebar primarily contains a list of links, but Javascript
   * can add additional content (such as a list of files being uploaded).
   * The folder browser is a view form contain a table of child files and
   * folders of the current item or root list.
   *
   * @param string $viewName
   *   The machine name of the view.
   * @param string $displayName
   *   The machine name of the display.
   * @param bool $enableSidebar
   *   Whether to include the sidebar.
   * @param \Drupal\foldershare\FolderShareInterface $item
   *   (optional, default = NULL) The current item.
   * @param int $rootListId
   *   (optional, default = USER_ROOT_LIST) The root list parent for
   *   menu commands. This is only used if there is no parent item.
   *
   * @return array
   *   Returns a render array containing the folder browser.
   */
  private function buildFolderBrowser(
    string $viewName,
    string $displayName,
    bool $enableSidebar,
    FolderShareInterface $item = NULL,
    int $rootListId = FolderShareInterface::USER_ROOT_LIST) {

    // Get the maximum browser height on a page, if any. Use it to
    // set a style field.
    $maximumHeight = Settings::getUserInterfaceFolderBrowserMaximumHeight();
    $style = '';
    if ($maximumHeight > 0) {
      $style =
        '--foldershare-folder-list-max-height: ' . $maximumHeight . 'em';
    }

    // The sidebar is ALWAYS added to the page. The boolean $enableSidebar
    // controls whether it is initially visible. This insures that any
    // Javascript that looks for the sidebar to add information will
    // always find it.
    return [
      '#attached' => [
        'drupalSettings' => [
          'foldershare-view-page' => [
            'viewName'        => $viewName,
            'displayName'     => $displayName,
            'viewAjaxEnabled' => TRUE,
          ],
        ],
      ],

      'sidebar-and-folder-browser' => [
        '#type'      => 'container',
        '#attributes' => [
          'class' => [
            'foldershare-sidebar-and-folder-browser',
          ],
        ],

        'sidebar' => [
          '#type' => 'container',
          '#attributes' => [
            'class' => [
              'foldershare-sidebar',
            ],
            'style' => [
              (($enableSidebar === TRUE) ? "" : "display: none"),
            ],
          ],

          'sidebar-link-block' => $this->buildSidebarLinkBlock($item),
        ],

        // Add the view.
        'view'          => [
          '#type'       => 'view',
          '#embed'      => TRUE,
          '#name'       => $viewName,
          '#display_id' => $displayName,
          '#arguments'  => (($item === NULL) ? [$rootListId] : [$item->id()]),
          '#attributes' => [
            'class'     => [
              'foldershare-folder-browser',
            ],
            'style'     => [
              $style,
            ],
          ],
        ],
      ],
    ];
  }

}
