<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\Core\Database\Database;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Utilities\CacheUtilities;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Get/set FolderShare entity system hidden field.
 *
 * This trait includes get and set methods for FolderShare entity
 * system hidden field.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetSystemHiddenTrait {

  /*---------------------------------------------------------------------
   *
   * SystemHidden field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function isSystemHidden() {
    $value = $this->get('systemhidden')->value;

    // An empty field is also FALSE.
    if (empty($value) === TRUE || $value === FALSE) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Sets the system hidden flag.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param bool $state
   *   The new flag state.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::isSystemHidden()
   */
  public function setSystemHidden(bool $state) {
    $this->systemhidden->setValue($state);
    return $this;
  }

  /**
   * Clears the system hidden flag on all hidden items.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * This method is intended for use in system debugging and file system
   * fixes in order to quickly reveal all hidden items. Since hidden items
   * are usually in the process of being deleted, revealing all hidden items
   * can cause confusion.
   *
   * The entity cache is cleared and the render cache invalidated to insure
   * that changed items are visible to the user.
   *
   * @return int
   *   Returns the number of items changed.
   *
   * @see ::setSystemHidden()
   * @see \Drupal\foldershare\Utilities\CacheUtilities::flushAllEntityCaches()
   * @see \Drupal\foldershare\Utilities\CacheUtilities::invalidateRenderCache()
   */
  public static function clearAllSystemHidden() {
    $connection = Database::getConnection();

    // Clear the hidden flag on all hidden items.
    $query = $connection->update(FolderShareInterface::BASE_TABLE);
    $query->condition('systemhidden', TRUE, '=');
    $query->fields([
      'systemhidden' => (int) FALSE,
    ]);
    $n = $query->execute();
    if ($n === 0) {
      return 0;
    }

    CacheUtilities::flushAllEntityCaches(FolderShareInterface::ENTITY_TYPE_ID);
    CacheUtilities::invalidateRenderCache();
  }

  /**
   * Sets the system hidden flag on all descendants.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * This method is intended for use by delete() in order to quickly mark
   * descendants hidden, pending actual deletion.
   *
   * This method recurses through a folder tree, starting with the given
   * parent item. All descendants are marked hidden via database updates.
   *
   * During recursion, this method skips subfolder trees that are already
   * marked hidden. It is assumed that if a parent folder is marked, the
   * children are already marked. This is a necessary assumption so that
   * repeated calls to this method during a scheduled task will eventually
   * find nothing more to mark.
   *
   * @param int $parentId
   *   The FolderShare entity ID of a parent. The parent is presumed to
   *   already have been marked hidden.
   *
   * @see ::setSystemHidden()
   * @see ::delete()
   */
  private static function setDescendantsSystemHidden(int $parentId) {
    $connection = Database::getConnection();

    // Find folder children in need of updates.
    $query = $connection->select(FolderShareInterface::BASE_TABLE, 'fs');
    $query->addField('fs', 'id', 'id');
    $query->condition('parentid', $parentId, '=');
    $query->condition('kind', FolderShareInterface::FOLDER_KIND, '=');

    // An empty field is also FALSE, so we have to test for !TRUE.
    $query->condition('systemhidden', TRUE, '<>');

    $childIds = [];
    foreach ($query->execute() as $result) {
      $childIds[] = (int) $result->id;
    }

    // Recurse through all folder children.
    foreach ($childIds as $childId) {
      self::setDescendantsSystemHidden($childId);
    }

    // Mark all children hidden.
    $query = $connection->update(FolderShareInterface::BASE_TABLE);
    $query->condition('parentid', $parentId, '=');
    $query->fields([
      'systemhidden' => (int) TRUE,
    ]);
    $query->execute();
  }

  /*---------------------------------------------------------------------
   *
   * Standard error messages.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a standard hidden message.
   *
   * This method provides a generic message that may be used by
   * operations that need to report that an item is hidden.
   *
   * @param string $itemName
   *   (optional, default = NULL = multiple items) The name of a single item
   *   involved in an operation that cannot be done. If NULL, a multi-item
   *   message is returned instead.
   *
   * @return \Drupal\Core\Render\Markup
   *   Returns a markup object containing a formatted standard
   *   exception message.
   */
  public static function getStandardHiddenMessage(
    string $itemName = NULL) {

    if (empty($itemName) === TRUE) {
      return FormatUtilities::createFormattedMessage(
        t('This item could be found.'));
    }

    return FormatUtilities::createFormattedMessage(
      t(
        'The item "@name" could be found.',
        [
          '@name' => $itemName,
        ]));
  }

}
