<?php

namespace Drupal\foldershare\Form;

use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Form\AdminSettingsTraits\AdminSettingsAboutTab;
use Drupal\foldershare\Form\AdminSettingsTraits\AdminSettingsFilesTab;
use Drupal\foldershare\Form\AdminSettingsTraits\AdminSettingsInterfaceTab;
use Drupal\foldershare\Form\AdminSettingsTraits\AdminSettingsServicesTab;

/**
 * Creates a form to adjust the module's configuration.
 *
 * The module configuration form is intended for administrators. It enables
 * changes to how files are stored, uploaded, and accessed. It also provides
 * configuration reset forms and links to related forms.
 *
 * <B>Internal class</B>
 * This class is internal to the FolderShare module. The class's existance,
 * name, and content may change from release to release without any promise
 * of backwards compatability.
 *
 * <B>Access control</B>
 * The route to this form should restrict access to those with administration
 * administration permission.
 *
 * @ingroup foldershare
 */
final class AdminSettings extends ConfigFormBase {
  use AdminSettingsAboutTab;
  use AdminSettingsFilesTab;
  use AdminSettingsInterfaceTab;
  use AdminSettingsServicesTab;

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The module handler, set at construction time.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new form.
   *
   * @param Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   The configuration factory.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   */
  public function __construct(
    ConfigFactoryInterface $configFactory,
    ModuleHandlerInterface $moduleHandler) {

    parent::__construct($configFactory);
    $this->moduleHandler = $moduleHandler;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('module_handler')
    );
  }

  /*---------------------------------------------------------------------
   *
   * Form interface.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return mb_strtolower(str_replace('\\', '_', get_class($this)));
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [Constants::SETTINGS];
  }

  /*---------------------------------------------------------------------
   *
   * Build form.
   *
   *---------------------------------------------------------------------*/
  /**
   * Builds a form to adjust the module configuration.
   *
   * The form has multiple vertical tabs, each built by a separate function.
   *
   * @param array $form
   *   An associative array containing the renderable structure of the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   (optional) The current state of the form.
   *
   * @return array
   *   The form renderable array.
   */
  public function buildForm(
    array $form,
    FormStateInterface $formState = NULL) {

    //
    // Vertical tabs
    // -------------
    // Setup vertical tabs. For these to work, all of the children
    // must be of type 'details' and refer to the 'tabs' group.
    $form['#attributes']['class'][] = 'foldershare-admin-settings';
    $form['tabs'] = [
      '#type'     => 'vertical_tabs',
      '#attached' => [
        'library' => [
          'foldershare/foldershare.general',
          'foldershare/foldershare.admin',
        ],
      ],
    ];

    // Create each of the tabs.
    $this->buildAboutTab($form, $formState, 'tabs');
    $this->buildFilesTab($form, $formState, 'tabs');
    $this->buildInterfaceTab($form, $formState, 'tabs');
    $this->buildServicesTab($form, $formState, 'tabs');

    // Build and return the form.
    return parent::buildForm($form, $formState);
  }

  /*---------------------------------------------------------------------
   *
   * Validate.
   *
   *---------------------------------------------------------------------*/
  /**
   * Validates the form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  public function validateForm(array &$form, FormStateInterface $formState) {

    parent::validateForm($form, $formState);

    $this->validateAboutTab($form, $formState);
    $this->validateFilesTab($form, $formState);
    $this->validateInterfaceTab($form, $formState);
    $this->validateServicesTab($form, $formState);
  }

  /*---------------------------------------------------------------------
   *
   * Submit.
   *
   *---------------------------------------------------------------------*/
  /**
   * Stores the submitted form values.
   *
   * @param array $form
   *   The form configuration.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The entered values for the form.
   */
  public function submitForm(array &$form, FormStateInterface $formState) {

    parent::submitForm($form, $formState);

    $this->submitAboutTab($form, $formState);
    $this->submitFilesTab($form, $formState);
    $this->submitInterfaceTab($form, $formState);
    $this->submitServicesTab($form, $formState);
  }

  /*---------------------------------------------------------------------
   *
   * Utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns a lower case CSS-friendly version of a string.
   *
   * The string is converted to lower case. White space is replaced with
   * dashes. Punctuation characters are removed.
   *
   * @param string $str
   *   The string to convert.
   *
   * @return string
   *   The converted string.
   */
  private static function makeCssSafe(string $str) {
    $str = mb_convert_case($str, MB_CASE_LOWER);
    $str = preg_replace('/[[:space:]]+/', '-', $str);
    return preg_replace('/[^-_[:alnum:]]+/', '', $str);
  }

}
