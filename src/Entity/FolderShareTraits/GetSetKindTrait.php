<?php

namespace Drupal\foldershare\Entity\FolderShareTraits;

use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Utilities\FormatUtilities;

/**
 * Get/set FolderShare entity kind field.
 *
 * This trait includes get methods for FolderShare entity kind field,
 * along with utility functions to test for specific kinds of items.
 *
 * <B>Internal trait</B>
 * This trait is internal to the FolderShare module and used to define
 * features of the FolderShare entity class. It is a mechanism to group
 * functionality to improve code management.
 *
 * @ingroup foldershare
 */
trait GetSetKindTrait {

  /*---------------------------------------------------------------------
   *
   * Kind field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getKind() {
    return $this->get('kind')->value;
  }

  /**
   * Sets the item kind.
   *
   * <B>This method is internal and strictly for use by the FolderShare
   * module itself.</B>
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @param string $kind
   *   The new kind, which must be one of the known kind types:
   *   FOLDER_KIND, FILE_KIND, IMAGE_KIND, MEDIA_KIND, or OBJECT_KIND. Setting
   *   the field to any other kind is ignored.
   *
   * @return \Drupal\foldershare\FolderShareInterface
   *   Returns this item.
   *
   * @see ::getKind()
   * @see ::isFile()
   * @see ::isFolder()
   * @see ::isImage()
   * @see ::isMedia()
   * @see ::isObject()
   */
  private function setKind(string $kind) {
    switch ($kind) {
      case FolderShareInterface::FILE_KIND:
      case FolderShareInterface::FOLDER_KIND:
      case FolderShareInterface::IMAGE_KIND:
      case FolderShareInterface::MEDIA_KIND:
      case FolderShareInterface::OBJECT_KIND:
        $this->kind->setValue($kind);
        return $this;

      default:
        return $this;
    }
  }

  /**
   * Swaps the image field into the file field and sets the file kind.
   *
   * If the item is an image, the image's File entity ID is moved into the
   * file field and the kind set to FILE_KIND.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @see ::updateFileAndImageKinds()
   * @see ::swapFileToImage()
   */
  private function swapImageToFile() {
    if ($this->isImage() === TRUE) {
      $fileId = $this->getImageId();
      $this->setFileId($fileId);
      $this->clearImageId();
      $this->setKind(FolderShareInterface::FILE_KIND);
    }
  }

  /**
   * Swaps the file field into the image field and sets the image kind.
   *
   * If the item is a file, the file's File entity ID is moved into the
   * image field and the kind set to IMAGE_KIND.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @see ::updateFileAndImageKinds()
   * @see ::swapImageToFile()
   */
  private function swapFileToImage() {
    if ($this->isFile() === TRUE) {
      $fileId = $this->getFileId();
      $this->setImageId($fileId);
      $this->clearFileId();
      $this->setKind(FolderShareInterface::IMAGE_KIND);
    }
  }

  /**
   * Swaps file and image fields and kinds if needed based on the MIME type.
   *
   * The item's MIME type is used to decide if the underlying file is an
   * image file or some other file type. If it is an image file and this
   * item is marked as a general file, or if it is a general file but this
   * item is marked as an image, then the file and image fields and their
   * File entity IDs are swapped and kinds are updated.
   *
   * The caller must call save() for the change to take effect.
   *
   * <B>Process locks</B>
   * This method does not lock access. The caller should lock around changes
   * to the entity.
   *
   * @see ::swapFileToImage()
   * @see ::swapImageToFile()
   */
  private function updateFileAndImageKinds() {
    if ($this->isFileOrImage() === FALSE) {
      return;
    }

    $mimeIsImage = self::isMimeTypeImage($this->getMimeType());

    if ($this->isFile() === TRUE && $mimeIsImage === TRUE) {
      $this->swapFileToImage();
    }
    elseif ($this->isImage() === TRUE && $mimeIsImage === FALSE) {
      $this->swapImageToFile();
    }
  }

  /*---------------------------------------------------------------------
   *
   * Test kind field.
   *
   *---------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function isFile() {
    return ($this->get('kind')->value === FolderShareInterface::FILE_KIND);
  }

  /**
   * {@inheritdoc}
   */
  public function isFileOrImage() {
    $k = $this->getKind();
    return ($k === FolderShareInterface::FILE_KIND || $k === FolderShareInterface::IMAGE_KIND);
  }

  /**
   * {@inheritdoc}
   */
  public function isFolder() {
    return ($this->get('kind')->value === FolderShareInterface::FOLDER_KIND);
  }

  /**
   * {@inheritdoc}
   */
  public function isImage() {
    return ($this->get('kind')->value === FolderShareInterface::IMAGE_KIND);
  }

  /**
   * {@inheritdoc}
   */
  public function isMedia() {
    return ($this->get('kind')->value === FolderShareInterface::MEDIA_KIND);
  }

  /**
   * {@inheritdoc}
   */
  public function isObject() {
    return ($this->get('kind')->value === FolderShareInterface::OBJECT_KIND);
  }

  /*--------------------------------------------------------------------
   *
   * Translations.
   *
   *-------------------------------------------------------------------*/
  /**
   * Returns a translation of the entity kind in singular form.
   *
   * Standard entity kinds are mapped to their corresponding terms.
   *
   * @param string $kind
   *   The kind of a FolderShare entity.
   * @param int $caseMode
   *   (optional, default = MB_CASE_LOWER) The mix of case for the returned
   *   value. One of MB_CASE_UPPER, MB_CASE_LOWER, or MB_CASE_TITLE.
   *
   * @return string
   *   The user-visible term for the singular kind.
   *
   * @see ::translateKinds()
   */
  public static function translateKind(
    string $kind,
    int $caseMode = MB_CASE_LOWER) {

    $term = '';
    switch ($kind) {
      case FolderShareInterface::FILE_KIND:
        $term = t('file');
        break;

      case FolderShareInterface::IMAGE_KIND:
        $term = t('image');
        break;

      case FolderShareInterface::MEDIA_KIND:
        $term = t('media');
        break;

      case FolderShareInterface::OBJECT_KIND:
        $term = t('object');
        break;

      case FolderShareInterface::FOLDER_KIND:
        $term = t('folder');
        break;

      case 'rootlist':
        $term = t('top-level items');
        break;

      default:
      case 'item':
      case 'items':
        $term = t('item');
        break;
    }

    return mb_convert_case($term, $caseMode);
  }

  /**
   * Returns a translation of the entity kind in plural form.
   *
   * Standard entity kinds are mapped to their corresponding terms.
   *
   * @param string $kind
   *   The kind of a FolderShare entity.
   * @param int $caseMode
   *   (optional, default = MB_CASE_LOWER) The mix of case for the returned
   *   value. One of MB_CASE_UPPER, MB_CASE_LOWER, or MB_CASE_TITLE.
   *
   * @return string
   *   The user-visible term for the plural kind.
   *
   * @see ::translateKind()
   */
  public static function translateKinds(
    string $kind,
    int $caseMode = MB_CASE_LOWER) {

    $term = '';
    switch ($kind) {
      case FolderShareInterface::FILE_KIND:
        $term = t('files');
        break;

      case FolderShareInterface::IMAGE_KIND:
        $term = t('images');
        break;

      case FolderShareInterface::MEDIA_KIND:
        $term = t('media');
        break;

      case FolderShareInterface::OBJECT_KIND:
        $term = t('object');
        break;

      case FolderShareInterface::FOLDER_KIND:
        $term = t('folders');
        break;

      case 'rootlist':
        $term = t('top-level items');
        break;

      default:
      case 'item':
      case 'items':
        $term = t('items');
        break;
    }

    return mb_convert_case($term, $caseMode);
  }

  /**
   * Returns a translated phrase for the mix of kinds in an entity ID list.
   *
   * The given list of entity IDs is used to query the variety of kinds
   * used by those entities. A translated phrase is returned that describes
   * the kinds:
   *
   * - For an empty list, the translated "nothing" is returned.
   *
   * - For a single item, the translated singular form of the item's kind
   *   is returned. Examples: "file", "folder", "image", "object".
   *
   * - For multiple items that are all the same kind, the translated plural
   *   form of the kind is returned, preceded by the number of items.
   *   Examples: "five files", "13 folders", "108 images".
   *
   * - For all other cases, the translated plural of "item" is returned,
   *   preceded by the number of items. Examples: "five items", "13 items".
   *
   * When working with multiple kinds, "file" and "image" kinds are
   * collapsed into simply "file" because images are really files anyway.
   *
   * @param int[] $ids
   *   The entity IDs with kinds to translate.
   *
   * @return array
   *   Returns an associative array with keys 'kinds' and 'translation'.
   *   The value for 'kinds' is an array containing kind values found for
   *   the entity IDs (e.g. if there are 2 kinds used by 10 IDs, the array
   *   will include 2 entries for those kinds). The value for 'translation'
   *   is translatable markup for a translated phrase to describe those kinds.
   *
   * @see ::translateKind()
   * @see ::translateKinds()
   * @see ::findKindsForIds()
   */
  public static function translateKindsForIds(array $ids) {
    //
    // Get kinds.
    // ----------
    // Get the mix of kinds involved.
    if (empty($ids) === TRUE) {
      return [
        'kinds'       => [],
        'translation' => t('nothing'),
      ];
    }

    $nIds = count($ids);

    // The $kindsAndIds array returned below is an associative array with
    // kind name keys (e.g. "file") and values that are arrays of integer
    // entity IDs of that kind. The IDs are all from the given $ids array.
    $kindsAndIds = self::findKindsForIds($ids);

    // The $kinds array below contains the names of all kinds that occur
    // in the above list.
    $kinds = array_keys($kindsAndIds);
    $nKinds = count($kinds);

    //
    // Convert the kind list into a phrase.
    // ------------------------------------
    // Handle several cases:
    // - One item, so singular kind.
    // - Multiple items:
    //   - One kind for all, so plural kind.
    //   - Two kinds, so plural for both kinds.
    //   - Multiple kinds, so plural "items".
    if ($nIds === 1) {
      return [
        'kinds'       => $kinds,
        'translation' => self::translateKind($kinds[0]),
      ];
    }

    if (in_array(FolderShareInterface::FILE_KIND, $kinds) === TRUE &&
        in_array(FolderShareInterface::IMAGE_KIND, $kinds) === TRUE) {
      // When files and images are in the selection, collapse the
      // kind list to be just files.
      $kindsAndIds[FolderShareInterface::FILE_KIND] = array_merge(
        $kindsAndIds[FolderShareInterface::FILE_KIND],
        $kindsAndIds[FolderShareInterface::IMAGE_KIND]);
      unset($kindsAndIds[FolderShareInterface::IMAGE_KIND]);
      unset($kinds[FolderShareInterface::IMAGE_KIND]);
      --$nKinds;
    }

    if ($nKinds === 1) {
      // One kind (e.g. "file" or "folder"), but multiple items
      // of that kind. Return the plural translation of the kind.
      return [
        'kinds'       => $kinds,
        'translation' => t(
          '@count @kinds',
          [
            '@count' => FormatUtilities::formatNumberAsWord($nIds),
            '@kinds' => self::translateKinds($kinds[0]),
          ]),
      ];
    }

    // More than two kinds and multiple items of one or all kinds.
    // Just return a translation of the generic "items".
    return [
      'kinds'       => $kinds,
      'translation' => t(
        '@count @kinds',
        [
          '@count' => FormatUtilities::formatNumberAsWord($nIds),
          '@kinds' => self::translateKinds('items'),
        ]),
    ];
  }

}
