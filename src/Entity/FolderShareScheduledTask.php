<?php

namespace Drupal\foldershare\Entity;

use Drupal\Core\Entity\ContentEntityBase;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;

use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\FolderShareScheduledTaskTraits\FindTrait;
use Drupal\foldershare\Entity\FolderShareScheduledTaskTraits\GetSetTrait;
use Drupal\foldershare\Entity\FolderShareScheduledTaskTraits\DeleteTrait;
use Drupal\foldershare\Entity\FolderShareScheduledTaskTraits\ExecuteTrait;

/**
 * Describes an internal task to be performed at a time in the future.
 *
 * The module's internal scheduled tasks are used to perform background
 * activity to finish long operations, such as copying, moving, or deleting
 * a large folder tree. Each task has:
 *
 * - A callback used to run the task.
 *
 * - An array of parameters, such as lists of entity IDs to copy, move,
 *   or delete.
 *
 * Each task has three dates/times:
 * - The date/time the task was first started.
 * - The date/time the current task object was created.
 * - The date/time the task is scheduled to run.
 *
 * When the task is first started, the creation date matches the start date.
 * The scheduled run date is a short time in the future. Each time the task
 * is run, it is removed from the database and its parameters passed to
 * the task callback. If a callback cannot finish the task within
 * execution and memory use limits, the callback creates a new task to continue
 * the work. The new task has the same original start date, a new creation
 * date, and a future scheduled date.
 *
 * For debugging and monitoring, each task also has:
 * - A brief text comment describing the task.
 * - A total execution time so far, measured in seconds.
 *
 * Like all entities, a task also has:
 * - An ID.
 * - A unique ID.
 * - A user ID for the user that started the task.
 *
 * The user ID is the current user when the task was first started. The task
 * will run by CRON or at the end of pages delivered to any user.
 * For those runs, the current user is whomever is running CRON or
 * whomever got the most recent page, but the user ID in the task remains the
 * ID of the original user that started the task. It is this original user
 * that owns any new content created by the task.
 *
 * <B>Task processing</B>
 * The list of scheduled tasks is processed in one of three ways:
 * - At the end of a request.
 * - When CRON runs.
 * - From a drush command.
 *
 * The module provides an event subscriber that listens for the "terminate"
 * event sent after a request has been finished. This is normally at the end
 * of every page delivered to a user, or after any REST or AJAX request.
 * The event subscriber calls this class's executeTasks() method.
 *
 * The module includes a CRON hook that is called each time CRON is invoked,
 * whether via an external CRON trigger or via a "terminate" event listened
 * to by the Automated Cron module. At this time, the hook calls this class's
 * executeTasks() method.
 *
 * The module includes drush commands to list tasks, delete tasks, and run
 * tasks. Drush can call this class's executeTasks() method.
 *
 * In any case, executeTasks() quickly checks if there are any tasks ready
 * to run, then runs them. A task is ready to run only if its scheduled time
 * is equal to the current time or in the past. If there are multiple tasks
 * ready to run, they are executed in order of their scheduled times.
 *
 * <B>Task visibility</B>
 * Tasks are strictly an internal implementation detail of this module.
 * They are not intended to be seen by users or administrators. For this
 * reason, the entity definition intentionally omits features:
 * - The entity type is marked as internal.
 * - None of the entity type's fields are viewable.
 * - None of the entity type's fields are editable.
 * - The entity type is not fieldable.
 * - The entity type has no "views_builder" to present view pages.
 * - The entity type has no "views_data" for creating views.
 * - The entity type has no "list_builder" to show lists.
 * - The entity type has no edit forms.
 * - The entity type has no access controller.
 * - The entity type has no admin permission.
 * - The entity type has no routes.
 * - The entity type has no caches.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 *
 * @see foldershare_cron()
 * @see \Drupal\foldershare\EventSubscriber\FolderShareScheduledTaskHandler
 *
 * @ContentEntityType(
 *   id               = "foldershare_scheduledtask",
 *   label            = @Translation("FolderShare internal scheduled task"),
 *   base_table       = "foldershare_scheduledtask",
 *   internal         = TRUE,
 *   persistent_cache = FALSE,
 *   render_cache     = FALSE,
 *   static_cache     = FALSE,
 *   fieldable        = FALSE,
 *   entity_keys      = {
 *     "id"           = "id",
 *     "uuid"         = "uuid",
 *     "label"        = "operation",
 *   },
 * )
 */
final class FolderShareScheduledTask extends ContentEntityBase {
  use FindTrait;
  use GetSetTrait;
  use DeleteTrait;
  use ExecuteTrait;

  /*---------------------------------------------------------------------
   *
   * Fields.
   *
   *---------------------------------------------------------------------*/
  /**
   * Indicates if task execution is enabled.
   *
   * In normal use, this value is TRUE. However, if task execution needs
   * to be disabled FOR THE CURRENT PROCESS ONLY, this flag may be set
   * to FALSE. Future calls to executeTasks() will return doing nothing.
   *
   * This is primarily used by Drush commands to disable task execution
   * during a command so that tasks don't interfer with whatever the command
   * is trying to do.
   *
   * @var bool
   *
   * @see ::executeTasks()
   * @see ::isTaskExecutionEnabled()
   * @see ::setTaskExecutionEnabled()
   */
  private static $enabled = TRUE;

  /*---------------------------------------------------------------------
   *
   * Constants - Entity type id.
   *
   *---------------------------------------------------------------------*/
  /**
   * The entity type id for the FolderShare Scheduled Task entity.
   *
   * This is 'foldershare_scheduledtask' and it must match the entity type
   * declaration in this class's comment block.
   *
   * @var string
   */
  const ENTITY_TYPE_ID = 'foldershare_scheduledtask';

  /*---------------------------------------------------------------------
   *
   * Constants - Database tables.
   *
   *---------------------------------------------------------------------*/
  /**
   * The base table for 'foldershare_scheduledtask' entities.
   *
   * This is 'foldershare_scheduledtask' and it must match the base table
   * declaration in this class's comment block.
   *
   * @var string
   */
  const BASE_TABLE = 'foldershare_scheduledtask';

  /*---------------------------------------------------------------------
   *
   * Constants - Task flags.
   *
   *---------------------------------------------------------------------*/
  /**
   * A task flag indicating the task repeats indefinitely.
   *
   * This flag, including in the 'flags' bitfield for a task, indicates
   * that the task repeats over and over, without end. Such tasks are
   * used to keep information uptodate by running every hour or so to
   * collect and update a table, such as a search index. On execution,
   * the task immediately re-queues a new task to do the next update,
   * and later that task re-queues a new task, and so on, repeating
   * until some external code stops the repeat.
   *
   * @var int
   */
  const REPEATING_FLAG = 1;

  /*---------------------------------------------------------------------
   *
   * Entity definition.
   *
   *---------------------------------------------------------------------*/
  /**
   * Defines the fields used by instances of this class.
   *
   * The following fields are defined, along with their intended
   * public or private access:
   *
   * | Field            | Allow for view | Allow for edit |
   * | ---------------- | -------------- | -------------- |
   * | id               | no             | no             |
   * | uuid             | no             | no             |
   * | uid              | no             | no             |
   * | created          | no             | no             |
   * | operation        | no             | no             |
   * | parameters       | no             | no             |
   * | scheduled        | no             | no             |
   * | started          | no             | no             |
   * | comments         | no             | no             |
   * | executiontime    | no             | no             |
   *
   * Some fields are supported by parent class methods:
   *
   * | Field            | Get method                           |
   * | ---------------- | ------------------------------------ |
   * | id               | ContentEntityBase::id()              |
   * | uuid             | ContentEntityBase::uuid()            |
   * | operation        | ContentEntityBase::getName()         |
   *
   * Some fields are supported by methods in this class:
   *
   * | Field            | Get method                           |
   * | ---------------- | ------------------------------------ |
   * | uid              | getRequester()                       |
   * | created          | getCreatedTime()                     |
   * | operation        | getCallback()                        |
   * | parameters       | getParameters()                      |
   * | scheduled        | getScheduledTime()                   |
   * | started          | getStartedTime()                     |
   * | comments         | getComments()                        |
   * | executiontime    | getAccumulatedExecutionTime()        |
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entityType
   *   The entity type for which we are returning base field definitions.
   *
   * @return array
   *   An array of field definitions where keys are field names and
   *   values are BaseFieldDefinition objects.
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entityType) {
    //
    // Base class fields
    // -----------------
    // The parent ContentEntityBase class supports several standard
    // entity fields:
    //
    // - id: the entity ID
    // - uuid: the entity unique ID
    // - langcode: the content language
    // - revision: the revision ID
    // - bundle: the entity bundle
    //
    // The parent class ONLY defines these fields if they exist in
    // THIS class's comment block declaring class fields.  Of the
    // above fields, we only define these for this class:
    //
    // - id
    // - uuid
    //
    // By invoking the parent class, we don't have to define these
    // ourselves below.
    $fields = parent::baseFieldDefinitions($entityType);

    // Entity id.
    // This field was already defined by the parent class.
    $fields[$entityType->getKey('id')]
      ->setDescription(t('The ID of the task.'))
      ->setDisplayConfigurable('view', FALSE);

    // Unique id (UUID).
    // This field was already defined by the parent class.
    $fields[$entityType->getKey('uuid')]
      ->setDescription(t('The UUID of the task.'))
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    //
    // Common fields
    // -------------
    // Tasks have several fields describing the task, when it was started,
    // and when it should next run.
    //
    // Operation (callback).
    $fields['operation'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Callback'))
      ->setDescription(t('The task callback.'))
      ->setRequired(TRUE)
      ->setSettings([
        'default_value'   => '',
        'max_length'      => 256,
        'text_processing' => FALSE,
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Requester (original) user id.
    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setLabel(t('Requester'))
      ->setDescription(t("The user ID that requested the task."))
      ->setRequired(TRUE)
      ->setSetting('target_type', 'user')
      ->setSetting('handler', 'default')
      ->setDefaultValueCallback(
        'Drupal\foldershare\Entity\FolderShareScheduledTask::getCurrentUserId')
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Creation date.
    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Created date'))
      ->setDescription(t('The date and time when this task entry was created.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Scheduled time to run.
    $fields['scheduled'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Scheduled time'))
      ->setDescription(t('The date and time when the task is scheduled to run.'))
      ->setRequired(TRUE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Task parameters.
    $fields['parameters'] = BaseFieldDefinition::create('string_long')
      ->setLabel(t('Task parameters'))
      ->setDescription(t('The JSON parameters for the task.'))
      ->setRequired(FALSE)
      ->setSettings([
        'case_sensitive' => TRUE,
      ])
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Original operation date.
    $fields['started'] = BaseFieldDefinition::create('timestamp')
      ->setLabel(t('Original start date'))
      ->setDescription(t('The date and time when the operation started.'))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Comments.
    $fields['comments'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Comments'))
      ->setDescription(t("The task's comments."))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Accumulated run time.
    $fields['executiontime'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Accumulated execution time'))
      ->setDescription(t("The task's total execution time to date."))
      ->setRequired(FALSE)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    // Task flags.
    $fields['flags'] = BaseFieldDefinition::create('integer')
      ->setLabel(t('Flags'))
      ->setDescription(t("Flags indicating the task type."))
      ->setRequired(FALSE)
      ->setDefaultValue(0)
      ->setDisplayConfigurable('view', FALSE)
      ->setDisplayConfigurable('form', FALSE);

    return $fields;
  }

  /*---------------------------------------------------------------------
   *
   * General utilities.
   *
   *---------------------------------------------------------------------*/
  /**
   * Returns the current user ID.
   *
   * This function provides the deault value callback for the 'uid'
   * base field definition.
   *
   * @return array
   *   An array of default values. In this case, the array only
   *   contains the current user ID.
   *
   * @see ::baseFieldDefinitions()
   */
  public static function getCurrentUserId() {
    return [\Drupal::currentUser()->id()];
  }

  /**
   * Validates a class and method callback.
   *
   * A standard PHP callback has one of these forms:
   * - String:
   *   - FUNCTIONNAME
   *   - CLASSNAME::METHODNAME
   *   - CLASSOBJECT::METHODNAME
   * - Array:
   *   - [CLASSNAME, METHODNAME]
   *   - [CLASSOBJECT, METHODNAME]
   *
   * FUNCTIONNAME, CLASSNAME, and METHODNAME must exist. The METHODNAME must
   * be a public static method.
   *
   * @param mixed $callback
   *   A string naming a function or a class and public static method, or
   *   an array with two values that include either the class name or an
   *   object instance, and a public static method name.
   *
   * @return string
   *   Returns a string of the form "FUNCTIONNAME" or "CLASSNAME::METHODNAME",
   *   where the function, class, and method have all been verified.
   *
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the callback is malformed, if the function,
   *   class, or method does not exist, or if the method is not public
   *   and static.
   *
   * @see ::createTask()
   * @see https://www.php.net/manual/en/function.is-callable.php
   */
  private static function verifyCallback($callback) {
    // PHP's is_callable() checks for all valid forms of a callback,
    // including verifying that a function, class, or method exists.
    // It returns TRUE/FALSE and sets the callable name to a string
    // of the form "FUNCTIONNAME" or "CLASSNAME::METHODNAME".
    $callableName = '';
    if (is_callable($callback, FALSE, $callableName) === FALSE) {
      // Developer-facing exception message.
      throw new ValidationException(
        'Malformed task callback is not in a recognized format or a class, method, or function does not exist.');
    }

    // PHP's is_callable() does not check that the method is static and
    // public. PHP's call_user_func() handles non-static and/or non-public
    // methods with errors:
    // - If the method is private, PHP issues a warning.
    // - If the method is not static, PHP issues a fatal error and aborts.
    //
    // We'd rather not have these happen during a task, so we need to
    // validate that a named method is public and static. This requires
    // using the PHP reflection API.
    //
    // Split the callable name into CLASSNAME and METHODNAME, or just
    // FUNCTIONNAME. For the FUNCTIONNAME case, there is no further
    // checking required.
    $pieces = explode('::', $callableName);
    if (count($pieces) !== 2) {
      return $callableName;
    }

    $className = $pieces[0];
    $methodName = $pieces[1];

    // Get the reflection. Since is_callable() has already confirmed that
    // the class and method exist, this should not throw exceptions.
    try {
      $rClass = new \ReflectionClass($className);
      $rMethod = $rClass->getMethod($methodName);

      if ($rMethod->isStatic() === FALSE) {
        // Developer-facing exception message.
        throw new ValidationException(
          "Invalid task callback method '$methodName' is not static.");
      }

      if ($rMethod->isPublic() === FALSE) {
        // Developer-facing exception message.
        throw new ValidationException(
          "Invalid task callback method '$methodName' is not public.");
      }
    }
    catch (\Exception $e) {
      // Developer-facing exception message.
      throw new ValidationException(
        'Malformed task callback class or method does not exist.');
    }

    return $callableName;
  }

  /*---------------------------------------------------------------------
   *
   * Create.
   *
   *---------------------------------------------------------------------*/
  /**
   * Creates a new task.
   *
   * Every task has:
   * - A "class::method" string for the task callback.
   * - A set of parameters for that task (this may be empty).
   * - A user ID for the user that requested the task.
   * - A timestamp for the future time at which to run the task.
   * - A timestamp for when the operation was first started.
   * - Comments to describe the task during debugging.
   * - An accumulated run time, in seconds.
   *
   * Parameters must be appropriate for the task.
   *
   * This method should be used in preference to create() in order to
   * insure that the callback is valid and to use JSON encoding to process
   * task parameters to be saved with the task.
   *
   * @param int $timestamp
   *   The future time at which the task will be executed.
   * @param mixed $callback
   *   The callback for the task following standard PHP callable formats
   *   (e.g. "class::method" or [object, "method"], etc.).
   * @param int $requester
   *   (optional, default = (-1) = current user) The user ID of the
   *   individual causing the task to be created.
   * @param array $parameters
   *   (optional, default = NULL) An array of parameters to save with the
   *   task and pass to the task when it is executed.
   * @param int $started
   *   (optional, default = 0) The timestamp of the start date & time for
   *   an operation that causes a chain of tasks.
   * @param string $comments
   *   (optional, default = '') A comment on the current task.
   * @param int $executionTime
   *   (optional, default = 0) The accumulated total execution time of the
   *   task chain, in seconds.
   * @param int $flags
   *   (optional, default = 0) A bitmask of task flags.
   *
   * @return \Drupal\foldershare\Entity\FolderShareScheduledTask
   *   Returns the newly created task. The task will already have been saved
   *   to the task table.
   *
   * @throws \Drupal\foldershare\Entity\Exception\ValidationException
   *   Throws an exception if the callback is malformed or unrecognized, or
   *   if the the parameters array cannot be encoded as JSON.
   */
  public static function createTask(
    int $timestamp,
    $callback,
    int $requester = (-1),
    array $parameters = NULL,
    int $started = 0,
    string $comments = '',
    int $executionTime = 0,
    int $flags = 0) {

    // Support old well-known callbacks. These are no longer used.
    switch ($callback) {
      case 'changeowner':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskChangeOwner';
        break;

      case 'copy-to-folder':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskCopyToFolder';
        break;

      case 'copy-to-root':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskCopyToRoot';
        break;

      case 'delete-hide':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskDelete1';
        break;

      case 'delete-delete':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskDelete2';
        break;

      case 'move-to-folder':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskMoveToFolder';
        break;

      case 'move-to-root':
        $callback = '\Drupal\foldershare\Entity\FolderShare::processTaskMoveToRoot';
        break;

      case 'rebuildusage':
        $callback = '\Drupal\foldershare\ManageUsageStatistics::taskUpdateUsage';
        break;
    }

    // Throws an exception if the callback does not validate.
    $callback = self::verifyCallback($callback);

    // Insure we have a requester.
    if ($requester < 0) {
      $requester = (int) \Drupal::currentUser()->id();
    }

    // Convert parameters to a JSON encoding.
    if ($parameters === NULL) {
      $json = '';
    }
    else {
      $json = json_encode($parameters);
      if ($json === FALSE) {
        // Developer-facing exception message.
        throw new ValidationException(
          "Task parameters cannot be JSON encoded for '$callback'.");
      }
    }

    // Create the task. Let the ID, UUID, and creation date be automatically
    // assigned.
    $task = self::create([
      // Required fields.
      'operation'     => $callback,
      'uid'           => $requester,
      'scheduled'     => $timestamp,

      // Optional fields.
      'parameters'    => $json,
      'started'       => $started,
      'comments'      => $comments,
      'executiontime' => $executionTime,
      'flags'         => $flags,
    ]);

    $task->save();

    return $task;
  }

}
