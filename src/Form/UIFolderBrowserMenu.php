<?php

namespace Drupal\foldershare\Form;

use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\DependencyInjection\ContainerInterface;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\InvokeCommand;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Routing\RedirectDestinationTrait;
use Drupal\Core\Url;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Site\Settings as SiteSettings;

use Drupal\user\Entity\User;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\Exception\RuntimeExceptionWithMarkup;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\FolderShareAccessControlHandler;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\Plugin\FolderShareCommandManager;
use Drupal\foldershare\Plugin\FolderShareCommand\FolderShareCommandInterface;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\AjaxUtilities;
use Drupal\foldershare\Utilities\FormatUtilities;
use Drupal\foldershare\Utilities\LimitUtilities;

/**
 * Creates a form for the folder browser's command menu.
 *
 * This form manages a menu of commands (e.g. new, delete, copy) and their
 * operands. The available commands are defined by command plugins whose
 * attributes sort commands into categories and define the circumstances
 * under which a command may be envoked (e.g. for one item or many, on files,
 * on folders, etc.).
 *
 * This form includes:
 * - A field to specify the command.
 * - A field containing the current selection, if any.
 * - A set of fields with command operands, such as the parent and destination.
 * - A file field used to specify uploaded files.
 * - A submit button.
 *
 * This form is hidden and none of its fields are intended to be directly
 * set by a user. Instead, Javascript fills in the form based upon the
 * current row selection, the results of a drag-and-drop, or the file choices
 * from a browser-provided file dialog.
 *
 * Javascript creates a menu button and pull-down menu of commands.
 * Javascript also creates a context menu of commands for table rows.
 * Scripting handles row selection, multi-row selection, double-click to
 * open, drag-and-drop of rows, and drag-and-drop of files from the desktop
 * into the table for uploading.
 *
 * This form *requires* that a view nearby on the page contain a base UI
 * view field plugin that adds selection checkboxes and entity attributes
 * to all rows in the view.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * @ingroup foldershare
 */
class UIFolderBrowserMenu extends FormBase {

  use RedirectDestinationTrait;

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The module handler, set at construction time.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected $moduleHandler;

  /**
   * The plugin command manager.
   *
   * @var \Drupal\foldershare\FolderShareCommand\FolderShareCommandManager
   */
  protected $commandPluginManager;

  /**
   * The form builder.
   *
   * @var \Drupal\Core\Form\FormBuilderInterface
   */
  protected $formBuilder;

  /*--------------------------------------------------------------------
   *
   * Fields - set by menu choice.
   *
   *--------------------------------------------------------------------*/
  /**
   * The current validated command prior to its execution.
   *
   * The command already has its configuration set. It has been
   * validated, but has not yet had access controls checked and it
   * has not been executed.
   *
   * @var \Drupal\foldershare\FolderShareCommand\FolderShareCommandInterface
   */
  protected $command;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new form.
   *
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $moduleHandler
   *   The module handler.
   * @param \Drupal\foldershare\FolderShareCommand\FolderShareCommandManager $pm
   *   The command plugin manager.
   * @param \Drupal\Core\Form\FormBuilderInterface $formBuilder
   *   The form builder.
   */
  public function __construct(
    ModuleHandlerInterface $moduleHandler,
    FolderShareCommandManager $pm,
    FormBuilderInterface $formBuilder) {

    $this->moduleHandler = $moduleHandler;
    $this->commandPluginManager = $pm;
    $this->formBuilder = $formBuilder;

    $this->command = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('module_handler'),
      $container->get('foldershare.plugin.manager.foldersharecommand'),
      $container->get('form_builder')
    );
  }

  /*--------------------------------------------------------------------
   *
   * Command utilities.
   *
   *--------------------------------------------------------------------*/
  /**
   * Returns an array of well known menu command categories.
   *
   * For each entry, the key is the category machine name, and the value
   * is the translated name for the category.
   *
   * @return array
   *   Returns an associative array with category names as keys and
   *   translated strings as values.
   */
  private function getWellKnownMenuCategories() {
    return [
      'open'            => (string) $this->t('Open'),
      'import & export' => (string) $this->t('Import/Export'),
      'close'           => (string) $this->t('Close'),
      'edit'            => (string) $this->t('Edit'),
      'delete'          => (string) $this->t('Delete'),
      'copy & move'     => (string) $this->t('Copy/Move'),
      'save'            => (string) $this->t('Save'),
      'archive'         => (string) $this->t('Archive'),
      'message'         => (string) $this->t('Message'),
      'settings'        => (string) $this->t('Settings'),
      'administer'      => (string) $this->t('Administer'),
    ];
  }

  /**
   * Creates an instance of a command and prevalidates it.
   *
   * Prevalidation checks configuration fields that must be specified at
   * the time the command is invoked in these forms, prior to any additional
   * configuration changes made by a command's own forms. This includes:
   *
   * - Validate the command is allowed.
   * - Validate parent constraints.
   * - Validate selection constraints.
   *
   * The command's validation methods all throw exceptions. These are caught
   * here and converted to form error messages. The command is returned,
   * or NULL if the command could not be created.
   *
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The current form state.
   * @param string $commandClass
   *   The form state entry to which to report command errors.
   * @param string $commandId
   *   The command plugin ID.
   * @param array $configuration
   *   The initial command configuration.
   *
   * @return \Drupal\foldershare\Plugin\FolderShareCommand\FolderShareCommandInterface
   *   Returns an initialized command, or NULL if the command could not be
   *   created. If there were validation errors, form state has been updated.
   */
  private function prevalidateCommand(
    FormStateInterface $formState,
    string $commandClass,
    string $commandId,
    array $configuration) {

    //
    // Get the command
    // ---------------
    // Get the command plugin manager, find the definition, and create an
    // instance of the command.
    if ($this->commandPluginManager === NULL) {
      $formState->setErrorByName(
        $commandClass,
        FormatUtilities::createFormattedMessage(
          $this->t('Missing FolderShare command plugin manager'),
          $this->t('This is a critical site configuration problem. Please report this to the site administrator.')));
      return NULL;
    }

    $commandDef = $this->commandPluginManager->getDefinition($commandId, FALSE);
    if ($commandDef === NULL) {
      $formState->setErrorByName(
        $commandClass,
        FormatUtilities::createFormattedMessage(
          $this->t(
            'Unrecognized FolderShare command ID "@id".',
            [
              '@id' => $commandId,
            ]),
          $this->t('This is probably due to a programming error in the user interface. Please report this to the developers.')));
      return NULL;
    }

    // Create a command instance.
    $command = $this->commandPluginManager->createInstance(
      $commandDef['id'],
      $configuration);

    //
    // Prevalidate
    // -----------
    // Validate command allowed, parent constraints, and selection constraints.
    try {
      $command->validateCommandAllowed();
        $command->validateParentConstraints();
        $command->validateSelectionConstraints();
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $formState->setErrorByName(
        $commandClass,
        $e->getMarkup());
    }
    catch (\Exception $e) {
        $formState->setErrorByName(
        $commandClass,
        $e->getMessage());
    }

      return $command;
  }

  /*--------------------------------------------------------------------
   *
   * Form setup.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return mb_strtolower(str_replace('\\', '_', get_class($this)));
  }

  /*--------------------------------------------------------------------
   *
   * Form build.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $formState = NULL) {
    //
    // Get context attributes
    // ----------------------
    // Get attributes of the current context, including:
    // - the ID of the page entity, if any
    // - the page entity's kind.
    // - the user's access permissions.
    //
    // The page entity ID comes from the form build arguments.
    $args = $formState->getBuildInfo()['args'];

    if (empty($args) === TRUE) {
      $pageEntityId = FolderShareInterface::USER_ROOT_LIST;
    }
    else {
      $pageEntityId = intval($args[0]);
    }

    if ($pageEntityId < 0) {
      // Menu is for a root list page.
      $pageEntity        = NULL;
      $kind              = 'rootlist';
      $disabled          = FALSE;
      $mimeType          = '';
      $filenameExtension = '';

      $perm = FolderShareAccessControlHandler::getRootAccessSummary(
        $pageEntityId);
    }
    else {
      // Menu is for an entity page.
      $pageEntity = FolderShare::load($pageEntityId);
      if ($pageEntity === NULL) {
        // Invalid entity. Revert to root list.
        $kind              = 'rootlist';
        $disabled          = FALSE;
        $pageEntityId      = FolderShareInterface::USER_ROOT_LIST;
        $mimeType          = '';
        $filenameExtension = '';

        $perm = FolderShareAccessControlHandler::getRootAccessSummary(
          $pageEntityId);
      }
      else {
        $kind              = $pageEntity->getKind();
        $disabled          = $pageEntity->isSystemDisabled();
        $mimeType          = $pageEntity->getMimeType();
        $filenameExtension = $pageEntity->getFilenameExtension();

        $perm = FolderShareAccessControlHandler::getAccessSummary($pageEntity);
      }
    }

    // Get the user.
    $user      = $this->currentUser();
    $userId    = (int) $user->id();
    $anonymous = User::getAnonymousUser();

    // Get the command list.
    if ($disabled === TRUE) {
      // No commands are allowed on a disabled entity.
      $commands = [];
    }
    else {
      $commands = Settings::getAllowedCommandDefinitions($pageEntity, $userId);
    }

    // Create an array that lists the names of access control operators
    // (e.g. "view", "update", "delete") if the user has permission for
    // those operators on this parent entity (if there is one).
    $access = [];
    foreach ($perm as $op => $allowed) {
      if ($allowed === TRUE) {
        $access[] = $op;
      }
    }

    //
    // Form setup
    // ----------
    // Add the form's ID as a class for styling.
    //
    // The 'drupalSettings' attribute defines arbitrary data that can be
    // attached to the page. Here we use it to:
    // - Flag whether AJAX is enabled.
    // - Give the ID and human-readable name of this module.
    // - Give translations for various terms.
    // - Give singular and plural translations of entity kinds.
    // - List all installed commands and their attributes.
    $kinds = [
      FolderShare::FOLDER_KIND,
      FolderShare::FILE_KIND,
      FolderShare::IMAGE_KIND,
      FolderShare::MEDIA_KIND,
      FolderShare::OBJECT_KIND,
      'rootlist',
      'item',
    ];

    $kindTerms = [];
    foreach ($kinds as $k) {
      $kindTerms[$k] = [
        'singular' => FolderShare::translateKind($k, MB_CASE_UPPER),
        'plural'   => FolderShare::translateKinds($k, MB_CASE_UPPER),
      ];
    }

    $categories = $this->getWellKnownMenuCategories();

    $isAnonymous = ($user->isAnonymous() === TRUE);
    $isAuthenticated = ($user->isAuthenticated() === TRUE);
    $hasAdmin = ($user->hasPermission(Constants::ADMINISTER_PERMISSION) === TRUE);
    $hasAuthor = ($user->hasPermission(Constants::AUTHOR_PERMISSION) === TRUE);
    $hasView = ($user->hasPermission(Constants::VIEW_PERMISSION) === TRUE);
    $hasShare = ($user->hasPermission(Constants::SHARE_PERMISSION) === TRUE);
    $hasSharePublic = ($user->hasPermission(Constants::SHARE_PUBLIC_PERMISSION) === TRUE);

    $ownerId                     = (-1);
    $ownedByUser                 = FALSE;
    $ownedByAnother              = FALSE;
    $ownedByAnonymous            = FALSE;
    $sharedByUser                = FALSE;
    $sharedWithUserToView        = FALSE;
    $sharedWithUserToAuthor      = FALSE;
    $sharedWithAnonymousToView   = FALSE;
    $sharedWithAnonymousToAuthor = FALSE;

    if ($pageEntity !== NULL) {
      $ownerId = $pageEntity->getOwnerId();
      $anonId = (int) $anonymous->id();

      if ($ownerId === $userId) {
        $ownedByUser = TRUE;
      }
      elseif ($ownerId === $anonId) {
        $ownedByAnonymous = TRUE;
      }
      else {
        $ownedByAnother = TRUE;
      }

      // Since access grants are set on the root only, get the root and
      // use it to determine the sharing state.
      $root = $pageEntity->getRootItem();

      $sharedByUser                = $root->isSharedBy($userId);
      $sharedWithUserToView        = $root->isSharedWith($userId, 'view');
      $sharedWithUserToAuthor      = $root->isSharedWith($userId, 'author');
      $sharedWithAnonymousToView   = $root->isSharedWith($anonId, 'view');
      $sharedWithAnonymousToAuthor = $root->isSharedWith($anonId, 'author');
    }
    elseif ($pageEntityId === FolderShareInterface::USER_ROOT_LIST) {
      $ownerId = $user->id();
      $ownedByUser = TRUE;
    }
    elseif ($pageEntityId === FolderShareInterface::PUBLIC_ROOT_LIST) {
      $ownerId = $anonymous->id();
      $ownedByAnonymous = TRUE;
      $sharedWithAnonymousToView = $anonymous->hasPermission(
        Constants::VIEW_PERMISSION);
      $sharedWithAnonymousToAuthor = $anonymous->hasPermission(
        Constants::AUTHOR_PERMISSION);
    }

    // Get the file upload size limit, modified by module hooks.
    $fileUploadSizeLimit = ManageHooks::callHookFileUploadSizeLimitAlter(
      $pageEntity,
      $ownerId,
      0);

    // Get PHP's number of files limit.
    $fileUploadNumberLimit = LimitUtilities::getPhpFileUploadNumberLimit();

    // Get PHP's POST size limit.
    $postSizeLimit = LimitUtilities::getPhpPostSizeLimit();

      $form['#attached']['drupalSettings']['foldershare'] = [
      'ajaxEnabled'   => Constants::ENABLE_UI_COMMAND_DIALOGS,
      'module'        => [
        'id'          => Constants::MODULE,
        'title'       => $this->moduleHandler->getName(Constants::MODULE),
        'submenuthreshold' => Settings::getCommandMenuSubmenuThreshold(),
        'pollinterval' => Settings::getStatusPollingInterval(),
        'fileuploadsizelimit' => $fileUploadSizeLimit,
        'fileuploadnumberlimit' => $fileUploadNumberLimit,
        'postsizelimit' => $postSizeLimit,
      ],
      'page'          => [
        'id'          => $pageEntityId,
        'kind'        => $kind,
        'mimetype'    => $mimeType,
        'filenameextension' => $filenameExtension,
        'disabled'    => $disabled,
        'ownerid'     => $ownerId,
        'ownedbyuser'                 => $ownedByUser,
        'ownedbyanonymous'            => $ownedByAnonymous,
        'ownedbyanother'              => $ownedByAnother,
        'sharedbyuser'                => $sharedByUser,
        'sharedwithusertoview'        => $sharedWithUserToView,
        'sharedwithusertoauthor'      => $sharedWithUserToAuthor,
        'sharedwithanonymoustoview'   => $sharedWithAnonymousToView,
        'sharedwithanonymoustoauthor' => $sharedWithAnonymousToAuthor,
      ],
      'user'          => [
        'id'          => $user->id(),
        'accountName' => $user->getAccountName(),
        'displayName' => $user->getDisplayName(),
        'pageAccess'  => $access,
        'anonymous'             => $isAnonymous,
        'authenticated'         => $isAuthenticated,
        'adminpermission'       => $hasAdmin,
        'noadminpermission'     => $hasAdmin !== TRUE,
        'authorpermission'      => $hasAuthor,
        'sharepermission'       => $hasShare,
        'sharepublicpermission' => $hasSharePublic,
        'viewpermission'        => $hasView,
      ],
      'terminology'   => [
        'kinds'       => $kindTerms,
        'text'        => [
          'this'      => $this->t('this'),
          'menu'      => $this->t('menu'),
        ],
        'categories'  => $categories,
      ],
      'categories'    => array_keys($categories),
      'commands'      => $commands,
    ];

    //
    // Create UI
    // ---------
    // The UI is primarily built by Javascript based upon the command list
    // and other information in the settings attached to the form above.
    // The remainder of the form contains input fields and a submit button
    // for sending a command's ID and operands back to the server.
    //
    // Set up classes.
    $uiClass            = 'foldershare-folder-browser-menu';
    $submitClass        = $uiClass . '-submit';
    $uploadClass        = $uiClass . '-upload';
    $commandClass       = $uiClass . '-commandname';
    $selectionClass     = $uiClass . '-selection';
    $parentIdClass      = $uiClass . '-parentId';
    $destinationIdClass = $uiClass . '-destinationId';
    $menuButtonClass    = 'foldershare-folder-browser-mainmenu-button';

    if ($disabled === TRUE) {
      // TODO delete this code. It uses the wrong icon.
      $menuButtonUrl = '/' . \Drupal::service('extension.list.module')->getPath('foldershare') .
        '/images/menu-icon.disabled.27x15.png';
    }
    else {
      $menuButtonUrl = '/' . \Drupal::service('extension.list.module')->getPath('foldershare') .
        '/images/menu-file.svg';
    }

    $menuButtonAlt = $this->t('File menu');
    $menuButtonImage = "<img alt=\"$menuButtonAlt\" src=\"$menuButtonUrl\">";

    // When AJAX is enabled, add an AJAX callback to the submit button.
    $submitAjax = '';
    if (Constants::ENABLE_UI_COMMAND_DIALOGS === TRUE) {
      $submitAjax = [
        'callback' => '::submitFormAjax',
        'event'    => 'submit',
      ];
    }

    $form['#attributes']['class'][] = 'foldershare-folder-browser-menu-form';

    $form[$uiClass] = [
      '#type'              => 'container',
      '#weight'            => -100,
      '#attributes'        => [
        'class'            => [$uiClass],
      ],

      // Add a button to show the main menu. A Javascript behavior handles
      // creating the menu and responding to menu choices. The button is
      // hidden initially and only shown if the browser supports Javascript.
      //
      // Drupal work-around: If we use the #type "button" or "image_button",
      // we'll get a button, but not necessarily using HTML's <button>.
      // This varies by theme. The core themes, based on "classy",
      // implement buttons with <input>. Some Bootstrap-based themes
      // implement buttons with <button>, while others do not.
      // Unfortunately, <input> does not support wrapping an <img> for
      // an icon, while <button> does (which makes it odd that core's
      // #type "image_button" still generates <input>, which will never
      // work).
      //
      // To guarantee that we get an <img> on the button, we have to
      // force use of <button>, and that requires using #type "html_tag".
      // Unfortunately, "html_tag" only allows a string as the value
      // within the tag, so we are forced to enter explicit HTML below.
      //
      // A downside of using "html_tag" is that the button does not get
      // processed by core's #type "button" code, a theme's button
      // template pre-processor, or a theme's button template. Since those
      // are responsible for adding button classes, we have to add them
      // here explicitly:
      //
      // - "button", "js-form-submit", and "form-submit" are added by
      //   core #type "button".
      //
      // - "button--image-button" is added by core #type "button" if the
      //   #button_type is "image_button".
      //
      // - "btn" is added by many Bootstrap-based themes.
      //
      // - "w3-button", "w3-border", and "w3-theme-border" are W3.CSS
      //   framework standards.
      //
      // While it is possible to create a custom button template for this
      // kind of button, core's themeing system always cascades from
      // simplest to most custom template, invoking ALL of them. Because
      // the core themes do not define twig blocks around <input>, it is
      // not possible for a custom theme late in the cascade to override
      // the <input> tag created earlier in the cascade. This is why
      // custom themes (like Bootstrap) do not extend the core themes.
      // But here we don't have that option - we have to work with any
      // theme, including core themes, and therefore we cannot use a
      // custom template to override button handling.
      $menuButtonClass     => [
        '#type'            => 'html_tag',
        '#tag'             => 'button',
        '#value'           => $menuButtonImage,
        '#name'            => $menuButtonClass,
        '#attributes'      => [
          'title'          => $this->t('Perform tasks with the selected items.'),
          'class'          => [
            $menuButtonClass,
            'hidden',
            // Core theme classes.
            'button',
            'button--image-button',
            'js-form-submit',
            'form-submit',
            // Bootstrap classes.
            'btn',
            // W3.CSS classes.
            'w3-button',
            'w3-border',
            'w3-theme-border',
            // jQuery.UI. With some themes, these get lost when set by
            // jQuery using button(). Why?
            'ui-button',
            'ui-corner-all',
            'ui-widget',
          ],
        ],
      ],

      // The form acts as a container for Javascript-generated menus and
      // menu buttons. Those items need to be visible, but the form's
      // inputs for sending a command back to the server never need to
      // be visible. These are grouped into a container that is marked hidden.
      'hiddenGroup'        => [
        '#type'            => 'container',
        '#attributes'      => [
          'class'          => ['hidden'],
        ],

        // Add the command plugin ID field to indicate the selected command.
        // Later, when a user selects a command, Javascript sets this field
        // to the command plugin's unique ID.
        //
        // The command is a required input. There's no point in submitting
        // a form without one.
        //
        // Implementation note: There is no documented maximum plugin ID
        // length, but most Drupal IDs are limited to 256 characters. So
        // we use that limit.
        $commandClass        => [
          '#type'            => 'textfield',
          '#maxlength'       => 256,
          '#size'            => 1,
          '#default_value'   => '',
          '#required'        => TRUE,
        ],

        // Add the selection field that lists the IDs of selected entities.
        // Later, when a user selects a command, Javascript sets this field
        // to a list of entity IDs for zero or more items currently selected
        // from the view.
        //
        // The selection is optional. Some commands have no selection (such
        // as "New folder" and "Upload files").
        //
        // Implementation note: The textfield will be set with a JSON-encoded
        // string containing the list of numeric entity IDs for selected
        // entities in the view. There is no maximum view length if the site
        // disables paging. Drupal's default field maximum is 128 characters,
        // which is probably sufficient, but it is conceivable it could be
        // exceeded for a large selection. The HTML default maximum is
        // 524288 characters, so we use that.
        $selectionClass      => [
          '#type'            => 'textfield',
          '#maxlength'       => 524288,
          '#size'            => 1,
          '#default_value'   => $formState->getValue($selectionClass),
        ],

        // Add the parent field that gives the parent ID of a file or folder.
        //
        // The parent ID is not required, though Javascript always sets it.
        // If not set, the entity ID of the current page is used. This is
        // typical for most command use. Drag-and-drop operations, however,
        // may move/copy/upload items into a selected subfolder, and that
        // subfolder's parent ID is set in this field.
        //
        // Implementation note: The textfield may be left empty or set to
        // a single numeric entity ID. Since entity IDs are 64 bits, the
        // maximum number of characters here is 20 digits.
        $parentIdClass    => [
          '#type'            => 'textfield',
          '#maxlength'       => 24,
          '#size'            => 1,
          '#default_value'   => $formState->getValue($parentIdClass),
        ],

        // Add the destination field that gives the entity ID of a
        // destination folder for move/copy operations. Later, when a user
        // selects a command, Javascript may set this field if the destination
        // is known. If the field is left empty, the command may prompt for
        // the move/copy destination.
        //
        // The destination ID field is optional. Most commands don't use it.
        //
        // Implementation note: The textfield may be left empty or set to
        // a single numeric entity ID. Since entity IDs are 64 bits, the
        // maximum number of characters here is 20 digits.
        $destinationIdClass    => [
          '#type'            => 'textfield',
          '#maxlength'       => 24,
          '#size'            => 1,
          '#default_value'   => $formState->getValue($destinationIdClass),
        ],

        // Add the file field for uploading files. Later, when a user
        // selects a command that needs to upload a file, Javascript invokes
        // the browser's file dialog to set this field.
        //
        // The field needs to have a processing callback to set up file
        // extension filtering, if file extension limitations are enabled
        // for the module.
        //
        // The upload field is optional and it is only used by file upload
        // commands.
        $uploadClass         => [
          '#type'            => 'file',
          '#multiple'        => TRUE,
          '#attributes'      => [
            'accept'         => $this->getFilenameExtensions($pageEntity, $userId),
          ],
        ],

        // Add the submit button for the form. Javascript triggers the
        // submit when a command is selected from the menu.
        $submitClass         => [
          '#type'            => 'submit',
          '#value'           => '',
          '#ajax'            => $submitAjax,
        ],
      ],
    ];
    return $form;
  }

  /**
   * Returns the filename extensions accepted for file uploads.
   *
   * @param \Drupal\foldershare\FolderShareInterface $parent
   *   The parent entity to contain uploaded files, or NULL for the root list.
   * @param int $userId
   *   The user ID.
   *
   * @return string
   *   Returns a comma-separated list of filename extensions, with leading
   *   dots. If there are no filename extension restrictions, an empty string
   *   is returned.
   */
  private function getFilenameExtensions(
    FolderShareInterface $parent = NULL,
    int $userId = (-1)) {

    // Call module hooks to get a list of allowed filename extensions.
    $extensions = ManageHooks::callHookAllowedFilenameExtensionsAlter(
      $parent,
      $userId,
      []);

    if (empty($extensions) === TRUE) {
      return '';
    }

    // The extensions list does not have leading dots. But the form
    // needs them.
    $list = [];
    foreach ($extensions as $ext) {
      $list[] = '.' . $ext;
    }

    return implode(',', $list);
  }

  /*--------------------------------------------------------------------
   *
   * Form validate.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $formState) {
    //
    // Setup
    // -----
    // Set up classes.
    $uiClass            = 'foldershare-folder-browser-menu';
    $commandClass       = $uiClass . '-commandname';
    $selectionClass     = $uiClass . '-selection';
    $parentIdClass      = $uiClass . '-parentId';
    $destinationIdClass = $uiClass . '-destinationId';
    $uploadClass        = $uiClass . '-upload';

    //
    // Get parent ID (if any)
    // ----------------------
    // The parent entity ID, if present, is the sole URL argument.
    $args = $formState->getBuildInfo()['args'];
    if (empty($args) === TRUE) {
      $parentId = FolderShareInterface::USER_ROOT_LIST;
    }
    else {
      $parentId = intval($args[0]);
      // The parent ID may still be negative, indicating a root list.
    }

    // If a parent ID is in the form, it overrides the URL, which describes
    // the page the operation began on, and not necessary the context in
    // which it should take place.
    $formParentId = $formState->getValue($parentIdClass);
    if (empty($formParentId) === FALSE) {
      $parentId = intval($formParentId);
      // The parent ID may still be negative, indicating a root list.
    }

    //
    // Get command
    // -----------
    // The command's plugin ID is set in the command field. Get it and
    // the command definition.
    $commandId = $formState->getValue($commandClass);

      if (empty($commandId) === TRUE) {
      // Fail. This should never happen. The field is required, so the form
      // should not be submittable without a command.
      $formState->setErrorByName(
        $commandClass,
        $this->t('Please select a command from the menu.'));
      return;
    }

    //
    // Get selection (if any)
    // ----------------------
    // The selection field contains a JSON encoded array of entity IDs
    // in the selection. The list could be empty.
    $selectionIds = json_decode($formState->getValue($selectionClass), TRUE);

    //
    // Get destination (if any)
    // ------------------------
    // The destination field contains a single numeric entity ID for the
    // destination of a move/copy. The value could be empty.
    $destinationId = $formState->getValue($destinationIdClass);
    if (empty($destinationId) === TRUE) {
      $destinationId = FolderShareCommandInterface::EMPTY_ITEM_ID;
    }
    else {
      $destinationId = intval($destinationId);
    }

      //
    // Create configuration
    // --------------------
    // Create an initial command configuration.
    $configuration = [
      'parentId'      => $parentId,
      'selectionIds'  => $selectionIds,
      'destinationId' => $destinationId,
      'uploadClass'   => $uploadClass,
    ];
    \Drupal::moduleHandler()->invokeAll('foldershare_alter_uifolderbrowsermenu_form_configuration', [$commandId, &$configuration, &$form, $formState]);

      //
    // Prevalidate
    // -----------
    // Create a command instance. Prevalidate and add errors to form state.
    // If the command could not be created, a NULL is returned and a message
    // is already in form state.
    $command = $this->prevalidateCommand(
      $formState,
      $commandClass,
      $commandId,
      $configuration);

      if ($command !== NULL) {
          $this->command = $command;
      }
  }

  /*--------------------------------------------------------------------
   *
   * Form submit (no-AJAX).
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $formState) {
    if (Constants::ENABLE_UI_COMMAND_DIALOGS === TRUE) {
      // AJAX is in use. Let the AJAX submit method handle it.
        return;
    }

    if ($this->command === NULL) {
      return;
    }

    //
    // Pre-execute redirect to another page, if required.
    // --------------------------------------------------
    // If the command needs to redirect to a special page (such as an edit
    // form), redirect instead of executing.
    $executeBehavior = $this->command->getExecuteBehavior();
    if ($executeBehavior === FolderShareCommandInterface::PRE_EXECUTE_PAGE_REDIRECT) {
      // Get the URL to redirect to. If the URL is empty (it shouldn't be),
      // then fall back to teh current page.
      $url = $this->command->getExecuteRedirectUrl();
      if (empty($url) === TRUE) {
        $url = Url::fromRoute('<current>');
      }

      // Set the redirect.
      $formState->setRedirectUrl($url);
      return;
    }

    //
    // Redirect to command's form, if required.
    // ----------------------------------------
    // If the command has its own configuration form, redirect to a page that
    // hosts the form. This is common and can be a form as simple as a
    // delete command's "Are you sure?" prompt.
    //
    // If the command has any post-execute behaviors, they will be handled
    // by the form.
    if ($this->command->hasConfigurationForm() === TRUE) {
      try {
        $parameters = [
          'pluginId'      => $this->command->getPluginId(),
          'configuration' => $this->command->getConfiguration(),
          'url'           => $this->getRequest()->getUri(),
          'enableAjax'    => Constants::ENABLE_UI_COMMAND_DIALOGS,
        ];

        // Set the redirect.
        $formState->setRedirect(
          'entity.foldersharecommand.plugin',
          [
            'encoded' => base64_encode(json_encode($parameters)),
          ]);
        return;
      }
      catch (RuntimeExceptionWithMarkup $e) {
        $this->messenger()->addMessage($e->getMarkup(), 'error');
      }
      catch (\Exception $e) {
        $this->messenger()->addMessage($e->getMessage(), 'error');
        return;
      }
    }

    //
    // Execute directly.
    // -----------------
    // The command doesn't need a redirect to a special page or to a
    // configuration form hosting page. Execute it directly.
    try {
      $this->command->validateConfiguration();
      $this->command->execute();
    }
    catch (AccessDeniedHttpException $e) {
      $this->messenger()->addMessage(FormatUtilities::createFormattedMessage(
        $this->t('You do not have sufficient permissions.'),
        $this->t('The operation could not be completed.')),
        'error');
    }
    catch (NotFoundHttpException $e) {
      $this->messenger()->addMessage(FormatUtilities::createFormattedMessage(
        $this->t('One or more items could not be found.'),
        $this->t('The operation could not be completed.')),
        'error');
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $this->messenger()->addMessage($e->getMarkup(), 'error');
    }
    catch (\Exception $e) {
      $this->messenger()->addMessage($e->getMessage(), 'error');
    }

    //
    // Post execute.
    // -------------
    // After the command has executed, handle its post-execute behavior.
    $executeBehavior = $this->command->getExecuteBehavior();
    switch ($executeBehavior) {
      case FolderShareCommandInterface::POST_EXECUTE_PAGE_REDIRECT:
        // Redirect to a new page.
        $url = $this->command->getExecuteRedirectUrl();
        if (empty($url) === TRUE) {
          // No URL? Fall through and let the current page refresh.
          break;
        }

        // Set the redirect.
        $formState->setRedirectUrl($url);
        break;

      case FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH:
      case FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH:
        // Page or view refresh. Since this submit form is only used
        // when there is no AJAX enabled, there is no way to do a
        // view refresh separate from a page refresh. And a page
        // refresh happens automatically by simply returning.
        break;
    }

    $this->command = NULL;
  }

  /*--------------------------------------------------------------------
   *
   * Form submit (AJAX)
   *
   *--------------------------------------------------------------------*/
  /**
   * Handles form submission via AJAX.
   *
   * If the selected command has no configuration form, the command is
   * executed immediately. Any errors are reported in a modal error dialog.
   *
   * If the selected command requires a redirect to a full-page form, the
   * redirect is executed immediately.
   *
   * Otherwise, when the selected command has a configuration form, the
   * form is built and added to a modal dialog.
   *
   * @param array $form
   *   An array of form elements.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The input values for the form's elements.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   */
  public function submitFormAjax(array &$form, FormStateInterface $formState) {
    if ($this->command === NULL) {
      // Errors that would cause this are catastrophic and will float to
      // the page's messages area.
        return AjaxUtilities::createRefreshResponse();
    }

    // If prevalidation failed, there will be form errors to report.
    if ($formState->hasAnyErrors() === TRUE) {
        return AjaxUtilities::createFormErrorsDialogResponse($formState);
    }

    // If the command needs to redirect to a special page (such as an edit
    // form), redirect instead of executing.
    $executeBehavior = $this->command->getExecuteBehavior();
    if ($executeBehavior === FolderShareCommandInterface::PRE_EXECUTE_PAGE_REDIRECT) {
      $url = $this->command->getExecuteRedirectUrl();
      return AjaxUtilities::createRedirectResponse($url);
    }

    // If the command has its own configuration form, show a modal dialog
    // with the form. Command execution is handled by the form's submit.
    if ($this->command->hasConfigurationForm() === TRUE) {
      return $this->respondWithConfigurationDialog();
    }

    // The command doesn't need a redirect to a special page or present
    // a configuration form. Execute it directly.
    return $this->respondWithCommandExecution();
  }

  /**
   * Returns an AJAX response that shows a command configuration form dialog.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   */
  private function respondWithConfigurationDialog() {
    //
    // Set up and encode parameters.
    // -----------------------------
    // A list of parameters will be sent to the configuration form.
    // Set them up then encode them to send to the form.
    $parameters = [
      'pluginId'      => $this->command->getPluginId(),
      'configuration' => $this->command->getConfiguration(),
      'enableAjax'    => Constants::ENABLE_UI_COMMAND_DIALOGS,
      'url'           => AjaxUtilities::stripAjaxFromUrl(NULL),
      'parentFormId'  => str_replace('_', '-', $this->getFormId()),
    ];

    $encoded = base64_encode(json_encode($parameters));

    //
    // Create the form.
    // ----------------
    // Build a form for the command. The returned form may have
    // prompts, buttons, attachments, etc.
    $form = $this->formBuilder->getForm(
      CommandFormWrapper::class,
      $encoded);

    if ($form === NULL) {
      // Form build failed, probably due to validation errors.
      return AjaxUtilities::createMessengerErrorsDialogResponse();
    }

    if (isset($form['#title']) === TRUE) {
      $title = (string) $form['#title'];
    }
    else {
      $title = $this->command->getPluginDefinition()['label'];
    }

    // Return the form within a modal dialog.
    $dialog = new OpenModalDialogCommand(
      $title,
      $form,
      [
        'modal'             => TRUE,
        'draggable'         => FALSE,
        'resizable'         => FALSE,
        'autoResize'        => FALSE,
        'refreshAfterClose' => FALSE,
        'closeOnEscape'     => TRUE,
        'closeText'         => $this->t('Cancel and close'),
        'width'             => '75%',
        'minHeight'         => 0,
        'show'              => [
          'duration'        => 150,
          'effect'          => 'slideDown',
        ],
        'classes'           => [
          'ui-dialog'       => [
            'foldershare-ui-dialog',
          ],
        ],
      ]);

    $response = new AjaxResponse();
    $response->setAttachments($form['#attached']);
    $response->addCommand($dialog);
    return $response;
  }

  /**
   * Executes the command and returns an AJAX response with the results.
   *
   * The command is validated and executed. If there are any errors, they
   * are shown in an error dialog. Otherwise the page is refreshed or
   * redirected per the command's needs.
   *
   * @return \Drupal\Core\Ajax\AjaxResponse
   *   Returns an AJAX response.
   */
  private function respondWithCommandExecution() {
    // Clear any pending messenger messages. While there should not be any,
    // if there are they will infiltrate any error dialog shown below. Since
    // we only want those dialogs to show errors with the command about to
    // be executed, clear pending messenger messages first.
    $this->messenger()->deleteAll();

    // Validate the command and execute it. This will throw exceptions if
    // validation or execution fails.
    try {
      $this->command->validateConfiguration();
      $this->command->execute();
    }
    catch (AccessDeniedHttpException $e) {
      $this->messenger()->addError(FormatUtilities::createFormattedMessage(
        $this->t('You do not have sufficient permissions.'),
        $this->t('The operation could not be completed.')));
    }
    catch (NotFoundHttpException $e) {
      $this->messenger()->addError(FormatUtilities::createFormattedMessage(
        $this->t('One or more items could not be found.'),
        $this->t('The operation could not be completed.')));
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $this->messenger()->addError($e->getMarkup());
    }
    catch (\Exception $e) {
      $this->messenger()->addError($e->getMessage());
    }

    // If there are any messenger messages, show them in an error dialog.
    $msgs = $this->messenger()->all();
    if (isset($msgs[MessengerInterface::TYPE_ERROR]) === TRUE ||
        isset($msgs[MessengerInterface::TYPE_WARNING]) === TRUE) {
      $this->command = NULL;
      return AjaxUtilities::createMessengerErrorsDialogResponse();
    }

    // After command execution, redirect or refresh as the command requires.
    $executeBehavior = $this->command->getExecuteBehavior();
    if (isset($msgs[MessengerInterface::TYPE_STATUS]) === TRUE) {
      // The command posted a status message. If it asked for a view refresh
      // after execution, change it to a page refresh so that the page's
      // status area gets refreshed and shows the status message.
      if ($executeBehavior === FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH) {
        $executeBehavior = FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH;
      }
    }

    switch ($executeBehavior) {
      case FolderShareCommandInterface::POST_EXECUTE_PAGE_REDIRECT:
        // Redirect to a new page.
        $url = $this->command->getExecuteRedirectUrl();
        $this->command = NULL;
        return AjaxUtilities::createRedirectResponse($url);

      default:
      case FolderShareCommandInterface::POST_EXECUTE_PAGE_REFRESH:
        // Refresh the current page.
        $this->command = NULL;
        return AjaxUtilities::createRefreshResponse();

      case FolderShareCommandInterface::POST_EXECUTE_VIEW_REFRESH:
        // Refresh the view associated with the menu the command came from.
        $response = new AjaxResponse();
        $selector = '#' . str_replace('_', '-', $this->getFormId());
        $method   = 'trigger';
        $args     = [
          'FolderShareRefreshView',
        ];

        $this->command = NULL;
        $response->addCommand(new InvokeCommand($selector, $method, $args));
        return $response;
    }
  }

}
