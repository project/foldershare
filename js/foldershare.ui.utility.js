/**
 * @file
 * Implements the FolderShare utility functions.
 *
 * The utility functions are shared among multiple UI scripts for the module.
 * They provide string handling and error message printing.
 *
 * @ingroup foldershare
 */
(($, Drupal) => {
  // Define Drupal.foldershare if it hasn't been defined yet.
  if ("foldershare" in Drupal === false) {
    Drupal.foldershare = {};
  }

  Drupal.foldershare.utility = {
    /*--------------------------------------------------------------------
     *
     * String utilities.
     *
     *--------------------------------------------------------------------*/

    /**
     * Returns the title-case form of a string.
     *
     * @param {string} text
     *   The string to convert to title-case.
     *
     * @return {string}
     *   Returns the converted string.
     */
    getTitleCase(text) {
      return text.replace(
        /\w\S*/g,
        t => t.charAt(0).toUpperCase() + t.substr(1).toLowerCase()
      );
    },

    /**
     * Returns the translated singular entity kind name in title-case.
     *
     * The kind name (e.g. "file", "folder", etc.) is looked up in a list
     * of translated kinds provided by the server. The value is converted to
     * singular title-case and returned.
     *
     * @param {object} terminology
     *   A terminology object containing a 'kinds' property that is an array
     *   with kind name keys. Each entry in the array is an object with
     *   'plural' and 'singular' properties that provide the translated
     *   plural and singular forms of the kind name.
     * @param {string} kind
     *   The kind name to look up.
     *
     * @return {string}
     *   The title-case translated singular kind.
     */
    getKindSingular(terminology, kind) {
      if (
        "kinds" in terminology === true &&
        kind in terminology.kinds === true
      ) {
        kind = terminology.kinds[kind].singular;
      }

      return Drupal.foldershare.utility.getTitleCase(kind);
    },

    /**
     * Returns the translated plural entity kind name in title-case.
     *
     * The kind name (e.g. "file", "folder", etc.) is looked up in a list
     * of translated kinds provided by the server. The value is converted to
     * plural title-case and returned.
     *
     * @param {object} terminology
     *   A terminology object containing a 'kinds' property that is an array
     *   with kind name keys. Each entry in the array is an object with
     *   'plural' and 'singular' properties that provide the translated
     *   plural and singular forms of the kind name.
     * @param {string} kind
     *   The kind name to look up.
     *
     * @return {string}
     *   The title-case translated plural kind.
     */
    getKindPlural(terminology, kind) {
      if (
        "kinds" in terminology === true &&
        kind in terminology.kinds === true
      ) {
        kind = terminology.kinds[kind].plural;
      }

      return Drupal.foldershare.utility.getTitleCase(kind);
    },

    /**
     * Returns the translated term in title-case or lower-case.
     *
     * The term is looked up in the list of translated terms provided by
     * the server. The term is converted to title case and returned.
     *
     * @param {object} terminology
     *   A terminology object containing a 'text' property that is an array
     *   with strings as keys, and the translated form of the string as
     *   values.
     * @param {string} term
     *   The term to look up.
     * @param {boolean} titleCase
     *   (optional, default = true) When true, the returned term uses
     *   title-case. When false, it is entirely lower case.
     *
     * @return {string}
     *   The title case translated term.
     */
    getTerm(terminology, term, titleCase = true) {
      // Find the translated singular or plural term.
      if ("text" in terminology === true && term in terminology.text === true) {
        term = terminology.text[term];
      }

      if (titleCase === true) {
        return Drupal.foldershare.utility.getTitleCase(term);
      }

      // Map to lower case.
      term = term.toLowerCase();

      return term;
    },

    /*--------------------------------------------------------------------
     *
     * Print utilities.
     *
     *--------------------------------------------------------------------*/

    /**
     * Prints a malformed page error message to the console.
     *
     * @param {string} body
     *   The body of the message.
     */
    printMalformedError(body = "") {
      Drupal.foldershare.utility.printMessage(
        "Malformed page",
        `${body} The user interface cannot be enabled.`
      );
    },

    /**
     * Prints a message to the console.
     *
     * The title is printed in bold, followed by optional body text
     * in a normal weight font, indented below the title.
     *
     * @param {string} title
     *   The short title of the message.
     * @param {string} body
     *   The body of the message.
     */
    printMessage(title, body = "") {
      console.log(
        `%cFolderShare: ${title}:%c\n%c${body}%c`,
        "font-weight: bold",
        "font-weight: normal",
        "padding-left: 2em",
        "padding-left: 0"
      );
    },

    /*--------------------------------------------------------------------
     *
     * Dialog utilities.
     *
     *--------------------------------------------------------------------*/
    /**
     * Presents a modal error dialog.
     *
     * @param {string} summary
     *   The dialog summary text.
     * @param {string} body
     *   The dialog body text.
     */
    showErrorDialog(summary, body) {
      const text = `<div class="foldershare-error-dialog-body"><span class="foldershare-message-summary">${summary}</span><p class="foldershare-message-body">${body}</p></div>`;

      Drupal.dialog(text, {
        title: "Problem",
        modal: true,
        draggable: false,
        resizable: false,
        refreshAfterClose: false,
        closeOnEscape: true,
        closeText: "Done",
        width: "75%",

        // Add classes for compatibility with Drupal-created dialogs.
        classes: {
          "ui-dialog": "foldershare-ui-dialog",
          "ui-dialog-buttonset": "form-actions"
        },

        buttons: [
          {
            text: "Done",
            class:
              "dialog-cancel btn button button--primary js-form-submit form-submit",
            click: function (element) {
              Drupal.dialog(this).close();
            }
          }
        ]
      }).showModal();
    }
  };

  /*--------------------------------------------------------------------
   *
   * On Drupal ready behaviors.
   *
   * Set up behaviors to execute when the page is fully loaded, or whenever
   * AJAX sends a new page fragment.
   *
   *--------------------------------------------------------------------*/

  Drupal.behaviors.foldershare_utility = {
    attach(pageContext, settings) {
      /*
       * This is a terrible hack to get around a poorly implemented
       * feature of Drupal themes that load jQuery.UI themes (e.g. Bartik).
       * When the jQuery.UI theme is loaded, it sets page colors, but then
       * it sets dialogs and anything marked with "ui-widget-content" to
       * use a *different* link color, font, etc. than the page. Why?
       * This makes dialogs not look like other content, which is surely
       * not good user interface design. A link should look the same
       * whether it is on a page or in a dialog.
       *
       * It is not really possible in module CSS to override a jQuery.UI theme
       * loaded by a Drupal theme. While the anchor color could be forced
       * to something new, it cannot be reverted to whatever the theme is
       * using for page anchors. CSS color variables would help here, except
       * that themes rarely set them and there are no standards. This leaves
       * us no way to restore the link color via CSS. Nor can we reliably
       * restore the font, text color, or anything else.
       *
       * The "fix" is to get rid of the "ui-widget-content" class in the
       * first place. Since this class is added at runtime when jQuery.UI
       * creates a dialog, we have to remove the class at that time as well.
       * Thus, we have to do this with Javascript code here.
       *
       * Below we listen to the Drupal dialog creator's "dialog:aftercreate"
       * event triggered on the window each time a dialog is created. If the
       * created dialog is for the FolderShare module (and nobody else),
       * then remove the "ui-widget-content" class on it and the modal
       * descendant for the dialog body.
       */
      $(window).on("dialog:aftercreate", () => {
        $(".foldershare-ui-dialog.ui-widget-content", document).each(
          (index, element) => {
            // Remove problem classes from the outer dialog <div> and
            // the inner dialog <div> (for the dialog body).
            $(element).removeClass("ui-widget-content");
            $(element).removeClass("ui-widget");
            $("#drupal-modal.ui-widget-content", element)
              .removeClass("ui-widget-content");
          });
      });
    }
  };
})(jQuery, Drupal);
