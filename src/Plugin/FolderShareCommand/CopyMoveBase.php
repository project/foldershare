<?php

namespace Drupal\foldershare\Plugin\FolderShareCommand;

use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;


use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\Exception\RuntimeExceptionWithMarkup;
use Drupal\foldershare\Entity\Exception\ValidationException;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\FolderShareInterface;
use Drupal\foldershare\Form\UIAncestorMenu;
use Drupal\foldershare\ManageLog;
use Drupal\foldershare\Entity\Controller\FolderShareViewController;

/**
 * Defines a command plugin base class to copy or move files and folders.
 *
 * @ingroup foldershare
 */
abstract class CopyMoveBase extends FolderShareCommandBase {

  /*--------------------------------------------------------------------
   *
   * Fields - dependency injection.
   *
   *--------------------------------------------------------------------*/
  /**
   * The title of the all items root list, set at construction time.
   *
   * @var string
   */
  protected $rootItemsAllTitle;

  /**
   * The title of the public items root list, set at construction time.
   *
   * @var string
   */
  protected $rootItemsPublicTitle;

  /*--------------------------------------------------------------------
   *
   * Construction.
   *
   *--------------------------------------------------------------------*/
  /**
   * Constructs a new plugin with the given configuration.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container from which to get services.
   * @param array $configuration
   *   The array of named parameters for the new command instance.
   * @param string $pluginId
   *   The ID for the new plugin instance.
   * @param mixed $pluginDefinition
   *   The plugin's implementation definition.
   */
  public function __construct(
    ContainerInterface $container,
    array $configuration,
    $pluginId,
    $pluginDefinition) {

    parent::__construct(
      $container,
      $configuration,
      $pluginId,
      $pluginDefinition);

    $routeProvider = $container->get('router.route_provider');
    $titleResolver = $container->get('title_resolver');

    $this->rootItemsAllTitle = $titleResolver->getTitle(
      new Request(),
      $routeProvider->getRouteByName(Constants::ROUTE_ROOT_ITEMS_ALL));

    $this->rootItemsPublicTitle = $titleResolver->getTitle(
      new Request(),
      $routeProvider->getRouteByName(Constants::ROUTE_ROOT_ITEMS_PUBLIC));

  }

  /*--------------------------------------------------------------------
   *
   * Configuration form.
   *
   *--------------------------------------------------------------------*/
  /**
   * {@inheritdoc}
   */
  public function hasConfigurationForm() {
    // Copy and move both require a destination ID. If there isn't one
    // yet, then a configuration form is required.
    return ($this->getDestinationId() === self::EMPTY_ITEM_ID);
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(
    array $form,
    FormStateInterface $formState) {

    //
    // Validate destination.
    // ---------------------
    // Get the destination ID from the form state, or use a default.
    $destinationId = NULL;

    // If there is prior form state, and it includes a destination ID,
    // then get it. The ID might still be NULL, or it might be a word
    // naming a root list.
    if ($formState !== NULL) {
      $userInput = $formState->getUserInput();
      if (isset($userInput['destinationid']) === TRUE) {
        $destinationId = $userInput['destinationid'];
        // The destination ID coming back from the UI could be an integer
        // ID, or one of several words like "home", "shared", "public",
        // and "all", to indicate specific root lists.
      }
    }

    // If the destination ID is still NULL, then default to the current parent.
    if ($destinationId === NULL) {
      $destinationId = $this->getParentId();
      if ($destinationId < 0) {
        $destinationId = FolderShareInterface::USER_ROOT_LIST;
      }
    }

    // Convert special root list names to negative numbers that flag the
    // equivalent root lists. This lets us pass around and compare integers,
    // rather than consider string values as well.
    $viewName = Constants::VIEW_LISTS;
    switch ($destinationId) {
      case FolderShareInterface::USER_ROOT_LIST:
      case 'home':
        $destinationId = FolderShareInterface::USER_ROOT_LIST;
        $displayName = Constants::VIEW_DISPLAY_DIALOG_PERSONAL;
        break;

      case FolderShareInterface::SHARED_ROOT_LIST:
      case 'shared':
        $destinationId = FolderShareInterface::SHARED_ROOT_LIST;
        $displayName = Constants::VIEW_DISPLAY_DIALOG_SHARED;
        break;

      case FolderShareInterface::PUBLIC_ROOT_LIST:
      case 'public':
        // The public root list, and its children, is never an allowed
        // destination. Public items are created as shared-with-anonymous
        // and then managed via the home folder by the owner and those
        // the owner allows author access.
        //
        // So, treat 'public' as 'home'.
        $destinationId = FolderShareInterface::USER_ROOT_LIST;
        $displayName = Constants::VIEW_DISPLAY_DIALOG_PERSONAL;
        break;

      case FolderShareInterface::ALL_ROOT_LIST:
      case 'all':
        $destinationId = FolderShareInterface::ALL_ROOT_LIST;
        $displayName = Constants::VIEW_DISPLAY_DIALOG_ALL;
        break;

      default:
        $destinationId = (int) $destinationId;
        $displayName = Constants::VIEW_DISPLAY_DIALOG_FOLDER;
        if ($destinationId < 0) {
          $destinationId = FolderShareInterface::USER_ROOT_LIST;
          $displayName = Constants::VIEW_DISPLAY_DIALOG_PERSONAL;
        }
        break;
    }

    // And save the destination ID.
    $this->setDestinationId($destinationId);

    //
    // Set up view.
    // ------------
    // Find the embedded view and display, confirming that both exist and
    // that the user has access. Log errors if something is wrong.
    $error = FALSE;

    try {
      // The returned view executable is not needed here because we
      // use the view and display names below in a view field.
      FolderShareViewController::getViewExecutable($viewName, $displayName, TRUE);
    }
    catch (ValidationException $e) {
      // All validation exceptions are serious problems.
      ManageLog::critical($e->getMessage());
      $error = TRUE;
    }
    catch (AccessDeniedHttpException $e) {
      throw $e;
    }

    $form['#attached']['library'][] = 'foldershare/foldershare.folderselectiondialog';

    //
    // Disable inline form errors.
    // ---------------------------
    // When the core Inline Form Errors module is enabled, it attempts to
    // distributed error messages to immediately follow the form fields they
    // relate to. While this can be a good thing, for this dialog it causes
    // errors to be inserted oddly.
    //
    // To disable this strangeness, the Inline Form Errors module's behavior
    // is diabled for the entire dialog.
    $form['#disable_inline_form_errors'] = TRUE;

    // If the view could not be found, there is nothing to embed and there
    // is no point in adding a UI. Return an error message in place of the
    // view's content.
    if ($error === TRUE) {
      $form['destinationselector'] = [
        '#attributes' => [
          'class'   => [
            'foldershare-error',
          ],
        ],

        // Do not cache this page. If any of the above conditions change,
        // the page needs to be regenerated.
        '#cache' => [
          'max-age' => 0,
        ],

        '#weight'   => 10,

        'error'     => [
          '#type'   => 'item',
          '#markup' => $this->t(
            "The website has encountered an administrator configuration problem with this page.\nPlease report this to the website administrator."),
        ],
      ];
      return $form;
    }

    //
    // Build view.
    // -----------
    // Add an embedded view to show the destination folder.
    //
    // Include hidden fields containing the current destination ID,
    // and the current destination selection ID. Add a hidden refresh
    // button that, when clicked by Javascript, triggers use of the
    // destination ID to create a new folder list.
    $form['foldershare-folder-selection-title'] = [
      '#type'               => 'label',
      '#title'              => $this->t('Select a destination folder:'),
    ];

    $form['foldershare-folder-selection'] = [
      '#type'               => 'container',
      '#name'               => 'foldershare-folder-selection',
      '#weight'             => 10,
      '#attributes'         => [
        'class'             => [
          'foldershare-folder-selection',
          'foldershare-toolbar-and-folder-browser',
        ],
      ],

      // Add prefix/suffix so that this entire section of the content is
      // replaced whenever the view needs to be refreshed for a new
      // destination.
      '#prefix'             => '<div id="foldershare-refresh"',
      '#suffix'             => '</div>',

      // Do not cache this. If anybody adds or removes a folder or changes
      // sharing, the view will change and this needs to be regenerated.
      '#cache'              => [
        'max-age'           => 0,
      ],

      'hiddengroup'         => [
        '#type'             => 'container',
        '#attributes'       => [
          'class'           => [
            'hidden',
          ],
        ],

        // Include a hidden text field filled in by Javascript as the user
        // selects new destination folders by double-clicks or selecting from
        // the ancestor menu.
        'destinationid'     => [
          '#type'           => 'textfield',
          '#name'           => 'destinationid',
          '#default_value'  => $destinationId,
        ],

        // Include a hidden refresh button "clicked" by Javascript each time the
        // destination ID above is changed. Below we add AJAX callbacks
        // to trigger a refresh of this part of the form.
        'refresh'           => [
          '#type'           => 'button',
          '#name'           => 'refresh',
          '#value'          => 'Refresh',
        ],

        // Include a hidden text field filled in by Javascript if the user
        // selects, but does not double-click, a folder in the list. This
        // becomes the selected destination. If nothing is selected, then
        // the parent of the current view is the destination.
        'selectionid'       => [
          '#type'           => 'textfield',
          '#name'           => 'selectionid',
          '#default_value'  => self::EMPTY_ITEM_ID,
        ],
      ],

      // Include a toolbar with an ancestor menu like that found on
      // regular folder lists. Javascript adjusts the ancestor menu
      // so that it selects destination folders rather than jumping
      // to a new page.
      'toolbar'             => [
        '#type'             => 'container',
        '#attributes'       => [
          'class'           => [
            'foldershare-toolbar',
          ],
        ],

        'ancestormenu'      => UIAncestorMenu::build($destinationId, FALSE),
      ],

      // Add the view showing the destination folder.
      'view'                => [
        '#type'             => 'view',
        '#name'             => $viewName,
        '#embed'            => TRUE,
        '#display_id'       => $displayName,
        '#arguments'        => [$destinationId],
        '#attributes'       => [
          'autofocus'       => 'autofocus',
          'class'           => [
            'foldershare-folder-selection-table',
          ],
        ],
      ],
    ];

    // When AJAX is in use, copy the AJAX configuration from the submit
    // button and use it for the refresh button as well.
    //
    // @todo What to do if AJAX is not in use?
    if (isset($form['actions']['submit']['#ajax']) === TRUE) {
      // Copy the main dialog AJAX configuration. This gets us the right
      // submit URL, etc.
      $form['foldershare-folder-selection']['hiddengroup']['refresh']['#ajax'] =
        $form['actions']['submit']['#ajax'];

      // Override the submit callback since we don't want to submit the
      // form on a refresh.
      $form['foldershare-folder-selection']['hiddengroup']['refresh']['#ajax']['callback'] =
        [$this, 'refreshConfigurationFormAjax'];

      // Set the wrapper for the part of the dialog to refresh.
      $form['foldershare-folder-selection']['hiddengroup']['refresh']['#ajax']['wrapper'] =
        'foldershare-refresh';
    }

    //
    // Handle messages.
    // ----------------
    // This form is intended to select a folder. This is not the place to
    // report warnings and status messages. Only those messages relevant to
    // this form should be shown.
    //
    // So, delete pending status and warning messages. If we don't do this,
    // Drupal's form processing automatically adds them to the top of the form.
    //
    // Then, add a field for the remaining messages.
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_STATUS);
    $this->getMessenger()->deleteByType(MessengerInterface::TYPE_WARNING);

    $form['status'] = [
      '#type'   => 'status_messages',
      '#weight' => 100,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    //
    // Validate trigger.
    // -----------------
    // Ignore triggers other than 'submit' and 'refresh'. They should not
    // occur anyway.
    $triggerName = $formState->getTriggeringElement()['#name'];
    if ($triggerName !== 'refresh' && $triggerName !== 'submit') {
      return;
    }

    //
    // Validate form input.
    // --------------------
    // Get the user's input to the form, if any, and the current parent.
    $userInput     = $formState->getUserInput();
    $destinationId = $userInput['destinationid'];
    $selectionId   = $userInput['selectionid'];

    if ($selectionId === NULL) {
      $selectionId = self::EMPTY_ITEM_ID;
    }

    // If the selection ID is NULL or negative, then there is no selection.
    // Default to the currently shown folder, which is in the destination ID.
    if ($selectionId >= 0) {
      if ($triggerName === 'submit' && $selectionId >= 0) {
        $destinationId = $selectionId;
      }
      else {
        $destinationId = $this->getParentId();
        if ($destinationId < 0) {
          $destinationId = FolderShareInterface::USER_ROOT_LIST;
        }
      }
    }

    // Convert special root list names to negative numbers that flag the
    // equivalent root lists. This lets us pass around and compare integers,
    // rather than consider string values as well.
    switch ($destinationId) {
      case FolderShareInterface::USER_ROOT_LIST:
      case 'home':
        $destinationId = FolderShareInterface::USER_ROOT_LIST;
        break;

      case FolderShareInterface::SHARED_ROOT_LIST:
      case 'shared':
        $destinationId = FolderShareInterface::SHARED_ROOT_LIST;
        break;

      case FolderShareInterface::PUBLIC_ROOT_LIST:
      case 'public':
        // The public root list is not supported. Map it to the home folder.
        $destinationId = FolderShareInterface::USER_ROOT_LIST;
        break;

      case FolderShareInterface::ALL_ROOT_LIST:
      case 'all':
        $destinationId = FolderShareInterface::ALL_ROOT_LIST;
        break;

      default:
        if (is_numeric($destinationId) === FALSE) {
          // Developer-facing exception message.
          throw new NotFoundHttpException(
            "A top-level item with ID '$destinationId' could not be found.");
        }

        $destinationId = (int) $destinationId;
        if ($destinationId < 0) {
          $destinationId = FolderShareInterface::USER_ROOT_LIST;
        }
        else {
          $destination = FolderShare::load($destinationId);

          if ($destination === NULL ||
              $destination->isSystemHidden() === TRUE) {
            throw new NotFoundHttpException(
              FolderShare::getStandardHiddenMessage($destination->getName()));
          }

          if ($destination->isSystemDisabled() === TRUE) {
            throw new ConflictHttpException(
              FolderShare::getStandardDisabledMessage(
                'accessed',
                $destination->getName()));
          }
        }
        break;
    }

    // Save the destination.
    $this->configuration['destinationId'] = $destinationId;

    //
    // Dispatch based on trigger.
    // --------------------------
    // For a refresh, rebuild the form. For a submit, validate it.
    if ($triggerName === 'refresh') {
      $formState->setRebuild(TRUE);
      return;
    }

    // Submit button pressed. Validate everything.
    try {
      $this->validateConfiguration();
    }
    catch (RuntimeExceptionWithMarkup $e) {
      $formState->setErrorByName('hiddengroup][destinationid', $e->getMarkup());
    }
    catch (\Exception $e) {
      $formState->setErrorByName('hiddengroup][destinationid', $e->getMessage());
    }
  }

  /**
   * Refresh the configuration form.
   *
   * @param array $form
   *   The render array for the form.
   * @param \Drupal\Core\Form\FormStateInterface $formState
   *   The form state.
   *
   * @return array
   *   Returns a portion of the form to be refreshed.
   */
  public function refreshConfigurationFormAjax(
    array &$form,
    FormStateInterface $formState) {

    // Just return the part of the rebuilt form that has the folder
    // selection. This is the part to refresh.
    return $form['foldershare-folder-selection'];
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(
    array &$form,
    FormStateInterface $formState) {

    if ($this->isValidated() === TRUE) {
      if ($this->getDestinationId() === $this->getParentId()) {
        // Move or copy to same location. Do nothing. No error.
        return;
      }

      $this->execute();
    }
  }

}
