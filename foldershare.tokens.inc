<?php

/**
 * @file
 * Implements token hooks for the module.
 */

use Drupal\Core\Datetime\Entity\DateFormat;
use Drupal\Core\Render\BubbleableMetadata;

use Drupal\foldershare\Constants;
use Drupal\foldershare\Entity\FolderShare;

/**
 * Implements hook_token_info().
 *
 * This hook defines tokens in the 'foldershare' group that provide access
 * to useful public fields of the FolderShare entity. These include:
 * - 'id': the entity ID.
 * - 'kind': the entity kind, such as 'file' or 'folder'.
 * - 'owner': the owner's user ID.
 * - 'name': the name of the entity.
 * - 'description': the description of the entity.
 * - 'created': the creation date of the entity.
 * - 'changed': the last-modified date of the entity.
 * - 'size': the size, in bytes, of the entity.
 * - 'url': the URL to the entity's view page.
 */
function foldershare_token_info() {
  //
  // Define the 'foldershare' token type that replaces tokens
  // with values from a FolderShare entity.
  $folderShareType = [
    'name'        => t('Shared files and folders'),
    'description' => t('Tokens related to a shared file or folder.'),
    'needs-data'  => FolderShare::ENTITY_TYPE_ID,
  ];

  //
  // Entity ID as 'id'.
  $folderShareTokens['id'] = [
    'name'        => t('ID'),
    'description' => t('The unique ID of a file or folder.'),
  ];

  // Entity kind as 'kind'.
  $folderShareTokens['kind'] = [
    'name'        => t('Kind'),
    'description' => t('The kind of item, such as a file or folder.'),
  ];

  // User ID as 'owner', 'author', and 'uid'.
  $folderShareTokens['owner'] = [
    'name'        => t('Owner'),
    'description' => t('The user who owns the file or folder.'),
    'type'        => 'user',
  ];

  $folderShareTokens['author'] = [
    'name'        => t('Author'),
    'description' => t('Synonym for "owner". The user who owns the file or folder.'),
    'type'        => 'user',
  ];

  $folderShareTokens['user'] = [
    'name'        => t('User'),
    'description' => t('Synonym for "owner". The user who owns the file or folder.'),
    'type'        => 'user',
  ];

  $folderShareTokens['uid'] = [
    'name'        => t('User ID'),
    'description' => t('Synonym for "owner". The user who owns the file or folder.'),
    'type'        => 'user',
  ];

  // Entity name as 'name' and 'title'.
  $folderShareTokens['name'] = [
    'name'        => t('Name'),
    'description' => t('The name of the file or folder.'),
  ];

  $folderShareTokens['title'] = [
    'name'        => t('Title'),
    'description' => t('Synonym for "name". The name of the file or folder.'),
  ];

  // Entity description as 'description' and 'body'.
  $folderShareTokens['description'] = [
    'name'        => t('Description'),
    'description' => t('The description of the file or folder.'),
  ];

  $folderShareTokens['body'] = [
    'name'        => t('Body'),
    'description' => t('Synonym for "description". The description of the file or folder.'),
  ];

  // Entity creation date as 'created'.
  $folderShareTokens['created'] = [
    'name'        => t('Created'),
    'description' => t('The date the file or folder was created.'),
    'type'        => 'date',
  ];

  // Entity modified date as 'changed'.
  $folderShareTokens['changed'] = [
    'name'        => t('Changed'),
    'description' => t('The date the file or folder was most recently changed.'),
    'type'        => 'date',
  ];

  // Entity size as 'size'.
  $folderShareTokens['size'] = [
    'name'        => t('Size'),
    'description' => t('The storage space used by the file or folder, including all folder content.'),
  ];

  // Entity MIME type as 'mime'.
  $folderShareTokens['mime'] = [
    'name'        => t('MIME type'),
    'description' => t('The MIME type of the file or folder.'),
  ];

  // Entity URL as 'url'.
  $folderShareTokens['url'] = [
    'name'        => t('URL'),
    'description' => t('The URL of the file or folder.'),
  ];

  // Entity parent as 'parent'.
  $folderShareTokens['parent'] = [
    'name'        => t('Parent folder'),
    'description' => t('The name of the parent folder, if any.'),
  ];

  // Entity root as 'root'.
  $folderShareTokens['root'] = [
    'name'        => t('Top-level item'),
    'description' => t('The name of the top-level item, if any.'),
  ];

  return [
    'types' => [
      Constants::FOLDERSHARE_TOKENS => $folderShareType,
    ],
    'tokens' => [
      Constants::FOLDERSHARE_TOKENS => $folderShareTokens,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function foldershare_tokens(
  string $group,
  array $tokens,
  array $data,
  array $options,
  BubbleableMetadata $metadata) {
  //
  // Given token group (a.k.a. type), and the list of tokens in use,
  // find replacements for the tokens based upon values extracted
  // from the given data.
  //
  // Ignore anything except for the token groups we define.
  $item = NULL;
  switch ($group) {
    case Constants::FOLDERSHARE_TOKENS:
      // For these tokens we need a FolderShare object.
      if (isset($data[FolderShare::ENTITY_TYPE_ID]) === FALSE) {
        // No object, so no token replacement.
        return [];
      }

      $item = $data[FolderShare::ENTITY_TYPE_ID];
      break;

    default:
      // Unrecognized token group.
      return [];
  }

  //
  // Replace tokens
  // --------------
  // For each, add the replacement value.
  $replacements = [];
  $dateFormatter = \Drupal::service('date.formatter');

  if ($group === Constants::FOLDERSHARE_TOKENS) {
    // These tokens use values from the FolderShare object.
    foreach ($tokens as $name => $original) {
      switch ($name) {
        // ID.
        case 'id':
          $replacements[$original] = $item->id();
          break;

        // Kind.
        case 'kind':
          $replacements[$original] = $item->getKind();
          break;

        // MIME type.
        case 'mime':
          $replacements[$original] = $item->getMimeType();
          break;

        // Owner ID.
        case 'owner':
        case 'author':
        case 'user':
        case 'uid':
          $owner = $item->getOwner();
          $metadata->addCacheableDependency($owner);
          $replacements[$original] = $owner->getDisplayName();
          break;

        // Creation date.
        case 'created':
          // Date formatting defaults to the time zone and language
          // of the page being displayed.
          $metadata->addCacheableDependency(DateFormat::load('medium'));
          $replacements[$original] = $dateFormatter->format(
            $item->getCreatedTime(),
            'medium',
            '',
            NULL,
            NULL);
          break;

        // Changed date.
        case 'changed':
          // Date formatting defaults to the time zone and language
          // of the page being displayed.
          $metadata->addCacheableDependency(DateFormat::load('medium'));
          $replacements[$original] = $dateFormatter->format(
            $item->getChangedTime(),
            'medium',
            '',
            NULL,
            NULL);
          break;

        // Size.
        case 'size':
          // Could be empty.
          $sz = $item->getSize();
          $replacements[$original] = ($sz === FALSE) ? '' : $sz;
          break;

        // Name.
        case 'name':
        case 'title':
          $replacements[$original] = $item->getName();
          break;

        // Description.
        case 'description':
        case 'body':
          $replacements[$original] = $item->getDescription();
          break;

        // URL.
        case 'url':
          $replacements[$original] = $item->toUrl(
            'canonical',
            ['absolute' => TRUE])->toString();
          break;

        // Parent folder.
        case 'parent':
          $parent = $item->getParentFolder();
          if ($parent === NULL) {
            // When there is no parent, the item is a root item.
            // Return an empty string for the parent, since there isn't one.
            $replacements[$original] = '';
          }
          else {
            $replacements[$original] = $parent->getName();
          }
          break;

        // Root folder.
        case 'root':
          $root = $item->getRootItem();
          if ($root === NULL) {
            // It should not be possible for there to be no root. When an item
            // is at the root level, it is it's own root. But if this
            // somehow happens, return an empty string.
            $replacements[$original] = '';
          }
          else {
            $replacements[$original] = $root->getName();
          }
          break;

        default:
          // Ignore all other unrecognized tokens.
          break;
      }
    }
  }

  //
  // Replace chained tokens
  // ----------------------
  // A chained token is something like [foldershare:created:format].
  $tokenService = \Drupal::token();

  $chained = $tokenService->findWithPrefix($tokens, 'created');
  if (empty($chained) === FALSE) {
    $replacements += $tokenService->generate(
      'date',
      $chained,
      ['date' => $item->getCreatedTime()],
      $options,
      $metadata);
  }

  $chained = $tokenService->findWithPrefix($tokens, 'changed');
  if (empty($chained) === FALSE) {
    $replacements += $tokenService->generate(
      'date',
      $chained,
      ['date' => $item->getChangedTime()],
      $options,
      $metadata);
  }

  $chained = $tokenService->findWithPrefix($tokens, 'owner');
  if (empty($chained) === TRUE) {
    $chained = $tokenService->findWithPrefix($tokens, 'author');
  }
  if (empty($chained) === TRUE) {
    $chained = $tokenService->findWithPrefix($tokens, 'user');
  }
  if (empty($chained) === TRUE) {
    $chained = $tokenService->findWithPrefix($tokens, 'uid');
  }
  if (empty($chained) === FALSE) {
    $replacements += $tokenService->generate(
      'user',
      $chained,
      ['user' => $item->getOwner()],
      $options,
      $metadata);
  }

  return $replacements;
}
