<?php

namespace Drupal\foldershare\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\ProxyClass\File\MimeType\MimeTypeGuesser;
use Drupal\Component\Utility\Unicode;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;

use Drupal\foldershare\Utilities\FormatUtilities;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\HttpKernel\Exception\ConflictHttpException;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\HttpKernel\Exception\UnsupportedMediaTypeHttpException;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

use Drupal\foldershare\ManageLog;
use Drupal\foldershare\ManageHooks;
use Drupal\foldershare\Settings;
use Drupal\foldershare\Utilities\FileUtilities;
use Drupal\foldershare\Entity\FolderShare;
use Drupal\foldershare\Entity\Exception\LockException;
use Drupal\foldershare\Entity\Exception\SystemException;
use Drupal\foldershare\Entity\Exception\FileDownloadFailedException;
use Drupal\foldershare\Entity\Exception\InvalidFileStreamException;
use Symfony\Component\Mime\Header\UnstructuredHeader;

/**
 * Defines a class to handle downloading one or more FolderShare entities.
 *
 * <B>Warning:</B> This class is strictly internal to the FolderShare
 * module. The class's existance, name, and content may change from
 * release to release without any promise of backwards compatability.
 *
 * This class handles translating stored FolderShare content into a byte
 * stream to download to a browser via HTTP. It handles two cases:
 *
 * - Download a single FolderShare entity for a file.
 *
 * - Download one or more FolderShare entities for files and folders.
 *
 * In both cases, FolderShare entities for files wrap File entities, which
 * in turn reference stored files. For security reasons, those files are
 * stored with numeric file names and no extensions. To send them to a browser,
 * these files have to be processed to send their human-readable names,
 * file extensions, and MIME types so that a browser or client OS knows what
 * to do with them.
 *
 * When downloading a single FolderShare entity for a file, the file's data
 * is retrieved and sent to the browser as a file attachment.
 *
 * When downloading a single FolderShare entity for a folder, or multiple
 * FolderShare entities, the entities are compressed into a temporary ZIP
 * archive, and that archive's data sent to the browser as a file
 * attachment.
 *
 * @ingroup foldershare
 */
final class FolderShareDownload extends ControllerBase
{

    /*--------------------------------------------------------------------
     *
     * Constants.
     *
     *--------------------------------------------------------------------*/
    /**
     * The name of the ZIP archive downloaded for groups of entities.
     *
     * @var string
     */
    const DOWNLOAD_NAME = 'Download.zip';

    /*--------------------------------------------------------------------
     *
     * Fields - dependency injection.
     *
     *--------------------------------------------------------------------*/
    /**
     * The MIME type guesser, set at construction time.
     *
     * @var MimeTypeGuesser
     */
    private $mimeTypeGuesser;

    /*--------------------------------------------------------------------
     *
     * Construction.
     *
     *--------------------------------------------------------------------*/
    /**
     * Constructs a new FolderShare entity download controller.
     *
     * @param MimeTypeGuesser $mimeTypeGuesser
     *   The MIME type guesser.
     */
    public function __construct(MimeTypeGuesser $mimeTypeGuesser)
    {
        // Parent class does not have a __construct() method.
        $this->mimeTypeGuesser = $mimeTypeGuesser;
    }

    /**
     * {@inheritdoc}
     */
    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('file.mime_type.guesser')
        );
    }

    /*--------------------------------------------------------------------
     *
     * Download.
     *
     *--------------------------------------------------------------------*/
    /**
     * Downloads the file by transfering the file in binary.
     *
     * The file is sent with a custom HTTP header that includes the full
     * human-readable name of the file and its MIME type. If the $style argument
     * is "show", the file is sent so that a browser may display the file
     * directly. If the $style argument is "download" (the default), the file
     * is sent with a special HTTP header to encourage the browser to save
     * the file instead of displaying it.
     *
     * <B>Post-operation hooks</B>
     * This method calls the "hook_foldershare_post_operation_download" hook
     * for each item downloaded.
     *
     * <B>Activity log</B>
     * If the site hs enabled logging of operations, this method posts a
     * log message for each item downloaded.
     *
     * @param Request $request
     *   The request object that contains the entity ID of the
     *   file being requested. The entity ID is included in the URL
     *   for links to the file.
     * @param string $encoded
     *   A string containing a comma-separated list of entity IDs.
     *   NOTE: Because this function is the target of a route with a string
     *   argument, the name of the function argument here *must be* named
     *   after the argument name: 'encoded'.
     *
     * @return BinaryFileResponse
     *   A binary transfer response is returned to send the file to the
     *   user's browser.
     *
     * @throws AccessDeniedHttpException
     *   Thrown when the user does not have access to the entities.
     *
     * @throws NotFoundHttpException
     *   Thrown if the URL argument is empty or malformed, if any entity ID
     *   in that encoded argument is invalid, if the entities don't all have
     *   the same parent, if the file's those entities refer to cannot be
     *   found, or if a ZIP archive of those entities could not be created.
     *
     * @todo Support media entity download.
     *
     * @todo Support large ZIP files that may take longer to create than the
     * PHP or web server time limits allow.
     */
    public function download(
        Request $request,
        string $encoded = NULL)
    {

        //
        // Validate arguments
        // ------------------
        // Decode the argument an array of FolderShare entity IDs.
        if (empty($encoded) === TRUE) {
            // Developer-facing exception message.
            throw new BadRequestHttpException(
                "Malformed request. Nothing selected to download.");
        }

        $entityIds = explode(',', $encoded);

        if (empty($entityIds) === TRUE) {
            // Developer-facing exception message.
            throw new BadRequestHttpException(
                "Malformed request. Nothing selected to download.");
        }

        // Load all of those entities and make sure the user has access
        // permission.
        $entities = FolderShare::loadMultiple($entityIds);
        foreach ($entities as $entityId => $entity) {
            if ($entity === NULL) {
                // User-facing exception message.
                $message = $this->t(
                    "The entity with ID @id could not be found to download.",
                    [
                        '@id' => $entityId,
                    ]);
                throw new NotFoundHttpException($message);
            }

            if ($entity->isSystemHidden() === TRUE) {
                // Hidden items do not exist.
                // User-facing exception message.
                throw new NotFoundHttpException(
                    FolderShare::getStandardHiddenMessage($entity->getName()));
            }

            if ($entity->isSystemDisabled() === TRUE) {
                // Disabled items cannot be edited.
                // User-facing exception message.
                $operator = $this->t('downloaded');
                throw new ConflictHttpException(
                    FolderShare::getStandardDisabledMessage($operator, $entity->getName()));
            }

            if ($entity->access('view') === FALSE) {
                // User-facing exception message.
                $message = $this->t(
                    "You do not have permission to download '@name'.",
                    [
                        '@name' => $entity->getName(),
                    ]);
                throw new AccessDeniedHttpException($message);
            }
        }

        //
        // Prepare to download
        // -------------------
        // Get the file to download.
        //
        // Note that downloading Media objects is not supported.
        $entity = reset($entities);
        $mimeType = '';
        $filename = '';
        $filesize = '';
        $uri = '';

        if (count($entities) > 1 || $entity->isFolder() === TRUE) {
            // There is more than one entity to download, or there is just one
            // entity but it is a folder. ZIP them together.
            try {
                // ZIP the entities.
                //
                // @todo Support (somehow) large ZIP archives that either have a lot
                // of entries, or the entries are big. When ZIPs are large, this can
                // take a long time and get interrupted by a PHP or web server timeout.
                $uri = FolderShare::createZipArchive($entities);
            } catch (LockException $e) {
                throw new ConflictHttpException($e->getMessage());
            } catch (SystemException $e) {
                throw new HttpException(
                    Response::HTTP_INTERNAL_SERVER_ERROR,
                    $e->getMessage());
            } catch (Exception $e) {
                // User-facing exception message.
                $message = $this->t("The items could not be download.");
                throw new NotFoundHttpException($message);
            }

            // Use a generic name for the download ZIP.
            $filename = self::DOWNLOAD_NAME;
            $filesize = FileUtilities::filesize($uri);
            $mimeType = (new UnstructuredHeader('mimeType', $this->mimeTypeGuesser->guessMimeType($filename)))->getBodyAsString();
        } elseif ($entity->isFile() === TRUE || $entity->isImage() === TRUE) {
            // Download a single data or image file.
            //
            // Get the file's URI, human-readable name, MIME type, and size.
            if ($entity->isFile() === TRUE) {
                $file = $entity->getFile();
            } else {
                $file = $entity->getImage();
            }

            $uri = $file->getFileUri();
            $filename = $file->getFilename();
            $mimeType = (new UnstructuredHeader('mimeType', $file->getMimeType()))->getBodyAsString();
            $realPath = FileUtilities::realpath($uri);

            $streamWrapperManager = \Drupal::service('stream_wrapper_manager');
            $streamWrapper = $streamWrapperManager->getViaUri($uri);

            if (!$streamWrapper) {
                // There is no stream wrapper available for this uri
                throw new InvalidFileStreamException("Invalid file stream: $uri", $uri);
            }

            $isLocalStream = ($streamWrapper->getType() & StreamWrapperInterface::LOCAL) == StreamWrapperInterface::LOCAL;

            if ($isLocalStream && ($realPath === FALSE || file_exists($realPath) === FALSE)) {
                // User-facing exception message.
                $message = $this->t(
                    "The file '@name' (ID '@id') could not be found to download.",
                    [
                        '@name' => $file->getFilename(),
                        '@id' => $file->id(),
                    ]);
                throw new NotFoundHttpException($message);
            }
        } elseif ($entity->isMedia() === TRUE) {
            // Media entities are currently not supported for download.
            //
            // @todo  Support media entity download.
            // User-facing exception message.
            $message = $this->t(
                "The media item '@name' currently does not support downloading.",
                [
                    '@name' => $entity->getName(),
                ]);
            throw new UnsupportedMediaTypeHttpException($message);
        } elseif ($entity->isObject() === TRUE) {
            // Object entities do not have downloadable data.
            // User-facing exception message.
            $message = $this->t(
                "The object '@name' does not support downloading.",
                [
                    '@name' => $entity->getName(),
                ]);
            throw new UnsupportedMediaTypeHttpException($message);
        } else {
            // The entity is of an unknown type.
            // User-facing exception message.
            $message = $this->t(
                "The item '@name' does not support downloading.",
                [
                    '@name' => $entity->getName(),
                ]);
            throw new UnsupportedMediaTypeHttpException($message);
        }

        //
        // Build header
        // ------------
        // Build an HTTP header for the file by getting the user-visible
        // file name and MIME type. Both of these are essential in the HTTP
        // header since they tell the browser what type of file it is getting,
        // and the name of the file if the user wants to save it their disk.
        $headers = [
            // Use the File object's MIME type.
            'Content-Type' => $mimeType,

            // Use the human-visible file name.
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',

            // Don't cache the file because permissions and content may
            // change.
            'Pragma' => 'no-cache',
            'Cache-Control' => 'must-revalidate, post-check=0, pre-check=0',
            'Expires' => '0',
            'Accept-Ranges' => 'bytes',
        ];

        if ($isLocalStream) {
            $filesize = FileUtilities::filesize($realPath);
            // Use the saved file size, in bytes.
            $headers['Content-Length'] = $filesize;
        }

        $scheme = Settings::getFileScheme();
        $isPrivate = ($scheme === 'private');
        $requestingUid = (int)$this->currentUser()->id();

        foreach ($entities as $entity) {
            ManageHooks::callHookPostOperation(
                'download',
                [
                    $entity,
                    $requestingUid,
                ]);
            ManageLog::activity(
                "Downloaded @kind '@name' (# @id).",
                [
                    '@id' => $entity->id(),
                    '@kind' => $entity->getKind(),
                    '@name' => $entity->getName(),
                    'entity' => $entity,
                    'uid' => $requestingUid,
                ]);
        }

        //
        // Respond
        // -------
        // \Drupal\Core\EventSubscriber\FinishResponseSubscriber::onRespond()
        // sets response as not cacheable if the Cache-Control header is not
        // already modified. We pass in FALSE for non-private schemes for the
        // $public parameter to make sure we don't change the headers.

        try {
            return new BinaryFileResponse($uri, 200, $headers, !$isPrivate);
        } catch (FileNotFoundException $exception) {
            $message = $this->t(
                "The file '@name' (ID '@id') could not be found for downloading.",
                [
                    '@name' => $file->getFilename(),
                    '@id' => $file->id(),
                ]);
            throw new FileDownloadFailedException(FormatUtilities::createFormattedMessage($message));
        }
    }

}
