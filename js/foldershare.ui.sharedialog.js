/**
 * @file
 * Implements the FolderShare share dialog user interface.
 *
 * The dialog for the "Share" command presents a table of users and the
 * sharing permissions they have been granted. The dialog also contains
 * an auto-complete text field for entering the name of a new user to add
 * to the list. An "Add" button triggers the addition.
 *
 * This script supports the dialog by mapping a carriage return on the add
 * user text field into a trigger of the "Add" button.
 *
 * @ingroup foldershare
 * @see \Drupal\foldershare\Plugin\FolderShareCommand\Share
 * @see \Drupal\foldershare\Form\CommandFormWrapper
 */
(($, Drupal) => {
  Drupal.foldershare.ShareDialog = {
    /**
     * Attaches the module's UI behaviors.
     *
     * @param {Document} pageContext
     *   The page context for this call. Initially, this is the full document.
     *   Later, this is only portions of the document added via AJAX.
     *
     * @return {boolean}
     *   Always returns true.
     */
    attach(pageContext) {
      //
      // Test and exit
      // -------------
      // Quickly exit if the page context does not include the add-user form.
      const addUserForm = ".foldershare_share_add_user";
      let $addUserForm;

      if (typeof pageContext.tagName === "undefined") {
        // The full document. Search down.
        $addUserForm = $(addUserForm, pageContext);
      } else {
        // Above or below. Search up first, then down.
        $addUserForm = $(pageContext).parents(addUserForm);
        if ($addUserForm.length === 0) {
          $addUserForm = $(addUserForm, pageContext);
        }
      }

      if ($addUserForm.length === 0) {
        // Fail. The document does not contain the add-user form.
        return true;
      }

      //
      // Process top elements
      // --------------------
      // Process each add user form. There should be only one.
      $addUserForm.each((index, element) => {
        // Get the text field and button.
        const $addUserField = $(
          ".foldershare_share_add_user_name",
          $(element)
        ).eq(0);
        if ($addUserField.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI add-user field is missing."
          );
          return false;
        }

        const $addUserButton = $(
          ".foldershare_share_add_user_button",
          $(element)
        ).eq(0);
        if ($addUserButton.length === 0) {
          Drupal.foldershare.utility.printMalformedError(
            "The UI add-user button is missing."
          );
          return false;
        }

        // Attach a behavior to trigger the button on a carriage return.
        $addUserField.off("keypress.foldershare");
        $addUserField.on("keypress.foldershare", ev => {
          if (ev.keyCode === 13) {
            $addUserButton.prop("disabled", false);
            $addUserButton.click();
            ev.preventDefault();
            return false;
          }
        });

        // Attach a behavior to enable/disable the add button based on whether
        // the text field is empty.
        $addUserField.off("keyup.foldershare");
        $addUserField.on("keyup.foldershare", () => {
          const name = $addUserField.val();
          if (name === null || name.length === 0) {
            $addUserButton.prop("disabled", true);
          } else {
            $addUserButton.prop("disabled", false);
          }
        });

        $addUserField.focus();

        // Enable/disable the add button initially based on whether the text
        // field is empty.
        const name = $addUserField.val();
        if (name === null || name.length === 0) {
          $addUserButton.prop("disabled", true);
        } else {
          $addUserButton.prop("disabled", false);
        }
      });

      return true;
    }
  };

  /*--------------------------------------------------------------------
   *
   * On Drupal ready behaviors.
   *
   * Set up behaviors to execute when the page is fully loaded, or whenever
   * AJAX sends a new page fragment.
   *
   *--------------------------------------------------------------------*/

  Drupal.behaviors.foldershare_ShareDialog = {
    attach(pageContext) {
      Drupal.foldershare.ShareDialog.attach(pageContext);
    }
  };
})(jQuery, Drupal);
